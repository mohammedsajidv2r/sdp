﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Web.Services.Description;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Reflection;
using log4net;
using StopDataPro.Classes;


namespace DataPro.APPMOD.ADMIN
{
    /// <summary>
    /// CallWebServices
    /// </summary>
    public class CallWebServices
    {
        public CallWebServices()
        {
            this.log = log4net.LogManager.GetLogger(typeof(CallWebServices));
        }

        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public ILog log { get; set; }

        public object CallWebService(string webServiceAsmxUrl, string serviceName, string methodName, object[] args)
        {
            System.Net.WebClient client = new System.Net.WebClient();
            try
            {
         
                string proxyConfig = ConfigurationSettings.AppSettings.Get("NotifyProxy");
                WebProxy proxy = null;
                if (proxyConfig != "")
                {
                    proxy = new WebProxy(proxyConfig);
                }
                client.Proxy = proxy;
                log.InfoFormat("1. Proxy:" + proxy);
                // Connect To the web service
                System.IO.Stream stream = client.OpenRead(webServiceAsmxUrl + "?wsdl");
                log.InfoFormat("2. stream:" + stream.ToString());
                // Now read the WSDL file describing a service.
                ServiceDescription description = ServiceDescription.Read(stream);
                log.InfoFormat("3. stream:" + description.ToString());
                ///// LOAD THE DOM /////////
                // Initialize a service description importer.
                ServiceDescriptionImporter importer = new ServiceDescriptionImporter();
                importer.ProtocolName = "Soap12"; // Use SOAP 1.2.
                importer.AddServiceDescription(description, null, null);
                // Generate a proxy client.
                importer.Style = ServiceDescriptionImportStyle.Client;
                // Generate properties to represent primitive values.
                importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;
                // Initialize a Code-DOM tree into which we will import the service.
                CodeNamespace nmspace = new CodeNamespace();
                CodeCompileUnit unit1 = new CodeCompileUnit();
                unit1.Namespaces.Add(nmspace);
                // Import the service into the Code-DOM tree. This creates proxy code that uses the service.
                ServiceDescriptionImportWarnings warning = importer.Import(nmspace, unit1);
                log.InfoFormat("4. warning:" + warning.ToString());
                if (warning == 0) // If zero then we are good to go
                {
                    // Generate the proxy code
                    CodeDomProvider provider1 = CodeDomProvider.CreateProvider("CSharp");
                    // Compile the assembly proxy with the appropriate references
                    string[] assemblyReferences = new string[5] { "System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll" };
                    CompilerParameters parms = new CompilerParameters(assemblyReferences);
                    parms.GenerateInMemory = true;
                    CompilerResults results = provider1.CompileAssemblyFromDom(parms, unit1);
                    // Check For Errors
                    if (results.Errors.Count > 0)
                    {
                        foreach (CompilerError oops in results.Errors)
                        {
                            System.Diagnostics.Debug.WriteLine("========Compiler error============");
                            System.Diagnostics.Debug.WriteLine(oops.ErrorText);
                        }
                        throw new System.Exception("Compile Error Occured calling webservice. Check Debug ouput window.");
                    }
                    log.InfoFormat("5. Check For Errors:" + results.Errors.Count);
                    // Finally, Invoke the web service method
                    object wsvcClass = results.CompiledAssembly.CreateInstance(serviceName);
                    // create a list of data types for each argument
                    List<Type> listTypes = new List<Type>();
                    for (int i = 0; i < args.Length; i++)
                    {
                        listTypes.Add(args[i].GetType());
                    }
                    Type[] paramTypes = listTypes.ToArray<Type>();
                    MethodInfo mi = wsvcClass.GetType().GetMethod(methodName, paramTypes);
                    log.InfoFormat("6. Method info Invoke");
                    return mi.Invoke(wsvcClass, args);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                log.InfoFormat("Exception Call Web Service:" + ex.ToString());
                throw;
            }
            finally
            {
                client.Dispose();
            }
        }
    }
}
