﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// Approval Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ApprovalController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApprovalController"/> class.
        /// </summary>
        public ApprovalController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ApprovalController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the approval data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getApprovalData")]
        [APIAuthAttribute]
        public HttpResponseMessage getApprovalData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string defaultsiteId = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string observeId = Convert.ToString(request.SelectToken("observeId"));
                    string fromDate = Convert.ToString(request.SelectToken("fromDate"));
                    string toDate = Convert.ToString(request.SelectToken("toDate"));
                    string STATUSID = Convert.ToString(request.SelectToken("statusId"));
                    string cleraFilter = Convert.ToString(request.SelectToken("clearFilter"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    if (SITEID != "")
                    {
                        SITEID = SITEID;
                    }
                    else if (cleraFilter == "0")
                    {
                        SITEID = "";
                    }
                    else
                    {
                        SITEID = "";
                    }
                    strquery = ScriptEngine.GetValue("GETOFFLINEAPPFILTERBASEDSTOPCARDLISTING");
                    string[] paraminsert = { "USERID", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "OBSERVERID", "FROMDATE", "TODATE", "STATUSID" };
                    object[] pValuesinsert = { userId, SITEID, areaId, subAreaId, shiftId, observeId, fromDate, toDate, STATUSID, };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    ParameterApprovalList obj = new ParameterApprovalList();
                    string sitequery = "WASITES";
                    DataSet dssite = getValueTable(sitequery, userId, languageId, SITEID, defaultRoleId, areaId);

                    string areaquery = "WAAREAS";
                    DataSet dsarea = getValueTable(areaquery, userId, languageId, SITEID, defaultRoleId, areaId);

                    string shiftQuery = "WOSHIFTS";
                    DataSet dsShift = getValueTable(shiftQuery, userId, languageId, SITEID, defaultRoleId, areaId);

                    string obsQuery = "WOSSITEOBSERVERS";
                    DataSet dsObs = getValueTable(obsQuery, userId, languageId, SITEID, defaultRoleId, areaId);

                    string statusQuery = "WAACCESSSETUPSTATUS";
                    DataSet dsStatus = getValueTable(statusQuery, userId, languageId, SITEID, defaultRoleId, areaId);
                    List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                    try
                    {
                        if (subAreaEnable == "1")
                        {
                            string subareaquery = "WACCESSSUBAREAS";
                            DataSet dssubarea = getValueTable(subareaquery, userId, languageId, SITEID, defaultRoleId, areaId);
                            obj.filterSubArea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                        }
                        else
                        {
                            obj.filterSubArea = new string[] { };
                            ds.Tables[0].Columns.Remove("SUBAREAS");
                        }
                        list = ScriptEngine.GetTableRows(ds.Tables[0]);
                    }
                    catch(Exception ex)
                    {}
                    List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dssite.Tables[0]);
                    List<Dictionary<string, object>> listArea = ScriptEngine.GetTableRows(dsarea.Tables[0]);
                    List<Dictionary<string, object>> listShift = ScriptEngine.GetTableRows(dsShift.Tables[0]);
                    List<Dictionary<string, object>> listObs = ScriptEngine.GetTableRows(dsObs.Tables[0]);
                    List<Dictionary<string, object>> listStatus = ScriptEngine.GetTableRows(dsStatus.Tables[0]);
                    obj.defaultSiteId = int.Parse(defaultsiteId);
                    if (list.Count > 0)
                    {
                        obj.status = true;
                        obj.message = "Success";
                        obj.generalData = list;
                    }
                    else
                    {
                        obj.status = false;
                        obj.message = "FMKNORECS";
                        obj.generalData = new string[] { };
                    }
                    obj.filterSites = listSite;
                    obj.filterArea = listArea;
                    obj.filterShift = listShift;
                    obj.filterObserver = listObs;
                    obj.filterStatus = listStatus;
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Data for Approval:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getSiteFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSiteFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    //if (String.IsNullOrEmpty(siteId))
                    //{
                    //    obj1.status = false;
                    //    obj1.message = "Please fill Mandatory Parameters";
                    //    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    //}
                    //else
                    //{
                    strquery = ScriptEngine.GetValue("WAAREAS");
                    DataSet dsAllFilter = new DataSet();
                    string[] paraminsert = { "USERID", "SITEID", "ROLEID" };
                    object[] pValuesinsert = { userId, siteId, defaultRoleId };
                    dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WOSHIFTS");
                    DataSet dsShift = new DataSet();
                    dsShift = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WOSSITEOBSERVERS");
                    DataSet dsObs = new DataSet();
                    dsObs = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    ParameterSiteFilter obj = new ParameterSiteFilter();
                    obj.status = true;
                    obj.message = "Success";
                    obj.area = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                    obj.shift = ScriptEngine.GetTableRows(dsShift.Tables[0]);
                    obj.observer = ScriptEngine.GetTableRows(dsObs.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Site Filter Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Gets the sub area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getSubAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WACCESSSUBAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "AREAID" };
                        object[] pValuesinsert2 = { areaId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterValuesSubArea obj = new ParameterValuesSubArea();
                            obj.status = true;
                            obj.message = "Success";
                            obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Sub Area Filter Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Approvals the delete.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/approvalDelete")]
        [APIAuthAttribute]
        public HttpResponseMessage approvalDelete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    if (String.IsNullOrEmpty(cardId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEOFFLINEDATA");
                        string[] paraminsert = { "CARDID", "UPDATEDBY" };
                        object[] pValuesinsert = { cardId, userId };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Approval Delete:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Approvals the data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/approvalData")]
        [APIAuthAttribute]
        public HttpResponseMessage approvalData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    if (String.IsNullOrEmpty(cardId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("APPROVEOFFLINEDATA");
                        string[] paraminsert = { "CARDID", "UPDATEDBY" };
                        object[] pValuesinsert = { cardId, userId };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALERTAPPROVEDSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Approved Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the group checklist edit.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getGroupChecklistEdit")]
        [APIAuthAttribute]
        public HttpResponseMessage getGroupChecklistEdit(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsObserver = new DataSet();
                DataSet dsSub = new DataSet();
                DataSet ds = new DataSet();
                DataSet dsCustName = new DataSet();
                DataSet dsCustValue = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    if (String.IsNullOrEmpty(userId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETOFFLINESTOPCARDCHECKLISTINFO");
                        string[] paraminsert = { "CARDID" };
                        object[] pValuesinsert = { cardId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            string siteId = ds.Tables[0].Rows[0]["SITEID"].ToString();
                            string areaId = ds.Tables[0].Rows[0]["AREAID"].ToString();
                            strquery = ScriptEngine.GetValue("GETOFFLINESTOPCARDCHECKLISTAREAOBSERVERSHIFTINFO");
                            string[] paraminsert1 = { "CARDID", "MODE", "OBSAPPROVALPC", "SITEID", "AREAID", "LANGUAGEID" };
                            object[] pValuesinsert1 = { cardId, "1", "1", siteId, areaId, languageId };
                            dsObserver = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                            DataTable dtAvail = dsObserver.Tables[1].Copy();
                            dtAvail.Merge(dsObserver.Tables[0]);
                            if (dsObserver.Tables[2].Rows.Count == 0)
                            {
                                DataRow dr = dsObserver.Tables[2].NewRow();
                                dr[0] = "";
                                dsObserver.Tables[2].Rows.Add(dr);
                            }
                            strquery = ScriptEngine.GetValue("GETOFFLINESUBAREALIST");
                            dsSub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                            strquery = ScriptEngine.GetValue("GETCUSTOMFIELDSNAME");
                            dsCustName = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                            strquery = ScriptEngine.GetValue("GETCUSTOMFIELDSVALUE");
                            dsCustValue = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);


                            strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                            string[] paraminsert11 = { "LANGUAGECODE" };
                            object[] pValuesinsert11 = { languageId };
                            DataSet dschecklst = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert11, pValuesinsert11);
                            ParameterGroupCheckList obj = new ParameterGroupCheckList();
                            List<Dictionary<string, object>> listdata = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> listUser = ScriptEngine.GetTableRows(dtAvail);
                            List<Dictionary<string, object>> listUserId = ScriptEngine.GetTableRows(dsObserver.Tables[2]);
                            List<Dictionary<string, object>> listArea = ScriptEngine.GetTableRows(dsObserver.Tables[3]);
                            List<Dictionary<string, object>> listShift = ScriptEngine.GetTableRows(dsObserver.Tables[4]);
                            List<Dictionary<string, object>> listSubArea = ScriptEngine.GetTableRows(dsSub.Tables[0]);
                            List<Dictionary<string, object>> listCustName = ScriptEngine.GetTableRows(dsCustName.Tables[0]);
                            List<Dictionary<string, object>> listCustValue = ScriptEngine.GetTableRows(dsCustValue.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.approveData = listdata[0];
                            obj.observerList = listUser;
                            obj.observerrAssignId = listUserId[0]["userIds"];
                            obj.area = listArea;
                            obj.shift = listShift;
                            obj.subArea = listSubArea;
                            obj.customName = listCustName;
                            obj.customValue = listCustValue;
                            obj.checkListConfig = ScriptEngine.GetTableRows(dschecklst.Tables[0])[0];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Approval Details:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the approval area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getApprovalArea")]
        [APIAuthAttribute]
        public HttpResponseMessage getApprovalArea(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dssubarea = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "MODE", "AREAID", };
                        object[] pValuesinsert = { "1", areaId };
                        strquery = ScriptEngine.GetValue("GETOFFLINESUBAREALIST");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        ParameterValuesSubArea obj = new ParameterValuesSubArea();
                        if (dssubarea.Tables[0].Rows.Count > 0)
                        {
                            List<Dictionary<string, object>> listsubarea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.subArea = listsubarea;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Area:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the approval.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/updateApproval")]
        [APIAuthAttribute]
        public HttpResponseMessage updateApproval(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string customFieldvalueId = Convert.ToString(request.SelectToken("customFieldvalueId"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string observerId = Convert.ToString(request.SelectToken("observerId"));
                    string obsDate = Convert.ToString(request.SelectToken("obsDate"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string length = Convert.ToString(request.SelectToken("length"));
                    string peopleCont = Convert.ToString(request.SelectToken("peopleContact"));
                    string peopleObserver = Convert.ToString(request.SelectToken("peopleObserver"));
                    string safeComment = Convert.ToString(request.SelectToken("safeComment"));
                    string unsafeComment = Convert.ToString(request.SelectToken("unsafeComment"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string defuserid = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(length) || String.IsNullOrEmpty(peopleCont) || String.IsNullOrEmpty(peopleObserver) || String.IsNullOrEmpty(obsDate))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEOFFLINESTOPCARDCHECKLIST");
                        string[] paraminsert = { "CUSTOMFIELDVALUEID", "CARDID", "CHECKLISTSETUPID", "SITEID", "SELOBSERVERID", "OBSERVATIONDATE", "SHIFTID", "AREAID", "SUBAREAID",
                            "LENGTH", "PEOPLECONTACTED", "PEOPLEOBSERVED", "SAFECOMMENTS", "UNSAFECOMMENTS", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { customFieldvalueId, cardId, checkListId, siteId, observerId,obsDate, shiftId, areaId, subAreaId,
                            length,peopleCont,    peopleObserver,safeComment,unsafeComment,status, defuserid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHECKLISTGENERALUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update Approval:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getObservation")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(checkListId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETOFFLINESTOPCARDOBSERVATION");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "CARDID", "CHECKLISTSETUPID", "LANGUAGEID" };
                        object[] pValuesinsert2 = { cardId, checkListId, languageId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            strquery = ScriptEngine.GetValue("GETOFFLINECATEGORYCOMMENTS");
                            ParameterValuesObser obj = new ParameterValuesObser();
                            obj.status = true;
                            obj.message = "Success";
                            obj.mainCat = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            obj.subCat = ScriptEngine.GetTableRows(dsAllFilter.Tables[1]);
                            obj.detail = ScriptEngine.GetTableRows(dsAllFilter.Tables[2]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Observation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observation comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getObservationComment")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservationComment(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAllFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatId = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatId = Convert.ToString(request.SelectToken("subCatId"));
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(mainCatId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETOFFLINECATEGORYCOMMENTS");
                        string[] paraminsert2 = { "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "LANGUAGEID" };
                        object[] pValuesinsert2 = { cardId, mainCatId, subCatId, languageId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterValuesObserComment obj = new ParameterValuesObserComment();
                            obj.status = true;
                            obj.message = "Success";
                            obj.details = ScriptEngine.GetTableRows(dsAllFilter.Tables[0])[0];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Observation Comment Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the update request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/getUpdateRequest")]
        [APIAuthAttribute]
        public HttpResponseMessage getUpdateRequest(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgData obj1 = new StatusMsgData();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    if (String.IsNullOrEmpty(cardId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETUPDATEREQUESTINFO");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "CARDID", "USERID" };
                        object[] pValuesinsert2 = { cardId, userId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        try
                        {
                            if (dsAllFilter.Tables[0].Rows.Count > 0)
                            {
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.data = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        catch (Exception ex)
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Update Request Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the update req.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/insertUpdateReq")]
        [APIAuthAttribute]
        public HttpResponseMessage insertUpdateReq(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string comments = Convert.ToString(request.SelectToken("comments"));
                    string requestType = Convert.ToString(request.SelectToken("requestType"));  //approvalComment
                    if (requestType == "observerComment")
                    {
                        requestType = "2";
                    }
                    else
                    {
                        requestType = "1";
                    }
                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSMETHOD1 = dctpartdata["WSMETHOD1"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();

                    if (String.IsNullOrEmpty(cardId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTUPDATEREQUESTED");
                        string[] paraminsert = { "CARDID", "COMMENTS", "REQUESTTYPE", "CREATEDBY" };
                        object[] pValuesinsert = { cardId, comments, requestType, userId };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCOMMREATED";
                            Log.InfoFormat("==Approval Mail Process Start==");
                            try
                            {
                                string[] paramsiteupdate = { "CARDID", "USERID" };
                                object[] pValuesiteupdate = { cardId, userId };
                                strquery = ScriptEngine.GetValue("MAILUPDATEREQUESTED");
                                DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramsiteupdate, pValuesiteupdate);
                                if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                {
                                    string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl, str;
                                    int ischeduleid;
                                    DateTime senddate;
                                    for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                    {
                                        ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["TOKENID"].ToString());
                                        strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                        strreplyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                        strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                        strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                        strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                        strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                        strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                        senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                        strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                        str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + COMPANYID + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                        Log.InfoFormat("JSON String:" + str);
                                        //mail.SendNotificationDetails(str);
                                        object[] objSendNotificationDetails = new object[1];
                                        objSendNotificationDetails[0] = str;
                                        CallWebServices CWS = new CallWebServices();
                                        object objCWS = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                        Log.InfoFormat("WebService Return Value:" + objCWS.ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for Update Request" + ex.StackTrace);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update Request:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the update comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Approval/insertUpdateComment")]
        [APIAuthAttribute]
        public HttpResponseMessage insertUpdateComment(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string safeComments = Convert.ToString(request.SelectToken("safeComments"));
                    string unsafeComments = Convert.ToString(request.SelectToken("unsafeComments"));
                    string observations = Convert.ToString(request.SelectToken("observations"));
                    string isFromComments = Convert.ToString(request.SelectToken("isFromComments"));
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));

                    DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(observations);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ROOT>");
                    for (int i = 0; i < dtTokenVals.Rows.Count; i++)
                    {
                        sb.Append("<ROW MAINCATEGORYID='").Append(dtTokenVals.Rows[i]["MAINCATEGORYID"]).Append("' SUBCATEGORYID='").Append(dtTokenVals.Rows[i]["SUBCATEGORYID"]).Append("' SAFE='").Append(dtTokenVals.Rows[i]["SAFE"]).Append("' UNSAFE='").Append(dtTokenVals.Rows[i]["UNSAFE"]).Append("'/>");
                    }
                    sb.Append("</ROOT>");

                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(observations))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (isFromComments == "1")
                        {
                            strquery = ScriptEngine.GetValue("INSERTOFFLINEUPDATECATEGORYCOMMENTS");
                            string[] paraminsert = { "OBSAPPROVALPC", "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "SAFECOMMENTS", "UNSAFECOMMENTS", "UPDATEDBY" };
                            object[] pValuesinsert = { obsApprovalPc, cardId, mainCatid, subCatid, safeComments, unsafeComments, userId };
                            dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        DataSet dsNew = new DataSet();
                        strquery = ScriptEngine.GetValue("INSERTOFFLINESTOPCARDOBSERVATIONS");
                        int getcardid = BaseAdapter.ExecuteInlineQueries_XMLPARAM(strquery, cardId, userId, sb.ToString(), obsApprovalPc);
                        if (getcardid > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHECKLISTOBSERVATIONSUPDATED";
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update Comment :" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="siteId">The site identifier.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string siteId, string roleId, string areaId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID", "AREAID" };
            object[] pValuesinsert = { userid, languageId, siteId, roleId, areaId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion
    }
}

