﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// ChecklistSetupController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ChecklistSetupController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChecklistSetupController"/> class.
        /// </summary>
        public ChecklistSetupController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ChecklistSetupController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the checklist setup.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/getChecklistSetup")]
        [APIAuthAttribute]
        public HttpResponseMessage getChecklistSetup(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string ROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(siteId))
                    {
                        siteId = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    }


                    string status = Convert.ToString(request.SelectToken("status"));
                    string categoryTpye = Convert.ToString(request.SelectToken("categoryTpye"));
                    strquery = ScriptEngine.GetValue("WASITES");
                    string[] paraminsert = { "USERID", "ROLEID" };
                    object[] pValuesinsert = { USERID, ROLEID };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WAACCESSSETUPSTATUS");
                    ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);

                    strquery = ScriptEngine.GetValue("GETCHECKLISTSETUPLIST");
                    string[] paraminsert1 = { "USERID", "SITEID", "STATUS", "CATEGORYTYPE" };
                    object[] pValuesinsert1 = { USERID, siteId, status, categoryTpye };
                    ds2 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                    //if (ds.Tables[0].Rows.Count != 0 && ds1.Tables[0].Rows.Count != 0 && ds2.Tables[0].Rows.Count != 0)
                    //{
                   
                      ParameterCheckList obj = new ParameterCheckList();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds1.Tables[0]);
                        List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds2.Tables[0]);
                        if(ds2.Tables[0].Rows.Count != 0)
                        {
                            obj.message = "Success";
                        }
                        else
                        {
                            obj.message = "FMKNORECS";
                        }
                        obj.status = true;
                        obj.sites = lstPersons;
                        obj.checkStatus = lstPersons1;
                        obj.checkList = lstPersons2;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    //}
                    //else
                    //{
                    //    obj1.status = false;
                    //    obj1.message = "FMKNORECS";
                    //    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get CheckListSetup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/updateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage updateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    if (String.IsNullOrEmpty(checkListSetupId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESETUPSTATUS");
                        string[] paraminsert = { "UPDATEDBY", "CHECKLISTSETUPID" };
                        object[] pValuesinsert = { USERID, checkListSetupId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update CheckListSetup Status:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the check list setup.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/deleteCheckListSetup")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteCheckListSetup(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterValuesDelete obj1 = new ParameterValuesDelete();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string delFlag = Convert.ToString(request.SelectToken("delFlag"));
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    if (String.IsNullOrEmpty(delFlag))
                    {
                        delFlag = "1";
                    }
                    if (String.IsNullOrEmpty(checkListSetupId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESETUP");
                        string[] paraminsert = { "UPDATEDBY", "DELFLAG", "CHECKLISTSETUPID" };
                        object[] pValuesinsert = { USERID, delFlag, checkListSetupId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult == -1 || iresult == 1)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSETUPWARNING";
                            obj1.delStatus = 1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELCONFIRM?";
                            obj1.delStatus = 0;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == 2)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            obj1.delStatus = 2;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            obj1.delStatus = -1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete CheckListSetup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the check list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/insertCheckList")]
        [APIAuthAttribute]
        public HttpResponseMessage insertCheckList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string checkListName = Convert.ToString(request.SelectToken("checkListName"));
                    string allSites = Convert.ToString(request.SelectToken("allSites"));
                    string copyFrom = Convert.ToString(request.SelectToken("copyFrom"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string allSafeCheck = Convert.ToString(request.SelectToken("allSafeCheck"));
                    string url = Convert.ToString(request.SelectToken("url"));
                    string enableSafeUnsafe = Convert.ToString(request.SelectToken("enableSafeUnsafe"));
                    if (String.IsNullOrEmpty(checkListName) || String.IsNullOrEmpty(allSites) || String.IsNullOrEmpty(copyFrom) || String.IsNullOrEmpty(status) || String.IsNullOrEmpty(allSafeCheck) || String.IsNullOrEmpty(enableSafeUnsafe))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTCHECKLISTSETUP");
                        string[] paraminsert = { "COMPANYID", "CREATEDBY", "SETUPNAME", "ALLSITES", "STATUS", "ALLSAFECHECK", "COPYFROM", "URL", "ENABLESAFEUNSAFE" };
                        object[] pValuesinsert = { COMPANYID, USERID, checkListName, allSites, status, allSafeCheck, copyFrom, url, enableSafeUnsafe };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult > 0)
                        {
                            StatusMsgId obj = new StatusMsgId();
                            obj.status = true;
                            obj.message = "ALTSETUPCREATED";
                            obj.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPNOTCREATE";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert CheckListSetup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the check list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/updateCheckList")]
        [APIAuthAttribute]
        public HttpResponseMessage updateCheckList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string CHECKLISTSETUPID = Convert.ToString(request.SelectToken("checklistId"));
                    string checkListName = Convert.ToString(request.SelectToken("checkListName"));
                    string allSites = Convert.ToString(request.SelectToken("allSites"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string allSafeCheck = Convert.ToString(request.SelectToken("allSafeCheck"));
                    string url = Convert.ToString(request.SelectToken("url"));
                    string enableSafeUnsafe = Convert.ToString(request.SelectToken("enableSafeUnsafe"));
                    if (String.IsNullOrEmpty(checkListName) || String.IsNullOrEmpty(CHECKLISTSETUPID) || String.IsNullOrEmpty(allSites) || String.IsNullOrEmpty(status) || String.IsNullOrEmpty(allSafeCheck) || String.IsNullOrEmpty(enableSafeUnsafe))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATECHECKLISTSETUP");
                        string[] paraminsert = { "CHECKLISTSETUPID", "UPDATEDBY", "SETUPNAME", "ALLSITES", "STATUS", "ALLSAFECHECK", "ENABLESAFEUNSAFE" };
                        object[] pValuesinsert = { CHECKLISTSETUPID, USERID, checkListName, allSites, status, allSafeCheck, enableSafeUnsafe };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSETUPEDIT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPEDITPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update CheckListSetup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the main cat assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/updateMainCatAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateMainCatAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    if (String.IsNullOrEmpty(checkListSetupId) || String.IsNullOrEmpty(mainCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESETUPMAINCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID", "CHECKLISTSETUPID" };
                        object[] pValuesinsert = { USERID, mainCategoryId, checkListSetupId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgIU obj = new StatusMsgIU();
                            obj.status = true;
                            obj.message = "ALTSETUPMAINCTG";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAINCATEGORYNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPMAINCTGFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Main Category Assign Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the sub cat assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/updateSubCatAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateSubCatAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    string subCategoryId = Convert.ToString(request.SelectToken("subCategoryId"));
                    if (String.IsNullOrEmpty(checkListSetupId) || String.IsNullOrEmpty(mainCategoryId) || String.IsNullOrEmpty(subCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "ALTASGNSUBCTG";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESETUPSUBCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID", "CHECKLISTSETUPID", "SUBCATEGORYID" };
                        object[] pValuesinsert = { USERID, mainCategoryId, checkListSetupId, subCategoryId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgIU obj = new StatusMsgIU();
                            obj.status = true;
                            obj.message = "ALTSETUPSUBCTG";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPSUBCTGFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Sub Category Assign Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Updates the site assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/updateSiteAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateSiteAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(checkListSetupId) || String.IsNullOrEmpty(siteId))
                    {
                        obj1.status = false;
                        obj1.message = "ALTASGNSITE";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESETUPSITES");
                        string[] paraminsert = { "UPDATEDBY", "SITEID", "CHECKLISTSETUPID" };
                        object[] pValuesinsert = { USERID, siteId, checkListSetupId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgIU obj = new StatusMsgIU();
                            obj.status = true;
                            obj.message = "ALTSETUPSITES";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSETUPSITESFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Site Assign Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the checklist data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/getChecklistData")]
        [APIAuthAttribute]
        public HttpResponseMessage getChecklistData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();
                DataSet ds4 = new DataSet();
                DataSet ds5 = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    ParameterCheckListData obj = new ParameterCheckListData();
                    if (!String.IsNullOrEmpty(checkListId))
                    {
                        strquery = ScriptEngine.GetValue("GETSETUPINFO");
                        string[] paraminsert1 = { "CHECKLISTSETUPID" };
                        object[] pValuesinsert1 = { checkListId };
                        ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        if (ds1.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstchecklist = ScriptEngine.GetTableRows(ds1.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.checkListData = lstchecklist[0];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        strquery = ScriptEngine.GetValue("GETSETUPINFOMAINCATEGORY");
                        string[] paraminsert2 = { "CHECKLISTSETUPID", "LANGUAGEID" };
                        object[] pValuesinsert2 = { checkListId, LANGUAGEID };
                        ds2 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);

                        //if (ds2.Tables[2].Rows.Count == 0)
                        //{
                        //    DataRow dr = ds2.Tables[2].NewRow();
                        //    dr[0] = "";
                        //    ds2.Tables[2].Rows.Add(dr);
                        //}
                        //DataTable dAvailMain = ds2.Tables[1].Copy();
                        //dAvailMain.Merge(ds2.Tables[0]);
                        //List<Dictionary<string, object>> lstPersonss = ScriptEngine.GetTableRows(dAvailMain);
                        //List<Dictionary<string, object>> lstPersonss1 = ScriptEngine.GetTableRows(ds2.Tables[2]);
                        //obj.category = lstPersonss;
                        //obj.categoryId = lstPersonss1[0]["MAINCATEGORYID"];

                        obj.categoryAvailable = ScriptEngine.GetTableRows(ds2.Tables[0]);
                        obj.categoryAssigned = ScriptEngine.GetTableRows(ds2.Tables[1]);
                        string mainCatId = "0";
                        if (ds2.Tables[2].Rows.Count != 0)
                        {
                             mainCatId = ds2.Tables[2].Rows[0]["MAINCATEGORYID"].ToString();
                        }
                        obj.selectedCategoryId = int.Parse(mainCatId);

                        strquery = ScriptEngine.GetValue("GETSETUPASSIGNEDMAINCATEGORY");
                        string[] paraminsert3 = { "CHECKLISTSETUPID", "LANGUAGEID", "MAINCATEGORYID" };
                        object[] pValuesinsert3 = { checkListId, LANGUAGEID, mainCatId };
                        DataSet dsAssignMainCat = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert3, pValuesinsert3);
                        obj.mainCategoryList = ScriptEngine.GetTableRows(dsAssignMainCat.Tables[0]);
                        strquery = ScriptEngine.GetValue("GETSETUPSUBCATEGORYNEW");
                        ds3 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert3, pValuesinsert3);

                        //DataTable dAvailSub = ds3.Tables[1].Copy();
                        //dAvailSub.Merge(ds3.Tables[0]);
                        //List<Dictionary<string, object>> lstPersonsSub = ScriptEngine.GetTableRows(dAvailSub);
                        //List<Dictionary<string, object>> lstPersonsSub1 = ScriptEngine.GetTableRows(ds3.Tables[2]);
                        //obj.subCategory = lstPersonsSub;
                        //obj.subCategoryId = lstPersonsSub1;
                        obj.subCategoryAvailable = ScriptEngine.GetTableRows(ds3.Tables[0]);
                        obj.subCategoryAssigned = ScriptEngine.GetTableRows(ds3.Tables[1]);
                        strquery = ScriptEngine.GetValue("GETSETUPSITELIST");
                        string[] paraminsert4 = { "CHECKLISTSETUPID", "USERID" };
                        object[] pValuesinsert4 = { checkListId, USERID };
                        ds4 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert4, pValuesinsert4);

                        if (ds4.Tables[1].Rows.Count == 0)
                        {
                            DataRow dr = ds4.Tables[2].NewRow();
                            dr[0] = "";
                            ds4.Tables[2].Rows.Add(dr);
                        }
                        DataTable dAvailSite = ds4.Tables[1].Copy();
                        dAvailSite.Merge(ds4.Tables[0]);
                        List<Dictionary<string, object>> lstPersonsSite = ScriptEngine.GetTableRows(dAvailSite);
                        List<Dictionary<string, object>> lstPersonsSite1 = ScriptEngine.GetTableRows(ds4.Tables[2]);
                        obj.sites = lstPersonsSite;
                        obj.sitesId = lstPersonsSite1[0]["SITEID"];

                        strquery = ScriptEngine.GetValue("GETSTOPCARDOBSERVATIONPREVIEW");
                        string[] paraminsert5 = { "LANGUAGEID", "CHECKLISTSETUPID" };
                        object[] pValuesinsert5 = { LANGUAGEID, checkListId };
                        ds5 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert5, pValuesinsert5);
                        if (ds5.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds5.Tables[0]);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds5.Tables[1]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.mainCategoryPreview = lstPersons;
                            obj.subCategoryPreview = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj.mainCategoryPreview = new string[] { };
                            obj.subCategoryPreview = new string[] { };
                        }

                        if (ds1.Tables[0].Rows.Count != 0)
                        {
                            obj.status = true;
                            obj.message = "Success";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETCHECKLISTSETUP");
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.copyCheckList = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Checklist setup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Gets the checklist setup.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistSetup/getSubCatData")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubCatData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string mainCatId = Convert.ToString(request.SelectToken("mainCatId"));
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListId"));
                    strquery = ScriptEngine.GetValue("GETSETUPSUBCATEGORYNEW");
                    string[] paraminsert = { "CHECKLISTSETUPID", "LANGUAGEID", "MAINCATEGORYID" };
                    object[] pValuesinsert = { checkListSetupId, LANGUAGEID, mainCatId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0 || ds.Tables[1].Rows.Count != 0)
                    {
                        ParameterSubCatg obj = new ParameterSubCatg();
                        obj.status = true;
                        obj.message = "Success";
                        obj.subCategoryAvailable = ScriptEngine.GetTableRows(ds.Tables[0]); 
                        obj.subCategoryAssigned = ScriptEngine.GetTableRows(ds.Tables[1]); 
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get CheckListSetup Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }



        #endregion

    }
}

