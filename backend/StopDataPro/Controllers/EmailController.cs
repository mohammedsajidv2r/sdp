﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// Email Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class EmailController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailController"/> class.
        /// </summary>
        public EmailController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(EmailController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the email list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEmailList")]
        public HttpResponseMessage getEmailList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmailList = new DataSet();
                DataSet dsEmailType = new DataSet();
                DataSet dsStatus = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailList pv = new ParameterMailList();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string dformat = dtTokenVal.Rows[0]["DATEFORMAT"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string status = Convert.ToString(request.SelectToken("status"));

                    strquery = ScriptEngine.GetValue("GETMAILLIST");
                    string[] paramrptinfo = { "SCHEDULETYPEID", "STATUS" };
                    object[] pValuesrptinfo = { scheduleId, status };
                    dsEmailList = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                    strquery = ScriptEngine.GetValue("GETACTIVESHEDULETYPE");
                    dsEmailType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                    strquery = ScriptEngine.GetValue("WAACCESSSETUPSTATUS");
                    dsStatus = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    pv.status = true;
                    pv.message = "Success";
                    pv.data = ScriptEngine.GetTableRows(dsEmailList.Tables[0]);
                    pv.filterEmailType = ScriptEngine.GetTableRows(dsEmailType.Tables[0]);
                    pv.filterStatus = ScriptEngine.GetTableRows(dsStatus.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, pv);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Email Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the email.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/deleteEmail")]
        public HttpResponseMessage deleteEmail(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    strquery = ScriptEngine.GetValue("DELETEMAILOPTION");
                    string[] paramrptinfo = { "SCHEDULEID", "UPDATEDBY" };
                    object[] pValuesrptinfo = { scheduleId, userId };
                    int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                    if (iresult > 0)
                    {
                        obj1.status = true;
                        obj1.message = "ALTDELSUCCESS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else if (iresult == 0)
                    {
                        obj1.status = false;
                        obj1.message = "ALTDELDEFMAILTYPE";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete Email:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/changeStatus")]
        public HttpResponseMessage changeStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    strquery = ScriptEngine.GetValue("UPDATEMAILOPTIONSTATUS");
                    string[] paramrptinfo = { "SCHEDULEID", "UPDATEDBY" };
                    object[] pValuesrptinfo = { scheduleId, userId };
                    int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                    if (iresult > 0 && iresult != 1)
                    {
                        obj1.status = true;
                        obj1.message = "ALTCHANGESTATUSSUCCESS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else if (iresult == 1)
                    {
                        obj1.status = false;
                        obj1.message = "ALTCHGSTATUSDEFMAILTYPE";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When change Status Email:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the email add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEmailAdd")]
        public HttpResponseMessage getEmailAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmailList = new DataSet();
                DataSet dsEmailType = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailAdd pv = new ParameterMailAdd();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    strquery = ScriptEngine.GetValue("GETSCHEDULETYPEANDMAILDETAILSFROM");
                    dsEmailList = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    string SCHEDULETYPEID = dsEmailList.Tables[0].Rows[0]["SCHEDULETYPEID"].ToString();
                    string[] paramrptinfo1 = { "SCHEDULETYPEID", "LANGUAGEID", "SCHEDULEID" };
                    object[] pValuesrptinfo1 = { SCHEDULETYPEID, languageId, "" };
                    strquery = ScriptEngine.GetValue("GETMAILSCHEDULEFILTER");
                    dsEmailType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);
                    int count = dsEmailType.Tables[0].Rows.Count;
                    DataTable dtTokenValNew = new DataTable();
                    dtTokenValNew.Columns.Add("Name", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                    dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                    dtTokenValNew.Columns.Add("Data", typeof(String));
                    for (int i = 0; i < count; i++)
                    {
                        DataSet dsfilter = new DataSet();
                        string query = dsEmailType.Tables[0].Rows[i]["QUERY"].ToString();
                        dsfilter = getValueTable(query, userId, languageId, roleId);
                        DataRow dr = dtTokenValNew.NewRow();
                        dr[0] = dsEmailType.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                        dr[1] = dsEmailType.Tables[0].Rows[i]["OPTIONID"].ToString();
                        dr[2] = dsEmailType.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                        dr[3] = dsEmailType.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                        dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                        dtTokenValNew.Rows.Add(dr);
                    }
                    //DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = 2").CopyToDataTable();
                    DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = 2").Count() > 0 ? dtTokenValNew.Select("OPTIONSCATEGORYID = 2").CopyToDataTable() : dtTokenValNew.Clone();
                    DataTable dtTokenValSiteFilter = dtTokenValNew.Select("OPTIONSCATEGORYID = 1").CopyToDataTable();
                    DataTable dtTokenValRoleFilter = new DataTable();
                    try
                    {
                        dtTokenValRoleFilter = dtTokenValNew.Select("OPTIONSCATEGORYID = 5").CopyToDataTable();
                    }
                    catch (Exception ex)
                    { dtTokenValRoleFilter = null; }
                    if (dsEmailList.Tables[0].Rows.Count > 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.emailType = ScriptEngine.GetTableRows(dsEmailList.Tables[0]);
                        pv.schedule = ScriptEngine.GetTableRows(dsEmailList.Tables[1]);
                        try
                        {
                            pv.emailDetail = ScriptEngine.GetTableRows(dsEmailList.Tables[2])[0];
                        }
                        catch (Exception ex)
                        {
                            pv.emailDetail = "";
                        }
                        pv.filterSite = ScriptEngine.GetTableRows(dtTokenValSiteFilter)[0];
                        pv.filterGroup = ScriptEngine.GetTableRows(dtTokenValoptional);
                        pv.filterLabel = ScriptEngine.GetTableRows(dsEmailType.Tables[1]);
                        try
                        {
                            pv.filterRole = ScriptEngine.GetTableRows(dtTokenValRoleFilter)[0];
                        }
                        catch (Exception ex)
                        {
                            pv.filterRole = "";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Email Data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the emai type data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEmaiTypeData")]
        public HttpResponseMessage getEmaiTypeData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmaildetail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailType pv = new ParameterMailType();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleTypeId"));
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string[] paramrptinfo = { "SCHEDULETYPEID", "LANGUAGEID" };
                    object[] pValuesrptinfo = { scheduleId, languageId };
                    strquery = ScriptEngine.GetValue("GETEMAILOPTIONNAME");
                    dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmaildetail.Tables[0].Rows.Count > 0 || dsEmaildetail.Tables[1].Rows.Count > 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.emailType = ScriptEngine.GetTableRows(dsEmaildetail.Tables[0]);
                        try
                        {
                            pv.emailDetail = ScriptEngine.GetTableRows(dsEmaildetail.Tables[1])[0];
                        }
                        catch (Exception ex)
                        {
                            pv.emailDetail = "";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Emailtype Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the emai detail from.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEmaiDetailFrom")]
        public HttpResponseMessage getEmaiDetailFrom(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmaildetail = new DataSet();
                DataSet dsEmailType = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string[] paramrptinfo = { "SCHEDULEID" };
                    object[] pValuesrptinfo = { scheduleId };
                    strquery = ScriptEngine.GetValue("GETMAILDETAILS");
                    dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmaildetail.Tables[0].Rows.Count > 0)
                    {
                        obj1.status = true;
                        obj1.message = "Success";
                        try
                        {
                            obj1.Data = ScriptEngine.GetTableRows(dsEmaildetail.Tables[0])[0];
                        }
                        catch (Exception ex)
                        { obj1.Data = ""; }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get EmailFrom Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the edit body.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEditBody")]
        public HttpResponseMessage getEditBody(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmaildetail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailBody pv = new ParameterMailBody();
                    string scheduleTypeId = Convert.ToString(request.SelectToken("scheduleTypeId"));
                    string[] paramrptinfo = { "SCHEDULETYPEID" };
                    object[] pValuesrptinfo = { scheduleTypeId };
                    strquery = ScriptEngine.GetValue("GETSCHEDULEMAILKEYWORD");
                    dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmaildetail.Tables[0].Rows.Count > 0)
                    {
                        string[] KeyWordstemp = new string[] { };
                        StringBuilder KeyWords = new StringBuilder();
                        StringBuilder KeyWordsSpanId = new StringBuilder();
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("label", typeof(String));
                        dtTokenValNew.Columns.Add("keyWordId", typeof(String));
                        string keyword = dsEmaildetail.Tables[0].Rows[0]["KEYWORD"].ToString();
                        var keyCommaSplit = keyword.Split(',');
                        for (int i = 0; i < keyCommaSplit.Length; i++)
                        {
                            DataRow dr = dtTokenValNew.NewRow();
                            KeyWordstemp = keyCommaSplit[i].Split('~');
                            dr[0] = KeyWordstemp[0];
                            dr[1] = KeyWordstemp[4];
                            dtTokenValNew.Rows.Add(dr);
                        }
                        dsEmaildetail.Tables[0].Rows[0]["KEYWORD"] = "";
                        dsEmaildetail.Tables[0].Rows[0]["KEYWORD"] = JsonConvert.SerializeObject(dtTokenValNew);
                        pv.status = true;
                        pv.message = "Success";
                        try
                        {
                            pv.emailBody = ScriptEngine.GetTableRows(dsEmaildetail.Tables[0])[0];
                        }
                        catch (Exception ex)
                        {
                            pv.emailBody = "";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Emailbody Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the content of the multilingual.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getMultilingualContent")]
        public HttpResponseMessage getMultilingualContent(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ClsLogWrite cs = new ClsLogWrite();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmaildetail = new DataSet();
                DataSet dsEmailType = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailEdit pv = new ParameterMailEdit();
                    string scheduleTypeId = Convert.ToString(request.SelectToken("scheduleTypeId"));
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string languageId = Convert.ToString(request.SelectToken("languageId"));
                    strquery = ScriptEngine.GetValue("GETACTIVELANGUAGES");
                    dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (String.IsNullOrEmpty(languageId))
                    { languageId = dsEmaildetail.Tables[0].Rows[0]["LANGUAGEID"].ToString(); }
                    string[] paramrptinfo = { "SCHEDULETYPEID", "LANGUAGEID", "SCHEDULEID" };
                    object[] pValuesrptinfo = { scheduleTypeId, languageId, scheduleId };
                    strquery = ScriptEngine.GetValue("GETCONTENTANDKEYWORD");
                    dsEmailType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmaildetail.Tables[0].Rows.Count > 0)
                    {
                        string[] KeyWordstemp = new string[] { };
                        StringBuilder KeyWords = new StringBuilder();
                        StringBuilder KeyWordsSpanId = new StringBuilder();
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("label", typeof(String));
                        dtTokenValNew.Columns.Add("keyWordId", typeof(String));
                        string keyword = dsEmailType.Tables[1].Rows[0]["KEYWORD"].ToString();
                        var keyCommaSplit = keyword.Split(',');
                        for (int i = 0; i < keyCommaSplit.Length; i++)
                        {
                            DataRow dr = dtTokenValNew.NewRow();
                            KeyWordstemp = keyCommaSplit[i].Split('~');
                            dr[0] = KeyWordstemp[0];
                            dr[1] = KeyWordstemp[4];
                            dtTokenValNew.Rows.Add(dr);
                        }
                        dsEmailType.Tables[1].Rows[0]["KEYWORD"] = "";
                        dsEmailType.Tables[1].Rows[0]["KEYWORD"] = JsonConvert.SerializeObject(dtTokenValNew);
                        pv.status = true;
                        pv.message = "Success";
                        pv.language = ScriptEngine.GetTableRows(dsEmaildetail.Tables[0]);
                        try
                        {
                            pv.scheduleDetail = ScriptEngine.GetTableRows(dsEmailType.Tables[0])[0];
                        }
                        catch (Exception ex)
                        { pv.scheduleDetail = ""; }
                        try
                        {
                            pv.emailBody = ScriptEngine.GetTableRows(dsEmailType.Tables[1])[0];
                        }
                        catch (Exception ex)
                        { pv.emailBody = ""; }
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Multilingual Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the insert multi lingual.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/updateInsertMultiLingual")]
        public HttpResponseMessage updateInsertMultiLingual(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string languageId = Convert.ToString(request.SelectToken("languageId"));
                    string body = Convert.ToString(request.SelectToken("body"));
                    string subject = Convert.ToString(request.SelectToken("subject"));
                    strquery = ScriptEngine.GetValue("INSERTMAILMULTILINGUALCONTENT");
                    string[] paramrptinfo = { "LANGUAGEID", "SCHEDULEID", "SUBJECT", "BODY" };
                    object[] pValuesrptinfo = { languageId, scheduleId, subject, body };
                    DataSet result = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (result.Tables[0].Rows[0][0].ToString() == "2")
                    {
                        obj1.status = true;
                        obj1.message = "ALTMAILDETAILEDIT";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else if (result.Tables[0].Rows[0][0].ToString() == "1")
                    {
                        obj1.status = false;
                        obj1.message = "ALTMAILDETAILCREATED";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "ALTMAILDETAILFAIL";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Multilingual Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the email for edit.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/getEmailForEdit")]
        public HttpResponseMessage getEmailForEdit(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmaildetail = new DataSet();
                DataSet dsEmailType = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterMailEditList pv = new ParameterMailEditList();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string[] paramrptinfo = { "SCHEDULEID" };
                    object[] pValuesrptinfo = { scheduleId };
                    strquery = ScriptEngine.GetValue("GETMAILINFO");
                    dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmaildetail.Tables[0].Rows.Count > 0)
                    {
                        string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                        string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                        string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                        string SCHEDULETYPEID = dsEmaildetail.Tables[0].Rows[0]["SCHEDULETYPEID"].ToString();
                        string[] paramrptinfo1 = { "SCHEDULETYPEID", "LANGUAGEID", "SCHEDULEID" };
                        object[] pValuesrptinfo1 = { SCHEDULETYPEID, languageId, scheduleId };
                        strquery = ScriptEngine.GetValue("GETMAILSCHEDULEFILTER");
                        dsEmailType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);
                        int count = dsEmailType.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));

                        for (int i = 0; i < count; i++)
                        {
                            DataSet dsfilter = new DataSet();
                            string query = dsEmailType.Tables[0].Rows[i]["QUERY"].ToString();
                            dsfilter = getValueTable(query, userId, languageId, roleId);
                            DataRow dr = dtTokenValNew.NewRow();
                            dr[0] = dsEmailType.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsEmailType.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsEmailType.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsEmailType.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            dtTokenValNew.Rows.Add(dr);
                        }

                        DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = 2").Count() > 0  ? dtTokenValNew.Select("OPTIONSCATEGORYID = 2").CopyToDataTable() : dtTokenValNew.Clone();
                        DataTable dtTokenValSiteFilter = dtTokenValNew.Select("OPTIONSCATEGORYID = 1").CopyToDataTable();
                        DataTable dtTokenValRoleFilter = new DataTable();
                        try
                        {
                            dtTokenValRoleFilter = dtTokenValNew.Select("OPTIONSCATEGORYID = 5").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { dtTokenValRoleFilter = null; }
                        pv.status = true;
                        pv.message = "Success";
                        try
                        {
                            pv.editData = ScriptEngine.GetTableRows(dsEmaildetail.Tables[0])[0];
                        }
                        catch (Exception ex)
                        { pv.editData = ""; }
                        try
                        {
                            pv.filterRole = ScriptEngine.GetTableRows(dtTokenValRoleFilter)[0];
                        }
                        catch (Exception ex)
                        { pv.filterRole = ""; }
                        pv.filterSite = ScriptEngine.GetTableRows(dtTokenValSiteFilter)[0];
                        pv.filterGroup = ScriptEngine.GetTableRows(dtTokenValoptional);
                        pv.filterLabel = ScriptEngine.GetTableRows(dsEmailType.Tables[1]);
                        string[] KeyWordstemp = new string[] { };
                        StringBuilder KeyWords = new StringBuilder();
                        StringBuilder KeyWordsSpanId = new StringBuilder();
                        DataTable dtTokenValNew1 = new DataTable();
                        dtTokenValNew1.Columns.Add("ID", typeof(String));
                        dtTokenValNew1.Columns.Add("VALUE", typeof(String));

                        string filSiteId = "";
                        string filterGroup1 = "";
                        string filterGroup2 = "";
                        string filterGroup3 = "";
                        string filterGroup4 = "";
                        string filterGroup5 = "";
                        string filterGroup6 = "";
                        string filterGroup7 = "";
                        string filterGroup8 = "";
                        string filterGroup9 = "";
                        string filterGroup10 = "";
                        string filterRoleId = "";
                        DataTable dtSbFilter = new DataTable();
                        string[] filterSplit = new string[] { };
                        string[] filterstringName = new string[] { };
                        StringBuilder sbFilter = new StringBuilder();
                        try
                        {
                            int row = 0;
                            for (int i = 0; i < dsEmailType.Tables[2].Rows.Count; i++)
                            {
                                int emailCount = 1;
                                filterSplit = dsEmailType.Tables[2].Rows[i]["RESULT"].ToString().Split(':');
                                filterstringName = Regex.Split(filterSplit[1], @"\*\~\*");
                                dtSbFilter.Columns.Add(filterSplit[0], typeof(String));
                                if (dtSbFilter.Rows.Count == 0)
                                {
                                    DataRow dr = dtSbFilter.NewRow();
                                    dtSbFilter.Rows.Add(dr);
                                }
                                foreach (string aa in filterstringName)
                                {
                                    if (emailCount == filterstringName.Length)
                                    {
                                        sbFilter.Append(aa.Split('~')[0].ToString());
                                    }
                                    else
                                    {
                                        sbFilter.Append(aa.Split('~')[0].ToString() + ",");
                                    }
                                    emailCount++;
                                }
                                if ("SITEID" == filterSplit[0])
                                {
                                    filSiteId = sbFilter.ToString();
                                }
                                if ("GROUP1" == filterSplit[0])
                                {
                                    filterGroup1 = sbFilter.ToString();
                                }
                                if ("GROUP2" == filterSplit[0])
                                {
                                    filterGroup2 = sbFilter.ToString();
                                }
                                if ("GROUP3" == filterSplit[0])
                                {
                                    filterGroup3 = sbFilter.ToString();
                                }
                                if ("GROUP4" == filterSplit[0])
                                {
                                    filterGroup4 = sbFilter.ToString();
                                }
                                if ("GROUP5" == filterSplit[0])
                                {
                                    filterGroup5 = sbFilter.ToString();
                                }
                                if ("GROUP6" == filterSplit[0])
                                {
                                    filterGroup6 = sbFilter.ToString();
                                }
                                if ("GROUP7" == filterSplit[0])
                                {
                                    filterGroup7 = sbFilter.ToString();
                                }
                                if ("GROUP8" == filterSplit[0])
                                {
                                    filterGroup8 = sbFilter.ToString();
                                }
                                if ("GROUP9" == filterSplit[0])
                                {
                                    filterGroup9 = sbFilter.ToString();
                                }
                                if ("GROUP10" == filterSplit[0])
                                {
                                    filterGroup10 = sbFilter.ToString();
                                }
                                if ("ROLEID" == filterSplit[0])
                                {
                                    filterRoleId = sbFilter.ToString();
                                }
                                dtSbFilter.Rows[0][filterSplit[0]] = sbFilter.ToString();
                                row++;
                                sbFilter.Clear();
                            }
                            pv.filterSetting = ScriptEngine.GetTableRows(dtSbFilter)[0];
                        }
                        catch (Exception ex)
                        {
                            pv.filterSetting = ScriptEngine.GetTableRows(dtSbFilter);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Email Data for Edit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the email configuration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/insertEmailConfig")]
        public HttpResponseMessage insertEmailConfig(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                 request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string SCHEDULENAME = Convert.ToString(request.SelectToken("scheduleName"));
                    string SCHEDULETYPEID = Convert.ToString(request.SelectToken("scheduleTypeId"));
                    string DUPLICATEMAILFROM = Convert.ToString(request.SelectToken("duplicateEmailFrom"));
                    string REPLYTO = Convert.ToString(request.SelectToken("replyTo"));
                    string CC = Convert.ToString(request.SelectToken("cc"));
                    string SUBJECT = Convert.ToString(request.SelectToken("subject"));
                    string BODY = Convert.ToString(request.SelectToken("body"));
                    string STATUS = Convert.ToString(request.SelectToken("status"));
                    string DWM = Convert.ToString(request.SelectToken("dwm"));
                    string EVERYDWM = Convert.ToString(request.SelectToken("everyDwm"));
                    string DAY = Convert.ToString(request.SelectToken("day"));
                    string MAXEMAILS = Convert.ToString(request.SelectToken("maxEmails"));
                    string DAYLIST = Convert.ToString(request.SelectToken("dayList"));
                    string STARTTIME = Convert.ToString(request.SelectToken("startTime"));
                    string siteIds = Convert.ToString(request.SelectToken("siteId"));
                    string group1Id = Convert.ToString(request.SelectToken("group1Id"));
                    string group2Id = Convert.ToString(request.SelectToken("group2Id"));
                    string group3Id = Convert.ToString(request.SelectToken("group3Id"));
                    string group4Id = Convert.ToString(request.SelectToken("group4Id"));
                    string group5Id = Convert.ToString(request.SelectToken("group5Id"));
                    string group6Id = Convert.ToString(request.SelectToken("group6Id"));
                    string group7Id = Convert.ToString(request.SelectToken("group7Id"));
                    string group8Id = Convert.ToString(request.SelectToken("group8Id"));
                    string group9Id = Convert.ToString(request.SelectToken("group9Id"));
                    string group10Id = Convert.ToString(request.SelectToken("group10Id"));
                    string roleId = Convert.ToString(request.SelectToken("roleId"));
                    string MONTH = "";
                    string PRIORNOTDAYS = "0";
                    if (DWM == "1")
                    {
                        MONTH = "0";
                        DAY = "0";
                    }
                    else if (DWM == "2")
                    {
                        MONTH = "0";
                        EVERYDWM = "0";
                        double day1 = Math.Pow(2, int.Parse(DAY));
                        DAY = day1.ToString();
                    }
                    else if (DWM == "3")
                    {
                        MONTH = "4095";
                        double day1 = Math.Pow(2, int.Parse(DAY));
                        DAY = day1.ToString();
                    }
                    else
                    { }

                    if (!String.IsNullOrEmpty(siteIds))
                    {
                        siteIds = "SITEID=" + siteIds + ";";
                    }
                    else
                    {
                        siteIds = ";";
                    }

                    if (!String.IsNullOrEmpty(group1Id))
                    {
                        group1Id = "GROUP1=" + group1Id + ";";
                    }
                    else
                    {
                        group1Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group2Id))
                    {
                        group2Id = "GROUP2=" + group2Id + ";";
                    }
                    else
                    {
                        group2Id = ";";
                    }

                    if (!String.IsNullOrEmpty(group3Id))
                    {
                        group3Id = "GROUP3=" + group3Id + ";";
                    }
                    else
                    {
                        group3Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group4Id))
                    {
                        group4Id = "GROUP4=" + group4Id + ";";
                    }
                    else
                    {
                        group4Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group5Id))
                    {
                        group5Id = "GROUP5=" + group5Id + ";";
                    }
                    else
                    {
                        group5Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group6Id))
                    {
                        group6Id = "GROUP6=" + group6Id + ";";
                    }
                    else
                    {
                        group6Id = ";";
                    }

                    if (!String.IsNullOrEmpty(group7Id))
                    {
                        group7Id = "GROUP7=" + group7Id + ";";
                    }
                    else
                    {
                        group7Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group8Id))
                    {
                        group8Id = "GROUP8=" + group8Id + ";";
                    }
                    else
                    {
                        group8Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group9Id))
                    {
                        group9Id = "GROUP9=" + group9Id + ";";
                    }
                    else
                    {
                        group9Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group10Id))
                    {
                        group10Id = "GROUP10=" + group10Id + ";";
                    }
                    else
                    {
                        group10Id = ";";
                    }

                    if (!String.IsNullOrEmpty(roleId))
                    {
                        roleId = "ROLEID=" + roleId + ";";
                    }
                    else
                    {
                        roleId = ";";
                    }
                    var OPTIONVALUE = siteIds + group1Id + group2Id + group3Id + group4Id + group5Id + group6Id + group7Id + group8Id + group9Id + group10Id + roleId;
                    if (String.IsNullOrEmpty(SCHEDULENAME) || String.IsNullOrEmpty(REPLYTO))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTEMAILINFO");
                        string[] paramrptinfo = { "COMPANYID","CREATEDBY", "STARTTIME", "SCHEDULENAME", "SCHEDULETYPEID", "DUPLICATEMAILFROM", "REPLYTO" ,
                    "CC","SUBJECT","BODY","DWM","EVERYDWM","DAY","MONTH","MAXEMAILS","DAYLIST","PRIORNOTDAYS","OPTIONVALUE"};
                        object[] pValuesrptinfo = {COMPANYID, CREATEDBY, STARTTIME, SCHEDULENAME, SCHEDULETYPEID, DUPLICATEMAILFROM, REPLYTO, CC, SUBJECT
                            ,BODY,DWM,EVERYDWM,DAY,MONTH,MAXEMAILS,DAYLIST,PRIORNOTDAYS,OPTIONVALUE
                    };
                        DataSet dsResult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                        int result = 0;
                        result = int.Parse(dsResult.Tables[0].Rows[0]["Column1"].ToString());
                        string dtime = dsResult.Tables[0].Rows[0]["Column2"].ToString();
                  
                        ParameterMailInsert pv = new ParameterMailInsert();
                        if (result > 0)
                        {
                            DateTime dateTime = Convert.ToDateTime(dtime);
                            pv.status = true;
                            pv.message = "ALTMAILOPTIONCREATED";
                            pv.scheduleId = result;
                            DataSet dsReportInfo = new DataSet();
                            DateTime startdate, enddate;
                            string callbackMethod = "EmailSchedule";
                            String strJSON = "";
                            try
                            {
                                int strMode = 1;
                                Dictionary<string, object> dctpartdata1 = ScriptEngine.scriptengine_data();
                                string strAPPID = dctpartdata1["APPID"].ToString();
                                Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_dataSchedular();
                                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                string RSCH_WSURL = dctpartdata["WSURL"].ToString();

                                Dictionary<string, object> dctpartdataD = ScriptEngine.scriptengine_dataSchedularReportService();
                                string DRS_WSNAME = dctpartdataD["WSNAME"].ToString();
                                string DRS_WSMETHOD = dctpartdataD["WSMETHOD"].ToString();
                                string DRS_WSURL = dctpartdataD["WSURL"].ToString();
                                DRS_WSURL = DRS_WSURL.Substring(0, DRS_WSURL.LastIndexOf('/')) + "/emailServiceCall" + "?schid=" + result + "&compid=" + COMPANYID;
                                startdate = DateTime.Now;
                                enddate = DateTime.Now.AddYears(10);
                                strJSON = "{\"MODE\":" + strMode
                              + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                              + ",\"COMPANYID\":\"" + COMPANYID
                              + "\",\"TOKENID\":" + result
                              + ",\"STARTDATE\":\"" + startdate.ToString("yyyy-MM-dd")
                              + "\",\"ENDDATE\":\"" + enddate.ToString("yyyy-MM-dd")
                              + "\",\"DWM\":" + DWM
                              + ",\"EVERYDWM\":" + EVERYDWM
                              + ",\"DAY\":" + DAY
                              + ",\"MONTH\":" + MONTH
                              + ",\"ACTIVE\":" + STATUS
                              + ",\"TOKENTOBESENTDATE\":\"" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                              + "\",\"STARTTIME\":" + STARTTIME
                              + ",\"TIMEZONE\":" + dsResult.Tables[0].Rows[0]["Column3"].ToString()
                              + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                              + "\",\"CALLBACKWS\":\"" + DRS_WSURL
                              + "\",\"CALLBACKWSMETHOD\":\"" + callbackMethod + "\"}]}";
                                object[] objSendScheduleDetails = new object[1];
                                objSendScheduleDetails[0] = strJSON;
                                CallWebServices CWS = new CallWebServices();
                                object obj = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                            }
                            catch (Exception ex)
                            {
                                strquery = ScriptEngine.GetValue("INSERTSCHEDULEAUDITLOG");
                                string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                object[] pValuesrptinfomail = { result, "Problem with create email schedule", strJSON.ToString(), "", "", "", "", "", ex.ToString() };
                                int ELMAIL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, pv);
                        }
                        else if (result == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAILOPTIONEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Email Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the email configuration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Email/updateEmailConfig")]
        public HttpResponseMessage updateEmailConfig(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string SCHEDULENAME = Convert.ToString(request.SelectToken("scheduleName"));
                    string DUPLICATEMAILFROM = Convert.ToString(request.SelectToken("duplicateEmailFrom"));
                    string SCHEDULETYPEID = Convert.ToString(request.SelectToken("scheduleTypeId"));
                    string REPLYTO = Convert.ToString(request.SelectToken("replyTo"));
                    string CC = Convert.ToString(request.SelectToken("cc"));
                    string SUBJECT = Convert.ToString(request.SelectToken("subject"));
                    string BODY = Convert.ToString(request.SelectToken("body"));
                    string STATUS = Convert.ToString(request.SelectToken("status"));
                    string DWM = Convert.ToString(request.SelectToken("dwm"));
                    string EVERYDWM = Convert.ToString(request.SelectToken("everyDwm"));
                    string DAY = Convert.ToString(request.SelectToken("day"));
                    string MAXEMAILS = Convert.ToString(request.SelectToken("maxEmails"));
                    string DAYLIST = Convert.ToString(request.SelectToken("dayList"));
                    string STARTTIME = Convert.ToString(request.SelectToken("startTime"));
                    string siteIds = Convert.ToString(request.SelectToken("siteId"));
                    string group1Id = Convert.ToString(request.SelectToken("group1Id"));
                    string group2Id = Convert.ToString(request.SelectToken("group2Id"));
                    string group3Id = Convert.ToString(request.SelectToken("group3Id"));
                    string group4Id = Convert.ToString(request.SelectToken("group4Id"));
                    string group5Id = Convert.ToString(request.SelectToken("group5Id"));
                    string group6Id = Convert.ToString(request.SelectToken("group6Id"));
                    string group7Id = Convert.ToString(request.SelectToken("group7Id"));
                    string group8Id = Convert.ToString(request.SelectToken("group8Id"));
                    string group9Id = Convert.ToString(request.SelectToken("group9Id"));
                    string group10Id = Convert.ToString(request.SelectToken("group10Id"));
                    string roleId = Convert.ToString(request.SelectToken("roleId"));
                    string MONTH = "";
                    string PRIORNOTDAYS = "0";
                    if (DWM == "1")
                    {
                        MONTH = "0";
                        DAY = "0";
                    }
                    else if (DWM == "2")
                    {
                        MONTH = "0";
                    }
                    else if (DWM == "3")
                    {
                        MONTH = "4095";
                    }
                    else
                    { }

                    if (!String.IsNullOrEmpty(siteIds))
                    {
                        siteIds = "SITEID=" + siteIds + ";";
                    }
                    else
                    {
                        siteIds = ";";
                    }

                    if (!String.IsNullOrEmpty(group1Id))
                    {
                        group1Id = "GROUP1=" + group1Id + ";";
                    }
                    else
                    {
                        group1Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group2Id))
                    {
                        group2Id = "GROUP2=" + group2Id + ";";
                    }
                    else
                    {
                        group2Id = ";";
                    }

                    if (!String.IsNullOrEmpty(group3Id))
                    {
                        group3Id = "GROUP3=" + group3Id + ";";
                    }
                    else
                    {
                        group3Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group4Id))
                    {
                        group4Id = "GROUP4=" + group4Id + ";";
                    }
                    else
                    {
                        group4Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group5Id))
                    {
                        group5Id = "GROUP5=" + group5Id + ";";
                    }
                    else
                    {
                        group5Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group6Id))
                    {
                        group6Id = "GROUP6=" + group6Id + ";";
                    }
                    else
                    {
                        group6Id = ";";
                    }

                    if (!String.IsNullOrEmpty(group7Id))
                    {
                        group7Id = "GROUP7=" + group7Id + ";";
                    }
                    else
                    {
                        group7Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group8Id))
                    {
                        group8Id = "GROUP8=" + group8Id + ";";
                    }
                    else
                    {
                        group8Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group9Id))
                    {
                        group9Id = "GROUP9=" + group9Id + ";";
                    }
                    else
                    {
                        group9Id = ";";
                    }
                    if (!String.IsNullOrEmpty(group10Id))
                    {
                        group10Id = "GROUP10=" + group10Id + ";";
                    }
                    else
                    {
                        group10Id = ";";
                    }

                    if (!String.IsNullOrEmpty(roleId))
                    {
                        roleId = "ROLEID=" + roleId + ";";
                    }
                    else
                    {
                        roleId = ";";
                    }
                    var OPTIONVALUE = siteIds + group1Id + group2Id + group3Id + group4Id + group5Id + group6Id + group7Id + group8Id + group9Id + group10Id + roleId;
                    if (String.IsNullOrEmpty(scheduleId) || String.IsNullOrEmpty(SCHEDULENAME) || String.IsNullOrEmpty(REPLYTO))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if(SCHEDULETYPEID == "3" || SCHEDULETYPEID == "4")
                        {
                            string[] paramrptinfo1 = { "SCHEDULEID" };
                            object[] pValuesrptinfo1 = { scheduleId };
                            strquery = ScriptEngine.GetValue("GETMAILINFO");
                            DataSet dsEmaildetail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);
                            STATUS= dsEmaildetail.Tables[0].Rows[0]["STATUS"].ToString();
                        }

                        strquery = ScriptEngine.GetValue("UPDATEMAILINFO");
                        string[] paramrptinfo = { "SCHEDULEID", "UPDATEDBY", "STARTTIME", "SCHEDULENAME", "DUPLICATEMAILFROM", "REPLYTO", "CC", "SUBJECT", "BODY", "DWM", "EVERYDWM", "DAY", "MONTH", "MAXEMAILS", "DAYLIST", "PRIORNOTDAYS", "STATUS", "OPTIONVALUE" };
                        object[] pValuesrptinfo = {scheduleId, CREATEDBY, STARTTIME, SCHEDULENAME,  DUPLICATEMAILFROM, REPLYTO, CC, SUBJECT
                            ,BODY,DWM,EVERYDWM,DAY,MONTH,MAXEMAILS,DAYLIST,PRIORNOTDAYS,STATUS,OPTIONVALUE };
                        DataSet dsResult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                        int result = 0;
                        result = int.Parse(dsResult.Tables[0].Rows[0]["Column1"].ToString());
                        if (result > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTMAILOPTIONEDIT";
                            string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                            DataSet dsReportInfo = new DataSet();
                            DateTime startdate, enddate;
                            string callbackMethod = "EmailSchedule";
                            String strJSON = "";
                            try
                            {
                                int strMode = 2;
                                Dictionary<string, object> dctpartdata1 = ScriptEngine.scriptengine_data();
                                string strAPPID = dctpartdata1["APPID"].ToString();
                                Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_dataSchedular();
                                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                string RSCH_WSURL = dctpartdata["WSURL"].ToString();

                                Dictionary<string, object> dctpartdataD = ScriptEngine.scriptengine_dataSchedularReportService();
                                string DRS_WSNAME = dctpartdataD["WSNAME"].ToString();
                                string DRS_WSMETHOD = dctpartdataD["WSMETHOD"].ToString();
                                string DRS_WSURL = dctpartdataD["WSURL"].ToString();
                                DRS_WSURL = DRS_WSURL.Substring(0, DRS_WSURL.LastIndexOf('/')) + "/emailServiceCall" + "?schid=" + result + "&compid=" + COMPANYID;
                                startdate = DateTime.Now;
                                enddate = DateTime.Now.AddYears(10);
                                string dtime = dsResult.Tables[0].Rows[0]["TOKENSENTDATE"].ToString();
                                if (dtime != "")
                                {
                                    DateTime dateTime = Convert.ToDateTime(dtime);
                                    strJSON = "{\"MODE\":" + strMode
                              + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                              + ",\"COMPANYID\":\"" + COMPANYID
                              + "\",\"TOKENID\":" + result
                              + ",\"STARTDATE\":\"" + startdate.ToString("yyyy-MM-dd")
                              + "\",\"ENDDATE\":\"" + enddate.ToString("yyyy-MM-dd")
                              + "\",\"DWM\":" + DWM
                              + ",\"EVERYDWM\":" + EVERYDWM
                              + ",\"DAY\":" + DAY
                              + ",\"MONTH\":" + MONTH
                              + ",\"ACTIVE\":" + STATUS
                              + ",\"TOKENTOBESENTDATE\":\"" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                              + "\",\"STARTTIME\":" + STARTTIME
                              + ",\"TIMEZONE\":" + dsResult.Tables[0].Rows[0]["Column2"].ToString()
                              + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                              + "\",\"CALLBACKWS\":\"" + DRS_WSURL
                              + "\",\"CALLBACKWSMETHOD\":\"" + callbackMethod + "\"}]}";
                                }
                                else
                                {
                                    strJSON = "{\"MODE\":" + strMode
                               + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                               + ",\"COMPANYID\":\"" + COMPANYID
                               + "\",\"TOKENID\":" + result
                               + ",\"STARTDATE\":\"" + startdate.ToString("yyyy-MM-dd")
                               + "\",\"ENDDATE\":\"" + enddate.ToString("yyyy-MM-dd")
                               + "\",\"DWM\":" + DWM
                               + ",\"EVERYDWM\":" + EVERYDWM
                               + ",\"DAY\":" + DAY
                               + ",\"MONTH\":" + MONTH
                               + ",\"ACTIVE\":" + STATUS
                               + ",\"STARTTIME\":" + STARTTIME
                               + ",\"TIMEZONE\":" + dsResult.Tables[0].Rows[0]["Column2"].ToString()
                               + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                               + "\",\"CALLBACKWS\":\"" + DRS_WSURL
                               + "\",\"CALLBACKWSMETHOD\":\"" + callbackMethod + "\"}]}";
                                }
                                object[] objSendScheduleDetails = new object[1];
                                objSendScheduleDetails[0] = strJSON;
                                CallWebServices CWS = new CallWebServices();
                                object obj = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                            }
                            catch (Exception ex)
                            {
                                strquery = ScriptEngine.GetValue("INSERTSCHEDULEAUDITLOG");
                                string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                object[] pValuesrptinfomail = { result, "Problem with create email schedule", strJSON.ToString(), "", "", "", "", "", ex.ToString() };
                                int ELMAIL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (result == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAILOPTIONEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Email Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string roleId)
        {
            DataSet dataSet = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "ROLEID" };
            object[] pValuesinsert = { userid, languageId, roleId };
            dataSet = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return dataSet;
        }

        #endregion

    }
}
