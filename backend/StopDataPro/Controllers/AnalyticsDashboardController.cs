﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// AnalyticsDashboardController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class AnalyticsDashboardController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AnalyticsDashboardController"/> class.
        /// </summary>
        public AnalyticsDashboardController()
        {
            this.Log = log4net.LogManager.GetLogger(typeof(AnalyticsDashboardController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>Gets the tableau ticket.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/AnalyticsDashboard/getTableauTicket")]
        public HttpResponseMessage getTableauTicket(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            string username = Convert.ToString(request.SelectToken("username"));
            string target_site = Convert.ToString(request.SelectToken("target_site"));
            try
            {
                Task<string> Api_Resp = APIParamreq(username, target_site);
                Log.InfoFormat("Analytics Dashboard Response:" + JsonConvert.SerializeObject(Api_Resp));
                response = Request.CreateResponse(HttpStatusCode.OK, Api_Resp.Result);
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Call Analytics Dashboard API:" + ex.StackTrace);
            }
            return response;
        }

        /// <summary>APIs the paramreq.</summary>
        /// <param name="username">The username.</param>
        /// <returns></returns>
        public async Task<string> APIParamreq(string username, string target_site)
        {
            var responseString = "";
            JObject JsonObject = new JObject();
            string anlyticsIp = WebConfigurationManager.AppSettings["analyticsDashBoardIp"];
            try
            {
                //ServicePointManager.ServerCertificateValidationCallback += RemoteCertValidate;
                //,{ "verify", "False" }
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(anlyticsIp);
                    var param = new Dictionary<string, string> { { "username", username },{ "target_site", target_site } };
                    var encodedContent = new FormUrlEncodedContent(param);
                    HttpResponseMessage response = await client.PostAsync("", encodedContent).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    responseString = await response.Content.ReadAsStringAsync();
                    var responseCode = response.StatusCode;
                    HttpResponseMessage responseMsg = new HttpResponseMessage(responseCode)
                    {
                        Content = new StringContent(responseString, Encoding.UTF8, "application/json")
                    };
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Call APIParamreq Function:" + ex.StackTrace);
            }
            //finally
            //{
            //    ServicePointManager.ServerCertificateValidationCallback -= RemoteCertValidate;
            //}
            return responseString.ToString();
        }

        //private static bool RemoteCertValidate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslpolicyerrors)
        //{
        //    return true;
        //}

        public class analyticsDashboardModel
        {
            public string username { get; set; }
        }

        #endregion

    }
}
