﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// GroupOnChecklist Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class GroupOnChecklistController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EmailController"/> class.
        /// </summary>
        public GroupOnChecklistController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.log = log4net.LogManager.GetLogger(typeof(GroupOnChecklistController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public ILog log { get; set; }
        #endregion

        #region Action Method

        /// <summary>
        /// Gets the group checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/GroupOnChecklist/getGroupChecklist")]
        [APIAuthAttribute]
        public HttpResponseMessage getGroupChecklist(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string USERTYPEID = Convert.ToString(request.SelectToken("userType"));
                    string STATUSID = Convert.ToString(request.SelectToken("statusId"));
                    strquery = ScriptEngine.GetValue("GETOBSERVATIONUSERSLIST");
                    string[] paraminsert = { "USERID", "SITEID", "STATUSID", "USERTYPE", "ROLEID", "DEFAULTROLEID", };
                    object[] pValuesinsert = { userId, SITEID, STATUSID, USERTYPEID, ROLEID, defaultRoleId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameteGroupOnCheckList obj = new ParameteGroupOnCheckList();
                        string sitequery = "WASITES";
                        DataSet dssite = getValueTable(sitequery, userId, languageId, SITEID, defaultRoleId);

                        string userlevelQuery = "OBSERVATIONGROUPUSERLEVEL";
                        DataSet dsUSERLEVEL = getValueTable(userlevelQuery, userId, languageId, SITEID, defaultRoleId);

                        string roleQuery = "GETROLES";
                        DataSet dsRole = getValueTable(roleQuery, userId, languageId, SITEID, defaultRoleId);

                        string statusQuery = "WACCESSSTATUS";
                        DataSet dsStatus = getValueTable(statusQuery, userId, languageId, SITEID, defaultRoleId);

                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dssite.Tables[0]);
                        List<Dictionary<string, object>> listUser = ScriptEngine.GetTableRows(dsUSERLEVEL.Tables[0]);
                        List<Dictionary<string, object>> listRole = ScriptEngine.GetTableRows(dsRole.Tables[0]);
                        List<Dictionary<string, object>> listStatus = ScriptEngine.GetTableRows(dsStatus.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.GeneralData = list;
                        obj.filterSites = listSite;
                        obj.filterUserLevel = listUser;
                        obj.filterRole = listRole;
                        obj.filterStatus = listStatus;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                log.InfoFormat("Exception Occured When Get Group on checklist Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the group checklist add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/GroupOnChecklist/getGroupChecklistAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getGroupChecklistAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsObserver = new DataSet();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = Convert.ToString(request.SelectToken("userId"));
                    if (String.IsNullOrEmpty(userId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETGROUPTYPEWITHMANDATOTY");
                        string[] paraminsert = { "LANGUAGEID", "USERID" };
                        object[] pValuesinsert = { languageId, userId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        strquery = ScriptEngine.GetValue("GETUSERGROUPSINOBSERVATION");
                        dsObserver = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (dsObserver.Tables[0].Rows.Count != 0)
                        {
                            ParameteGroupOnCheckListAdd obj = new ParameteGroupOnCheckListAdd();
                            List<Dictionary<string, object>> listGroup = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> listGroupData = ScriptEngine.GetTableRows(dsObserver.Tables[0]);
                            List<Dictionary<string, object>> listObserverName = ScriptEngine.GetTableRows(dsObserver.Tables[1]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.group = listGroup;
                            obj.groupData = listGroupData;
                            if (listObserverName.Count > 0)
                            {
                                obj.observerName = listObserverName[0];
                            }
                            else
                            { obj.observerName = ""; }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.InfoFormat("Exception Occured When Get Group on checklist Data on Add Click Button:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Saves the group checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/GroupOnChecklist/SaveGroupChecklist")]
        [APIAuthAttribute]
        public HttpResponseMessage SaveGroupChecklist(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = Convert.ToString(request.SelectToken("userId"));
                    string groupId = Convert.ToString(request.SelectToken("groupIds"));
                    string obsDateFrom = Convert.ToString(request.SelectToken("obsDateFrom"));
                    string obsDateTo = Convert.ToString(request.SelectToken("obsDateTo"));
                    string defuserid = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(userId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEOBSCHECKLISTGROUPS");
                        string[] paraminsert = { "OBSDATEFROM", "OBSDATETO", "SELECTUSERID", "GROUPID", "UPDATEDBY" };
                        object[] pValuesinsert = { obsDateFrom, obsDateTo, userId, groupId, defuserid };
                        int iresult1 = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult1 == 1)
                        {
                            obj1.status = true;
                            obj1.message = "ALTOBSCHKLISTGRPINFOUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult1 == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTNOOBSERVATION";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.InfoFormat("Exception Occured When Save Group on checklist Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion
        
    }
}

