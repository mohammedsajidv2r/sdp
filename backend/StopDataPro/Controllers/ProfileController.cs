﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using WebAPI.Filters;
using WebAPI.Helpers;
using WebAPI.Library;


namespace StopDataPro.Controllers
{
    /// <summary>
    /// ProfileController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ProfileController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProfileController"/> class.
        /// </summary>
        public ProfileController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ProfileController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method
        
        /// <summary>
        /// Gets the profile information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/getProfileInfo")]
        public HttpResponseMessage getProfileInfo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsProfile = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterValuesProfile pv = new ParameterValuesProfile();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETMYPROFILEINFO");
                    string[] paramrptinfo = { "LANGUAGEID", "USERID" };
                    object[] pValuesrptinfo = { languageId, userId };
                    dsProfile = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    pv.status = true;
                    pv.message = "Success";
                    pv.userId = int.Parse(userId);
                    pv.language = ScriptEngine.GetTableRows(dsProfile.Tables[0]);
                    pv.site = ScriptEngine.GetTableRows(dsProfile.Tables[1]);
                    pv.timeZone = ScriptEngine.GetTableRows(dsProfile.Tables[2]);
                    pv.userInfo = ScriptEngine.GetTableRows(dsProfile.Tables[3])[0];
                    pv.globalDetail = ScriptEngine.GetTableRows(dsProfile.Tables[4])[0];
                    pv.allTimeZone = ScriptEngine.GetTableRows(dsProfile.Tables[5]);
                    response = Request.CreateResponse(HttpStatusCode.OK, pv);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Profile Info Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the profile information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/updateProfileInfo")]
        public HttpResponseMessage updateProfileInfo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string createdBy = dtTokenVal.Rows[0]["USERID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = Convert.ToString(request.SelectToken("userId"));
                    string email = Convert.ToString(request.SelectToken("email"));
                    string userName = Convert.ToString(request.SelectToken("userName"));
                    string firstName = Convert.ToString(request.SelectToken("firstName"));
                    string lastName = Convert.ToString(request.SelectToken("lastName"));
                    string password = Convert.ToString(request.SelectToken("password"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string dateFormat = Convert.ToString(request.SelectToken("dateFormat"));
                    string languageId = Convert.ToString(request.SelectToken("languageId"));
                    if (String.IsNullOrEmpty(userId) || String.IsNullOrEmpty(email) || String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName) || String.IsNullOrEmpty(password) || String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(dateFormat) || String.IsNullOrEmpty(languageId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEMYPROFILEINFO");
                        string[] paramrptinfo = { "EMAIL", "USERID", "USERNAME", "FIRSTNAME", "LASTNAME", "PASSWORD", "CREATEDBY", "DEFAULTROLEID", "DEFAULTSITEID", "DATEFORMAT", "LANGUAGEID" };
                        object[] pValuesrptinfo = { email, userId, userName, firstName, lastName, password, createdBy, DEFAULTROLEID, siteId, dateFormat, languageId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paramrptinfo, pValuesrptinfo);
                        if (iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTPROFILEUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTUSERMAILEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Profile Info Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the contact detail.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/getContactDetail")]
        public HttpResponseMessage getContactDetail(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsProfile = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    StatusMsgData pv = new StatusMsgData();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETSITEADMINFORLOGGEDUSER");
                    string[] paramrptinfo = { "LANGUAGEID", "USERID" };
                    object[] pValuesrptinfo = { languageId, userId };
                    dsProfile = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsProfile.Tables[0].Rows.Count > 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.data = ScriptEngine.GetTableRows(dsProfile.Tables[0]);
                    }
                    else
                    {
                        pv.status = false;
                        pv.message = "FMKNORECS";
                        pv.data = ScriptEngine.GetTableRows(dsProfile.Tables[0]);
                    }
                    response = Request.CreateResponse(HttpStatusCode.OK, pv);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Contact Details data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the alert unsafe.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/getAlertUnsafe")]
        public HttpResponseMessage getAlertUnsafe(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            DataSet dsReportFilter = new DataSet();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterValuesFilter ob = new ParameterValuesFilter();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    DataTable dtTokenValAlert = new DataTable();
                    dtTokenValAlert = BaseAdapter.getAlertUnsafe(userId);
                    strquery = ScriptEngine.GetValue("GETUNSAFEALERT");
                    string[] paramrptinfo = { "LANGUAGEID", "USERID" };
                    object[] pValuesrptinfo = { languageId, userId };
                    dsReportFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    int count = dsReportFilter.Tables[0].Rows.Count;
                    DataTable dtTokenValNew = new DataTable();
                    dtTokenValNew.Columns.Add("Name", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                    dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                    dtTokenValNew.Columns.Add("Data", typeof(String));
                    for (int i = 0; i < count; i++)
                    {
                        string query = dsReportFilter.Tables[0].Rows[i]["QUERY"].ToString();
                        DataSet dsfilter = getValueTable(query, userId, languageId, "", roleId);
                        DataRow dr = dtTokenValNew.NewRow();
                        dr[0] = dsReportFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                        dr[1] = dsReportFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                        dr[2] = dsReportFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                        dr[3] = dsReportFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                        dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                        dtTokenValNew.Rows.Add(dr);
                    }

                    #region for filter setting data
                    DataTable dtTokenValNewF = new DataTable();
                    string[] group1split = new string[] { };
                    string[] group11 = new string[] { };
                    string[] g = new string[] { };
                    StringBuilder sbb = new StringBuilder();
                    try
                    {
                        int row = 0;
                        for (int i = 0; i < dsReportFilter.Tables[2].Rows.Count; i++)
                        {
                            int gg = 1;
                            group1split = dsReportFilter.Tables[2].Rows[i]["RESULT"].ToString().Split(':');
                            group11 = Regex.Split(group1split[1], @"\*\~\*");
                            dtTokenValNewF.Columns.Add(group1split[0], typeof(String));
                            if (dtTokenValNewF.Rows.Count == 0)
                            {
                                DataRow dr = dtTokenValNewF.NewRow();
                                dtTokenValNewF.Rows.Add(dr);
                            }
                            foreach (string aa in group11)
                            {
                                if (gg == group11.Length)
                                {
                                    sbb.Append(aa.Split('~')[0].ToString());
                                }
                                else
                                {
                                    sbb.Append(aa.Split('~')[0].ToString() + ",");
                                }
                                gg++;
                            }
                            dtTokenValNewF.Rows[0][group1split[0]] = sbb.ToString();
                            row++;
                            sbb.Clear();
                        }
                        ob.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF)[0];
                    }
                    catch (Exception ex)
                    {
                        ob.filterSetting = "";
                    }
                    #endregion
                    List<Dictionary<string, object>> lstUserDetail = ScriptEngine.GetTableRows(dtTokenValNew);
                    List<Dictionary<string, object>> lstUserDetail1 = ScriptEngine.GetTableRows(dsReportFilter.Tables[1]);
                    ob.status = true;
                    ob.message = "Success";
                    ob.filter = lstUserDetail;
                    ob.filterLabel = lstUserDetail1;
                    ob.unsafeAlert = int.Parse(dtTokenValAlert.Rows[0]["UNSAFEALERTENABLE"].ToString());
                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Alert Unsafe data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the alert unsafe.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/insertAlertUnsafe")]
        public HttpResponseMessage insertAlertUnsafe(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteIds"));
                    string areaIds = Convert.ToString(request.SelectToken("areaIds"));
                    string subAreaIds = Convert.ToString(request.SelectToken("subAreaIds"));
                    string shiftIds = Convert.ToString(request.SelectToken("shiftIds"));
                    string optionValue = Convert.ToString(request.SelectToken("alertValue"));
                    string siteid1 = "";
                    string areaId1 = "";
                    string subAreaIds1 = "";
                    string shiftIds1s = "";
                    if (!String.IsNullOrEmpty(siteId))
                    {
                        siteid1 = "SITEID=" + siteId + ";";
                    }
                    else
                    {
                        siteid1 = ";";
                    }
                    if (!String.IsNullOrEmpty(areaIds))
                    {
                        areaId1 = "AREAID=" + areaIds + ";";
                    }
                    else
                    {
                        areaId1 = ";";
                    }
                    if (!String.IsNullOrEmpty(subAreaIds))
                    {
                        subAreaIds1 = "SUBAREAID=" + subAreaIds + ";";
                    }
                    else
                    {
                        subAreaIds1 = ";";
                    }
                    if (!String.IsNullOrEmpty(shiftIds))
                    {
                        shiftIds1s = "SHIFTID=" + shiftIds + ";";
                    }
                    else
                    {
                        shiftIds1s = ";";
                    }
                    var varFilters = siteid1 + areaId1 + subAreaIds1 + shiftIds1s;
                    if (optionValue == "0")
                    {
                        strquery = ScriptEngine.GetValue("DELETEUNSAFEALERT");
                        string[] paramrptinfo1 = { "USERID" };
                        object[] pValuesrptinfo1 = { userId };
                        int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);
                        obj1.message = "ALTUNSUBSCRIBEDSUCCESS";
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTUNSAFEALERT");
                        string[] paramrptinfo = { "OPTIONVALUE", "USERID" };
                        object[] pValuesrptinfo = { varFilters, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                        obj1.message = "ALTMAILOPTIONCREATED";
                    }
                    obj1.status = true;

                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Alert Unsafe data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Dashes the board information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/dashBoardInfo")]
        public HttpResponseMessage dashBoardInfo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            DataSet dsReportFilter = new DataSet();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterValuesDashboard ob = new ParameterValuesDashboard();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string langId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string idefaultsiteid = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETDASHBOARDFULLINFO");
                    string[] paramuserrolepermissions1 = { "ROLEID", "USERID", "SITEID", "PUSERID" };
                    object[] pValueuserrolepermissions1 = { roleId, userId, idefaultsiteid, userId };
                    DataSet dsDashboard = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramuserrolepermissions1, pValueuserrolepermissions1);
                    ob.status = true;
                    ob.message = "Success";
                    ob.rolePermission = ScriptEngine.GetTableRows(dsDashboard.Tables[0]);
                    ob.quickLink = ScriptEngine.GetTableRows(dsDashboard.Tables[1]);
                    ob.dashboard = ScriptEngine.GetTableRows(dsDashboard.Tables[2]);
                    ob.dupontNews = ScriptEngine.GetTableRows(dsDashboard.Tables[3]);
                    ob.activeUser = dsDashboard.Tables[4].Rows[0]["USERS"].ToString();
                    ob.activeSite = dsDashboard.Tables[5].Rows[0]["SITES"].ToString();
                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Dashboard Info data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Globals the data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/globalData")]
        public HttpResponseMessage globalData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            DataSet dsReportFilter = new DataSet();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                Dictionary<string, object> scriptengine_data;
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                string[] urldomain = httpUrl.Split('/');
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                string strSettings = sr.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                Dictionary<string, object> cmpdata = new Dictionary<string, object>();
                cmpdata = (Dictionary<string, object>)scriptengine_data.FirstOrDefault(x => x.Key.ToLower() == urldomain[2].ToLower()).Value;
                string CmpId = cmpdata["COMPANYID"].ToString();

                cmpdata = (Dictionary<string, object>)scriptengine_data.FirstOrDefault(x => x.Key == CmpId).Value;
                int STATUS = Convert.ToInt32(cmpdata["STATUS"].ToString());
                string MESSAGE = cmpdata["MESSAGE"].ToString();

                Dictionary<string, object> scriptengine;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctpartdata = (Dictionary<string, object>)scriptengine.FirstOrDefault(x => x.Key.ToLower() == "upload").Value;
                string uploadFileUrl = dctpartdata["UPLOAFILEURL"].ToString();

                string[] paramcompanyid = { "COMPANYID" };
                object[] pValuescompanyid = { CmpId };
                ParameterValuesGlobal ob = new ParameterValuesGlobal();
                strquery = ScriptEngine.GetValue("GETGLOBALDATA");
                DataSet dsGlobal = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcompanyid, pValuescompanyid);

                dsGlobal.Tables[0].Columns.Add("STATUS", typeof(int));
                dsGlobal.Tables[0].Rows[0]["STATUS"] = STATUS;
                dsGlobal.Tables[0].Columns.Add("MESSAGE", typeof(string));
                dsGlobal.Tables[0].Rows[0]["MESSAGE"] = MESSAGE;

                ob.status = true;
                ob.message = "Success";
                ob.globalData = ScriptEngine.GetTableRows(dsGlobal.Tables[0])[0];
                ob.uploadUrl = uploadFileUrl;
                //ob.CustomColor = dsGlobal.Tables[0].Rows[0]["CUSTOMCOLOR"].ToString();
                //ob.CustomFont = dsGlobal.Tables[0].Rows[0]["FONTCOLOR"].ToString();
                response = Request.CreateResponse(HttpStatusCode.OK, ob);
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Global data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// updateLogo.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Profile/updateLogo")]
        public HttpResponseMessage updateLogo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    strquery = ScriptEngine.GetValue("UPDATECOMPLOGO");
                    string[] paramrptinfo1 = { "COMPANYID", "DEFAULTCOMPANYLOGO", "UPDATEDBY" };
                    object[] pValuesrptinfo1 = { compId, "1", userId };
                    int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);
                    obj1.message = "";
                    obj1.status = true;
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Alert Unsafe data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID", "AREAID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId, "" };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }
        
        [HttpGet]
        [Route("api/Profile/cookies")]
        public HttpResponseMessage cookies()
        {
            var response = new HttpResponseMessage();
            var Coki = new CookieHeaderValue("session-Id", "123");
            Coki.Expires = DateTimeOffset.Now.AddDays(2);
            Coki.Domain = Request.RequestUri.Host;
            Coki.Path = "/";
            response.Headers.AddCookies(new CookieHeaderValue[] { Coki });
            return response;
        }
        
        [HttpGet]
        [Route("api/Profile/getcookiesValue")]
        public HttpResponseMessage getcookiesValue()
        {
            HttpResponseMessage respMessage = new HttpResponseMessage();
            //StatusMsgIU obj1 = new StatusMsgIU();
            CookieHeaderValue cookie1 = Request.Headers.GetCookies("session-Id").FirstOrDefault();
            if (cookie1 != null)
            {
                //obj1.message = cookie1["session-Id"].Value;
                //obj1.status = true;
                //  respMessage = Request.CreateResponse(HttpStatusCode.OK, obj1);
                var resp = new HttpResponseMessage(HttpStatusCode.OK);
                resp.Content = new StringContent("", System.Text.Encoding.UTF8, "text/plain");
                respMessage = resp;

            }
            else
            {
                //obj1.message = "";
                //obj1.status = false;
                //respMessage = Request.CreateResponse(HttpStatusCode.OK, obj1);
                respMessage = Request.CreateResponse(HttpStatusCode.OK, "<div style='background: #f00; font-size: 14px; text-align: center; color: #fff; padding: 10px;'>The third partycookies are not enabled in your browser, please enable to access the analytics dashboard data.</ div >");

                //var resp = new HttpResponseMessage(HttpStatusCode.OK);
                //resp.Content = new StringContent("<div style='background: #f00; font-size: 14px; text-align: center; color: #fff; padding: 10px;'>The third partycookies are not enabled /in/ your browser, please enable to access the analytics dashboard data.</ div >", System.Text.Encoding.UTF8, "text/plain");
                //respMessage = resp;
            }

            return respMessage;
        }



        #endregion

    }
}
