﻿using DataPro;
using Newtonsoft.Json.Linq;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;
using StopDataPro.Business.Adapters;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using System.Web.Script.Serialization;
using System.Linq;
using DataPro.APPMOD.ADMIN;
using log4net;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// Site Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SiteController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteController"/> class.
        /// </summary>
        public SiteController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.Log = log4net.LogManager.GetLogger(typeof(SiteController));
            this.BaseAdapter = new BaseAdapter();
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the application information.
        /// </summary>
        /// <value>
        /// The application information.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the BaseAdapter.
        /// </summary>
        /// <value>
        /// The BaseAdapter.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }
        #endregion

        #region Action Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenValTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenValTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenValTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenValTokenVal.Rows[0]["COMPANYID"].ToString();
                    string SITENAME = Convert.ToString(request.SelectToken("siteName"));
                    string ADDRESS = Convert.ToString(request.SelectToken("address"));
                    string CITY = Convert.ToString(request.SelectToken("city"));
                    string STATE = Convert.ToString(request.SelectToken("state"));
                    string ZIPCODE = Convert.ToString(request.SelectToken("zipCode"));
                    string COMMENTS = Convert.ToString(request.SelectToken("comments"));
                    int STATUS = Convert.ToInt32(request.SelectToken("status"));
                    string EMPCOUNT = Convert.ToString(request.SelectToken("employees"));
                    string COUNTRYID = Convert.ToString(request.SelectToken("countryId"));
                    string TIMEZONEID = Convert.ToString(request.SelectToken("defaultTimeZone"));
                    if (String.IsNullOrEmpty(SITENAME) || String.IsNullOrEmpty(ADDRESS) || String.IsNullOrEmpty(CITY) || String.IsNullOrEmpty(COUNTRYID) || String.IsNullOrEmpty(TIMEZONEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTSITEGENERALINFO");
                        string[] paraminsert = { "COMPANYID", "SITENAME", "ADDRESS", "CITY", "STATE", "ZIPCODE", "COUNTRYID", "COMMENTS", "STATUS", "EMPCOUNT", "CREATEDBY", "TIMEZONEID" };
                        object[] pValuesinsert = { COMPANYID, SITENAME, ADDRESS, CITY, STATE, ZIPCODE, COUNTRYID, COMMENTS, STATUS, EMPCOUNT, CREATEDBY, TIMEZONEID };

                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSITECREATED";
                            obj1.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSITENAMEEXITS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSITENOTCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert Site:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(SITEID) || String.IsNullOrEmpty(userid))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESITES");
                        string[] paraminsert = { "SITEID", "UPDATEDBY" };
                        object[] pValuesinsert = { SITEID, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 || iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSITEDEL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSITENOTDEL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Site:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }


        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string UPDATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string SITENAME = Convert.ToString(request.SelectToken("siteName"));
                    string ADDRESS = Convert.ToString(request.SelectToken("address"));
                    string CITY = Convert.ToString(request.SelectToken("city"));
                    string STATE = Convert.ToString(request.SelectToken("state"));
                    string ZIPCODE = Convert.ToString(request.SelectToken("zipCode"));
                    string COMMENTS = Convert.ToString(request.SelectToken("comments"));
                    int STATUS = Convert.ToInt32(request.SelectToken("status"));
                    string EMPCOUNT = Convert.ToString(request.SelectToken("employees"));
                    string COUNTRYID = Convert.ToString(request.SelectToken("countryId"));
                    string TIMEZONEID = Convert.ToString(request.SelectToken("defaultTimeZone"));
                    string siteID = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(siteID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESITEINFO");
                        string[] paraminsert = { "SITEID", "COMPANYID", "SITENAME", "ADDRESS", "CITY", "STATE", "ZIPCODE", "COUNTRYID", "COMMENTS", "STATUS", "EMPCOUNT", "UPDATEDBY", "TIMEZONEID" };
                        object[] pValuesinsert = { siteID, COMPANYID, SITENAME, ADDRESS, CITY, STATE, ZIPCODE, COUNTRYID, COMMENTS, STATUS, EMPCOUNT, UPDATEDBY, TIMEZONEID };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSITENAMEEXITS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult != 0 && iresult > 0 && iresult == 1)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSITEUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSITENOTUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Site:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string siteID = Convert.ToString(request.SelectToken("siteId"));
                    int STATUS = Convert.ToInt32(request.SelectToken("status"));
                    string UPDATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(siteID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESITESTATUS");
                        string[] paraminsert = { "SITEID", "UPDATEDBY" };
                        object[] pValuesinsert = { siteID, UPDATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 1)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSUCESSCHANGESTATUS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update statue Site:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/GetSiteData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetSiteData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterSiteValues obj = new ParameterSiteValues();
            DataSet dsSite = new DataSet();
            DataSet dsAssing = new DataSet();
            DataSet dsTarget = new DataSet();
            DataSet dsTargetWeek = new DataSet();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string assignType = Convert.ToString(request.SelectToken("assignType"));
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string AreaManagEnable = dtTokenVal.Rows[0]["ENABLEAREAMANAGER"].ToString();

                    DataTable dAvailSort = new DataTable();
                    if (String.IsNullOrEmpty(SITEID))
                    {
                        strquery = ScriptEngine.GetValue("GETSITELISTING");
                        string[] paraminsert = { "USERID" };
                        object[] pValuesinsert = { USERID };
                        dsSite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    }
                    else if (!String.IsNullOrEmpty(SITEID))
                    {
                        strquery = ScriptEngine.GetValue("GETSITEINFO");
                        string[] paraminsert = { "SITEID" };
                        object[] pValuesinsert = { SITEID };
                        dsSite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETSITEAREASINFO");
                        string[] paraminsertt = { "SITEID" };
                        object[] pValuesinsertt = { SITEID, };
                        dsAssing = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsertt, pValuesinsertt);
                        
                        strquery = ScriptEngine.GetValue("GETTARGETFORSITE");
                        string[] paraminsert2 = { "SITEID" };
                        object[] pValuesinsert2 = { SITEID };
                        dsTarget = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);

                        strquery = ScriptEngine.GetValue("GETWEEKLYTARGETSITES");
                        dsTargetWeek = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        
                        if (dsAssing.Tables[2].Rows.Count == 0)
                        {
                            DataRow dr = dsAssing.Tables[2].NewRow();
                            dr[0] = "";
                            dsAssing.Tables[2].Rows.Add(dr);
                        }
                    }
                    if (dsSite.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstList = ScriptEngine.GetTableRows(dsSite.Tables[0]);
                        if (String.IsNullOrEmpty(SITEID))
                        {
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstList;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            if (AreaManagEnable == "1")
                            {
                                string[] paraminsertt = { "SITEID", "AREAID" };
                                object[] pValuesinsertt = { SITEID, 0 };
                                strquery = ScriptEngine.GetValue("GETAREAMANAGERAREALIST");
                                DataSet dsAreaManger = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsertt, pValuesinsertt);
                                DataTable dAvailSort1 = dsAreaManger.Tables[2].Copy();
                                dAvailSort1.Merge(dsAreaManger.Tables[1]);
                                obj.areas = ScriptEngine.GetTableRows(dsAreaManger.Tables[0]);
                                obj.areaAvailable = ScriptEngine.GetTableRows(dAvailSort1);
                            }
                            else
                            {
                                obj.areas = new string[] { };
                                obj.areaAvailable = new string[] { };
                            }
                            List<Dictionary<string, object>> lstcountry = ScriptEngine.GetTableRows(dsSite.Tables[1]);
                            List<Dictionary<string, object>> lsttime = ScriptEngine.GetTableRows(dsSite.Tables[2]);
                            List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssing.Tables[2]);
                            List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dAvailSort);
                            List<Dictionary<string, object>> lstMonth = ScriptEngine.GetTableRows(dsTarget.Tables[0]);
                            List<Dictionary<string, object>> lstWeek = ScriptEngine.GetTableRows(dsTargetWeek.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.Data = lstList[0];
                            obj.country = lstcountry;
                            obj.timeZone = lsttime;
                            obj.available = lstAvail;
                            obj.assign = lstAssign[0]["areaIds"];
                            if (lstWeek.Count > 0)
                            {
                                obj.targetWeekly = lstWeek[0];
                            }
                            else
                            {
                                obj.targetWeekly = "";
                            }

                            if (lstMonth.Count > 0)
                            {
                                obj.targetMonthly = lstMonth[0];
                            }
                            else
                            {
                                obj.targetMonthly = "";
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Site Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site assign data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/GetSiteAssignData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetSiteAssignData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterSiteAssign obj = new ParameterSiteAssign();
            DataSet dsAssing = new DataSet();
            DataSet dsGroup = new DataSet();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string assignType = Convert.ToString(request.SelectToken("assignType"));
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(SITEID) || String.IsNullOrEmpty(assignType) || int.Parse(assignType) >= 5)
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string strquery = "";
                        string[] paraminsert = { "SITEID", "USERID" };
                        object[] pValuesinsert = { SITEID, USERID };
                        if (assignType == "1")
                        {
                            strquery = ScriptEngine.GetValue("GETSITEAREASINFO");
                        }
                        else if (assignType == "2")
                        {
                            strquery = ScriptEngine.GetValue("GETSITESHIFTSINFO");
                        }
                        else if (assignType == "3")
                        {
                            strquery = ScriptEngine.GetValue("GETSITEUSERSINFO");
                        }
                        else if (assignType == "4")
                        {
                            strquery = ScriptEngine.GetValue("GETGROUPTYPE");
                            string[] paraminsert1 = { "LANGUAGEID" };
                            object[] pValuesinsert1 = { languageId };
                            dsGroup = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            if (String.IsNullOrEmpty(groupTypeId))
                            {
                                groupTypeId = dsGroup.Tables[0].Rows[0]["GROUPTYPEID"].ToString();
                            }
                            strquery = ScriptEngine.GetValue("GETSITEGROUPSINFO");
                            string[] paraminsert2 = { "LANGUAGEID", "GROUPTYPEID", "SITEID" };
                            object[] pValuesinsert2 = { languageId, groupTypeId, SITEID };
                            dsAssing = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        }
                        if (assignType != "4")
                        {
                            dsAssing = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if (dsAssing.Tables[2].Rows.Count == 0)
                        {
                            DataRow dr = dsAssing.Tables[2].NewRow();
                            dr[0] = "";
                            dsAssing.Tables[2].Rows.Add(dr);
                        }
                        if (dsAssing.Tables[0].Rows.Count != 0 || dsAssing.Tables[1].Rows.Count != 0)
                        {
                            DataTable dtAvailSort = dsAssing.Tables[1].Copy();
                            dtAvailSort.Merge(dsAssing.Tables[0]);

                            if (assignType == "4")
                            {
                                ParameterSiteAssignGroup ob2 = new ParameterSiteAssignGroup();
                                List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssing.Tables[2]);
                                List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dtAvailSort);
                                List<Dictionary<string, object>> lstGroup = ScriptEngine.GetTableRows(dsGroup.Tables[0]);
                                ob2.status = true;
                                ob2.message = "Success";
                                ob2.group = lstGroup;
                                ob2.available = lstAvail;
                                ob2.assign = lstAssign[0]["groupIds"];
                                response = Request.CreateResponse(HttpStatusCode.OK, ob2);
                            }
                            else
                            {
                                List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssing.Tables[2]);
                                List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dtAvailSort);
                                obj.status = true;
                                obj.message = "Success";
                                obj.available = lstAvail;
                                if (assignType == "1")
                                {
                                    obj.assign = lstAssign[0]["areaIds"];
                                }
                                if (assignType == "2")
                                {
                                    obj.assign = lstAssign[0]["shiftIds"];
                                }
                                if (assignType == "3")
                                {
                                    obj.assign = lstAssign[0]["userIds"];
                                }
                                if (assignType == "4")
                                {
                                    obj.assign = lstAssign[0]["groupIds"];
                                }
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                        }
                        else
                        {
                            if (assignType == "4")
                            {
                                ParameterSiteAssignGroup ob2 = new ParameterSiteAssignGroup();
                                List<Dictionary<string, object>> lstGroup = ScriptEngine.GetTableRows(dsGroup.Tables[0]);
                                ob2.status = true;
                                ob2.message = "Success";
                                ob2.group = lstGroup;
                                ob2.available = new string[] { };
                                ob2.assign = "";
                                response = Request.CreateResponse(HttpStatusCode.OK, ob2);
                            }
                            else
                            {
                                obj.status = true;
                                obj.message = "Success";
                                obj.available = new string[] { };
                                obj.assign = "";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                                //obj1.status = false;
                                //obj1.message = "FMKNORECS";
                                //response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Site Assign data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>Gets the area manager data.</summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/getAreaManagerData")]
        [APIAuthAttribute]
        public HttpResponseMessage getAreaManagerData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterValuesForAdd obj1 = new ParameterValuesForAdd();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterSiteAreaMangerValues obj = new ParameterSiteAreaMangerValues();
                    string AreaManagEnable = dtTokenVal.Rows[0]["ENABLEAREAMANAGER"].ToString();
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string[] paraminsertt = { "SITEID", "AREAID" };
                    object[] pValuesinsertt = { siteId, areaId };
                    strquery = ScriptEngine.GetValue("GETAREAMANAGERAREALIST");
                    DataSet dsAreaManger = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsertt, pValuesinsertt);
                    DataTable dAvailSort = dsAreaManger.Tables[2].Copy();
                    dAvailSort.Merge(dsAreaManger.Tables[1]);
                    if (dsAreaManger.Tables[3].Rows.Count == 0)
                    {
                        DataRow dr = dsAreaManger.Tables[3].NewRow();
                        dr[0] = "";
                        dsAreaManger.Tables[3].Rows.Add(dr);
                    }
                    obj.status = true;
                    obj.message = "Success";
                    obj.userAssign = ScriptEngine.GetTableRows(dAvailSort);
                    obj.userAvailable = ScriptEngine.GetTableRows(dsAreaManger.Tables[3])[0]["userIds"];
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Sites the add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/siteAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage siteAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterValuesForAdd obj1 = new ParameterValuesForAdd();
            DataSet dsSite = new DataSet();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    strquery = ScriptEngine.GetValue("GETCOUNTRIES");
                    dsSite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (dsSite.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> countryData = ScriptEngine.GetTableRows(dsSite.Tables[0]);
                        List<Dictionary<string, object>> timeZoneData = ScriptEngine.GetTableRows(dsSite.Tables[1]);
                        List<Dictionary<string, object>> defaultTimeZoneValue = ScriptEngine.GetTableRows(dsSite.Tables[2]);
                        obj1.status = true;
                        obj1.message = "Success";
                        obj1.countries = countryData;
                        obj1.timeZone = timeZoneData;
                        obj1.globaltimeZone = defaultTimeZoneValue[0]["OPTIONVALUE"];
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the site assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/updateSiteAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateSiteAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string assignType = Convert.ToString(request.SelectToken("assignType"));
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string assignIds = Convert.ToString(request.SelectToken("assignIds"));
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(SITEID) || String.IsNullOrEmpty(assignType) || int.Parse(assignType) >= 5)
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                    }
                    else
                    {
                        string strquery = "";
                        int iresult = 0;
                        if (assignType == "1")
                        {
                            strquery = ScriptEngine.GetValue("UPDATESITEAREAASSIGN");
                            string[] paraminsert = { "SITEID", "USERID", "AREAID" };
                            object[] pValuesinsert = { SITEID, USERID, assignIds };
                            iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            obj1.message = "ALTSITEAREAASSIGNUPDATE";

                        }
                        else if (assignType == "2")
                        {
                            strquery = ScriptEngine.GetValue("UPDATESITESHIFTSASSIGN");
                            string[] paraminsert = { "SITEID", "USERID", "SHIFTID" };
                            object[] pValuesinsert = { SITEID, USERID, assignIds };
                            iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            obj1.message = "ALTSITESHIFTASSIGNUPDATE";
                        }
                        else if (assignType == "3")
                        {
                            strquery = ScriptEngine.GetValue("UPDATESITEUSERSASSIGN");
                            string[] paraminsert = { "SITEID", "USERID", "SITEUSERID" };
                            object[] pValuesinsert = { SITEID, USERID, assignIds };
                            iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            obj1.message = "ALTSITEUSERASSIGNUPDATE";

                        }
                        else if (assignType == "4")
                        {
                            strquery = ScriptEngine.GetValue("UPDATESITEGROUPSASSIGN");
                            string[] paraminsert = { "SITEID", "USERID", "SITEGROUPID", "GROUPTYPEID" };
                            object[] pValuesinsert = { SITEID, USERID, assignIds, groupTypeId };
                            if (String.IsNullOrEmpty(groupTypeId))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add Mandatory Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                iresult = -1;
                            }
                            else
                            {
                                iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                                obj1.message = "ALTSITEGROUPASSIGNUPDATE";
                            }

                        }
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Site assign:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;

        }

        [HttpPost]
        [Route("api/Site/updateAreaManager")]
        [APIAuthAttribute]
        public HttpResponseMessage updateAreaManager(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                 request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string userIds = Convert.ToString(request.SelectToken("userIds"));
                    if (String.IsNullOrEmpty(areaId) || String.IsNullOrEmpty(SITEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                    }
                    else
                    {
                        string strquery = ScriptEngine.GetValue("UPDATEAREAMANAGERASSIGN");
                        string[] paraminsert = { "SITEID", "AREAID", "USERID", "@CREATEDBY" };
                        object[] pValuesinsert = { SITEID, areaId, userIds, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        obj1.message = "ALTAREAMANAGERASSIGNUPDATE";
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Site assign:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        /// <summary>
        /// Targets the data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/TargetData")]
        [APIAuthAttribute]
        public HttpResponseMessage TargetData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string strquery;
                    string siteid = Convert.ToString(request.SelectToken("siteId"));
                    string JAN = Convert.ToString(request.SelectToken("jan"));
                    string FEB = Convert.ToString(request.SelectToken("feb"));
                    string MAR = Convert.ToString(request.SelectToken("mar"));
                    string APR = Convert.ToString(request.SelectToken("apr"));
                    string MAY = Convert.ToString(request.SelectToken("may"));
                    string JUN = Convert.ToString(request.SelectToken("jun"));
                    string JUL = Convert.ToString(request.SelectToken("jul"));
                    string AUG = Convert.ToString(request.SelectToken("aug"));
                    string SEP = Convert.ToString(request.SelectToken("sep"));
                    string OCT = Convert.ToString(request.SelectToken("oct"));
                    string NOV = Convert.ToString(request.SelectToken("nov"));
                    string DEC = Convert.ToString(request.SelectToken("dec"));
                    string APPLYTOALLOBSERVERS = Convert.ToString(request.SelectToken("applyToAllObser"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string target = Convert.ToString(request.SelectToken("targets"));
                    string weeklyTarget = Convert.ToString(request.SelectToken("weeklyTarget"));

                    int ischeduleid;
                    string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl;
                    DateTime senddate;
                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();
                    int iresult = 0;
                    DataSet dsmaildetails = new DataSet();
                    if (target == "0")
                    {
                        strquery = ScriptEngine.GetValue("INSERTTARGETFORSITE");
                        string[] paraminsert = { "SITEID", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "APPLYTOALLOBSERVERS", "CREATEDBY" };
                        object[] pValuesinsert = { siteid, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, APPLYTOALLOBSERVERS, userid };
                        iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                    }
                    else if (target == "1")
                    {
                        strquery = ScriptEngine.GetValue("INSERTWEEKLYTARGETSITES");
                        string[] paraminsert = { "SITEID", "WEEKTARGET", "APPLYTOALLOBSERVERS", "CREATEDBY" };
                        object[] pValuesinsert = { siteid, weeklyTarget, APPLYTOALLOBSERVERS, userid };
                        iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                    }
                    if (iresult != 0 && iresult > 0)
                    {
                        obj1.status = true;
                        obj1.message = "ALTTARGETUPDATE";

                        string[] paramsiteupdate = { "SITEID", "TARGETTYPE", "USERID" };
                        object[] pValuesiteupdate = { siteid, target, userid };
                        strquery = ScriptEngine.GetValue("MAILSITETARGETUPDATE");
                        dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramsiteupdate, pValuesiteupdate);
                        try
                        {
                            Log.InfoFormat("==Site Process Start==");
                            ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                            strmailfrom = dsmaildetails.Tables[0].Rows[0]["FROM"].ToString();
                            strreplyto = dsmaildetails.Tables[0].Rows[0]["REPLYTO"].ToString();
                            strto = dsmaildetails.Tables[0].Rows[0]["TO"].ToString();
                            strcc = dsmaildetails.Tables[0].Rows[0]["CC"].ToString();
                            strbcc = dsmaildetails.Tables[0].Rows[0]["BCC"].ToString();
                            strsubject = dsmaildetails.Tables[0].Rows[0]["SUBJECT"].ToString();
                            strbody = dsmaildetails.Tables[0].Rows[0]["BODY"].ToString();
                            senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[0]["SENTDATE"].ToString());
                            strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                            String str = "[{\"APPID\":\"" + strAPPID
                                    + "\",\"COMPANYID\":\"" + COMPANYID
                                    + "\",\"TOKENID\":\"" + ischeduleid
                                    + "\",\"URL\":\"" + strcompanyurl
                                    + "\",\"MAILFROM\":\"" + strmailfrom
                                    + "\",\"MAILREPLYTO\":\"" + strreplyto
                                    + "\",\"MAILTO\":\"" + strto
                                    + "\",\"MAILCC\":\"" + strcc
                                    + "\",\"MAILBCC\":\"" + strbcc
                                    + "\",\"MAILSUBJECT\":\"" + strsubject
                                    + "\",\"MAILBODY\":\"" + strbody
                                    + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                            Log.InfoFormat("JSON String:" + str);
                            //mail.SendNotificationDetails(str);
                            object[] objSendNotificationDetails = new object[1];
                            objSendNotificationDetails[0] = str;
                            CallWebServices CWS = new CallWebServices();
                            object bj = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                            Log.InfoFormat("WebService Return Value:" + bj.ToString());
                        }
                        catch (Exception ex)
                        {
                            Log.InfoFormat("Exception Occured When Send Mail for site Target:" + ex.StackTrace);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "ALTTARGETUPDATEFAIL";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Target data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Site/getKeyWord")]
        [APIAuthAttribute]
        public HttpResponseMessage getKeyWord(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SITESCHEDULETYPEID = "10";
                    strquery = ScriptEngine.GetValue("GETSCHEDULEMAILKEYWORD");
                    string[] paraminsert = { "SCHEDULETYPEID" };
                    object[] pValuesinsert = { SITESCHEDULETYPEID };
                    DataSet dsSite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    string[] KeyWordstemp = new string[] { };
                    StringBuilder KeyWords = new StringBuilder();
                    StringBuilder KeyWordsSpanId = new StringBuilder();
                    DataTable dtTokenValNew = new DataTable();
                    dtTokenValNew.Columns.Add("label", typeof(String));
                    dtTokenValNew.Columns.Add("keyWordId", typeof(String));
                    string keyword = dsSite.Tables[0].Rows[0]["KEYWORD"].ToString();
                    var keyCommaSplit = keyword.Split(',');
                    for (int i = 0; i < keyCommaSplit.Length; i++)
                    {
                        DataRow dr = dtTokenValNew.NewRow();
                        KeyWordstemp = keyCommaSplit[i].Split('~');
                        dr[0] = KeyWordstemp[0];
                        dr[1] = KeyWordstemp[4];
                        dtTokenValNew.Rows.Add(dr);
                    }
                    dsSite.Tables[0].Rows[0]["KEYWORD"] = "";
                    dsSite.Tables[0].Rows[0]["KEYWORD"] = JsonConvert.SerializeObject(dtTokenValNew);
                    obj1.status = true;
                    obj1.Data = ScriptEngine.GetTableRows(dsSite.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Email keywork:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        [HttpPost]
        [Route("api/Site/sendMailForSite")]
        public HttpResponseMessage sendMailForSite(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string subject = Convert.ToString(request.SelectToken("subject"));
                    string body = Convert.ToString(request.SelectToken("body"));
                    if (String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(subject) || String.IsNullOrEmpty(body))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        //DataPro configuration setting 
                        Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                        string strAPPID = dctpartdata["APPID"].ToString();
                        string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                        string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                        string ES_WSMETHOD1 = dctpartdata["WSMETHOD1"].ToString();
                        string ES_WSURL = dctpartdata["WSURL"].ToString();
                        try
                        {
                            string[] paramsiteupdate = { "COMPANYID", "SITEID", "SUBJECT", "BODY", "CREATEDBY" };
                            object[] pValuesiteupdate = { COMPANYID, siteId, subject, body, USERID };
                            string strquery = ScriptEngine.GetValue("MAILSITEUPDATE");
                            DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramsiteupdate, pValuesiteupdate);
                            int ischeduleid;
                            string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl;
                            DateTime senddate;
                            if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                            {
                                for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                {
                                    ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                    strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                    strreplyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                    strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                    strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                    strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                    strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                    strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                    senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                    strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                    String str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + COMPANYID + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";

                                    Log.InfoFormat("String" + str);
                                    Log.InfoFormat("1. ES_WSMETHOD:" + ES_WSMETHOD);
                                    Log.InfoFormat("2. ES_WSURL:" + ES_WSURL);
                                    Log.InfoFormat("3. ES_WSNAME:" + ES_WSNAME);

                                    object[] objSendNotificationDetails = new object[1];
                                    objSendNotificationDetails[0] = str;
                                    CallWebServices CWS = new CallWebServices();
                                    object obj = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                    Log.InfoFormat("4. ObjectResponse:" + obj.ToString());
                                }
                                obj1.status = true;
                                obj1.message = "ALTMAILSENT";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.InfoFormat("Exception Occured When send Email for site:" + ex.StackTrace);
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
    }
}