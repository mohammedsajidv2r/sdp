﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// UserTargetController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class UserTargetController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserTargetController"/> class.
        /// </summary>
        public UserTargetController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(UserTargetController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SELECTEDUSERID = Convert.ToString(request.SelectToken("userIds"));
                    string siteid = Convert.ToString(request.SelectToken("siteId"));
                    string JAN = Convert.ToString(request.SelectToken("jan"));
                    string FEB = Convert.ToString(request.SelectToken("feb"));
                    string MAR = Convert.ToString(request.SelectToken("mar"));
                    string APR = Convert.ToString(request.SelectToken("apr"));
                    string MAY = Convert.ToString(request.SelectToken("may"));
                    string JUN = Convert.ToString(request.SelectToken("jun"));
                    string JUL = Convert.ToString(request.SelectToken("jul"));
                    string AUG = Convert.ToString(request.SelectToken("aug"));
                    string SEP = Convert.ToString(request.SelectToken("sep"));
                    string OCT = Convert.ToString(request.SelectToken("oct"));
                    string NOV = Convert.ToString(request.SelectToken("nov"));
                    string DEC = Convert.ToString(request.SelectToken("dec"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string targets = Convert.ToString(request.SelectToken("targets"));
                    string targetallsite = "0";
                    string weeklyTarget = Convert.ToString(request.SelectToken("weeklyTarget"));
                    int ischeduleid;
                    string strmailfrom, replyto, strto, cc, bcc, strsubject, strbody, strcompanyurl;
                    DateTime dtmailsentdate;

                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();
                    if (String.IsNullOrEmpty(siteid) || String.IsNullOrEmpty(SELECTEDUSERID) || String.IsNullOrEmpty(targets))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataSet dsresult = new DataSet();
                        string mode = "";
                        if (targets == "0")
                        {
                            mode = "INSERTTARGETMULTIUSERS";
                            strquery = ScriptEngine.GetValue("INSERTTARGETMULTIUSERS");
                            string[] paraminsert = { "TARGETALLSITES", "SITEID", "USERID", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "CREATEDBY" };
                            object[] pValuesinsert = { targetallsite, siteid, SELECTEDUSERID, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, userid };
                            dsresult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        else if (targets == "1")
                        {
                            mode = "INSERTWEEKLYTARGETMULTIUSER";
                            strquery = ScriptEngine.GetValue("INSERTWEEKLYTARGETMULTIUSER");
                            string[] paraminsert = { "TARGETALLSITES", "SITEID", "USERID", "WEEKTARGET", "CREATEDBY" };
                            object[] pValuesinsert = { targetallsite, siteid, SELECTEDUSERID, weeklyTarget, userid };
                            dsresult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if ((dsresult.Tables.Count > 0) && (dsresult.Tables[0].Rows.Count > 0))
                        {
                            obj1.status = true;
                            obj1.message = "ALTTARGETUPDATE";
                            Log.InfoFormat("==User Outside Target Mail Process Start==");
                            try
                            {
                                #region Mail
                                ischeduleid = Convert.ToInt32(dsresult.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                strmailfrom = dsresult.Tables[0].Rows[0]["FROM"].ToString();
                                replyto = dsresult.Tables[0].Rows[0]["REPLYTO"].ToString();
                                strto = dsresult.Tables[0].Rows[0]["TO"].ToString();
                                cc = dsresult.Tables[0].Rows[0]["CC"].ToString();
                                bcc = dsresult.Tables[0].Rows[0]["BCC"].ToString();
                                strsubject = dsresult.Tables[0].Rows[0]["SUBJECT"].ToString();
                                strbody = dsresult.Tables[0].Rows[0]["BODY"].ToString();
                                dtmailsentdate = Convert.ToDateTime(dsresult.Tables[0].Rows[0]["SENTDATE"].ToString());
                                strcompanyurl = dsresult.Tables[0].Rows[0]["URL"].ToString();
                                String str = "[{\"APPID\":\"" + strAPPID
                                   + "\",\"COMPANYID\":\"" + COMPANYID
                                   + "\",\"TOKENID\":\"" + ischeduleid
                                   + "\",\"URL\":\"" + strcompanyurl
                                   + "\",\"MAILFROM\":\"" + strmailfrom
                                   + "\",\"MAILREPLYTO\":\"" + replyto
                                   + "\",\"MAILTO\":\"" + strto
                                   + "\",\"MAILCC\":\"" + cc
                                   + "\",\"MAILBCC\":\"" + bcc
                                   + "\",\"MAILSUBJECT\":\"" + strsubject
                                   + "\",\"MAILBODY\":\"" + strbody
                                   + "\",\"MAILTOBESENTDATE\":\"" + dtmailsentdate.ToString("MM-dd-yyyy") + "\"}]";
                                object[] objSendNotificationDetails = new object[1];
                                objSendNotificationDetails[0] = str;
                                Log.InfoFormat("JSON String:" + str);
                                CallWebServices CWS = new CallWebServices();
                                object obj = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                Log.InfoFormat("WebService Return Value:" + obj.ToString());
                                if (obj.ToString() != "1")
                                {
                                    strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                    string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                    object[] pValuesrptinfo = { COMPANYID, userid, mode, 1, mode, mode, obj.ToString() };
                                    int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                                }
                                #endregion
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for user Target:" + ex.StackTrace);
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { COMPANYID, userid, mode, 1, mode, mode, ex.ToString() };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTTARGETUPDATEFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert Target Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the target data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserTarget/getTargetData")]
        [APIAuthAttribute]
        public HttpResponseMessage getTargetData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterValuesUserTarget obj1 = new ParameterValuesUserTarget();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet dsUserFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ClsLogWrite cs = new ClsLogWrite();
                    string SITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID1 = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETCHECKLISTSITELIST");
                    string[] paraminsert = { "USERID" };
                    object[] pValuesinsert = { userid };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    //loadfiltedata
                    strquery = ScriptEngine.GetValue("GETUSERSFILTERTARGET");
                    string[] paramrptinfo = { "USERID", "LANGUAGEID" };
                    object[] pValuesrptinfo = { userid, languageId };
                    dsUserFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string USERTYPEID = Convert.ToString(request.SelectToken("userType"));
                    string STATUSID = Convert.ToString(request.SelectToken("statusId"));
                    string gropu1 = Convert.ToString(request.SelectToken("group1"));
                    string gropu2 = Convert.ToString(request.SelectToken("group2"));
                    string gropu3 = Convert.ToString(request.SelectToken("group3"));
                    string gropu4 = Convert.ToString(request.SelectToken("group4"));
                    string gropu5 = Convert.ToString(request.SelectToken("group5"));
                    string gropu6 = Convert.ToString(request.SelectToken("group6"));
                    string gropu7 = Convert.ToString(request.SelectToken("group7"));
                    string gropu8 = Convert.ToString(request.SelectToken("group8"));
                    string gropu9 = Convert.ToString(request.SelectToken("group9"));
                    string gropu10 = Convert.ToString(request.SelectToken("group10"));
                    strquery = ScriptEngine.GetValue("GETUSERSAVAILTOSITEFORTARGET");
                    if (!String.IsNullOrEmpty(siteId))
                    {
                        SITEID1 = siteId;
                    }
                    string[] paraminsert1 = { "USERID", "SITEID", "USERTYPE", "ROLEID", "STATUSID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10" };
                    object[] pValuesinsert1 = { userid, SITEID1, USERTYPEID, ROLEID, STATUSID, gropu1, gropu2, gropu3, gropu4, gropu5, gropu6, gropu7, gropu8, gropu9, gropu10 };
                    ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds1.Tables[0]);
                        Dictionary<string, object> retresult = new Dictionary<string, object>();
                        int count = dsUserFilter.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));
                        for (int i = 0; i < count; i++)
                        {
                            string query = dsUserFilter.Tables[0].Rows[i]["QUERY"].ToString();
                            DataSet dsfilter = getValueTable(query, userid, languageId, SITEID1);
                            DataRow dr = dtTokenValNew.NewRow();

                            dr[0] = dsUserFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsUserFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsUserFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsUserFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            dtTokenValNew.Rows.Add(dr);
                        }
                        DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                        DataTable dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID <> " + "5" + "").Count() > 0 ? dtTokenValNew.Select("OPTIONSCATEGORYID  <> " + "5" + "").CopyToDataTable() : dtTokenValNew.Clone();
                        //DataTable dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID <> " + "5" + "").CopyToDataTable();
                        List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(dtTokenValoptional);
                        List<Dictionary<string, object>> lstPersons4 = ScriptEngine.GetTableRows(dtTokenValfilter);
                        obj1.status = true;
                        obj1.message = "Success";
                        obj1.defaultSiteId = SITEID;
                        obj1.site = lstPersons;
                        obj1.users = lstPersons2;
                        obj1.filterOptional = lstPersons3;
                        obj1.filterData = lstPersons4;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Target Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID" };
            object[] pValuesinsert = { userid, languageId, SITEID };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        /// <summary>
        /// Gets the filter data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserTarget/getFilterData")]
        [APIAuthAttribute]
        public HttpResponseMessage getFilterData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet dsUserFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SITEID = "";
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    SITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string filterQuery = Convert.ToString(request.SelectToken("filterQuery"));
                    strquery = ScriptEngine.GetValue(filterQuery);
                    string[] paraminsert = { "USERID", "LANGUAGEID" };
                    object[] pValuesinsert = { userid, languageId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj1.status = true;
                        obj1.message = "Success";
                        obj1.Data = lstPersons;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion

    }




}
