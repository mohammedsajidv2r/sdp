﻿using DataPro;
using Ionic.Zip;
using log4net;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// UploadController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class UploadController : ApiController
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="UploadController"/> class.
        /// </summary>
        public UploadController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.infoconfig = new AppInfo();
            this.Bal = new BaseAdapter();
            this.log = log4net.LogManager.GetLogger(typeof(UploadController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the infoconfig.
        /// </summary>
        /// <value>
        /// The infoconfig.
        /// </value>
        public AppInfo infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter Bal { get; set; }


        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public ILog log { get; set; }

        #endregion


        #region "Objects and Variables Declaration for file & data upload"
        Dictionary<string, object> retdata = null;
        JavaScriptSerializer Serializer;
        Dictionary<string, object> inputData = null;
        #endregion

        #region Action Method

        /// <summary>
        /// Fileuploadwithauthes this instance.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Upload/fileuploadwithauth")]
        [APIAuthAttribute]
        public object fileuploadwithauth()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string trace = "";
            try
            {
                string encodedString = "";
                var authorization = HttpContext.Current.Request.Headers["Authorization"];
                char[] whitespace = new char[] { ' ', '\t' };
                string[] tokenstr = authorization.Split(whitespace);
                encodedString = tokenstr[1].ToString();
                //for read config get connectionstring
                //string httpUrl = "https://dp50review-qcdp50.stopdatapro.com";
                trace += "A1,";
                log.InfoFormat("==A1==");
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                trace += "A2,";
                Bal.constringBal = infoconfig.createConn(httpUrl);
                trace += "A3,";
                trace += "A4,";
                log.InfoFormat("==A2==");
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                dt = Bal.CheckLoginToken(encodedString);
                trace += "A4,";
                if (dt.Rows.Count == 0)
                {
                    trace += "A5,";
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    trace += "1,";
                    log.InfoFormat("==A3==");
                    retdata = new Dictionary<string, object>();
                    Serializer = new JavaScriptSerializer();
                    Serializer.MaxJsonLength = Int32.MaxValue;
                    trace += "2,";
                    log.InfoFormat("==A4==");
                    inputData = Serializer.Deserialize<Dictionary<string, object>>(HttpContext.Current.Request.Params["hdndata"].ToString());
                    log.InfoFormat("==A4==" + inputData);
                    trace += "3,";
                    if (inputData.ContainsKey("action") && inputData["action"].ToString() == "binarydata")
                    {
                        log.InfoFormat("==A5==" + inputData["binaryimgdata"].ToString());
                        trace += "4,";
                        retdata.Add("RETVAL", WriteBinaryData(inputData, inputData["binaryimgdata"].ToString()));
                        trace += "5,";
                    }
                    else if (HttpContext.Current.Request.Files.AllKeys.Any())
                    {
                        trace += "6,";
                        var httpPostedFile = HttpContext.Current.Request.Files["filebinarydata"];
                        trace += "7,";
                        if (httpPostedFile != null)
                        {
                            trace += "8,";
                            retdata.Add("RETVAL", writefile(inputData, httpPostedFile.InputStream));
                            trace += "9,";
                        }
                    }
                    //return retdata;
                    trace += "10,";
                    response = Request.CreateResponse(HttpStatusCode.OK, retdata);
                }
            }
            catch (Exception ex)
            {
                obj1.status = false;
                obj1.message = ex.Message + "Trace" + trace;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Fileuploads this instance.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Upload/fileupload")]
        public object fileupload()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string trace = "";
            try
            {
                trace += "1,";
                retdata = new Dictionary<string, object>();
                Serializer = new JavaScriptSerializer();
                Serializer.MaxJsonLength = Int32.MaxValue;
                trace += "2,";
                inputData = Serializer.Deserialize<Dictionary<string, object>>(HttpContext.Current.Request.Params["hdndata"].ToString());
                trace += "3,";
                //since the "action" is not included in App version 3.1.4 we handle to include the action here
                string value = inputData.ContainsKey("action") ? inputData["action"].ToString() : "binarydata";
                //if (inputData.ContainsKey("action") && inputData["action"].ToString() == "binarydata")
                if (value == "binarydata")
                {
                    trace += "4,";
                    retdata.Add("RETVAL", WriteBinaryData(inputData, inputData["binaryimgdata"].ToString()));
                    trace += "5,";
                }
                else if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    trace += "6,";
                    var httpPostedFile = HttpContext.Current.Request.Files["filebinarydata"];
                    trace += "7,";
                    if (httpPostedFile != null)
                    {
                        trace += "8,";
                        retdata.Add("RETVAL", writefile(inputData, httpPostedFile.InputStream));
                        trace += "9,";
                    }
                }
            }
            catch (Exception ex)
            {
                obj1.status = false;
                obj1.message = ex.Message + "Trace" + trace;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return retdata;
        }

        /// <summary>
        /// Writes the binary data.
        /// </summary>
        /// <param name="inputData">The input data.</param>
        /// <param name="filebinarydata">The filebinarydata.</param>
        /// <returns></returns>
        /// <exception cref="Exception">Exception in WriteBinaryData, " + ex.StackTrace + ", Messaage: " + ex.Message.ToString() + ", Trace" + trace</exception>
        public object WriteBinaryData(Dictionary<string, object> inputData, string filebinarydata)
        {
            object Result = null;
            System.IO.FileStream fs = null;
            byte[] bytes;
            string trace = "";
            var res = new RETURNVAL
            {
                SUCCESS = "",
                ERROR = trace
            };
            try
            {
                trace += "1,";
                bytes = Convert.FromBase64String(filebinarydata);
                trace += "2,";

                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "upload").Value;

                string destinationpath = dctpartdata["UPLOADPATH"].ToString();
                //since the companyid is appended on the filepath no need to include here
                //destinationpath += "\\" + inputData["companyid"].ToString().Trim();
                destinationpath += "\\" + inputData["filepath"].ToString().Trim();

                if (Directory.Exists(destinationpath) == true)
                {
                    trace += "3,";
                    fs = new System.IO.FileStream(destinationpath + "\\" + inputData["filename"].ToString(), System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write, FileShare.Read);
                    trace += "3a,";
                }
                else
                {
                    trace += "4,";
                    Directory.CreateDirectory(destinationpath);
                    fs = new System.IO.FileStream(destinationpath + "\\" + inputData["filename"].ToString(), System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.Write, FileShare.Read);
                    trace += "4a,";

                }
                trace += "5,";

                fs.Write(bytes, 0, bytes.Length);
                trace += "5a,";

                fs.Close();
                fs.Dispose();

                res.SUCCESS = "1";
                trace += "11,";
            }
            catch (Exception ex)
            {
                res.SUCCESS = "-1";
                res.ERROR = ex.Message;

                throw new Exception("Exception in WriteBinaryData, " + ex.StackTrace + ", Messaage: " + ex.Message.ToString() + ", Trace" + trace, ex);
            }
            return res;
        }

        /// <summary>
        /// Writefiles the specified input data.
        /// </summary>
        /// <param name="inputData">The input data.</param>
        /// <param name="myStream">My stream.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public object writefile(Dictionary<string, object> inputData, System.IO.Stream myStream)
        {
            object Result = null;
            System.IO.FileStream fs = null;
            byte[] bytes;

            try
            {
                bytes = new byte[myStream.Length];
                var httpPostedFile = HttpContext.Current.Request.Files[0];

                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "upload").Value;

                string destinationpath = dctpartdata["UPLOADPATH"].ToString();
                destinationpath += "\\" + inputData["companyid"].ToString().Trim();
                destinationpath += "\\" + inputData["filepath"].ToString().Trim();

                if (Directory.Exists(destinationpath) == true)
                {
                    httpPostedFile.SaveAs(destinationpath + "\\" + inputData["filename"].ToString());
                }
                else
                {
                    Directory.CreateDirectory(destinationpath);
                    httpPostedFile.SaveAs(destinationpath + "\\" + inputData["filename"].ToString());
                }

                var res = new RETURNVAL
                {
                    SUCCESS = "1"
                };
                Result = res;
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message.ToString() + "Error in writefile()");
            }
            return Result;
        }

        /// <summary>
        /// download the specified input data.
        /// </summary>
        [HttpGet]
        [Route("api/Upload/download")]
        public dynamic download(string compId, string siteId, string userId, string foldName)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string trace = "";
            try
            {
                trace += "A1,";
                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "upload").Value;
                trace += "A2,";
                string folderName = dctpartdata["UPLOADPATH"].ToString();
                folderName = folderName + compId + "/" + siteId + "/" + userId + "/" + foldName + "/";
                string[] filenames = Directory.GetFiles(folderName);
                DateTime current = DateTime.Now;
                string zipfName = "Request" + current.Date.Day.ToString() + current.Date.Month.ToString() + current.Date.Year.ToString() + current.TimeOfDay.Duration().Hours.ToString() + current.TimeOfDay.Duration().Minutes.ToString() + current.TimeOfDay.Duration().Seconds.ToString();
                HttpResponseMessage responseSet = Request.CreateResponse(HttpStatusCode.OK);
                using (ZipFile zip = new ZipFile())
                {
                    trace += "A3,";
                    zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                    zip.AddDirectory(folderName);

                    string zipName = String.Format("Download.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        zip.Save(memoryStream);
                        responseSet.Content = new ByteArrayContent(memoryStream.ToArray());
                        responseSet.Content.Headers.ContentLength = memoryStream.ToArray().LongLength;
                        responseSet.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                        responseSet.Content.Headers.ContentDisposition.FileName = zipName;
                        responseSet.Content.Headers.ContentType = new MediaTypeHeaderValue("application/zip");
                        trace += "A4,";
                        return responseSet;
                    }

                }
            }
            catch (Exception ex)
            {
                obj1.status = false;
                obj1.message = ex.Message + "Trace" + trace;
                return response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
        }


        /// <summary>
        /// Model RETURNVAL
        /// </summary>
        public class RETURNVAL
        {
            public string SUCCESS { get; set; }
            public string ERROR { get; set; }
        }

        #endregion
    }
}
