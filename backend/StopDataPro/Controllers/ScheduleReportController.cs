﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// ScheduleReport Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ScheduleReportController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ScheduleReportController"/> class.
        /// </summary>
        public ScheduleReportController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ScheduleReportController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method
        /// <summary>
        /// Gets the schedule list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/getScheduleList")]
        public HttpResponseMessage getScheduleList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesSchedule pv = new ParameterValuesSchedule();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsEmailList = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    if (String.IsNullOrEmpty(reportId))
                    {
                        reportId = "0";
                    }
                    strquery = ScriptEngine.GetValue("GETREPORTSCHEDULELIST");
                    string[] paramrptinfo = { "ROLEID ", "REPORTID" };
                    object[] pValuesrptinfo = { DEFAULTROLEID, reportId };
                    dsEmailList = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsEmailList.Tables[0].Rows.Count > 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.scheduleData = ScriptEngine.GetTableRows(dsEmailList.Tables[0]);
                        pv.reportData = ScriptEngine.GetTableRows(dsEmailList.Tables[1]);
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get ScheduleList:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, pv);
            }
            return response;
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/changeStatus")]
        public HttpResponseMessage changeStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    strquery = ScriptEngine.GetValue("CHANGESTATUSREPORTSCHEDULE");
                    string[] paramrptinfo = { "SCHEDULEID", "USERID" };
                    object[] pValuesrptinfo = { scheduleId, userId };
                    int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                    if (iresult > 0)
                    {
                        obj1.status = true;
                        obj1.message = "ALTCHANGESTATUSSUCCESS";
                        String strErrParam = "";
                        try
                        {
                            Log.InfoFormat("==ScheduleStatus Change Mail Process Start==");
                            //DataPro configuration setting 
                            Dictionary<string, object> dctpartdata1 = ScriptEngine.scriptengine_data();
                            string strAPPID = dctpartdata1["APPID"].ToString();
                            Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_dataSchedular();
                            string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                            string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                            string RSCH_WSURL = dctpartdata["WSURL"].ToString();
                            int strMode = 2;//update
                            strquery = ScriptEngine.GetValue("GETSCHEDULESTATUS");
                            string[] paramrptinfo1 = { "SCHEDULEID" };
                            object[] pValuesrptinfo1 = { scheduleId };
                            DataSet dsReportInfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);
                            int schcount = dsReportInfo.Tables[0].Rows.Count;
                            if (schcount > 0)
                            {
                                for (int i = 0; i <= schcount - 1; i++)
                                {
                                    String strJSON = "{\"MODE\":" + strMode
                                    + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                                    + ",\"COMPANYID\":\"" + compId
                                    + "\",\"TOKENID\":" + dsReportInfo.Tables[0].Rows[i]["SCHEDULEID"].ToString()
                                    + ",\"ACTIVE\":" + dsReportInfo.Tables[0].Rows[i]["STATUS"].ToString() + "}]}";
                                    object[] objSendScheduleDetails = new object[1];
                                    objSendScheduleDetails[0] = strJSON;
                                    CallWebServices CWS = new CallWebServices();
                                    strErrParam = strJSON + "||" + RSCH_WSURL + "||" + RSCH_WSNAME + "||" + RSCH_WSMETHOD;
                                    object mailstatus = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                                    Log.InfoFormat("WebService Return Value:" + mailstatus.ToString());
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Log.InfoFormat("Exception Occured When Schedule Change Status service:" + ex.StackTrace);
                            strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                            string[] paramrptinfoex = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                            object[] pValuesrptinfoex = { compId, userId, "SCH_REPORT", 1, "INSERTERRORLOG", strErrParam, ex.Message };
                            int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfoex, pValuesrptinfoex);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When change status of Schedule:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/delete")]
        public HttpResponseMessage delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    strquery = ScriptEngine.GetValue("DELETEREPORTSCHEDULE");
                    string[] paramrptinfo = { "SCHEDULEID", "USERID" };
                    object[] pValuesrptinfo = { scheduleId, userId };
                    int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                    if (iresult > 0)
                    {
                        obj1.status = true;
                        obj1.message = "ALTDELSUCCESS";
                        try
                        {
                            Log.InfoFormat("==ScheduleDelete Mail Process Start==");
                            //DataPro configuration setting 
                            Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_dataSchedular();
                            string strAPPID = dctpartdata["APPID"].ToString();
                            string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                            string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                            string RSCH_WSURL = dctpartdata["WSURL"].ToString();
                            int strMode = 3;//delete
                            String strJSON = "{\"MODE\":" + strMode
                                + ", \"APPID\":" + strAPPID
                                + ", \"COMPANYID\":\"" + companyId
                                + "\", \"TOKENID\":[" + scheduleId + "]}";

                            Log.InfoFormat("JSON String:" + strJSON);
                            object[] objSendScheduleDetails = new object[1];
                            objSendScheduleDetails[0] = strJSON;
                            CallWebServices CWS = new CallWebServices();
                            object mailstatus = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                            Log.InfoFormat("WebService Return Value:" + mailstatus.ToString());

                        }
                        catch (Exception ex)
                        {
                            Log.InfoFormat("Exception Occured When Schedule Delete service:" + ex.StackTrace);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Schedule:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the data for add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/getDataForAdd")]
        public HttpResponseMessage getDataForAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesScheduleAdd pv = new ParameterValuesScheduleAdd();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsReport = new DataSet();
                DataSet dsTo = new DataSet();
                DataSet dsBody = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETREPORTSANDTIMEZONE");
                    string[] paramrptinfo = { "ROLEID" };
                    object[] pValuesrptinfo = { DEFAULTROLEID };
                    dsReport = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsReport.Tables[0].Rows.Count > 0)
                    {
                        strquery = ScriptEngine.GetValue("GETUSEREMAILID");
                        string[] paramrptinfo1 = { "SCHEDULEID", "USERID", "LANGUAGEID", "REPORTID" };
                        object[] pValuesrptinfo1 = { "", USERID, LANGUAGEID, "" };
                        dsTo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);

                        string[] paramrptinfo2 = { "SCHEDULETYPEID" };
                        object[] pValuesrptinfo2 = { "9" };
                        strquery = ScriptEngine.GetValue("GETSCHEDULEMAILKEYWORD");
                        dsBody = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo2, pValuesrptinfo2);

                        string[] KeyWordstemp = new string[] { };
                        StringBuilder KeyWords = new StringBuilder();
                        StringBuilder KeyWordsSpanId = new StringBuilder();
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("label", typeof(String));
                        dtTokenValNew.Columns.Add("keyWordId", typeof(String));
                        string keyword = dsBody.Tables[0].Rows[0]["KEYWORD"].ToString();
                        var keyCommaSplit = keyword.Split(',');
                        for (int i = 0; i < keyCommaSplit.Length; i++)
                        {
                            DataRow dr = dtTokenValNew.NewRow();
                            KeyWordstemp = keyCommaSplit[i].Split('~');
                            dr[0] = KeyWordstemp[0];
                            dr[1] = KeyWordstemp[4];
                            dtTokenValNew.Rows.Add(dr);
                        }
                        pv.status = true;
                        pv.message = "Success";
                        pv.report = ScriptEngine.GetTableRows(dsReport.Tables[0]);
                        pv.timeZone = ScriptEngine.GetTableRows(dsReport.Tables[1]);
                        pv.toUser = ScriptEngine.GetTableRows(dsTo.Tables[0]);
                        pv.bodyKeyword = ScriptEngine.GetTableRows(dtTokenValNew);
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Schedule data on Add click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the report data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/getReportData")]
        public HttpResponseMessage getReportData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesScheduleReport pv = new ParameterValuesScheduleReport();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsReport = new DataSet();
                DataSet dsReportFilter = new DataSet();
                string reportId = Convert.ToString(request.SelectToken("reportId"));
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string langId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string filSiteId = Convert.ToString(request.SelectToken("filSiteId"));
                    string filAreaId = Convert.ToString(request.SelectToken("filAreaId"));
                    string filSubAreaId = Convert.ToString(request.SelectToken("filSubAreaId"));
                    string filShiftId = Convert.ToString(request.SelectToken("filShiftId"));
                    string filSetupId = Convert.ToString(request.SelectToken("filSetupId"));
                    strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTTYPE");
                    string[] paramrptinfo = { "REPORTID", "USERID" };
                    object[] pValuesrptinfo = { reportId, userId };
                    dsReport = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTOPTIONS_FILTER");
                    string[] paramrptinfo1 = { "SCHEDULEID", "REPORTID", "USERID", "LANGUAGEID" };
                    object[] pValuesrptinfo1 = { "0", reportId, userId, langId };
                    dsReportFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);
                    int count = dsReportFilter.Tables[0].Rows.Count;
                    DataTable dtTokenValNew = new DataTable();
                    dtTokenValNew.Columns.Add("Name", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                    dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                    dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                    dtTokenValNew.Columns.Add("Data", typeof(String));
                    for (int i = 0; i < count; i++)
                    {
                        DataSet dsfilter = new DataSet();
                        string query = dsReportFilter.Tables[0].Rows[i]["QUERY"].ToString();
                        if (query != "DATE")
                            dsfilter = getValueTable(query, userId, langId, DEFAULTROLEID, filSiteId, filAreaId, filSetupId, "", "", "");
                        DataRow dr = dtTokenValNew.NewRow();
                        dr[0] = dsReportFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                        dr[1] = dsReportFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                        dr[2] = dsReportFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                        dr[3] = dsReportFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                        if (query != "DATE")
                            dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                        else
                            dr[4] = "";
                        dtTokenValNew.Rows.Add(dr);
                    }
                    DataTable dtTokenValoptional = new DataTable();
                    DataTable dtTokenValgroupby = new DataTable();
                    DataTable dtValCatg = new DataTable();
                    DataTable dtValGropup = new DataTable();
                    DataTable dtTokenValfilterr1 = new DataTable();
                    var dtTokenValfilterr = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                    DataTable charttype = new DataTable();
                    if (reportId == "17")
                    {
                        try
                        {
                            charttype = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                    }
                    else
                    {
                        try
                        {
                            dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                    }
                    try
                    {
                        dtTokenValgroupby = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "6" + "").CopyToDataTable();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        dtValCatg = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "3" + "").CopyToDataTable();
                    }
                    catch (Exception ex)
                    { }
                    try
                    {
                        dtValGropup = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable();
                    }
                    catch (Exception ex)
                    { }
                    if (dsReport.Tables[0].Rows.Count > 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.reportVal = ScriptEngine.GetTableRows(dsReport.Tables[0])[0];
                        pv.reportDesign = ScriptEngine.GetTableRows(dsReport.Tables[1]);
                        pv.reportSaveFilter = ScriptEngine.GetTableRows(dsReport.Tables[2]);
                        pv.filter = ScriptEngine.GetTableRows(dtTokenValfilterr);
                        pv.optionalFilter = ScriptEngine.GetTableRows(dtTokenValoptional);
                        pv.filterChartType = ScriptEngine.GetTableRows(charttype);
                        pv.filterGroupBy = ScriptEngine.GetTableRows(dtTokenValgroupby);
                        pv.filterGroup = ScriptEngine.GetTableRows(dtValGropup);
                        pv.filterCategories = ScriptEngine.GetTableRows(dtValCatg);
                        pv.filterLabel = ScriptEngine.GetTableRows(dsReportFilter.Tables[1]);
                        pv.filterSetting = new string[] { };
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Report Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the saved filter data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/getSavedFilterData")]
        public HttpResponseMessage getSavedFilterData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesScheduleFilter pv = new ParameterValuesScheduleFilter();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsReport = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string filterId = Convert.ToString(request.SelectToken("filterId"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    if (String.IsNullOrEmpty(filterId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string filSubAreaId = Convert.ToString(request.SelectToken("filSubAreaId"));
                        string filShiftId = Convert.ToString(request.SelectToken("filShiftId"));
                        string filSiteId = Convert.ToString(request.SelectToken("filSiteId"));
                        string filAreaId = Convert.ToString(request.SelectToken("filAreaId"));
                        string filSetUpId = Convert.ToString(request.SelectToken("filSetUpId"));
                        string groupBy1 = Convert.ToString(request.SelectToken("groupBy1"));
                        string groupBy2 = Convert.ToString(request.SelectToken("groupBy2"));
                        string groupBy3 = Convert.ToString(request.SelectToken("groupBy3"));
                        string[] paramrptinfo = { "USERID", "LANGUAGEID", "REPORTID", "FILTERCRITERIAID" };
                        object[] pValuesrptinfo = { userId, languageId, reportId, filterId };
                        strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTOPTIONS_FILTERCRITERIA");
                        dsReport = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                        string SITEID = "SITEID";
                        string AREAID = "AREAID";
                        string SETUPID = "SETUPID";
                        string CHKSUMGROUP1 = "CHKSUMGROUP1";
                        string CHKSUMGROUP2 = "CHKSUMGROUP2";
                        string CHKSUMGROUP3 = "CHKSUMGROUP3";
                        #region for filter setting data
                        DataTable dtTokenValNewF = new DataTable();
                        string[] group1split = new string[] { };
                        string[] group11 = new string[] { };
                        string[] g = new string[] { };
                        StringBuilder sbb = new StringBuilder();
                        try
                        {
                            int row = 0;
                            for (int i = 0; i < dsReport.Tables[2].Rows.Count; i++)
                            {
                                int gg = 1;
                                group1split = dsReport.Tables[2].Rows[i]["RESULT"].ToString().Split(':');
                                group11 = Regex.Split(group1split[1], @"\*\~\*");
                                dtTokenValNewF.Columns.Add(group1split[0], typeof(String));
                                if (dtTokenValNewF.Rows.Count == 0)
                                {
                                    DataRow dr = dtTokenValNewF.NewRow();
                                    dtTokenValNewF.Rows.Add(dr);
                                }
                                foreach (string aa in group11)
                                {
                                    if (gg == group11.Length)
                                    {
                                        if (aa.Split('~')[0].ToString() == "DateRange")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else if (aa.Split('~')[0].ToString() == "LastMonth")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else
                                        {
                                            sbb.Append(aa.Split('~')[0].ToString());
                                        }
                                    }
                                    else
                                    {
                                        sbb.Append(aa.Split('~')[0].ToString() + ",");
                                    }
                                    gg++;
                                }
                                if (SITEID == group1split[0])
                                {
                                    filSiteId = sbb.ToString();
                                }
                                if (AREAID == group1split[0])
                                {
                                    filAreaId = sbb.ToString();
                                }
                                if (SETUPID == group1split[0])
                                {
                                    filSetUpId = sbb.ToString();
                                }
                                if (CHKSUMGROUP1 == group1split[0])
                                {
                                    groupBy1 = sbb.ToString();
                                }
                                if (CHKSUMGROUP2 == group1split[0])
                                {
                                    groupBy2 = sbb.ToString();
                                }
                                if (CHKSUMGROUP3 == group1split[0])
                                {
                                    groupBy3 = sbb.ToString();
                                }

                                dtTokenValNewF.Rows[0][group1split[0]] = sbb.ToString();
                                row++;
                                sbb.Clear();
                            }
                            pv.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF)[0];
                        }
                        catch (Exception ex)
                        {
                            pv.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF);
                        }
                        #endregion
                        int count = dsReport.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));
                        for (int i = 0; i < count; i++)
                        {
                            DataSet dsfilter = new DataSet();
                            string query = dsReport.Tables[0].Rows[i]["QUERY"].ToString();
                            if (query != "DATE")
                                dsfilter = getValueTable(query, userId, languageId, DEFAULTROLEID, filSiteId, filAreaId, filSetUpId, groupBy1, groupBy2, groupBy3);
                            DataRow dr = dtTokenValNew.NewRow();
                            dr[0] = dsReport.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsReport.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsReport.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsReport.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            if (query != "DATE")
                                dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            else
                                dr[4] = "";
                            dtTokenValNew.Rows.Add(dr);
                        }
                        DataTable dtTokenValoptional = new DataTable();
                        DataTable charttype = new DataTable();
                        DataTable dtValCatg = new DataTable();
                        DataTable dtValGropup = new DataTable();
                        var dtTokenValfilter = new DataTable();
                        try
                        {
                            dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }

                        var dtTokenValgroupby = new DataTable();
                        try
                        {
                            dtTokenValgroupby = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "6" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        if (reportId == "17")
                        {
                            try
                            {
                                charttype = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex)
                            { }
                        }
                        else
                        {
                            try
                            {
                                dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex)
                            { }
                        }

                        try
                        {
                            dtValCatg = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "3" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            dtValGropup = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        pv.status = true;
                        pv.message = "Success";
                        pv.filter = ScriptEngine.GetTableRows(dtTokenValfilter);
                        pv.optionalFilter = ScriptEngine.GetTableRows(dtTokenValoptional);
                        pv.filterGroupBy = ScriptEngine.GetTableRows(dtTokenValgroupby);
                        pv.filterChartType = ScriptEngine.GetTableRows(charttype);
                        pv.filterGroup = ScriptEngine.GetTableRows(dtValGropup);
                        pv.filterCategories = ScriptEngine.GetTableRows(dtValCatg);
                        pv.filterLabel = ScriptEngine.GetTableRows(dsReport.Tables[1]);
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Saved Filter Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the edit schedule data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/getEditScheduleData")]
        public HttpResponseMessage getEditScheduleData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesScheduleEdit pv = new ParameterValuesScheduleEdit();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsReport = new DataSet();
                DataSet dsReportInfo = new DataSet();
                DataSet dsReportType = new DataSet();
                DataSet dsReportFilter = new DataSet();
                DataSet dsTo = new DataSet();
                DataSet dsBody = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    if (String.IsNullOrEmpty(scheduleId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETREPORTSANDTIMEZONE");
                        string[] paramrptinfo = { "ROLEID", "USERID" };
                        object[] pValuesrptinfo = { DEFAULTROLEID, USERID };
                        dsReport = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                        strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTINFO");
                        string[] paramrptinfo1 = { "SCHEDULEID" };
                        object[] pValuesrptinfo1 = { scheduleId };
                        dsReportInfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo1, pValuesrptinfo1);

                        string reportId = dsReportInfo.Tables[0].Rows[0]["REPORTID"].ToString();
                        string[] paramrptinfo11 = { "SCHEDULEID", "REPORTID", "USERID", "LANGUAGEID" };
                        object[] pValuesrptinfo11 = { scheduleId, reportId, USERID, LANGUAGEID };
                        strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTTYPE");
                        dsReportType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo11, pValuesrptinfo11);

                        strquery = ScriptEngine.GetValue("GETSCHEDULEREPORTOPTIONS_FILTER");
                        dsReportFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo11, pValuesrptinfo11);

                        string filSiteId = "";
                        string filAreaId = "";
                        string filSetUpId = "";
                        string groupBy1 = "";
                        string groupBy2 = "";
                        string groupBy3 = "";

                        #region for filter setting data
                        DataTable dtTokenValNewF = new DataTable();
                        string[] group1split = new string[] { };
                        string[] group11 = new string[] { };
                        string[] g = new string[] { };
                        StringBuilder sbb = new StringBuilder();
                        try
                        {
                            int row = 0;
                            for (int i = 0; i < dsReportFilter.Tables[2].Rows.Count; i++)
                            {
                                int gg = 1;
                                group1split = dsReportFilter.Tables[2].Rows[i]["RESULT"].ToString().Split(':');
                                group11 = Regex.Split(group1split[1], @"\*\~\*");
                                dtTokenValNewF.Columns.Add(group1split[0], typeof(String));
                                if (dtTokenValNewF.Rows.Count == 0)
                                {
                                    DataRow dr = dtTokenValNewF.NewRow();
                                    dtTokenValNewF.Rows.Add(dr);
                                }
                                foreach (string aa in group11)
                                {
                                    if (gg == group11.Length)
                                    {
                                        if (aa.Split('~')[0].ToString() == "DateRange")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else if (aa.Split('~')[0].ToString() == "LastMonth")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else
                                        {
                                            sbb.Append(aa.Split('~')[0].ToString());
                                        }
                                    }
                                    else
                                    {
                                        sbb.Append(aa.Split('~')[0].ToString() + ",");
                                    }
                                    gg++;
                                }
                                if ("SITEID" == group1split[0])
                                {
                                    filSiteId = sbb.ToString();
                                }
                                if ("AREAID" == group1split[0])
                                {
                                    filAreaId = sbb.ToString();
                                }
                                if ("SETUPID" == group1split[0])
                                {
                                    filSetUpId = sbb.ToString();
                                }
                                if ("CHKSUMGROUP1" == group1split[0])
                                {
                                    groupBy1 = sbb.ToString();
                                }
                                if ("CHKSUMGROUP2" == group1split[0])
                                {
                                    groupBy2 = sbb.ToString();
                                }
                                if ("CHKSUMGROUP3" == group1split[0])
                                {
                                    groupBy3 = sbb.ToString();
                                }
                                dtTokenValNewF.Rows[0][group1split[0]] = sbb.ToString();
                                row++;
                                sbb.Clear();
                            }
                            pv.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF)[0];
                        }
                        catch (Exception ex)
                        {
                            pv.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF);
                        }
                        #endregion

                        int count = dsReportFilter.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));
                        for (int i = 0; i < count; i++)
                        {
                            DataSet dsfilter = new DataSet();
                            string query = dsReportFilter.Tables[0].Rows[i]["QUERY"].ToString();
                            if (query != "DATE")
                                dsfilter = getValueTable(query, USERID, LANGUAGEID, DEFAULTROLEID, filSiteId, filAreaId, filSetUpId, groupBy1, groupBy2, groupBy3);
                            DataRow dr = dtTokenValNew.NewRow();
                            dr[0] = dsReportFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsReportFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsReportFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsReportFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            if (query != "DATE")
                                dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            else
                                dr[4] = "";
                            dtTokenValNew.Rows.Add(dr);
                        }

                        var dtTokenValgroupby = new DataTable();
                        DataTable dtTokenValoptional = new DataTable();
                        DataTable charttype = new DataTable();
                        DataTable dtValCatg = new DataTable();
                        DataTable dtValGropup = new DataTable();
                        var dtTokenValfilterr = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                        try
                        {
                            dtTokenValgroupby = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "6" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        if (reportId == "17")
                        {
                            try
                            {
                                charttype = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex)
                            { }
                        }
                        else
                        {
                            try
                            {
                                dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex)
                            { }
                        }
                        try
                        {
                            dtValCatg = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "3" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        try
                        {
                            dtValGropup = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable();
                        }
                        catch (Exception ex)
                        { }
                        strquery = ScriptEngine.GetValue("GETUSEREMAILID");
                        dsTo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo11, pValuesrptinfo11);
                        string[] paramrptinfo2 = { "SCHEDULETYPEID" };
                        object[] pValuesrptinfo2 = { "9" };
                        strquery = ScriptEngine.GetValue("GETSCHEDULEMAILKEYWORD");
                        dsBody = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo2, pValuesrptinfo2);
                        string[] KeyWordstemp = new string[] { };
                        StringBuilder KeyWords = new StringBuilder();
                        StringBuilder KeyWordsSpanId = new StringBuilder();
                        DataTable dtTokenValNew1 = new DataTable();
                        dtTokenValNew1.Columns.Add("label", typeof(String));
                        dtTokenValNew1.Columns.Add("keyWordId", typeof(String));
                        string keyword = dsBody.Tables[0].Rows[0]["KEYWORD"].ToString();
                        var keyCommaSplit = keyword.Split(',');
                        for (int i = 0; i < keyCommaSplit.Length; i++)
                        {
                            DataRow dr = dtTokenValNew1.NewRow();
                            KeyWordstemp = keyCommaSplit[i].Split('~');
                            dr[0] = KeyWordstemp[0];
                            dr[1] = KeyWordstemp[4];
                            dtTokenValNew1.Rows.Add(dr);
                        }
                        pv.status = true;
                        pv.message = "Success";
                        pv.report = ScriptEngine.GetTableRows(dsReport.Tables[0]);
                        pv.timeZone = ScriptEngine.GetTableRows(dsReport.Tables[1]);
                        pv.generalData = ScriptEngine.GetTableRows(dsReportInfo.Tables[0])[0];
                        pv.reportVal = ScriptEngine.GetTableRows(dsReportType.Tables[0])[0];
                        pv.reportDesign = ScriptEngine.GetTableRows(dsReportType.Tables[1]);
                        pv.reportSaveFilter = ScriptEngine.GetTableRows(dsReportType.Tables[2]);
                        pv.filter = ScriptEngine.GetTableRows(dtTokenValfilterr);
                        pv.optionalFilter = ScriptEngine.GetTableRows(dtTokenValoptional);
                        pv.filterGroupBy = ScriptEngine.GetTableRows(dtTokenValgroupby);
                        pv.filterChartType = ScriptEngine.GetTableRows(charttype);
                        pv.filterGroup = ScriptEngine.GetTableRows(dtValGropup);
                        pv.filterCategories = ScriptEngine.GetTableRows(dtValCatg);
                        pv.filterLabel = ScriptEngine.GetTableRows(dsReportFilter.Tables[1]);
                        pv.toUser = ScriptEngine.GetTableRows(dsTo.Tables[0]);
                        pv.bodyKeyword = ScriptEngine.GetTableRows(dtTokenValNew1);
                        response = Request.CreateResponse(HttpStatusCode.OK, pv);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Schedular Data for Edit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the schedule report.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/insertScheduleReport")]
        public HttpResponseMessage insertScheduleReport(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string dateFormat = dtTokenVal.Rows[0]["DATEFORMAT"].ToString();
                    //string SCHEDULENAME = Convert.ToString(request.SelectToken("scheduleName"));
                    string SCHEDULENAME = "saw212aa";
                    string EMAILTO = Convert.ToString(request.SelectToken("emailTo"));
                    string CC = Convert.ToString(request.SelectToken("schedulecc"));
                    string SUBJECT = Convert.ToString(request.SelectToken("scheduleSubject"));
                    string BODY = Convert.ToString(request.SelectToken("scheduleBody"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string DESIGNID = Convert.ToString(request.SelectToken("designId"));
                    string EXPORTTYPE = Convert.ToString(request.SelectToken("exportType"));
                    string EXPORTFORMAT = Convert.ToString(request.SelectToken("exportFormat"));
                    string STATUS = Convert.ToString(request.SelectToken("status"));
                    string DWM = Convert.ToString(request.SelectToken("dwm"));
                    string EVERYDWM = Convert.ToString(request.SelectToken("everyDwm"));
                    string monthlyDwm = Convert.ToString(request.SelectToken("monthlyDwm"));
                    string DAY = Convert.ToString(request.SelectToken("scheduleDay"));
                    string STARTDATE = Convert.ToString(request.SelectToken("startDate"));
                    string ENDDATE = Convert.ToString(request.SelectToken("endDate"));
                    string STARTTIME = Convert.ToString(request.SelectToken("startTime"));
                    string TIMEZONE = Convert.ToString(request.SelectToken("timeZoneId"));
                    string DAYLIST = Convert.ToString(request.SelectToken("dayList"));
                    string RELATIVEFOR = Convert.ToString(request.SelectToken("relativeFor"));
                    string RELATIVEOPTION = Convert.ToString(request.SelectToken("relativeOption"));
                    string RELATIVEOPTIONTYPE = Convert.ToString(request.SelectToken("relativeOptionType"));
                    string PREVIOUSVALUE = Convert.ToString(request.SelectToken("previousValue"));
                    string TILLDATE = Convert.ToString(request.SelectToken("tillDate"));
                    string MONTH = "";
                    string OPTIONVALUE = Convert.ToString(request.SelectToken("options"));
                    if (String.IsNullOrEmpty(SCHEDULENAME) || String.IsNullOrEmpty(reportId) || String.IsNullOrEmpty(EMAILTO) || String.IsNullOrEmpty(SUBJECT) || String.IsNullOrEmpty(BODY) || String.IsNullOrEmpty(DWM) || String.IsNullOrEmpty(STARTDATE) || String.IsNullOrEmpty(ENDDATE))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHKSCHEDULENAMEEXISTS");
                        string[] paraminsert = { "SCHEDULEID", "SCHEDULENAME", };
                        object[] pValuesinsert = { "0", SCHEDULENAME };
                        int existChk = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (existChk == -1)
                        {
                            obj1.message = "ALTSCHEDULEREPORTNAMEEXIST";
                            obj1.status = false;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            DESIGNID = getDesignId(int.Parse(reportId), DESIGNID);
                            if (DWM == "1")
                            {
                                MONTH = "0";
                                DAY = "0";
                                DAYLIST = "0";
                            }
                            else if (DWM == "2")
                            {
                                MONTH = "0";
                                EVERYDWM = "0";
                                double day1 = Math.Pow(2, int.Parse(DAY));
                                DAY = day1.ToString();
                            }
                            else if (DWM == "3")
                            {
                                EVERYDWM = monthlyDwm;
                                MONTH = "4095";
                                double day1 = Math.Pow(2, int.Parse(DAY));
                                DAY = day1.ToString();
                            }
                            StringBuilder sb = new StringBuilder();
                            try
                            {
                                DataTable dts = JsonConvert.DeserializeObject<DataTable>(OPTIONVALUE);
                                for (int i = 0; i < dts.Columns.Count; i++)
                                {
                                    string columnaName = dts.Columns[i].ToString();
                                    string columnid = dts.Rows[0][columnaName].ToString();
                                    string[] idsplit = columnid.Split(',');
                                    sb.Append(columnaName + "=" + columnid + ";");
                                    if (reportId == "17")
                                    {
                                        DESIGNID = getTradeLineDesignId(columnaName, columnid);
                                    }
                                }
                            }
                            catch (Exception ex)
                            { }
                            string[] paraminsert1 = { "COMPANYID", "SCHEDULENAME", "EMAILTO", "CC", "SUBJECT", "BODY", "REPORTID", "DESIGNID", "EXPORTTYPE", "EXPORTFORMAT", "STATUS", "CREATEDBY", "OPTIONVALUE", "DWM", "EVERYDWM", "DAY", "MONTH", "STARTDATE", "ENDDATE", "TIMEZONE", "STARTTIME", "DAYLIST", "RELATIVEFOR", "RELATIVEOPTION", "RELATIVEOPTIONTYPE", "PREVIOUSVALUE", "TILLDATE" };
                            object[] pValuesinsert1 = { COMPANYID, SCHEDULENAME, EMAILTO, CC, SUBJECT, BODY, reportId, DESIGNID, EXPORTTYPE, EXPORTFORMAT, STATUS, USERID, sb.ToString(), DWM, EVERYDWM, DAY, MONTH, STARTDATE, ENDDATE, TIMEZONE, STARTTIME, DAYLIST, RELATIVEFOR, RELATIVEOPTION, RELATIVEOPTIONTYPE, PREVIOUSVALUE, TILLDATE };
                            strquery = ScriptEngine.GetValue("INSERTSCHEDULEREPORT");
                            DataSet dsResult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            int result = 0;
                            DateTime dateTime = new DateTime();
                            try
                            {
                                result = int.Parse(dsResult.Tables[0].Rows[0]["Column1"].ToString());
                                string dtime = dsResult.Tables[0].Rows[0]["Column2"].ToString();
                                dateTime = Convert.ToDateTime(dtime);
                            }
                            catch (Exception ex)
                            {

                            }
                            if (result != 0 && result > 0)
                            {
                                DataSet dsReportInfo = new DataSet();
                                String strErrParam = "";
                                try
                                {
                                    int strMode = 1;
                                    //DataPro configuration setting 
                                    Dictionary<string, object> scriptengine_data;
                                    StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                                    string strsettings = srSettings.ReadToEnd();
                                    scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                                    srSettings.Close();
                                    Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                                    Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                                    dctfulldata = scriptengine_data;

                                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
                                    string strAPPID = dctpartdata["APPID"].ToString();

                                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
                                    string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                                    string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                    string RSCH_WSURL = dctpartdata["WSURL"].ToString();

                                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "dataproreportservice").Value;
                                    string DRS_WSNAME = dctpartdata["WSNAME"].ToString();
                                    string DRS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                    string DRS_WSURL = dctpartdata["WSURL"].ToString();
                                    string DRS_WSURL1 = dctpartdata["WSURL1"].ToString();


                                    strquery = ScriptEngine.GetValue("GETTIMEZONEDURATION");
                                    string[] paramrptinfo = { "TIMEZONEID" };
                                    object[] pValuesrptinfo = { TIMEZONE };
                                    dsReportInfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                                    string strAlternateWS = DRS_WSURL;
                                    strAlternateWS = strAlternateWS + "?schid=" + result + "&compid=" + COMPANYID;
                                    String strJSON = "{\"MODE\":" + strMode
                                   + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                                   + ",\"COMPANYID\":\"" + COMPANYID
                                   + "\",\"TOKENID\":" + result
                                   + ",\"STARTDATE\":\"" + STARTDATE
                                   + "\",\"ENDDATE\":\"" + ENDDATE
                                   + "\",\"DWM\":" + DWM
                                   + ",\"EVERYDWM\":" + EVERYDWM
                                   + ",\"DAY\":" + DAY
                                   + ",\"MONTH\":" + MONTH
                                   + ",\"ACTIVE\":" + STATUS
                                   + ",\"TOKENTOBESENTDATE\":\"" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                                   + "\",\"STARTTIME\":" + STARTTIME
                                   + ",\"TIMEZONE\":" + dsReportInfo.Tables[0].Rows[0]["DURATION"].ToString()
                                   + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                                   + "\",\"CALLBACKWS\":\"" + strAlternateWS
                                   + "\",\"CALLBACKWSMETHOD\":\"" + DRS_WSMETHOD + "\"}]}";

                                    Log.InfoFormat("1. Email Schedular WSMETHOD:" + RSCH_WSMETHOD);
                                    Log.InfoFormat("2. Email Schedular WSURL:" + RSCH_WSURL);
                                    Log.InfoFormat("3. Email Schedular WSNAME:" + RSCH_WSNAME);
                                    Log.InfoFormat("4. Json String:" + strJSON);
                                    object[] objSendScheduleDetails = new object[1];
                                    objSendScheduleDetails[0] = strJSON;
                                    CallWebServices CWS = new CallWebServices();
                                    strErrParam = strJSON + "||" + RSCH_WSURL + "||" + RSCH_WSNAME + "||" + RSCH_WSMETHOD;
                                    Log.InfoFormat("5. strErrParam:" + strErrParam);
                                    object mailstatus = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                                    Log.InfoFormat("6. mailstatus:" + mailstatus.ToString());
                                }
                                catch (Exception Ex1)
                                {
                                    Log.InfoFormat("5.1. Exception occured when Schedule report service Insert" + Ex1.StackTrace);
                                    strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                    string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                    object[] pValuesrptinfo = { COMPANYID, result, "SCH_REPORT", 1, "INSERTERRORLOG", strErrParam, Ex1.Message };
                                    int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                                    Log.InfoFormat("5.2. Execute Result of INSERTERRORLOG config file:" + EL);
                                }
                                obj1.message = "ALTSCHEDULEREPORTCREATED";
                                obj1.status = true;
                                obj1.id = result;
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Schedule Report:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the schedule report.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ScheduleReport/updateScheduleReport")]
        public HttpResponseMessage updateScheduleReport(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string dateFormat = dtTokenVal.Rows[0]["DATEFORMAT"].ToString();
                    string scheduleId = Convert.ToString(request.SelectToken("scheduleId"));
                    string SCHEDULENAME = Convert.ToString(request.SelectToken("scheduleName"));
                    string EMAILTO = Convert.ToString(request.SelectToken("emailTo"));
                    string CC = Convert.ToString(request.SelectToken("schedulecc"));
                    string SUBJECT = Convert.ToString(request.SelectToken("scheduleSubject"));
                    string BODY = Convert.ToString(request.SelectToken("scheduleBody"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string DESIGNID = Convert.ToString(request.SelectToken("designId"));
                    string EXPORTTYPE = Convert.ToString(request.SelectToken("exportType"));
                    string EXPORTFORMAT = Convert.ToString(request.SelectToken("exportFormat"));
                    string STATUS = Convert.ToString(request.SelectToken("status"));
                    string DWM = Convert.ToString(request.SelectToken("dwm"));
                    string EVERYDWM = Convert.ToString(request.SelectToken("everyDwm"));
                    string monthlyDwm = Convert.ToString(request.SelectToken("monthlyDwm"));
                    string DAY = Convert.ToString(request.SelectToken("scheduleDay"));
                    string STARTDATE = Convert.ToString(request.SelectToken("startDate"));
                    string ENDDATE = Convert.ToString(request.SelectToken("endDate"));
                    string STARTTIME = Convert.ToString(request.SelectToken("startTime"));
                    string TIMEZONE = Convert.ToString(request.SelectToken("timeZoneId"));
                    string DAYLIST = Convert.ToString(request.SelectToken("dayList"));
                    string RELATIVEFOR = Convert.ToString(request.SelectToken("relativeFor"));
                    string RELATIVEOPTION = Convert.ToString(request.SelectToken("relativeOption"));
                    string RELATIVEOPTIONTYPE = Convert.ToString(request.SelectToken("relativeOptionType"));
                    string PREVIOUSVALUE = Convert.ToString(request.SelectToken("previousValue"));
                    string TILLDATE = Convert.ToString(request.SelectToken("tillDate"));
                    string MONTH = "";
                    string OPTIONVALUE = Convert.ToString(request.SelectToken("options"));
                    if (String.IsNullOrEmpty(scheduleId) || String.IsNullOrEmpty(SCHEDULENAME) || String.IsNullOrEmpty(reportId) || String.IsNullOrEmpty(EMAILTO) || String.IsNullOrEmpty(SUBJECT) || String.IsNullOrEmpty(BODY) || String.IsNullOrEmpty(DWM) || String.IsNullOrEmpty(STARTDATE) || String.IsNullOrEmpty(ENDDATE))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    strquery = ScriptEngine.GetValue("CHKSCHEDULENAMEEXISTS");
                    string[] paraminsert = { "SCHEDULEID", "SCHEDULENAME", };
                    object[] pValuesinsert = { scheduleId, SCHEDULENAME };
                    int existChk = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                    if (existChk == -1)
                    {
                        obj1.message = "ALTSCHEDULEREPORTNAMEEXIST";
                        obj1.status = false;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DESIGNID = getDesignId(int.Parse(reportId), DESIGNID);
                        if (DWM == "1")
                        {
                            MONTH = "0";
                            DAY = "0";
                            DAYLIST = "0";
                        }
                        else if (DWM == "2")
                        {
                            MONTH = "0";
                            EVERYDWM = "0";
                            double day1 = Math.Pow(2, int.Parse(DAY));
                            DAY = day1.ToString();
                        }
                        else if (DWM == "3")
                        {
                            EVERYDWM = monthlyDwm;
                            MONTH = "4095";
                            double day1 = Math.Pow(2, int.Parse(DAY));
                            DAY = day1.ToString();
                        }
                        StringBuilder sb = new StringBuilder();
                        try
                        {
                            DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(OPTIONVALUE);
                            for (int i = 0; i < dtTokenVals.Columns.Count; i++)
                            {
                                string columnaName = dtTokenVals.Columns[i].ToString();
                                string columnid = dtTokenVals.Rows[0][columnaName].ToString();
                                string[] idsplit = columnid.Split(',');
                                sb.Append(columnaName + "=" + columnid + ";");
                                if (reportId == "17")
                                {
                                    DESIGNID = getTradeLineDesignId(columnaName, columnid);
                                }
                            }
                        }
                        catch (Exception ex)
                        { }
                        string[] paraminsert1 = { "SCHEDULEID", "SCHEDULENAME", "EMAILTO", "CC", "SUBJECT", "BODY", "REPORTID", "DESIGNID", "EXPORTTYPE", "EXPORTFORMAT", "STATUS", "UPDATEDBY", "OPTIONVALUE", "DWM", "EVERYDWM", "DAY", "MONTH", "STARTDATE", "ENDDATE", "TIMEZONE", "STARTTIME", "DAYLIST", "RELATIVEFOR", "RELATIVEOPTION", "RELATIVEOPTIONTYPE", "PREVIOUSVALUE", "TILLDATE" };
                        object[] pValuesinsert1 = { scheduleId, SCHEDULENAME, EMAILTO, CC, SUBJECT, BODY, reportId, DESIGNID, EXPORTTYPE, EXPORTFORMAT, STATUS, USERID, sb.ToString(), DWM, EVERYDWM, DAY, MONTH, STARTDATE, ENDDATE, TIMEZONE, STARTTIME, DAYLIST, RELATIVEFOR, RELATIVEOPTION, RELATIVEOPTIONTYPE, PREVIOUSVALUE, TILLDATE };
                        strquery = ScriptEngine.GetValue("UPDATESCHEDULEREPORT");
                        DataSet dsResult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        int result = 0;
                        Log.InfoFormat("STARTDATE:" + STARTDATE);
                        Log.InfoFormat("ENDDATE:" + ENDDATE);
                        DateTime dateTime = new DateTime();
                        try
                        {
                            result = int.Parse(dsResult.Tables[0].Rows[0]["Column1"].ToString());
                            Log.InfoFormat("result:" + result);
                            string dtime = dsResult.Tables[0].Rows[0]["Column2"].ToString();
                            dateTime = Convert.ToDateTime(dtime);
                            Log.InfoFormat("dateTime:" + dateTime);

                        }
                        catch (Exception ex)
                        { }
                        if (result != 0 && result > 0)
                        {
                            obj1.message = "ALTSCHEDULEREPORTUPDATED";
                            obj1.status = true;
                            obj1.id = result;

                            DataSet dsReportInfo = new DataSet();
                            String strErrParam = "";
                            try
                            {
                                int strMode = 2;
                                Log.InfoFormat("Update Email Schedular start");
                                //DataPro configuration setting 
                                Dictionary<string, object> dctpartdata1 = ScriptEngine.scriptengine_data();
                                string strAPPID = dctpartdata1["APPID"].ToString();
                                Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_dataSchedular();
                                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                string RSCH_WSURL = dctpartdata["WSURL"].ToString();

                                Dictionary<string, object> dctpartdataD = ScriptEngine.scriptengine_dataSchedularReportService();
                                string DRS_WSNAME = dctpartdataD["WSNAME"].ToString();
                                string DRS_WSMETHOD = dctpartdataD["WSMETHOD"].ToString();
                                string DRS_WSURL = dctpartdataD["WSURL"].ToString();
                                string DRS_WSURL1 = dctpartdataD["WSURL1"].ToString();
                                strquery = ScriptEngine.GetValue("GETTIMEZONEDURATION");
                                string[] paramrptinfo = { "TIMEZONEID" };
                                object[] pValuesrptinfo = { TIMEZONE };
                                dsReportInfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                                //this used to create a schedule with alternate URL (for load balance)
                                string strAlternateWS = DRS_WSURL;
                                strAlternateWS = strAlternateWS + "?schid=" + result + "&compid=" + COMPANYID;
                                String strJSON = "{\"MODE\":" + strMode
                           + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                           + ",\"COMPANYID\":\"" + COMPANYID
                           + "\",\"TOKENID\":" + result
                           + ",\"STARTDATE\":\"" + STARTDATE
                           + "\",\"ENDDATE\":\"" + ENDDATE
                           + "\",\"DWM\":" + DWM
                           + ",\"EVERYDWM\":" + EVERYDWM
                           + ",\"DAY\":" + DAY
                           + ",\"MONTH\":" + MONTH
                           + ",\"ACTIVE\":" + STATUS
                           + ",\"TOKENTOBESENTDATE\":\"" + dateTime.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                           + "\",\"STARTTIME\":" + STARTTIME
                           + ",\"TIMEZONE\":" + dsReportInfo.Tables[0].Rows[0]["DURATION"].ToString()
                           + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                           + "\",\"CALLBACKWS\":\"" + strAlternateWS
                           + "\",\"CALLBACKWSMETHOD\":\"" + DRS_WSMETHOD + "\"}]}";
                                Log.InfoFormat("1. Update Email Schedular WSMETHOD:" + RSCH_WSMETHOD);
                                Log.InfoFormat("2. Update Email Schedular WSURL:" + RSCH_WSURL);
                                Log.InfoFormat("3. Update Email Schedular WSNAME:" + RSCH_WSNAME);
                                Log.InfoFormat("4. Json String:" + strJSON);
                                object[] objSendScheduleDetails = new object[1];
                                objSendScheduleDetails[0] = strJSON;
                                CallWebServices CWS = new CallWebServices();
                                strErrParam = strJSON + "||" + RSCH_WSURL + "||" + RSCH_WSNAME + "||" + RSCH_WSMETHOD;
                                Log.InfoFormat("5. strErrParam:" + strErrParam);
                                object mailstatus = CWS.CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                                Log.InfoFormat("6. mailstatus:" + mailstatus.ToString());
                            }
                            catch (Exception Ex1)
                            {
                                Log.InfoFormat("5.1. Exception occured when Schedule report service Insert" + Ex1.StackTrace);
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { COMPANYID, result, "SCH_REPORT", 1, "INSERTERRORLOG", strErrParam, Ex1.Message };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                                Log.InfoFormat("5.2. Execute Result of INSERTERRORLOG config file:" + EL);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Schedule Report:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Servcies the call.
        /// </summary>
        /// <param name="schid">The schid.</param>
        /// <param name="compid">The compid.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ScheduleReport/serviceCall")]
        public string serviceCall(int schid, string compid)
        {
            Log.InfoFormat("reportSchedule" + schid + ":" + compid);
            dataproreportservice dp = new dataproreportservice();
            Log.InfoFormat("ServiceCall End:");
            return dp.ReportSchedulingUrl(schid, compid);
        }

        /// <summary>Commons the servcie call.</summary>
        /// <param name="schid">The schid.</param>
        /// <param name="compid">The compid.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ScheduleReport/commonServiceCall")]
        public string commonServiceCall(int schid, string compid)
        {
            Log.InfoFormat("commonschedule" + schid + ":" + compid);
            dataproreportservice dp = new dataproreportservice();
            Log.InfoFormat("ServiceCall End:");
            return dp.CommonSchedulingUrl(schid, compid);
        }

        /// <summary>Commons the servcie call.</summary>
        /// <param name="schid">The schid.</param>
        /// <param name="compid">The compid.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ScheduleReport/emailServiceCall")]
        public string emailServiceCall(int schid, string compid)
        {
            Log.InfoFormat("emailServcieCall" + schid + ":" + compid);
            dataproreportservice dp = new dataproreportservice();
            Log.InfoFormat("ServiceCall End:");
            return dp.EmailSchedule(schid, compid);
        }

        /// <summary>Commons the servcie call.</summary>
        /// <param name="schid">The schid.</param>
        /// <param name="compid">The compid.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("api/ScheduleReport/commonScheduleCall")]
        public string commonScheduleCall(int schid, string compid)
        {
            Log.InfoFormat("emailServcieCall" + schid + ":" + compid);
            dataproreportservice dp = new dataproreportservice();
            Log.InfoFormat("ServiceCall End:");
            return dp.CommonSchedulingUrl(schid, compid);
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="siteId">The site identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <param name="SETUPID">The setupid.</param>
        /// <param name="groupBy1">The group by1.</param>
        /// <param name="groupBy2">The group by2.</param>
        /// <param name="groupBy3">The group by3.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string roleId, string siteId, string areaId, string SETUPID, string groupBy1, string groupBy2, string groupBy3)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "ROLEID", "SITEID", "AREAID", "CORRACTIONID", "SETUPID", "CHKSUMGROUP1", "CHKSUMGROUP2", "CHKSUMGROUP3" };
            object[] pValuesinsert = { userid, languageId, roleId, siteId, areaId, "", SETUPID, groupBy1, groupBy2, groupBy3 };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        /// <summary>Gets the design identifier.</summary>
        /// <param name="reportId">The report identifier.</param>
        /// <param name="DESIGNID1">The designi d1.</param>
        /// <returns></returns>
        private string getDesignId(int reportId, string DESIGNID1)
        {
            string DESIGNID = "";
            switch (reportId)
            {
                case 5:
                    DESIGNID = "8";
                    break;
                case 13:
                    DESIGNID = "21";
                    break;
                case 11:
                    DESIGNID = "19";
                    break;
                case 18:
                    DESIGNID = "39";
                    break;
                case 8:
                    DESIGNID = "12";
                    break;
                case 16:
                    DESIGNID = "33";
                    break;
                default:
                    DESIGNID = DESIGNID1;
                    break;
            }
            return DESIGNID;
        }

        /// <summary>Gets the trade line design identifier.</summary>
        /// <param name="columnaName">Name of the columna.</param>
        /// <param name="columnid">The columnid.</param>
        /// <returns></returns>
        private string getTradeLineDesignId(string columnaName, string columnid)
        {
            string DESIGNID = "";
            if (columnaName == "CHARTTYPE")
            {
                switch (int.Parse(columnid))
                {
                    case 1:
                        DESIGNID = "36";
                        break;
                    case 2:
                        DESIGNID = "37";
                        break;
                    case 3:
                        DESIGNID = "38";
                        break;
                    default:
                        DESIGNID = "36";
                        break;
                }
            }
            return DESIGNID;
        }
        #endregion

    }
}
