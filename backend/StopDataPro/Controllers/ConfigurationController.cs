﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// ConfigurationController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ConfigurationController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationController"/> class.
        /// </summary>
        public ConfigurationController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ConfigurationController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method 

        /// <summary>
        /// Gets the general data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/getGeneralData")]
        [APIAuthAttribute]
        public HttpResponseMessage getGeneralData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    strquery = ScriptEngine.GetValue("GETGLOBALOPTIONS");
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (ds.Tables[0].Rows.Count != 0)
                    {

                        ParameterValuesTarget obj = new ParameterValuesTarget();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.GeneralData = lstPersons[0];
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get General Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Updates the general data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/updateGeneralData")]
        [APIAuthAttribute]
        public HttpResponseMessage updateGeneralData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string txtautoLoginterval = Convert.ToString(request.SelectToken("autoLogInterval"));
                    string cmbdefaultdateformat = Convert.ToString(request.SelectToken("defaultDateFormat"));
                    string UPLODEDIMAGE = Convert.ToString(request.SelectToken("uploadImage"));
                    string txtcustomcolor = Convert.ToString(request.SelectToken("customColor"));
                    string txtcustomfont = Convert.ToString(request.SelectToken("customFont"));
                    string cmbenableLogin = Convert.ToString(request.SelectToken("enableLogin"));
                    string cmbLoginattempt = Convert.ToString(request.SelectToken("loginAttempt"));
                    string txtusernamelength = Convert.ToString(request.SelectToken("usernameLength"));
                    string txtpwdlength = Convert.ToString(request.SelectToken("pwdLength"));
                    string cmbpwdcomplexity = Convert.ToString(request.SelectToken("pwdComplexity"));
                    string txtpwdminnumchar = Convert.ToString(request.SelectToken("pwdMinnumChar"));
                    string txtpwdminalphachar = Convert.ToString(request.SelectToken("pwdMinalphaChar"));
                    string txtpwdminspecchar = Convert.ToString(request.SelectToken("pwdMinSpecChar"));
                    string txtpwdexpperiod = Convert.ToString(request.SelectToken("pwdExpPeriod"));
                    bool pwdcomplex = false;
                    if (cmbpwdcomplexity == "2")
                    {
                        if (String.IsNullOrEmpty(txtautoLoginterval) || String.IsNullOrEmpty(txtcustomcolor) || String.IsNullOrEmpty(txtcustomfont) || String.IsNullOrEmpty(txtusernamelength) || String.IsNullOrEmpty(txtpwdlength) || String.IsNullOrEmpty(txtpwdexpperiod) || String.IsNullOrEmpty(txtpwdminnumchar) || String.IsNullOrEmpty(txtpwdminalphachar))
                        {
                            pwdcomplex = true;
                        }
                    }
                    if (cmbpwdcomplexity == "3")
                    {
                        if (String.IsNullOrEmpty(txtautoLoginterval) || String.IsNullOrEmpty(txtcustomcolor) || String.IsNullOrEmpty(txtcustomfont) || String.IsNullOrEmpty(txtusernamelength) || String.IsNullOrEmpty(txtpwdlength) || String.IsNullOrEmpty(txtpwdexpperiod) || String.IsNullOrEmpty(txtpwdminnumchar) || String.IsNullOrEmpty(txtpwdminalphachar) || String.IsNullOrEmpty(txtpwdminspecchar))
                        {
                            pwdcomplex = true;
                        }
                    }
                    if (String.IsNullOrEmpty(txtautoLoginterval) || String.IsNullOrEmpty(txtcustomcolor) || String.IsNullOrEmpty(txtcustomfont) || String.IsNullOrEmpty(txtusernamelength) || String.IsNullOrEmpty(txtpwdlength) || String.IsNullOrEmpty(txtpwdexpperiod))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else if (pwdcomplex == true)
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string DefaultCompanyLogo = "";
                        if (String.IsNullOrEmpty(UPLODEDIMAGE))
                        {
                            DefaultCompanyLogo = "1";
                        }
                        else
                        {
                            DefaultCompanyLogo = "2";
                        }
                        strquery = ScriptEngine.GetValue("UPDATEGLOBALOPTIONS");
                        string[] paraminsert = {"PCOMPANYID", "UPDATEDBY", "PAUTOLOGOUT", "PDEFAULTDATEFORMAT", "PCOMPANYLOGO", "CUSTOMCOLOR", "CUSTOMFONT", "PENABLELOGINATTEMPTS", "PLOGINATTEMPTS",
                        "PUSERNAMEMINLEN", "PPASSWORDMINLEN", "PPASSWORDCOMPLEX", "PASSWORDMINNUMCHAR", "PASSWORDMINALPHACHAR", "PASSWORDMINSPECCHAR" , "PPWDEXPIRATIONPERIOD" ,"DEFAULTCOMPANYLOGO"};
                        object[] pValuesinsert = {  COMPANYID,USERID, txtautoLoginterval, cmbdefaultdateformat, UPLODEDIMAGE, txtcustomcolor, txtcustomfont,
                        cmbenableLogin, cmbLoginattempt, txtusernamelength,
                        txtpwdlength, cmbpwdcomplexity, txtpwdminnumchar, txtpwdminalphachar, txtpwdminspecchar,txtpwdexpperiod ,DefaultCompanyLogo };

                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTGLOBOPTEDIT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTGLOBOPTEDITPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update General Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the application configuration data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/getAppConfigData")]
        [APIAuthAttribute]
        public HttpResponseMessage getAppConfigData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCONFIG");
                    string[] paraminsert = { "LANGUAGECODE" };
                    object[] pValuesinsert = { LANGUAGECODE };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        DataTable dslang = ds.Tables[1].Copy();
                        dslang.Merge(ds.Tables[0]);
                        if (ds.Tables[2].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[1].NewRow();
                            dr[0] = "";
                            ds.Tables[2].Rows.Add(dr);
                        }

                        DataTable dsdept = ds.Tables[4].Copy();
                        dsdept.Merge(ds.Tables[3]);
                        if (ds.Tables[5].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[5].NewRow();
                            dr[0] = "";
                            ds.Tables[5].Rows.Add(dr);
                        }

                        DataTable dsSch = ds.Tables[7].Copy();
                        dsSch.Merge(ds.Tables[6]);
                        if (ds.Tables[8].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[8].NewRow();
                            dr[0] = "";
                            ds.Tables[8].Rows.Add(dr);
                        }

                        DataTable dstime = ds.Tables[11].Copy();
                        dstime.Merge(ds.Tables[10]);

                        if (ds.Tables[11].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[12].NewRow();
                            dr[0] = "";
                            ds.Tables[12].Rows.Add(dr);
                        }
                        ParameterAppConfig obj = new ParameterAppConfig();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(dslang);
                        List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[2]);
                        List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(dsdept);
                        List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(ds.Tables[5]);
                        List<Dictionary<string, object>> lstPersons4 = ScriptEngine.GetTableRows(dsSch);
                        List<Dictionary<string, object>> lstPersons5 = ScriptEngine.GetTableRows(ds.Tables[8]);
                        List<Dictionary<string, object>> lstPersons6 = ScriptEngine.GetTableRows(ds.Tables[9]);
                        List<Dictionary<string, object>> lstPersons7 = ScriptEngine.GetTableRows(dstime);
                        List<Dictionary<string, object>> lstPersons8 = ScriptEngine.GetTableRows(ds.Tables[12]);
                        List<Dictionary<string, object>> mandatoryGroupType = ScriptEngine.GetTableRows(ds.Tables[13]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.language = lstPersons;
                        obj.assignLanguage = lstPersons1[0]["ASSIGNLANGUAGEID"];
                        obj.entity = lstPersons2;
                        obj.assignEntity = lstPersons3[0]["ASSIGNENTITYID"];
                        obj.scheduletype = lstPersons4;
                        obj.assignScheduletype = lstPersons5[0]["ASSIGNSCHEDULETYPEID"];
                        obj.GeneralData = lstPersons6[0];
                        obj.timezone = lstPersons7;
                        obj.assignTimezone = lstPersons8[0]["ASSIGNTIMEZONEID"];
                        obj.mandatoryGroupType = mandatoryGroupType;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Application Config Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        [HttpPost]
        [Route("api/Configuration/updateAppConfigData")]
        [APIAuthAttribute]
        public HttpResponseMessage updateAppConfigData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languagelist = Convert.ToString(request.SelectToken("languageList"));
                    string cmbdefaultlang = Convert.ToString(request.SelectToken("cmbdefaultLang"));
                    string grouptypelist = Convert.ToString(request.SelectToken("groupTypeList"));
                    string sheduletypelist = Convert.ToString(request.SelectToken("sheduleTypeList"));
                    string timezonelist = Convert.ToString(request.SelectToken("timeZoneList"));
                    string cmbdefaulttimezone = Convert.ToString(request.SelectToken("cmbDefaultTimeZone"));
                    string SELGROUPTYPEID = Convert.ToString(request.SelectToken("groupTypeId"));
                    if (String.IsNullOrEmpty(languagelist) || String.IsNullOrEmpty(cmbdefaultlang) || String.IsNullOrEmpty(timezonelist) || String.IsNullOrEmpty(cmbdefaulttimezone))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEGLOBALOPTIONCONFIG");
                        string[] paraminsert = { "UPDATEDBY", "LANGUAGEID", "DEFAULTLANGUAGEID", "GROUPTYPEID", "SCHEDULETYPEID", "TIMEZONEID", "DEFAULTTIMEZONEID" };
                        object[] pValuesinsert = {  USERID, languagelist, cmbdefaultlang, grouptypelist, sheduletypelist, timezonelist,
                        cmbdefaulttimezone };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("UPDATEGROUPTYPES");
                            string[] paraminsert1 = { "SELGROUPTYPEID", "UPDATEDBY" };
                            object[] pValuesinsert1 = { SELGROUPTYPEID, USERID };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            obj1.status = true;
                            obj1.message = "ALTGLOBOPTEDIT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTGLOBOPTEDITPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update Application Config Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the checklist configuration data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/getChecklistConfigData")]
        [APIAuthAttribute]
        public HttpResponseMessage getChecklistConfigData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                    string[] paraminsert = { "LANGUAGECODE" };
                    object[] pValuesinsert = { LANGUAGECODE };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        DataTable dsrole = ds.Tables[2].Copy();
                        dsrole.Merge(ds.Tables[1]);
                        if (ds.Tables[3].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[3].NewRow();
                            dr[0] = "";
                            ds.Tables[3].Rows.Add(dr);
                        }

                        DataTable dsfield = ds.Tables[5].Copy();
                        dsfield.Merge(ds.Tables[4]);
                        if (ds.Tables[6].Rows.Count == 0)
                        {
                            DataRow dr = ds.Tables[6].NewRow();
                            dr[0] = "";
                            ds.Tables[6].Rows.Add(dr);
                        }
                        ParameterCheckConfig obj = new ParameterCheckConfig();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(dsrole);
                        List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds.Tables[3]);
                        List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(dsfield);
                        List<Dictionary<string, object>> lstPersons4 = ScriptEngine.GetTableRows(ds.Tables[6]);
                        List<Dictionary<string, object>> lstPersons5 = ScriptEngine.GetTableRows(ds.Tables[7]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.GeneralData = lstPersons[0];
                        obj.role = lstPersons1;
                        obj.assignRole = lstPersons2[0]["ASSIGNROLEID"];
                        obj.entity = lstPersons3;
                        obj.assignEntity = lstPersons4[0]["ASSIGNENTITYID"];
                        obj.mandUserFeilds = lstPersons5;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Checklist Config Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the checklist configuration data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/updateChecklistConfigData")]
        [APIAuthAttribute]
        public HttpResponseMessage updateChecklistConfigData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cmbenablecomments = Convert.ToString(request.SelectToken("enableComments"));
                    string obsApprovepc = Convert.ToString(request.SelectToken("obsApprovePc"));
                    string obsApproveMobile = Convert.ToString(request.SelectToken("obsApproveMobile"));
                    string rolelist = Convert.ToString(request.SelectToken("roleList"));
                    string maxobserved = Convert.ToString(request.SelectToken("maxObserved"));
                    string prevdayslimit = Convert.ToString(request.SelectToken("checklistOlderLimit"));
                    string userdefinedlist = Convert.ToString(request.SelectToken("userDefinedList"));
                    string expandcategory = Convert.ToString(request.SelectToken("expandCategory"));
                    string obslistingxmonth = Convert.ToString(request.SelectToken("obsListingxMonth"));
                    string SELCUSTOMFIELDS = Convert.ToString(request.SelectToken("customField"));
                    if (obsApprovepc == "0" && obsApproveMobile == "0")
                    {
                        rolelist = "0";
                    }
                    if (String.IsNullOrEmpty(obsApprovepc) || String.IsNullOrEmpty(obsApproveMobile) || String.IsNullOrEmpty(rolelist) || String.IsNullOrEmpty(cmbenablecomments) || String.IsNullOrEmpty(expandcategory))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEGLOBALOPTIONCHECKLISTCONFIG");
                        string[] paraminsert = { "UPDATEDBY", "OBSAPPROVEPC", "OBSAPPROVEMOBILE", "ROLEID", "ENABLECHECKLISTCOMMENTS", "MAXPEOPLEOBSERVEDLIMIT",
                            "PREVIOUSDAYSLIMIT","ASSIGNEDCUSTOMFIELDLIST","EXPANDCATEGORY","OBSLISTINGXMONTH" };
                        object[] pValuesinsert = {  USERID, obsApprovepc, obsApproveMobile, rolelist, cmbenablecomments, maxobserved,
                        prevdayslimit, userdefinedlist, expandcategory, obslistingxmonth, };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        strquery = ScriptEngine.GetValue("UPDATEUSERDEFINEDMANDATORYTYPES");
                        string[] paraminsert1 = { "UPDATEDBY", "SELCUSTOMFIELDS" };
                        object[] pValuesinsert1 = { USERID, SELCUSTOMFIELDS };
                        int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTGLOBOPTEDIT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTGLOBOPTEDITPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Checklist Config Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/getTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage getTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslang = new DataSet();
                DataSet dsfield = new DataSet();
                DataSet dsLabel = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageCode = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    string labelType = Convert.ToString(request.SelectToken("labelType"));
                    strquery = ScriptEngine.GetValue("GETACTIVELANGUAGES");
                    dslang = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (String.IsNullOrEmpty(language))
                    {
                        language = languageCode;
                    }
                    if (String.IsNullOrEmpty(labelType))
                    {
                        labelType = "1";
                    }
                    strquery = ScriptEngine.GetValue("GETTRANSLATIONS");
                    string[] paraminsert = { "LANGUAGEID", "LABELTYPE", "LANGUAGECODE" };
                    object[] pValuesinsert = { language, labelType, language };
                    dsfield = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("DataProGETCUSTOMLABELBYID");
                    dsLabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (dsfield.Tables[0].Rows.Count != 0)
                    {
                        ParameterTranslationList obj = new ParameterTranslationList();
                        List<Dictionary<string, object>> lstLang = ScriptEngine.GetTableRows(dslang.Tables[0]);
                        List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(dsfield.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.language = lstLang;
                        obj.data = lstData;
                        obj.label = dsLabel.Tables[0];
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Translation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Configuration/updateTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage updateTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("transId"));  
                    string labelType = Convert.ToString(request.SelectToken("labelType"));
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    bool pipeLineValid = true;
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(labelType) || String.IsNullOrEmpty(language))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(fieldId);
                        string entity = "";
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dtTokenVals.Rows.Count; i++)
                        {
                            string id = dtTokenVals.Rows[i]["eid"].ToString();
                            string value = dtTokenVals.Rows[i]["eval"].ToString();
                            if (value.Contains("|"))
                            {
                                pipeLineValid = false;
                                break;
                            }
                            entity = id + "~" + value + "|";
                            sb.Append(entity);
                        }
                        if (pipeLineValid == true)
                        {
                            strquery = ScriptEngine.GetValue("UPDATETRANSLATION");
                            string[] paraminsert = { "UPDATEDBY", "LABELTYPE", "TRANSLATIONID", "LANGUAGEID" };
                            object[] pValuesinsert = { CREATEDBY, labelType, sb.ToString(), language };
                            int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            if (iresult != 0 && iresult > 0)
                            {
                                obj1.status = true;
                                obj1.message = "ALTTRANSLATIONUPDATE";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTVALIDATETRANSTEXT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Translation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
        
    }
}

