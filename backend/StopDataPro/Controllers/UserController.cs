﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// UserController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class UserController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserController"/> class.
        /// </summary>
        public UserController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(UserController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        int ischeduleid;
        string strmailfrom, replyto, strto, cc, bcc, strsubject, strbody, strcompanyurl;
        DateTime dtmailsentdate;

        #region Action Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        [Route("api/User/Insert")]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string userName = Convert.ToString(request.SelectToken("userName"));
                    string fitstName = Convert.ToString(request.SelectToken("firstName"));
                    string lastName = Convert.ToString(request.SelectToken("lastName"));
                    string emailId = Convert.ToString(request.SelectToken("emailId"));
                    string jobTitle = Convert.ToString(request.SelectToken("jobTitle"));
                    string userType = Convert.ToString(request.SelectToken("userType"));
                    string defaultrole = Convert.ToString(request.SelectToken("defaultRole"));
                    string userOffline = Convert.ToString(request.SelectToken("userOffline"));  //moblieapp
                    string userOfflineapprove = Convert.ToString(request.SelectToken("userOfflineApprove"));
                    string userPassword = Convert.ToString(request.SelectToken("userPassword"));
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string userDefaultsite = Convert.ToString(request.SelectToken("userDefaultSite"));
                    string languageid = Convert.ToString(request.SelectToken("languageId"));
                    string userDateFormat = Convert.ToString(request.SelectToken("userDateFormat"));
                    string userTimezone = Convert.ToString(request.SelectToken("userTimeZone"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string ddlGroup = Convert.ToString(request.SelectToken("groupIds"));
                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();
                    if (userType == "1")
                    {
                        if (String.IsNullOrEmpty(emailId))
                        {
                            emailId = "email@gmail.com";
                        }
                    }
                    if (String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(fitstName) || String.IsNullOrEmpty(lastName) || String.IsNullOrEmpty(emailId) || String.IsNullOrEmpty(userType) || String.IsNullOrEmpty(userDefaultsite))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (userType == "1")
                        {
                            if (emailId == "email@gmail.com")
                            {
                                emailId = "";
                            }
                        }
                        if (defaultrole == "0" || defaultrole == "")
                        {
                            defaultrole = "-1";
                        }
                        if (ddlGroup == "")
                        {
                            ddlGroup = "0";
                        }
                        strquery = ScriptEngine.GetValue("INSERTUSERS");
                        string[] paraminsert = { "COMPANYID", "USERNAME", "FIRSTNAME", "LASTNAME", "EMAIL", "USERTYPEID", "OFFLINEACCESS", "APPROVEOFFLINE", "PASSWORD", "STATUS", "JOBTITLE", "ROLEID", "DEFAULTROLEID", "SITEID", "DEFAULTSITEID", "CREATEDBY", "LANGUAGEID", "TIMEZONE", "DATEFORMAT", "USERSCOUNT", "GROUPID" };
                        object[] pValuesinsert = { COMPANYID, userName, fitstName, lastName, emailId, userType, userOffline, userOfflineapprove, userPassword, status, jobTitle, defaultrole, defaultrole, siteIds, userDefaultsite, userid, languageid, userTimezone, userDateFormat, 0, ddlGroup };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        //Log.InfoFormat("1.USER ID:" + iresult);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgId obj = new StatusMsgId();
                            obj.status = true;
                            obj.message = "ALTUSERCREATED";
                            obj.id = iresult;
                            #region MAIL SERVICE
                            try
                            {
                                string[] parampwdreset = { "USERID" };
                                object[] pValuespwdreset = { iresult };
                                strquery = ScriptEngine.GetValue("MAILNEWUSERCREATION");
                                DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parampwdreset, pValuespwdreset);
                                if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                {
                                    for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                    {
                                        ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                        strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                        replyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                        strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                        cc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                        bcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                        strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                        strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                        dtmailsentdate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                        strcompanyurl = dsmaildetails.Tables[0].Rows[i]["URL"].ToString();
                                        String str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + COMPANYID + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + replyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + cc + "\",\"MAILBCC\":\"" + bcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + dtmailsentdate.ToString("MM-dd-yyyy") + "\"}]";
                                        // mail.SendNotificationDetails(str);
                                        object[] objSendNotificationDetails = new object[1];
                                        objSendNotificationDetails[0] = str;

                                        Log.InfoFormat("4. Json String:" + str);

                                        CallWebServices CWS = new CallWebServices();
                                        object objCWS = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);

                                        Log.InfoFormat("6. WebService UserInsert Return Value:" + objCWS.ToString());

                                        if (objCWS.ToString() == "1")
                                        {
                                            strquery = ScriptEngine.GetValue("USERINBOXENTRY");
                                            string[] paramuserinbox = { "COMPANYID", "USERID", "FROM", "REPLYTO", "TO", "SUBJECT", "BODY", "TOKENID" };
                                            object[] pValuesuserinbox = { COMPANYID, dsmaildetails.Tables[0].Rows[i]["USERID"].ToString(), strmailfrom, replyto, strto, strsubject, strbody, ischeduleid };
                                            int res = BaseAdapter.ExecuteInlineQueries(strquery, paramuserinbox, pValuesuserinbox);
                                        }
                                        else if (objCWS.ToString() != "1")
                                        {
                                            strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                            string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                            object[] pValuesrptinfo = { COMPANYID, iresult, "INSERTUSER", 1, "MAILNEWUSERCREATION", "INSERTUSER", obj.ToString() };
                                            int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { COMPANYID, userid, "INSERTUSER", 1, "MAILNEWUSERCREATION", "INSERTUSER", ex.ToString() };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                            }
                            #endregion
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            Log.InfoFormat("7. USER EXISTS:" + iresult);
                            obj1.status = false;
                            obj1.message = "ALTUSEREXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -3)
                        {
                            Log.InfoFormat("8. MAIL EXISTS:" + iresult);
                            obj1.status = false;
                            obj1.message = "ALTUSERMAILEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -4)
                        {
                            obj1.status = false;
                            obj1.message = "ALTUSERCNTEXCEED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for create User:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string useridd = Convert.ToString(request.SelectToken("userId"));
                    string userName = Convert.ToString(request.SelectToken("userName"));
                    string fitstName = Convert.ToString(request.SelectToken("firstName"));
                    string lastName = Convert.ToString(request.SelectToken("lastName"));
                    string emailId = Convert.ToString(request.SelectToken("emailId"));
                    string jobTitle = Convert.ToString(request.SelectToken("jobTitle"));
                    string userType = Convert.ToString(request.SelectToken("userType"));
                    string defaultrole = Convert.ToString(request.SelectToken("defaultRole"));
                    string userOffline = Convert.ToString(request.SelectToken("userOffline"));  //moblieapp
                    string userOfflineapprove = Convert.ToString(request.SelectToken("userOfflineApprove"));
                    string userPassword = Convert.ToString(request.SelectToken("userPassword"));
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string userDefaultsite = Convert.ToString(request.SelectToken("userDefaultSite"));
                    string languageid = Convert.ToString(request.SelectToken("languageId"));
                    string userDateFormat = Convert.ToString(request.SelectToken("userDateFormat"));
                    string userTimezone = Convert.ToString(request.SelectToken("userTimeZone"));
                    int status = Convert.ToInt32(request.SelectToken("status"));

                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();

                    if (userType == "1")
                    {
                        if (String.IsNullOrEmpty(emailId))
                        {
                            emailId = "email@gmail.com";
                        }
                    }
                    string ddlGroup = Convert.ToString(request.SelectToken("groupIds"));
                    if (String.IsNullOrEmpty(useridd) || String.IsNullOrEmpty(userName) || String.IsNullOrEmpty(fitstName) || String.IsNullOrEmpty(lastName) || String.IsNullOrEmpty(emailId) || String.IsNullOrEmpty(userType) || String.IsNullOrEmpty(defaultrole))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (userType == "1")
                        {
                            if (emailId == "email@gmail.com")
                            {
                                emailId = "";
                            }
                        }
                        if (defaultrole == "0" || defaultrole == "")
                        {
                            defaultrole = "-1";
                        }
                        if (ddlGroup == "")
                        {
                            ddlGroup = "0";
                        }
                        strquery = ScriptEngine.GetValue("UPDATEUSERS");
                        string[] paraminsert = { "USERID", "USERNAME", "FIRSTNAME", "LASTNAME", "EMAIL", "USERTYPEID", "OFFLINEACCESS", "APPROVEOFFLINE", "PASSWORD", "STATUS", "JOBTITLE", "ROLEID", "DEFAULTROLEID", "SITEID", "DEFAULTSITEID", "UPDATEDBY", "LANGUAGEID", "TIMEZONE", "DATEFORMAT", "USERSCOUNT", "GROUPID" };
                        object[] pValuesinsert = { useridd, userName, fitstName, lastName, emailId, userType, userOffline, userOfflineapprove, userPassword, status, jobTitle, defaultrole, defaultrole, siteIds, userDefaultsite, userid, languageid, userTimezone, userDateFormat, 0, ddlGroup };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult >= 0)
                        {
                            Log.InfoFormat("iresult value:" + iresult);
                            obj1.status = true;
                            obj1.message = "ALTUSERUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            #region MAIL SERVICE
                            if (iresult != 0)
                            {
                                try
                                {
                                    Log.InfoFormat("==User Update Mail Process Start==");
                                    string[] parampwdreset = { "USERID" };
                                    object[] pValuespwdreset = { useridd };
                                    strquery = ScriptEngine.GetValue("PASSWORDUPDATE");
                                    DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parampwdreset, pValuespwdreset);
                                    if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                    {
                                        for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                        {
                                            ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                            strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                            replyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                            strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                            cc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                            bcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                            strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                            strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                            dtmailsentdate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                            String str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + COMPANYID + "\",\"TOKENID\":\"" + ischeduleid + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + replyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + cc + "\",\"MAILBCC\":\"" + bcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + dtmailsentdate.ToString("MM-dd-yyyy") + "\"}]";
                                            object[] objSendNotificationDetails = new object[1];
                                            objSendNotificationDetails[0] = str;
                                            Log.InfoFormat("Json String:" + str);
                                            CallWebServices CWS = new CallWebServices();
                                            object obj = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                            Log.InfoFormat("WebService Return Value for user update:" + obj.ToString());
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Log.InfoFormat("Exception for mail send when User Update Password" + ex.StackTrace);
                                }
                            }
                            #endregion

                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTUSEREXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -3)
                        {
                            obj1.status = false;
                            obj1.message = "ALTUSERMAILEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -5)
                        {
                            obj1.status = false;
                            obj1.message = "ALTDEFSITE";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -4)
                        {
                            obj1.status = false;
                            obj1.message = "ALTUSERCNTEXCEED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for Update User:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string useridd = Convert.ToString(request.SelectToken("userId"));
                    if (String.IsNullOrEmpty(useridd))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEUSERS");
                        string[] paraminsert = { "USERID", "UPDATEDBY" };
                        object[] pValuesinsert = { useridd, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for Delete User:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string SELECTEDUSERID = Convert.ToString(request.SelectToken("userId"));
                    if (String.IsNullOrEmpty(SELECTEDUSERID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESTATUSUSERS");
                        string[] paraminsert = { "SELECTEDUSERID", "USERID" };
                        object[] pValuesinsert = { SELECTEDUSERID, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);

                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for Update User Status:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the user data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/GetUserData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetUserData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesUserDetail obj2 = new ParameterValuesUserDetail();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsGroupType = new DataSet();
                DataSet dsGroups = new DataSet();
                DataSet dsUserFilter = new DataSet();
                if (dtTokenVal != null && dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string SITEID1 = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string USERTYPEID = Convert.ToString(request.SelectToken("userType"));
                    string STATUSID = Convert.ToString(request.SelectToken("statusId"));
                    string gropu1 = Convert.ToString(request.SelectToken("group1"));
                    string gropu2 = Convert.ToString(request.SelectToken("group2"));
                    string gropu3 = Convert.ToString(request.SelectToken("group3"));
                    string gropu4 = Convert.ToString(request.SelectToken("group4"));
                    string gropu5 = Convert.ToString(request.SelectToken("group5"));
                    string gropu6 = Convert.ToString(request.SelectToken("group6"));
                    string gropu7 = Convert.ToString(request.SelectToken("group7"));
                    string gropu8 = Convert.ToString(request.SelectToken("group8"));
                    string gropu9 = Convert.ToString(request.SelectToken("group9"));
                    string gropu10 = Convert.ToString(request.SelectToken("group10"));
                    string SELECTEDUSERID = Convert.ToString(request.SelectToken("userId"));
                    if (!String.IsNullOrEmpty(SELECTEDUSERID))
                    {
                        strquery = ScriptEngine.GetValue("GETUSERINFO");
                        string[] paraminsert = { "USERID", "LOGGEDUSERID" };
                        object[] pValuesinsert = { SELECTEDUSERID, userid };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        //for add column siteassign
                        ds.Tables[0].Columns.Add("userSiteAssign", typeof(String));
                        try
                        {
                            ds.Tables[0].Rows[0]["userSiteAssign"] = ds.Tables[3].Rows[0]["siteIds"].ToString().Trim();
                        }
                        catch (Exception ex)
                        {
                            ds.Tables[0].Rows[0]["userSiteAssign"] = "";
                        }
                        //for add column
                        strquery = ScriptEngine.GetValue("GETGROUPTYPEWITHMANDATOTY");
                        string[] paramgrouptype = { "LANGUAGEID" };
                        object[] pValuesgrouptype = { languageId };
                        dsGroupType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramgrouptype, pValuesgrouptype);

                        strquery = ScriptEngine.GetValue("GETGROUPS_USERGROUPS");
                        string[] paramgroups = { "LANGUAGEID", "USERID" };
                        object[] pValuesgroups = { languageId, SELECTEDUSERID };
                        dsGroups = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramgroups, pValuesgroups);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            string timeZone1 = ds.Tables[0].Rows[0]["TIMEZONE"].ToString();
                            if (ds.Tables[0].Rows[0]["DEFAULTSITEID"].ToString() == "-1")
                            {
                                string[] usersite = ds.Tables[0].Rows[0]["userSiteAssign"].ToString().Split(',');
                                if(usersite[0] != "")
                                {
                                    ds.Tables[0].Rows[0]["DEFAULTSITEID"] = usersite[0].ToString();
                                }
                                else
                                {
                                    ds.Tables[0].Rows[0]["DEFAULTSITEID"] = 0;
                                }
                            }
                            DataTable timezone = new DataTable();
                            try
                            {
                                timezone = ds.Tables[5].Select("TIMEZONEID = " + timeZone1 + "").CopyToDataTable();
                            }
                            catch (Exception ex)
                            {
                                string timeZoneSite = ds.Tables[0].Rows[0]["DEFAULTSITEID"].ToString();
                                var result = from r in ds.Tables[9].AsEnumerable()
                                             where r.Field<string>("SITEID") == timeZoneSite
                                             select r;
                                if (result.Any())
                                { 
                                    timezone = result.CopyToDataTable();
                                }
                                else { 
                                    timezone = ds.Tables[9].Clone();
                                }
                                timezone.Columns.Remove("SITEID");
                            }
                            DataTable dtTokenValAll = ds.Tables[2].Copy();
                            dtTokenValAll.Merge(ds.Tables[1]);
                            List<Dictionary<string, object>> listUserInfo = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dtTokenValAll);
                            List<Dictionary<string, object>> listSiteAssing = ScriptEngine.GetTableRows(ds.Tables[3]);
                            List<Dictionary<string, object>> language = ScriptEngine.GetTableRows(ds.Tables[4]);
                            List<Dictionary<string, object>> timeZone = ScriptEngine.GetTableRows(timezone);
                            List<Dictionary<string, object>> role = ScriptEngine.GetTableRows(ds.Tables[6]);
                            List<Dictionary<string, object>> site = ScriptEngine.GetTableRows(ds.Tables[7]);
                            List<Dictionary<string, object>> validDetails = ScriptEngine.GetTableRows(ds.Tables[8]);
                            List<Dictionary<string, object>> allSiteTimezone = ScriptEngine.GetTableRows(ds.Tables[9]);
                            List<Dictionary<string, object>> groupMandatory = ScriptEngine.GetTableRows(dsGroupType.Tables[0]);
                            List<Dictionary<string, object>> userGroup = ScriptEngine.GetTableRows(dsGroups.Tables[0]);
                            obj2.status = true;
                            obj2.message = "Success";
                            obj2.userInfo = listUserInfo;
                            obj2.userSiteAvail = listSite;
                            obj2.language = language;
                            if(timeZone.Count>0)
                            {
                                obj2.timeZone = timeZone[0];
                            }
                            else
                            {
                                obj2.timeZone = "";
                            }
                            obj2.role = role;
                            if (site.Count > 0)
                            {
                                obj2.site = site;
                            }
                            else
                            {
                                obj2.site = "";
                            }
                            obj2.validDetails = validDetails;
                            obj2.allSiteTimezone = allSiteTimezone;
                            obj2.groupMandatory = groupMandatory;
                            obj2.userGroup = userGroup;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETUSERLIST");
                        string[] paraminsert = { "USERID", "DEFAULTROLEID", "SITEID", "USERTYPE", "ROLEID", "STATUSID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10" };
                        object[] pValuesinsert = { userid, DEFAULTROLEID, SITEID, USERTYPEID, ROLEID, STATUSID, gropu1, gropu2, gropu3, gropu4, gropu5, gropu6, gropu7, gropu8, gropu9, gropu10 };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        //load filter data
                        strquery = ScriptEngine.GetValue("GETUSERSFILTER");
                        string[] paramrptinfo = { "USERID", "LANGUAGEID" };
                        object[] pValuesrptinfo = { userid, languageId };
                        dsUserFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                        //if (ds.Tables[0].Rows.Count != 0)
                        if (dsUserFilter.Tables[0].Rows.Count != 0)
                        {
                            int count = dsUserFilter.Tables[0].Rows.Count;
                            DataTable dtTokenValNew = new DataTable();
                            dtTokenValNew.Columns.Add("Name", typeof(String));
                            dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                            dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                            dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                            dtTokenValNew.Columns.Add("Data", typeof(String));
                            for (int i = 0; i < count; i++)
                            {
                                string query = dsUserFilter.Tables[0].Rows[i]["QUERY"].ToString();
                                DataSet dsfilter = getValueTable(query, userid, languageId, SITEID1, DEFAULTROLEID);
                                DataRow dr = dtTokenValNew.NewRow();
                                dr[0] = dsUserFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                                dr[1] = dsUserFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                                dr[2] = dsUserFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                                dr[3] = dsUserFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                                dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                                dtTokenValNew.Rows.Add(dr);
                            }
                            DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").Count() > 0 ? dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable() : dtTokenValNew.Clone();
                            DataTable dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").Count() > 0 ? dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable() : dtTokenValNew.Clone();
                            DataTable dtSitefilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").Count() > 0 ? dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable() : dtTokenValNew.Clone();
                            //DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            //DataTable dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable();
                            //DataTable dtSitefilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                            ParameterValuesLoad obj3 = new ParameterValuesLoad();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(dsUserFilter.Tables[0]);
                            List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(dsUserFilter.Tables[1]);
                            List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(dtTokenValfilter);
                            List<Dictionary<string, object>> lstoptional = ScriptEngine.GetTableRows(dtTokenValoptional);
                            List<Dictionary<string, object>> lstSite = ScriptEngine.GetTableRows(dtSitefilter);

                            if(ds.Tables[0].Rows.Count != 0)
                            {
                                 obj3.message = "Success";
                            }
                            else
                            {
                                obj3.message = "FMKNORECS";
                            }
                            obj3.status = true;
                            obj3.Data = lstPersons;
                            obj3.filterData = lstPersons2;
                            obj3.optionalFilter = lstoptional;
                            obj3.siteFilter = lstSite;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj3);
                        }
                        else
                        {
                            //obj1.status = false;
                            //obj1.message = "FMKNORECS";
                            //response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for Get User Details:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Users the add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/userAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage userAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterValuesUserDetail obj2 = new ParameterValuesUserDetail();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsGroupType = new DataSet();
                DataSet dsGroups = new DataSet();
                if (dtTokenVal != null && dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string TIMEZONE = dtTokenVal.Rows[0]["TIMEZONE"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();

                    strquery = ScriptEngine.GetValue("GETUSERINFO");
                    string[] paraminsert = { "USERID", "LOGGEDUSERID" };
                    object[] pValuesinsert = { "", userid };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("GETGROUPTYPEWITHMANDATOTY");
                    string[] paramgrouptype = { "LANGUAGEID" };
                    object[] pValuesgrouptype = { languageId };
                    dsGroupType = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramgrouptype, pValuesgrouptype);

                    strquery = ScriptEngine.GetValue("GETGROUPS_USERGROUPS");
                    string[] paramgroups = { "LANGUAGEID", "USERID" };
                    object[] pValuesgroups = { languageId, userid };
                    dsGroups = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramgroups, pValuesgroups);

                    DataTable timezone = new DataTable();
                    try
                    {
                        timezone = ds.Tables[5].Select("TIMEZONEID = " + TIMEZONE + "").CopyToDataTable();
                    }
                    catch (Exception ex)
                    {
                        Log.InfoFormat("Exception for Get data of Time Zone when Add Click Button :" + ex.StackTrace);
                    }

                    DataTable dtTokenValAll = ds.Tables[1].Copy();
                    dtTokenValAll.Merge(ds.Tables[2]);
                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[0]);
                    List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(dtTokenValAll);
                    List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(ds.Tables[3]);
                    List<Dictionary<string, object>> lstPersons4 = ScriptEngine.GetTableRows(ds.Tables[4]);
                    List<Dictionary<string, object>> lstPersons5 = ScriptEngine.GetTableRows(timezone);
                    List<Dictionary<string, object>> lstPersons6 = ScriptEngine.GetTableRows(ds.Tables[6]);
                    List<Dictionary<string, object>> lstPersons7 = ScriptEngine.GetTableRows(ds.Tables[7]);
                    List<Dictionary<string, object>> lstPersons8 = ScriptEngine.GetTableRows(ds.Tables[8]);
                    List<Dictionary<string, object>> lstPersons9 = ScriptEngine.GetTableRows(ds.Tables[9]);
                    List<Dictionary<string, object>> lstPersons10 = ScriptEngine.GetTableRows(dsGroupType.Tables[0]);
                    List<Dictionary<string, object>> lstPersons11 = ScriptEngine.GetTableRows(dsGroups.Tables[0]);
                    obj2.status = true;
                    obj2.message = "Success";
                    obj2.userInfo = lstPersons1;
                    obj2.userSiteAvail = lstPersons2;
                    obj2.language = lstPersons4;
                    try
                    {
                        obj2.timeZone = lstPersons5[0];
                    }
                    catch (Exception ex)
                    {
                        obj2.timeZone = "";
                    }
                    obj2.role = lstPersons6;
                    try
                    {
                        obj2.site = lstPersons7[0];
                    }
                    catch (Exception ex)
                    {
                        obj2.site = "";
                    }
                    obj2.validDetails = lstPersons8;
                    obj2.allSiteTimezone = lstPersons9;
                    obj2.groupMandatory = lstPersons10;
                    obj2.userGroup = lstPersons11;
                    response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception for Get Data when Add click Button:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the user data target.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/GetUserDataTarget")]
        [APIAuthAttribute]
        public HttpResponseMessage GetUserDataTarget(JObject request)
        {
            ClsLogWrite cs = new ClsLogWrite();
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterUserTarget obj2 = new ParameterUserTarget();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                string Targets = Convert.ToString(request.SelectToken("targets"));
                if (dtTokenVal != null && dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string SELECTEDUSERID = Convert.ToString(request.SelectToken("userId"));
                    string siteid = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(SELECTEDUSERID) && String.IsNullOrEmpty(siteid))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETSITES");
                        string[] paraminsert = { "USERID" };
                        object[] pValuesinsert = { SELECTEDUSERID, };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (Targets == "0")
                        {
                            strquery = ScriptEngine.GetValue("GETTARGETUSERS");
                            string[] paraminsert1 = { "SITEID", "USERID" };
                            object[] pValuesinsert1 = { siteid, SELECTEDUSERID, };
                            ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        }
                        else if (Targets == "1")
                        {
                            strquery = ScriptEngine.GetValue("GETWEEKLYTARGET");
                            string[] paraminsert1 = { "SITEID", "USERID" };
                            object[] pValuesinsert1 = { siteid, SELECTEDUSERID, };
                            ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        }
                        if (ds.Tables[0].Rows.Count != 0 || ds1.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds1.Tables[0]);
                            obj2.status = true;
                            obj2.message = "Success";
                            obj2.site = lstPersons;
                            obj2.target = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get User Target Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the target data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/User/InsertTargetData")]
        [APIAuthAttribute]
        public HttpResponseMessage InsertTargetData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string strquery;
                    string SELECTEDUSERID = Convert.ToString(request.SelectToken("userId"));
                    string siteid = Convert.ToString(request.SelectToken("siteid"));
                    string JAN = Convert.ToString(request.SelectToken("jan"));
                    string FEB = Convert.ToString(request.SelectToken("feb"));
                    string MAR = Convert.ToString(request.SelectToken("mar"));
                    string APR = Convert.ToString(request.SelectToken("apr"));
                    string MAY = Convert.ToString(request.SelectToken("may"));
                    string JUN = Convert.ToString(request.SelectToken("jun"));
                    string JUL = Convert.ToString(request.SelectToken("jul"));
                    string AUG = Convert.ToString(request.SelectToken("aug"));
                    string SEP = Convert.ToString(request.SelectToken("sep"));
                    string OCT = Convert.ToString(request.SelectToken("oct"));
                    string NOV = Convert.ToString(request.SelectToken("nov"));
                    string DEC = Convert.ToString(request.SelectToken("dec"));
                    string APPLYTOALLOBSERVERS = Convert.ToString(request.SelectToken("APPLYTOALLOBSERVERS"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string targets = Convert.ToString(request.SelectToken("targets"));
                    string targetallsite = Convert.ToString(request.SelectToken("targetAllSite"));
                    string weeklyTarget = Convert.ToString(request.SelectToken("weeklyTarget"));

                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();
                    DataSet dsresult = new DataSet();
                    if (String.IsNullOrEmpty(siteid) || String.IsNullOrEmpty(SELECTEDUSERID) || String.IsNullOrEmpty(targets))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (targets == "0")
                        {
                            strquery = ScriptEngine.GetValue("INSERTTARGETUSERS");
                            string[] paraminsert = { "TARGETALLSITES", "SITEID", "USERID", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "CREATEDBY" };
                            object[] pValuesinsert = { targetallsite, siteid, SELECTEDUSERID, JAN, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC, userid };
                            dsresult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        else if (targets == "1")
                        {
                            strquery = ScriptEngine.GetValue("INSERTWEEKLYTARGET");
                            string[] paraminsert = { "TARGETALLSITES", "SITEID", "USERID", "WEEKTARGET", "CREATEDBY" };
                            object[] pValuesinsert = { targetallsite, siteid, SELECTEDUSERID, weeklyTarget, userid };
                            dsresult = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if ((dsresult.Tables.Count > 0) && (dsresult.Tables[0].Rows.Count > 0))
                        {
                            obj1.status = true;
                            obj1.message = "ALTTARGETUPDATE";
                            emailTarget(dsresult, targets, COMPANYID, userid);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Add User Target Details:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        public async Task<int> emailTarget(DataSet dsresult, string targets, string COMPANYID, string userid)
        {
            string mode = "";
            string strquery = "";
            int EL = 0;
            try
            {
                //DataPro configuration setting 
                Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                string strAPPID = dctpartdata["APPID"].ToString();
                string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string ES_WSURL = dctpartdata["WSURL"].ToString();
                Log.InfoFormat("==User Target Mail Process Start==");
                if ((dsresult.Tables.Count > 0) && (dsresult.Tables[0].Rows.Count > 0))
                {
                    ischeduleid = Convert.ToInt32(dsresult.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                    strmailfrom = dsresult.Tables[0].Rows[0]["FROM"].ToString();
                    replyto = dsresult.Tables[0].Rows[0]["REPLYTO"].ToString();
                    strto = dsresult.Tables[0].Rows[0]["TO"].ToString();
                    cc = dsresult.Tables[0].Rows[0]["CC"].ToString();
                    bcc = dsresult.Tables[0].Rows[0]["BCC"].ToString();
                    strsubject = dsresult.Tables[0].Rows[0]["SUBJECT"].ToString();
                    strbody = dsresult.Tables[0].Rows[0]["BODY"].ToString();
                    dtmailsentdate = Convert.ToDateTime(dsresult.Tables[0].Rows[0]["SENTDATE"].ToString());
                    strcompanyurl = dsresult.Tables[0].Rows[0]["URL"].ToString();

                    String str = "[{\"APPID\":\"" + strAPPID
                        + "\",\"COMPANYID\":\"" + COMPANYID
                        + "\",\"TOKENID\":\"" + ischeduleid
                        + "\",\"URL\":\"" + strcompanyurl
                        + "\",\"MAILFROM\":\"" + strmailfrom
                        + "\",\"MAILREPLYTO\":\"" + replyto
                        + "\",\"MAILTO\":\"" + strto
                        + "\",\"MAILCC\":\"" + cc
                        + "\",\"MAILBCC\":\"" + bcc
                        + "\",\"MAILSUBJECT\":\"" + strsubject
                        + "\",\"MAILBODY\":\"" + strbody
                        + "\",\"MAILTOBESENTDATE\":\"" + dtmailsentdate.ToString("MM-dd-yyyy") + "\"}]";
                    Log.InfoFormat("JSON String:" + str);
                    object[] objSendNotificationDetails = new object[1];
                    objSendNotificationDetails[0] = str;

                    Log.InfoFormat("JSON String:" + str);
                    CallWebServices CWS = new CallWebServices();

                    object obj = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);

                    Log.InfoFormat("WebService Return Value:" + obj.ToString());
                    if (obj.ToString() != "1")
                    {
                        if (targets == "0")
                        {
                            strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                            mode = "INSERTTARGETUSERS";
                        }
                        else
                        {
                            strquery = ScriptEngine.GetValue("INSERTWEEKLYTARGET");
                            mode = "INSERTWEEKLYTARGET";
                        }
                        string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                        object[] pValuesrptinfo = { COMPANYID, userid, mode, 1, mode, mode, obj.ToString() };
                        EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                    }
                }
            }
            catch (Exception ex)
            {
                if (targets == "0")
                {
                    strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                }
                else
                {
                    strquery = ScriptEngine.GetValue("INSERTWEEKLYTARGET");
                }
                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                object[] pValuesrptinfo = { COMPANYID, userid, mode, 1, mode, mode, ex.ToString() };
                EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                Log.InfoFormat("Exception Occured When Send Mail for user Target:" + ex.StackTrace);
            }
            return EL;
        }

    }
}





