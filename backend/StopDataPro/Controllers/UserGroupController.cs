﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// UserGroupController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class UserGroupController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserGroupController"/> class.
        /// </summary>
        public UserGroupController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(UserGroupController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion


        #region Action Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string groupName = Convert.ToString(request.SelectToken("groupName"));
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    if (String.IsNullOrEmpty(groupName) || String.IsNullOrEmpty(groupTypeId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTGROUPS");
                        string[] paraminsert = { "COMPANYID", "GROUPNAME", "GROUPTYPEID", "STATUS", "CREATEDBY", "LANGUAGEID" };
                        object[] pValuesinsert = { COMPANYID, groupName, groupTypeId, status, userid, languageId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgId obj = new StatusMsgId();
                            obj.status = true;
                            obj.message = "ALTGROUPCREAT";
                            obj.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTGROUPEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert Group:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string groupName = Convert.ToString(request.SelectToken("groupName"));
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string userName = Convert.ToString(request.SelectToken("userIds"));
                    string groupid = Convert.ToString(request.SelectToken("groupId"));
                    if (String.IsNullOrEmpty(groupTypeId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(groupName) || String.IsNullOrEmpty(groupTypeId) || String.IsNullOrEmpty(groupid))
                        {
                            obj1.status = false;
                            obj1.message = "Please Add Mandatory Parameter";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            strquery = ScriptEngine.GetValue("UPDATEGROUPS");
                            string[] paraminsert = { "GROUPNAME", "GROUPTYPEID", "STATUS", "UPDATEDBY", "GROUPID", "LANGUAGEID" };
                            object[] pValuesinsert = { groupName, groupTypeId, status, userid, groupid, languageId };
                            int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                            if (iresult != 0 && iresult > 0)
                            {
                                obj1.status = true;
                                obj1.message = "ALTGROUPSAVESUCC";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else if (iresult == -1)
                            {
                                obj1.status = true;
                                obj1.message = "ALTGROUPEXIST";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Group:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/updateAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string userName = Convert.ToString(request.SelectToken("userIds"));
                    string groupid = Convert.ToString(request.SelectToken("groupId"));
                    if (String.IsNullOrEmpty(groupTypeId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(groupid) || String.IsNullOrEmpty(groupTypeId) || String.IsNullOrEmpty(userName))
                        {
                            obj1.status = false;
                            obj1.message = "Please Add Mandatory Parameter";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            strquery = ScriptEngine.GetValue("ASSGNUSRTOGRP");
                            string[] paraminsert1 = { "GROUPTYPEID", "GROUPID", "USERID", "UPDATEDBY" };
                            object[] pValuesinsert1 = { groupTypeId, groupid, userName, userid };
                            int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            if (iresult != 0 && iresult > 0)
                            {
                                obj1.status = true;
                                obj1.message = "ALTUSERASSIGN";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Group assign:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string groupid = Convert.ToString(request.SelectToken("groupId"));
                    if (String.IsNullOrEmpty(groupid))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEGROUPS");
                        string[] paraminsert = { "GROUPTYPEID", "UPDATEDBY", "GROUPID" };
                        object[] pValuesinsert = { groupTypeId, userid, groupid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Group:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string groupid = Convert.ToString(request.SelectToken("groupId"));
                    if (String.IsNullOrEmpty(groupid))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESTATUSGROUPS");
                        string[] paraminsert = { "GROUPTYPEID", "UPDATEDBY", "GROUPID" };
                        object[] pValuesinsert = { groupTypeId, userid, groupid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);

                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When change status Group:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the user data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/GetUserData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetUserData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string groupid = Convert.ToString(request.SelectToken("groupId"));
                    if (!String.IsNullOrEmpty(groupid))
                    {
                        strquery = ScriptEngine.GetValue("GETGROUPINFO");
                        string[] paraminsert = { "GROUPID", "LANGUAGECODE", "GROUPTYPEID" };
                        object[] pValuesinsert = { groupid, languageId, groupTypeId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            string grouptypeid = ds.Tables[0].Rows[0]["GROUPTYPEID"].ToString();
                            strquery = ScriptEngine.GetValue("GETUSERSFORAVAILASSIGN");
                            string[] paraminsert1 = { "LANGUAGECODE", "GROUPTYPEID", "USERID", "GROUPID" };
                            object[] pValuesinsert1 = { languageId, grouptypeid, userid, groupid };
                            dsAvail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            ds.Tables[0].Columns.Add("userIds", typeof(String));
                            try
                            {
                                ds.Tables[0].Rows[0]["userIds"] = dsAvail.Tables[2].Rows[0]["userIds"].ToString();
                            }
                            catch (Exception ex)
                            {
                                ds.Tables[0].Rows[0]["userIds"] = "";
                            }
                            DataTable dtTokenValAll = dsAvail.Tables[1].Copy();
                            dtTokenValAll.Merge(dsAvail.Tables[0]);
                            ParameterGroupTarget obj = new ParameterGroupTarget();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(dtTokenValAll);
                            obj.status = true;
                            obj.message = "Success";
                            obj.groupInfo = lstPersons;
                            obj.userAvail = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else if (!String.IsNullOrEmpty(groupTypeId))
                    {
                        strquery = ScriptEngine.GetValue("GETGROUPLISTING");
                        string[] paraminsert = { "LANGUAGECODE", "GROUPTYPEID" };
                        object[] pValuesinsert = { languageId, groupTypeId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Field";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);

                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Group data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the type of the group.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/GetGroupType")]
        [APIAuthAttribute]
        public HttpResponseMessage GetGroupType(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETGROUPTYPE");
                    string[] paraminsert = { "LANGUAGEID" };
                    object[] pValuesinsert = { languageId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj1.status = true;
                        obj1.message = "Success";
                        obj1.Data = lstPersons;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }

                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Grouptype data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the group translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/getGroupTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage getGroupTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslang = new DataSet();
                DataSet dsfield = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageCode = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string[] paraminsert = { "LANGUAGEID" };
                    object[] pValuesinsert = { languageCode };
                    strquery = ScriptEngine.GetValue("GETGRPTYPELANGUGES");
                    dslang = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (String.IsNullOrEmpty(language))
                    {
                        language = dslang.Tables[1].Rows[0]["LANGUAGEID"].ToString();
                        groupTypeId = dslang.Tables[0].Rows[0]["GROUPTYPEID"].ToString();
                    }
                    string[] paraminsert2 = { "LANGUAGEID", "PARENTID" };
                    object[] pValuesinsert2 = { language, groupTypeId };
                    strquery = ScriptEngine.GetValue("GETTRNSLATIONFORGROUPS");
                    dsfield = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                    if (dsfield.Tables[0].Rows.Count != 0)
                    {
                        ParameterGroupList obj = new ParameterGroupList();
                        List<Dictionary<string, object>> lstLang = ScriptEngine.GetTableRows(dslang.Tables[1]);
                        List<Dictionary<string, object>> lstField = ScriptEngine.GetTableRows(dslang.Tables[0]);
                        List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(dsfield.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.language = lstLang;
                        obj.groupType = lstField;
                        obj.data = lstData;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get group Translation data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserGroup/updateTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage updateTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleid = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("fieldId")); 
                    string groupTypeId = Convert.ToString(request.SelectToken("groupTypeId"));
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(groupTypeId) || String.IsNullOrEmpty(language))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(fieldId);
                        string entity = "";
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dtTokenVals.Rows.Count; i++)
                        {
                            string id = dtTokenVals.Rows[i]["eid"].ToString();
                            string value = dtTokenVals.Rows[i]["eval"].ToString();
                            entity = id + ",," + value + "|";
                            sb.Append(entity);
                        }

                        strquery = ScriptEngine.GetValue("UPDATEGROUPSTRANSLATION");
                        string[] paraminsert = { "UPDATEDBY", "ENTITYID", "LANGUAGEID", "GROUPTYPEID" };
                        object[] pValuesinsert = { CREATEDBY, sb.ToString(), language, groupTypeId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSELECTEDITEMTRANSLATEDSUCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update group Translation:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
        
    }
}
