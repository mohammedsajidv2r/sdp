﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using WebAPI.Filters;
using WebAPI.Helpers;
using WebAPI.Library;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// Login
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class LoginController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginController"/> class.
        /// </summary>
        public LoginController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(LoginController));
            this.sec_helper = new SecurityHelper();
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the infoconfig.
        /// </summary>
        /// <value>
        /// The infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        /// <summary>
        /// Gets or sets the sec_helper.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public SecurityHelper sec_helper { get; set; }

        #endregion

        #region Action Method
        DateTime pwdmoddate;
        long userid, sessionid, sslid;
        public static Dictionary<string, object> scriptengine_Rdata;
        public string URL, ConStr, Msg, CmpId, strcompanyname;
        int status, userLoginattempt, globalLoginattempt, isobserver, ifirstLogin, iLoggeduserlangid, roleid, copyfrom, prevuserLoginattempt, enableLoginattempts;
        int idefaultsiteid, itimezoneid, ieditobservation, iaddnewobserver, iunsafealertpermission, iunsafealertsubscribe, iobsapprovalpc, iaddcapa;
        string msg, expperiod, alertmsg="", alertmsgflag, lastname, strfirstname, strdefaultsitname, strquery;
        bool pwdexpired;
        string strdateformat, strLoggeduserlangcode;
        DataSet Loginuserinfo, usrdashboardpermission;

        /// <summary>
        /// Logs the check token.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Login/LogCheckToken")]
        public HttpResponseMessage LogCheckToken(JObject request)
        {
          //  Log.InfoFormat("1. Login api start");
            Dictionary<string, object> retresult = new Dictionary<string, object>();
            ParameterLogin obj1 = new ParameterLogin();
            DataSet dsDashboard = new DataSet();
            HttpResponseMessage response = new HttpResponseMessage();
            AuthUser ClientKeys = new AuthUser();
            try
            {
                //Decrypt  request
                 request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
               // Log.InfoFormat("2. decrypt hdn data");
                string username = Convert.ToString(request.SelectToken("userName"));
                string pass = Convert.ToString(request.SelectToken("pass"));
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(pass) || String.IsNullOrEmpty(httpUrl))
                {
                    obj1.status = false;
                    obj1.message = "Please Add Mandatory Parameter";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                    string strSettings = sr.ReadToEnd();
                    scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                    sr.Close();
                    string[] urldomain = httpUrl.Split('/');
                    URL = urldomain[2];

                    //Log.InfoFormat("3. Read app info config file");

                    Dictionary<string, object> scriptengine_data;
                    StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                    string strsettings = srSettings.ReadToEnd();
                    scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                    srSettings.Close();
                    Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                    Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                    dctfulldata = scriptengine_data;
                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "upload").Value;
                    string uploadUrl = dctpartdata["UPLOADURL"].ToString();
                    string uploadFileUrl = dctpartdata["UPLOAFILEURL"].ToString();
                    string uploadPath = dctpartdata["UPLOADPATH"].ToString();
                    //Log.InfoFormat("HOST URL: " + URL);
                    Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                    Dictionary<string, object> cmpdata = new Dictionary<string, object>();
                    Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();
                    tmpdata = scriptengine_Rdata;
                    cmpdata = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key.ToLower() == URL.ToLower()).Value;
                    CmpId = cmpdata["COMPANYID"].ToString();

                    cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == CmpId).Value;// new
                    string FA = cmpdata1["FA"].ToString();
                    string NOTIFICATION = cmpdata1["NOTIFICATION"].ToString();
                    BaseAdapter.constringBal = FA;

                    //Log.InfoFormat("4. Read VALIDATELOGIN config file Start");
                    string tokenKey = "";
                    string[] paramuservalidate = { "PCOMPANYID", "PUSERNAME", "PPASSWORD" };
                    object[] pValuesuservalidate = { CmpId, username, pass };
                    strquery = ScriptEngine.GetValue("VALIDATELOGIN");
                    Loginuserinfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramuservalidate, pValuesuservalidate);

                    //Log.InfoFormat("5. Read VALIDATELOGIN config file End");
                    if (Loginuserinfo.Tables[0].Rows.Count > 0)
                    {
                        //Log.InfoFormat("6. Valid User");
                        globalLoginattempt = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["GLOBALLOGINATTEMPTS"]);
                        userLoginattempt = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["USERLOGINATTEMPTS"]);
                        isobserver = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["USERTYPEID"]);
                        if (isobserver != 1)
                        {
                            if (userLoginattempt < globalLoginattempt)
                            {
                                msg = Loginuserinfo.Tables[0].Rows[0]["MSG"].ToString();
                                if (msg == "VALID")
                                {
                                    status = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["STATUS"].ToString());
                                    if (status == 0)
                                    {
                                        obj1.status = false;
                                        status = 0;
                                        obj1.message = "ALTINACTIVEUSER";
                                    }
                                    else
                                    {
                                        userid = Convert.ToInt64(Loginuserinfo.Tables[0].Rows[0]["USERID"].ToString());
                                        username = Loginuserinfo.Tables[0].Rows[0]["USERNAME"].ToString();
                                        iLoggeduserlangid = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["LANGUAGEID"].ToString());
                                        alertmsgflag = Loginuserinfo.Tables[0].Rows[0]["ALERTMSG"].ToString();
                                        roleid = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["DEFAULTROLEID"].ToString());
                                        idefaultsiteid = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["SITEID"].ToString());
                                        strdateformat = Loginuserinfo.Tables[0].Rows[0]["DATEFORMAT"].ToString();
                                        if (strdateformat == "101")
                                        {
                                            strdateformat = "MM/dd/yyyy";
                                        }
                                        else if (strdateformat == "111")
                                        {
                                            strdateformat = "yyyy/MM/dd";
                                        }
                                        else
                                        {
                                            strdateformat = "dd/MM/yyyy";
                                        }
                                        strfirstname = Loginuserinfo.Tables[0].Rows[0]["FIRSTNAME"].ToString();
                                        lastname = Loginuserinfo.Tables[0].Rows[0]["LASTNAME"].ToString();
                                        strLoggeduserlangcode = Loginuserinfo.Tables[0].Rows[0]["USERLANGCODE"].ToString();
                                        expperiod = Loginuserinfo.Tables[0].Rows[0]["PWDEXPPERIOD"].ToString();
                                        strdefaultsitname = Loginuserinfo.Tables[0].Rows[0]["SITENAME"].ToString();
                                        sessionid = Convert.ToInt64(Loginuserinfo.Tables[0].Rows[0]["SESSIONID"].ToString());
                                        sslid = Convert.ToInt64(Loginuserinfo.Tables[0].Rows[0]["SSLID"].ToString());
                                        ieditobservation = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["EDITOBSERVATION"].ToString());
                                        itimezoneid = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["TIMEZONEID"].ToString());
                                        iaddnewobserver = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["ADDNEWOBS"].ToString());
                                        copyfrom = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["COPYFROM"].ToString());
                                        iunsafealertpermission = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["UNSAFEALERTPER"].ToString());
                                        iunsafealertsubscribe = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["UNSAFEALERTENABLE"].ToString());
                                        iobsapprovalpc = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["OBSAPPROVALPC"].ToString());
                                        iaddcapa = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["ADDCAPA"].ToString());
                                        if (NOTIFICATION == "1")
                                        {
                                            string[] strStartdate = cmpdata1["NOTIFICATIONSTARTDATE"].ToString().Split('-');
                                            string[] strEnddate = cmpdata1["NOTIFICATIONENDDATE"].ToString().Split('-');

                                            DateTime dtStartdate = new DateTime(Convert.ToInt32(strStartdate[2]), Convert.ToInt32(strStartdate[0]), Convert.ToInt32(strStartdate[1]));
                                            DateTime dtTodate = new DateTime(Convert.ToInt32(strEnddate[2]), Convert.ToInt32(strEnddate[0]), Convert.ToInt32(strEnddate[1]));
                                            string NOTIFICATIONMSG = cmpdata1["NOTIFICATIONMSG"].ToString();

                                            DateTime datetime = DateTime.UtcNow;
                                            DateTime dtToDayDate = new DateTime(datetime.Year,datetime.Month,datetime.Day);
                                            if ((dtStartdate <= dtToDayDate) && (dtTodate >= dtToDayDate) && (alertmsgflag == "0"))
                                            {
                                                alertmsg = NOTIFICATIONMSG;
                                            }
                                            else
                                            {
                                                alertmsgflag = "1";//if the notification=1 and the notification date is expired
                                            }
                                        }
                                        else
                                        {
                                            alertmsgflag = "1";//if the notification=0 is unassigned but the user read status (alertmsgflag=0) is unread 
                                        }
                                        if (Loginuserinfo.Tables[0].Rows[0]["PASSWORDLASTUPDATE"].ToString() == "")
                                        {
                                            ifirstLogin = 0;
                                        }
                                        else
                                        {
                                            pwdmoddate = Convert.ToDateTime(Loginuserinfo.Tables[0].Rows[0]["PASSWORDLASTUPDATE"]);
                                            ifirstLogin = 1;
                                        }
                                        if (expperiod != "0")
                                        {
                                            //true-expired,false-not expire 
                                            DateTime today_date = DateTime.Now;
                                            DateTime chkdate = pwdmoddate.AddDays(Convert.ToDouble(expperiod));
                                            if (chkdate < today_date)
                                            {
                                                pwdexpired = true;
                                            }
                                            else
                                            {
                                                pwdexpired = false;
                                            }
                                        }
                                        else
                                        {
                                            pwdexpired = false; // condition for Setting Password Expiration Period to OFF.
                                            if (ifirstLogin == 0)
                                            {
                                                pwdexpired = true;
                                            }
                                        }

                                        //Log.InfoFormat("7. Read GETUSERPERMISSIONFORDASHBOARD config file Start");
                                        #region USERROLEPERMISSIONS
                                        //---to get user role permissions----
                                        string[] paramuserrolepermissions = { "ROLEID", "USERID" };
                                        object[] pValueuserrolepermissions = { roleid, userid };
                                        strquery = ScriptEngine.GetValue("GETUSERPERMISSIONFORDASHBOARD");
                                        usrdashboardpermission = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramuserrolepermissions, pValueuserrolepermissions);
                                        #endregion
                                        //Log.InfoFormat("8. Read GETUSERPERMISSIONFORDASHBOARD config file End");

                                        //Log.InfoFormat("9. Read GETDASHBOARDFULLINFO config file Start");
                                        strquery = ScriptEngine.GetValue("GETDASHBOARDFULLINFO");
                                        string[] paramuserrolepermissions1 = { "ROLEID", "USERID", "SITEID" };
                                        object[] pValueuserrolepermissions1 = { roleid, userid, idefaultsiteid };
                                        dsDashboard = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramuserrolepermissions1, pValueuserrolepermissions1);
                                       // Log.InfoFormat("10. Read GETDASHBOARDFULLINFO config file End");

                                        //Log.InfoFormat("11. Read GETGLOBALOPTIONCHECKLISTCONFIG config file Start");
                                        strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                                        string[] paraminsert = { "LANGUAGECODE" };
                                        object[] pValuesinsert = { iLoggeduserlangid };
                                        DataSet dsChecklist = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                                       // Log.InfoFormat("12. Read GETGLOBALOPTIONCHECKLISTCONFIG config file End");
                                        string assignRole = "";
                                       if (dsChecklist.Tables[0].Rows[0]["OBSAPPROVALPC"].ToString() == "1" || dsChecklist.Tables[0].Rows[0]["OBSAPPROVALPC"].ToString() == "1")
                                       {
                                            assignRole = dsChecklist.Tables[3].Rows[0]["ASSIGNROLEID"].ToString();
                                       }
                                        strquery = ScriptEngine.GetValue("GETGLOBALOPTIONS");
                                        DataSet dsGlobal = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);

                                       // Log.InfoFormat("13. Read DataProGETROLEPERMISSION config file Start");
                                        strquery = ScriptEngine.GetValue("DataProGETROLEPERMISSION");
                                        string[] paramgetuserrolepermissions = { "ROLEID", "USERID",};
                                        object[] Valueuserrolepermissions = { roleid, userid };
                                        DataSet dsRolearam = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramgetuserrolepermissions, Valueuserrolepermissions);
                                        dsDashboard.Tables[0].Merge(dsRolearam.Tables[0]);
                                       // Log.InfoFormat("14. Read DataProGETROLEPERMISSION config file End");
                                        #region tokengenerate
                                        string clientID1 = string.Empty;
                                        string businessID1 = string.Empty;
                                        int id = int.Parse(Loginuserinfo.Tables[0].Rows[0]["USERID"].ToString());
                                        ClientKeys.ClientID = username;
                                        ClientKeys.ClientID = clientID1 = EncryptionLibrary.MD5Encrypt(ClientKeys.ClientID);
                                        string userinfo = EncryptionLibrary.GetVisitormachineInfo(false);
                                        ClientKeys.ClientIP = EncryptionLibrary.MD5Encrypt(userinfo);
                                        string DatatoHashKey = ClientKeys.ClientID + ClientKeys.ClientIP;
                                        string ClientHash = EncryptionLibrary.ComputeHash(DatatoHashKey, null);
                                        bool status1 = EncryptionLibrary.Confirm(DatatoHashKey, ClientHash);

                                       // Log.InfoFormat("15. Read Token Generate Start");
                                        if (status1 == true)
                                        {
                                            ClientKeys.ClientHash = ClientHash;
                                            tokenKey = GenerateandSaveKey(ClientKeys);
                                            string[] paramuser1 = { "TOKEN", "USERID", "MODE" };
                                            object[] pValueuser1 = { tokenKey, userid, "INSERT" };
                                            strquery = ScriptEngine.GetValue("GETTOKEN_INSERT");
                                            DataSet dstoken = new DataSet();
                                            dstoken = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramuser1, pValueuser1);
                                        }
                                        DataTable dtToken = BaseAdapter.CheckLoginToken(tokenKey);
                                      //  Log.InfoFormat("16. Read Token Generate   End");
                                        #endregion
                                        retresult.Add("status", status);
                                        retresult.Add("alertMessageFlag", alertmsgflag);
                                        retresult.Add("alertMessage", alertmsg);
                                        retresult.Add("userId", userid);
                                        retresult.Add("languageId", iLoggeduserlangid);
                                        retresult.Add("roleId", roleid);
                                        retresult.Add("defaultSiteId", idefaultsiteid);
                                        retresult.Add("dateFormat", strdateformat);
                                        retresult.Add("firstLogin", ifirstLogin);
                                        retresult.Add("firstName", strfirstname);
                                        retresult.Add("lastName", lastname);
                                        retresult.Add("passwordExpired", pwdexpired);
                                        retresult.Add("defaultSiteName", strdefaultsitname);
                                        retresult.Add("languageCode", strLoggeduserlangcode.ToLower());
                                        retresult.Add("sessionID", sessionid);
                                        retresult.Add("sslId", sslid);
                                        //retresult.Add("editObservation", ieditobservation);
                                        retresult.Add("editObservation", dsChecklist.Tables[0].Rows[0]["MaxPeopleObservedLimit"].ToString());
                                        retresult.Add("timeZoneId", itimezoneid);
                                        retresult.Add("addNewObserver", iaddnewobserver);
                                        retresult.Add("copyFrom", copyfrom);
                                        retresult.Add("unsafeAlertPermission", iunsafealertpermission);
                                        retresult.Add("unsafeAlertSbuscribe", iunsafealertsubscribe);
                                        retresult.Add("obsApprovalpc", iobsapprovalpc);
                                        retresult.Add("addCapa", iaddcapa);
                                        retresult.Add("token", tokenKey);
                                        retresult.Add("uploadUrl", uploadUrl);
                                        retresult.Add("uploadFileUrl", uploadFileUrl);
                                        retresult.Add("uploadPath", uploadPath);
                                        retresult.Add("imageToken", "Bearer nmSXT32to7nHsKvaLfM7SPGC79l44PdnNerYJE3GPXiLys5HFCXOLLol/wYH771/IrHEZaqog/GjoGn3zmpCdlMsGepIcbFAHM2LVrlw9/EJmPIUgl3XPxysgl3rGVRoucV2IAHqnGkmN6gemJMpzaJ6pX5RraWutL9VVMHLSQ2tBGlibocIcY+gb8ptMAG0");
                                        retresult.Add("companyId", CmpId);
                                        retresult.Add("userType", isobserver);
                                        retresult.Add("previousMonthLimit", dsChecklist.Tables[0].Rows[0]["OBSLISTINGXMONTHS"].ToString());
                                        retresult.Add("imageName", dsGlobal.Tables[0].Rows[0]["CompanyLogo"].ToString());
                                        retresult.Add("email", dtToken.Rows[0]["EMAIL"].ToString());
                                        retresult.Add("approveObservations", dtToken.Rows[0]["APPROVEOFFLINE"].ToString());
                                        retresult.Add("assignRole", assignRole);
                                        retresult.Add("inboxCount", dsDashboard.Tables[2].Rows[0]["INBOXCOUNT"].ToString());
                                        retresult.Add("rolesCopyFrom", Loginuserinfo.Tables[0].Rows[0]["COPYFROM"].ToString());
                                        retresult.Add("subAreaOptionValue", dtToken.Rows[0]["ENABLESUBAREAS"].ToString());
                                        retresult.Add("areaManagerOptionValue", dtToken.Rows[0]["ENABLEAREAMANAGER"].ToString());
                                        obj1.status = true;
                                        obj1.message = "Success";
                                    }
                                }
                                else if (msg == "PWD_INVALID")
                                {
                                    userid = Convert.ToInt64(Loginuserinfo.Tables[0].Rows[0]["USERID"].ToString());
                                    enableLoginattempts = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["ENABLELOGINATTEMPTS"]);
                                    prevuserLoginattempt = Convert.ToInt32(Loginuserinfo.Tables[0].Rows[0]["GLOBALLOGINATTEMPTS"]) - 1;
                                    status = 6;
                                    obj1.message = "ALTINVALIDLOGIN";
                                    if (userid != 1)
                                    {
                                        if (enableLoginattempts != 0)
                                        {
                                            if (userLoginattempt == prevuserLoginattempt)
                                            {
                                                obj1.message = "ALTLOCKEDUSERNOTIFY " + prevuserLoginattempt + " ALTLOCKEDUSERNOTIFY1";
                                                status = 11;
                                            }
                                        }
                                    }
                                    obj1.status = false;
                                }
                                else if (msg == "ISOBSERVER")
                                {
                                    status = 5;
                                    obj1.status = false;
                                    obj1.message = "ALTOBSNOLOGIN";
                                }
                                else if (msg == "LOCKED")
                                {
                                    obj1.status = false;
                                    obj1.message = "ALTLOCKED";
                                    status = 7;
                                }
                                else if (msg == "NO ROLE ASSIGNED")
                                {
                                    obj1.status = false;
                                    status = 8;
                                    obj1.message = "ALTNOROLE";
                                }
                                else if (msg == "NO ROLE ASSIGNED/SITE ASSIGNED")
                                {
                                    obj1.status = false;
                                    status = 9;
                                    obj1.message = "ALTNOSITEROLE";
                                }
                                else if (msg == "NO SITE ASSIGNED")
                                {
                                    obj1.status = false;
                                    status = 10;
                                    obj1.message = "ALTNOSITE";
                                }
                            }
                            else
                            {
                                obj1.status = false;
                                status = 7;
                                obj1.message = "ALTLOCKED";
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            status = 5;
                            obj1.message = "ALTOBSNOLOGIN";
                        }

                    }
                    else
                    {
                        obj1.status = false;
                        status = 2;
                        obj1.message = "ALTINVALIDLOGIN";
                    }
                    obj1.Data = retresult;
                    try
                    {
                        obj1.rolePermission = ScriptEngine.GetTableRows(dsDashboard.Tables[0]);
                        obj1.dashboard = ScriptEngine.GetTableRows(dsDashboard.Tables[2]);
                        obj1.dupontNews = ScriptEngine.GetTableRows(dsDashboard.Tables[3]);
                        obj1.userDashboardPermission = ScriptEngine.GetTableRows(usrdashboardpermission.Tables[0]);
                    }
                    catch (Exception ex)
                    {
                        obj1.rolePermission = "";
                        obj1.dashboard = "";
                        obj1.dupontNews = "";
                        obj1.userDashboardPermission = "";
                    }

                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
            }
            catch (Exception ex)
            {
                if(ex.Message.Contains("Object reference not set to an"))
                {
                    obj1.message ="Origin Mismatch";
                }
                else
                {
                    obj1.message = ex.Message;
                }
                Log.InfoFormat("Exception Occured for Login Time:" + ex.StackTrace);
                obj1.status = false;
                obj1.Data = null;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
           // Log.InfoFormat("15. Login API End");
            return response;
        }

        [HttpPost]
        [Route("api/Login/UpdateUserAlert")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateUserAlert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    //string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string userid = Convert.ToString(request.SelectToken("userId"));
                    string strquery = ScriptEngine.GetValue("UPDATEUSERSYSALERT");
                    string[] paraminsert = { "USERID" };
                    object[] pValuesinsert = { userid };
                    int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                    obj1.message = "ALTUSERALERTUPDATE";
                    if (iresult != 0 && iresult > 0)
                    {
                        obj1.status = true;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update User Alert:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Forgets the pass.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Login/ForgetPass")]
        public HttpResponseMessage ForgetPass(JObject request)
        {
            DataSet dsuserinfo, dsmaildetails;
            string username, emailid, strquery, validatemsg, lastname, firstname, strmailfrom, strto, strsubject, strbody, strcompanyurl, strreplyto, strcc, strbcc;
            int result1, ischeduleid;
            DateTime senddate;
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            DataSet ds = new DataSet();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                string httpUrl = HttpContext.Current.Request.Headers["Origin"];

                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                string strSettings = sr.ReadToEnd();
                scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();

                string[] urldomain = httpUrl.Split('/');
                URL = urldomain[2];
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                Dictionary<string, object> cmpdata = new Dictionary<string, object>();
                tmpdata = scriptengine_Rdata;
                cmpdata = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key.ToLower() == URL.ToLower()).Value;
                string companyid = cmpdata["COMPANYID"].ToString();
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                username = Convert.ToString(request.SelectToken("userName"));
                emailid = Convert.ToString(request.SelectToken("email"));

                //DataPro configuration setting 
                Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                string strAPPID = dctpartdata["APPID"].ToString();
                string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string ES_WSURL = dctpartdata["WSURL"].ToString();

                string[] parammailvalidate = { "USERNAME", "EMAIL" };
                object[] pValuesmailvalidate = { username, emailid };
                strquery = ScriptEngine.GetValue("VALIDATEUSERNAMEMAILID");
                dsuserinfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parammailvalidate, pValuesmailvalidate);
                //Log.InfoFormat("==Forgot Password API Start==");
                StatusMsgIU pv = new StatusMsgIU();
                if (dsuserinfo.Tables[0].Rows.Count > 0)
                {
                    userid = Convert.ToInt64(dsuserinfo.Tables[0].Rows[0]["USERID"]);
                    validatemsg = dsuserinfo.Tables[0].Rows[0]["MSG"].ToString();
                    //Log.InfoFormat("validatemsg:" + validatemsg);
                    if (validatemsg == "SUCCESS")
                    {
                        lastname = dsuserinfo.Tables[0].Rows[0]["LASTNAME"].ToString();
                        firstname = dsuserinfo.Tables[0].Rows[0]["FIRSTNAME"].ToString();
                        string[] parampwdreset = { "USERID" };
                        object[] pValuespwdreset = { userid };
                        strquery = ScriptEngine.GetValue("RESETFORGOTPWD");
                        dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parampwdreset, pValuespwdreset);
                        if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                        {
                            try
                            {
                                //Log.InfoFormat("==Forgot Password Mail Process Start==");
                                ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                strmailfrom = dsmaildetails.Tables[0].Rows[0]["FROM"].ToString();
                                strreplyto = dsmaildetails.Tables[0].Rows[0]["REPLYTO"].ToString();
                                strto = dsmaildetails.Tables[0].Rows[0]["TO"].ToString();
                                strcc = dsmaildetails.Tables[0].Rows[0]["CC"].ToString();
                                strbcc = dsmaildetails.Tables[0].Rows[0]["BCC"].ToString();
                                strsubject = dsmaildetails.Tables[0].Rows[0]["SUBJECT"].ToString();
                                strbody = dsmaildetails.Tables[0].Rows[0]["BODY"].ToString();
                                senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[0]["SENTDATE"].ToString());
                                strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                String str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + companyid + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                //Log.InfoFormat("JSON String:" + str);
                                object[] objSendNotificationDetails = new object[1];
                                objSendNotificationDetails[0] = str;
                                CallWebServices CWS = new CallWebServices();
                                object objCWS = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                //Log.InfoFormat("WebService Return Value:" + objCWS.ToString());
                                result1 = 1;
                                pv.status = true;
                                pv.message = "ALTPWDRESET";
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for forgot Password:" + ex.StackTrace);
                                result1 = 0;
                                pv.status = false;
                                pv.message = "ALTPWDCHANGEFAIL";
                            }
                        }
                        else
                        {
                            Log.InfoFormat("dsmaildetails value not found");
                            pv.status = false;
                            pv.message = "ALTPWDCHANGEFAIL";
                        }
                    }
                    else if (validatemsg == "INACTIVE")
                    {
                        pv.status = false;
                        result1 = -1;
                        pv.message = "ALTINACTIVEUSER";
                    }
                    else if (validatemsg == "LOCKED")
                    {
                        pv.status = false;
                        result1 = -4;
                        pv.message = "ALTLOCKED";
                    }
                }
                else
                {
                    Log.InfoFormat("User value not found");
                    pv.status = false;
                    result1 = 0;
                    pv.message = "ALTCHECKUSERNAMEMAILID";
                }
                response = Request.CreateResponse(HttpStatusCode.OK, pv);
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured for forgot Password:" + ex.ToString());
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Passwords the complex.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Login/passwordComplex")]
        [APIAuthAttribute]
        public HttpResponseMessage passwordComplex(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsComplex = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETPWDCOMPLEXITY");
                    dsComplex = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (dsComplex.Tables[0].Rows.Count > 0)
                    {
                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(dsComplex.Tables[0]);
                        obj1.status = true;
                        obj1.message = "Success";
                        obj1.Data = list[0];
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured for Get Data password complex:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Channges the password.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Login/channgePassword")]
        public HttpResponseMessage channgePassword(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            DataSet ds = new DataSet();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string oldPass = Convert.ToString(request.SelectToken("oldPass"));
                    string newPass = Convert.ToString(request.SelectToken("newPass"));
                    if (String.IsNullOrEmpty(oldPass) || String.IsNullOrEmpty(newPass))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                        string strquery = ScriptEngine.GetValue("FORCEPWD");
                        string[] paraminsert = { "OLDPASSWORD", "USERID", "PASSWORD" };
                        object[] pValuesinsert = { oldPass, userid, newPass };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult == 2)
                        {
                            obj1.status = true;
                            obj1.message = "ALTPWDCHANGED";
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTOLDPWDINCORRECT";
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTPWDCHANGEFAIL";
                        }

                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured for change Password:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the table rows.
        /// </summary>
        /// <param name="dtTokenValData">The dt token value data.</param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetTableRows(DataTable dtTokenValData)
        {
            List<Dictionary<string, object>>
            lstRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> dictRow = null;
            foreach (DataRow dr in dtTokenValData.Rows)
            {
                dictRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtTokenValData.Columns)
                {
                    dictRow.Add(col.ColumnName, dr[col]);
                }
                lstRows.Add(dictRow);
            }
            return lstRows;
        }

        /// <summary>
        /// Generateands the save key.
        /// </summary>
        /// <param name="clientkeys">The clientkeys.</param>
        /// <returns></returns>
        [NonAction]
        public string GenerateandSaveKey(AuthUser clientkeys)
        {
            var IssuedOn = DateTime.Now;
            NewToken token = new NewToken();
            token.ClientID = clientkeys.ClientID;
            token.pass = clientkeys.pass;
            token.ClientIP = clientkeys.ClientIP;
            token.CreateOn = IssuedOn;
            token.OS_Version = clientkeys.OS_Version;
            token.ClientSecret = sec_helper.GenerateToken(clientkeys, IssuedOn);
            return token.ClientSecret;
        }

        #endregion

    }
}
