﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// SiteSubAreaController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SiteSubAreaController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteSubAreaController"/> class.
        /// </summary>
        public SiteSubAreaController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(SiteSubAreaController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the BaseAdapter.
        /// </summary>
        /// <value>
        /// The BaseAdapter.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        /// <summary>
        /// Gets or sets the model subarea.
        /// </summary>
        /// <value>
        /// The model subarea.
        /// </value>
        //public SiteSubArea ModelSubarea { get; set; }

        #endregion

        #region  Action methods

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string areaID = Convert.ToString(request.SelectToken("areaIds"));
                    string subAreaName = Convert.ToString(request.SelectToken("subAreaName"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(subAreaName))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTSUBAREA");
                        string[] paraminsert = { "SUBAREANAME", "STATUS", "CREATEDBY" };
                        object[] pValuesinsert = { subAreaName, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            int subareaid = iresult;
                            strquery = ScriptEngine.GetValue("UPDATEAREASUBAREASSIGN");
                            string[] paraminsert1 = { "AREAID", "SUBAREAID", "USERID" };
                            object[] pValuesinsert1 = { areaID, subareaid, CREATEDBY };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            obj1.status = true;
                            obj1.message = "ALTSUBAREACREATED";
                            obj1.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSUBAREAEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSUBAREACREATEPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert SubArea:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string subAreaName = Convert.ToString(request.SelectToken("subAreaName"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaID = Convert.ToString(request.SelectToken("areaIds"));
                    if (String.IsNullOrEmpty(subAreaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESUBAREA");
                        string[] paraminsert = { "SUBAREANAME", "SUBAREAID", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { subAreaName, subAreaId, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("UPDATEAREASUBAREASSIGN");
                            string[] paraminsert1 = { "AREAID", "SUBAREAID", "USERID" };
                            object[] pValuesinsert1 = { areaID, subAreaId, CREATEDBY };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            obj1.status = true;
                            obj1.message = "ALTSUBZONENAMEUPDATESUCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSUBAREAEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update SubArea:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(subAreaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESUBAREA");
                        string[] paraminsert = { "USERID", "SUBAREAID" };
                        object[] pValuesinsert = { userid, subAreaId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete SubArea:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string userID = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(subAreaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESTATUSSUBAREAS");
                        string[] paraminsert = { "SUBAREAID", "USERID" };
                        object[] pValuesinsert = { subAreaId, userID };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update status SubArea:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Gets the site data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/GetSiteData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetSiteData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            ParameterSubAreaAssign obj = new ParameterSubAreaAssign();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAssign = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (!String.IsNullOrEmpty(subAreaId))
                    {
                        strquery = ScriptEngine.GetValue("GETSUBAREAGENERALINFO");
                        string[] paraminsert = { "SUBAREAID" };
                        object[] pValuesinsert = { subAreaId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        strquery = ScriptEngine.GetValue("GETAREAASSIGN");
                        dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);


                        DataTable dAvailSort = dsAssign.Tables[1].Copy();
                        dAvailSort.Merge(dsAssign.Tables[0]);
                        if (dsAssign.Tables[2].Rows.Count == 0)
                        {
                            DataRow dr = dsAssign.Tables[2].NewRow();
                            dr[0] = "";
                            dsAssign.Tables[2].Rows.Add(dr);
                        }

                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dAvailSort);
                            List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssign.Tables[2]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.Data = lstData[0];
                            obj.available = lstAvail;
                            obj.assign = lstAssign[0]["areaIds"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        ParameterData ob = new ParameterData();
                        strquery = ScriptEngine.GetValue("GETSUBAREALIST");
                        string[] paraminsert = { "SUBAREAID" };
                        object[] pValuesinsert = { "" };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            ob.status = true;
                            ob.message = "Success";
                            ob.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, ob);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get SubArea Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Adds the sub area data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/addSubAreaData")]
        [APIAuthAttribute]
        public HttpResponseMessage addSubAreaData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAssign = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string[] paraminsert = { "SUBAREAID" };
                    object[] pValuesinsert = { "" };
                    strquery = ScriptEngine.GetValue("GETAREAASSIGN");
                    dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (dsAssign.Tables[0].Rows.Count != 0)
                    {
                        ParameterSubArea ob = new ParameterSubArea();
                        List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dsAssign.Tables[0]);
                        ob.status = true;
                        ob.message = "Success";
                        ob.available = lstAvail;
                        response = Request.CreateResponse(HttpStatusCode.OK, ob);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        /// <summary>
        /// Updates the sub area assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteSubArea/updateSubAreaAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateSubAreaAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string assignIds = Convert.ToString(request.SelectToken("assignIds"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    if (String.IsNullOrEmpty(subAreaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEAREASUBAREASSIGN");
                        string[] paraminsert = { "AREAID", "SUBAREAID", "USERID" };
                        object[] pValuesinsert = { assignIds, subAreaId, USERID };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTAREAASSGNSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTAREAASSGNFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update SubArea assign:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
    }
}