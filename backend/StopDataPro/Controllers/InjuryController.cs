﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// Injury Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class InjuryController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InjuryController"/> class.
        /// </summary>
        public InjuryController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(InjuryController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the injury.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/getInjury")]
        [APIAuthAttribute]
        public HttpResponseMessage getInjury(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string defaultsite = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string injuryId = Convert.ToString(request.SelectToken("injuryId"));
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string cleraFilter = Convert.ToString(request.SelectToken("clearFilter"));
                    if (String.IsNullOrEmpty(injuryId))
                    {
                        if (SITEID != "")
                        {
                            SITEID = SITEID;
                        }
                        else if (cleraFilter == "0")
                        {
                            SITEID = "";
                        }
                        else
                        {
                            SITEID = defaultsite;
                        }
                        strquery = ScriptEngine.GetValue("GETFILTERBASEDINJURYSTATSLISTING");
                        string[] paraminsert = { "USERID", "SITEID" };
                        object[] pValuesinsert = { userId, SITEID };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        ParameterInjuryListData obj = new ParameterInjuryListData();
                        string sitequery = "WASITES";
                        DataSet dssite = getValueTable(sitequery, userId, languageId, SITEID, defaultRoleId);
                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dssite.Tables[0]);
                        obj.defaultSiteId = int.Parse(defaultsite);
                        if (list.Count > 0)
                        {
                            obj.status = true;
                            obj.message = "Success";
                            obj.generalData = list;
                        }
                        else
                        {
                            obj.status = false;
                            obj.message = "FMKNORECS";
                            obj.generalData = new string[] { };
                        }
                        obj.filterSites = listSite;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        DataSet dsyear = new DataSet();
                        DataSet dsavail = new DataSet();
                        DataSet dsassign = new DataSet();
                        strquery = ScriptEngine.GetValue("GETINJURYSTATSGENERALINFO");
                        string[] paraminsert = { "INJURYID" };
                        object[] pValuesinsert = { injuryId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            strquery = ScriptEngine.GetValue("GETYEAR");
                            dsyear = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                            strquery = ScriptEngine.GetValue("GETAVIALASSIGNSITESROLEBASED");
                            string[] paraminsert1 = { "USERID", "ROLEID", "INJURYID" };
                            object[] pValuesinsert1 = { userId, defaultRoleId, injuryId };
                            dsavail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                            DataTable dtAvail = dsavail.Tables[1].Copy();
                            dtAvail.Merge(dsavail.Tables[0]);
                            if (dsavail.Tables[2].Rows.Count == 0)
                            {
                                DataRow dr = dsavail.Tables[2].NewRow();
                                dr[0] = "";
                                dsavail.Tables[2].Rows.Add(dr);
                            }
                            ParameterInjuryList obj = new ParameterInjuryList();
                            List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> listavail = ScriptEngine.GetTableRows(dtAvail);
                            List<Dictionary<string, object>> listassign = ScriptEngine.GetTableRows(dsavail.Tables[2]);
                            List<Dictionary<string, object>> listyear = ScriptEngine.GetTableRows(dsyear.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.generalData = list[0];
                            obj.avail = listavail;
                            obj.assign = listassign[0]["siteIds"];
                            obj.year = listyear;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Injury Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the injury.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/deleteInjury")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteInjury(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string INJURYID = Convert.ToString(request.SelectToken("injuryId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(INJURYID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEINJURYSTATS");
                        string[] paraminsert = { "INJURYID", "UPDATEDBY" };
                        object[] pValuesinsert = { INJURYID, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete Injury  Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/changeStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage changeStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string INJURYID = Convert.ToString(request.SelectToken("injuryId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(INJURYID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEINJURYSTATSTATUS");
                        string[] paraminsert = { "INJURYID", "UPDATEDBY" };
                        object[] pValuesinsert = { INJURYID, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Change Status Injury:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the injury add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/getInjuryAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getInjuryAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsyear = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    strquery = ScriptEngine.GetValue("GETAVIALASSIGNSITESROLEBASED");
                    if (String.IsNullOrEmpty(SITEID))
                    {
                        SITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    }
                    string[] paraminsert = { "USERID", "ROLEID", "INJURYID" };
                    object[] pValuesinsert = { userId, defaultRoleId, "0" };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    //if (ds.Tables[0].Rows.Count != 0)
                    //{
                        ParameterInjuryListAdd obj = new ParameterInjuryListAdd();
                        strquery = ScriptEngine.GetValue("GETYEAR");
                        dsyear = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                        List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dsyear.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.sites = list;
                        obj.year = listSite;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    //}
                    //else
                    //{
                    //    obj1.status = false;
                    //    obj1.message = "FMKNORECS";
                    //    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    //}
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Injury data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/insert")]
        [APIAuthAttribute]
        public HttpResponseMessage insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string workHour = Convert.ToString(request.SelectToken("workHour"));
                    string injuries = Convert.ToString(request.SelectToken("injuries"));
                    string year = Convert.ToString(request.SelectToken("year"));
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    if (String.IsNullOrEmpty(workHour) || String.IsNullOrEmpty(injuries) || String.IsNullOrEmpty(year) || String.IsNullOrEmpty(siteIds) || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERINJURYSTATSGENERALINFO");
                        string[] paraminsert = { "COMPANYID", "STATUS", "WORKEDHOURS", "INJURIES", "INJURYYEAR", "CREATEDBY", "SITEID" };
                        object[] pValuesinsert = { compId, status, workHour, injuries, year, userId, siteIds };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTINJURYSTATSCREATED";
                            obj1.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Injury:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Injury/update")]
        [APIAuthAttribute]
        public HttpResponseMessage update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string workHour = Convert.ToString(request.SelectToken("workHour"));
                    string injuries = Convert.ToString(request.SelectToken("injuries"));
                    string year = Convert.ToString(request.SelectToken("year"));
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string injuryId = Convert.ToString(request.SelectToken("injuryId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    if (String.IsNullOrEmpty(injuryId) || String.IsNullOrEmpty(workHour) || String.IsNullOrEmpty(injuries) || String.IsNullOrEmpty(year) || String.IsNullOrEmpty(siteIds) || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEINJURYSTATSGENERALINFO");
                        string[] paraminsert = { "INJURYID", "STATUS", "WORKEDHOURS", "INJURIES", "INJURYYEAR", "UPDATEDBY", "SITEID", "CREATEDBY" };
                        object[] pValuesinsert = { injuryId, status, workHour, injuries, year, userId, siteIds, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTINJURYSTATSUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Injury:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion

    }
}

