﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// RedFlag Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class RedFlagController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="RedFlagController"/> class.
        /// </summary>
        /// 
        public RedFlagController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(RedFlagController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the red flag list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagList")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string defaultsiteId = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string statusId = Convert.ToString(request.SelectToken("statusId"));
                    string cleraFilter = Convert.ToString(request.SelectToken("clearFilter"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();

                    if (SITEID != "")
                    {
                        SITEID = SITEID;
                    }
                    else if (cleraFilter == "0")
                    {
                        SITEID = "";
                    }
                    else
                    {
                        SITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    }

                    strquery = ScriptEngine.GetValue("GETREDFLAGLISTING");
                    string[] paraminsert = { "USERID", "SITEID", "AREAID", "SUBAREAID", "STATUSID" };
                    object[] pValuesinsert = { userId, SITEID, areaId, subAreaId, statusId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    ParameterValuesForRedList obj = new ParameterValuesForRedList();
                    string sitequery = "WASITES";
                    DataSet dssite = getValueTable(sitequery, userId, languageId, SITEID, defaultRoleId, areaId);
                    string areaquery = "WAAREAS";
                    DataSet dsarea = getValueTable(areaquery, userId, languageId, SITEID, defaultRoleId, areaId);
                    if (subAreaEnable == "1")
                    {
                        string subareaquery = "WACCESSSUBAREAS";
                        DataSet dssubarea = getValueTable(subareaquery, userId, languageId, SITEID, defaultRoleId, areaId);
                        obj.filterSubArea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                    }
                    else
                    {
                        obj.filterSubArea = new string[] { };
                        ds.Tables[0].Columns.Remove("SUBAREAS");
                    }
                    string statusquery = "WAACCESSSETUPSTATUS";
                    DataSet dsstatus = getValueTable(statusquery, userId, languageId, SITEID, defaultRoleId, areaId);
                    List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                    List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dssite.Tables[0]);
                    List<Dictionary<string, object>> listArea = ScriptEngine.GetTableRows(dsarea.Tables[0]);
                    List<Dictionary<string, object>> listStatus = ScriptEngine.GetTableRows(dsstatus.Tables[0]);
                    obj.defaultSiteId = int.Parse(defaultsiteId);
                    if (list.Count > 0)
                    {
                        obj.message = "Success";
                        obj.status = true;
                        obj.generalData = list;
                    }
                    else
                    {
                        obj.message = "FMKNORECS";
                        obj.status = false;
                        obj.generalData = new string[] { };
                    }
                    obj.filterSites = listSite;
                    obj.filterArea = listArea;
                    //obj.filterSubArea = listSub;
                    obj.filterStatus = listStatus;
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Dashboard Info data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(siteId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WAAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert = { "USERID", "SITEID", "ROLEID" };
                        object[] pValuesinsert = { userId, siteId, defaultRoleId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterObsArea obj = new ParameterObsArea();
                            obj.status = true;
                            obj.message = "Success";
                            obj.area = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Area Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the sub area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getSubAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WACCESSSUBAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "AREAID" };
                        object[] pValuesinsert2 = { areaId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterObsSubAreaList obj = new ParameterObsSubAreaList();
                            obj.status = true;
                            obj.message = "Success";
                            obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Sub Area Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/delete")]
        [APIAuthAttribute]
        public HttpResponseMessage delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string redflagId = Convert.ToString(request.SelectToken("redflagId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(redflagId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEREDFLAG");
                        string[] paraminsert = { "REDFLAGID", "UPDATEDBY" };
                        object[] pValuesinsert = { redflagId, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete RedFlag:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/changeStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage changeStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string redflagId = Convert.ToString(request.SelectToken("redflagId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(redflagId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEREDFLAGSTATUS");
                        string[] paraminsert = { "REDFLAGID", "UPDATEDBY" };
                        object[] pValuesinsert = { redflagId, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When change Status RedFlag:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslabel = new DataSet();
                DataSet dssite = new DataSet();
                DataSet dsmain = new DataSet();
                DataSet dssub = new DataSet();
                DataSet dssubarea = new DataSet();
                DataSet dschklstobs = new DataSet();
                DataSet dsredflag = new DataSet();
                DataSet dschklstflg = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(SITEID))
                    {
                        SITEID = siteIdd;
                    }

                    string[] paraminsert = { "LANGUAGEID", "USERID", "MODE", "AREAID" };
                    object[] pValuesinsert = { languageId, userId, "0", "-1" };
                    strquery = ScriptEngine.GetValue("GETCUSTOMLABELBYID");
                    dslabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("GETCHECKLISTSITELIST");
                    dssite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("GETMAINCATEGORYFORREDFLAG");
                    dsmain = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("GETSUBCATEGORYFORREDFLAG");
                    dssub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("GETSUBAREALISTSFORCOMBO");
                    dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    string[] paraminsert1 = { "MODE", "AREAID", "REDFLAGID", "SITEID" };
                    object[] pValuesinsert1 = { "0", "-1", "", SITEID };
                    strquery = ScriptEngine.GetValue("GETCHECKLISTOBSERVERSFORREDFLAG");
                    dschklstobs = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                    strquery = ScriptEngine.GetValue("GETREDFLAGAREALIST");
                    dsredflag = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                    strquery = ScriptEngine.GetValue("GETCHECKLISTSETUPFORREDFLAG");
                    dschklstflg = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                    ParameterValuesForRedAdd obj = new ParameterValuesForRedAdd();
                    List<Dictionary<string, object>> listsite = ScriptEngine.GetTableRows(dssite.Tables[0]);
                    List<Dictionary<string, object>> listmain = ScriptEngine.GetTableRows(dsmain.Tables[0]);
                    List<Dictionary<string, object>> listsub = ScriptEngine.GetTableRows(dssub.Tables[0]);
                    List<Dictionary<string, object>> listsubarea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                    List<Dictionary<string, object>> listchkobs = ScriptEngine.GetTableRows(dschklstobs.Tables[0]);
                    List<Dictionary<string, object>> listredflag = ScriptEngine.GetTableRows(dsredflag.Tables[0]);
                    List<Dictionary<string, object>> listchklstflg = ScriptEngine.GetTableRows(dschklstflg.Tables[0]);
                    obj.status = true;
                    obj.message = "Success";
                    obj.sites = listsite;
                    obj.observer = listchkobs;
                    obj.area = listredflag;
                    obj.subarea = listsubarea;
                    obj.checklist = listchklstflg;
                    obj.maincat = listmain;
                    obj.subcat = listsub;
                    obj.defaultSiteId = int.Parse(siteIdd.ToString());
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag site.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagSite")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagSite(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslabel = new DataSet();
                DataSet dschklstobs = new DataSet();
                DataSet dsredflag = new DataSet();
                DataSet dschklstflg = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    if (String.IsNullOrEmpty(SITEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "LANGUAGEID", "USERID", "MODE", "AREAID" };
                        object[] pValuesinsert = { languageId, userId, "0", "-1" };

                        strquery = ScriptEngine.GetValue("GETCUSTOMLABELBYID");
                        dslabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        string[] paraminsert1 = { "MODE", "REDFLAGID", "SITEID" };
                        object[] pValuesinsert1 = { "0", "", SITEID };
                        strquery = ScriptEngine.GetValue("GETCHECKLISTOBSERVERSFORREDFLAG");
                        dschklstobs = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETREDFLAGAREALIST");
                        dsredflag = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETCHECKLISTSETUPFORREDFLAG");
                        dschklstflg = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        ParameterValuesForRedSite obj = new ParameterValuesForRedSite();
                        List<Dictionary<string, object>> listchkobs = ScriptEngine.GetTableRows(dschklstobs.Tables[0]);
                        List<Dictionary<string, object>> listredflag = ScriptEngine.GetTableRows(dsredflag.Tables[0]);
                        List<Dictionary<string, object>> listchklstflg = ScriptEngine.GetTableRows(dschklstflg.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.observer = listchkobs;
                        obj.area = listredflag;
                        obj.checklist = listchklstflg;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);

                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Red Flag site:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagArea")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagArea(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslabel = new DataSet();
                DataSet dssubarea = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "LANGUAGEID", "USERID", "MODE", "AREAID" };
                        object[] pValuesinsert = { languageId, userId, "0", areaId };

                        strquery = ScriptEngine.GetValue("GETCUSTOMLABELBYID");
                        dslabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETSUBAREALISTSFORCOMBO");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        ParameterObsSubAreaList obj = new ParameterObsSubAreaList();
                        if (dssubarea.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> listsubarea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.subArea = listsubarea;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Area:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagChecklist")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagChecklist(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslabel = new DataSet();
                DataSet dsmain = new DataSet();
                DataSet dssub = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string checklistId = Convert.ToString(request.SelectToken("checklistId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    if (String.IsNullOrEmpty(checklistId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "LANGUAGEID", "USERID", "MODE" };
                        object[] pValuesinsert = { languageId, userId, "0" };

                        strquery = ScriptEngine.GetValue("GETCUSTOMLABELBYID");
                        dslabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        string[] paraminsert1 = { "LANGUAGEID", "USERID", "MODE", "CHECKLISTSETUPID" };
                        object[] pValuesinsert1 = { languageId, userId, "0", checklistId };
                        string[] paraminsert2 = { "LANGUAGECODE", "USERID", "MODE", "CHECKLISTSETUPID", "MAINCATEGORYID", "REDFLAGID" };
                        object[] pValuesinsert2 = { languageId, userId, "0", checklistId, "-1", "" };
                        strquery = ScriptEngine.GetValue("GETMAINCATEGORYFORSETUP");
                        dsmain = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETSUBCATEGORYLISTFORMAINCATEGORYREDFLAG");
                        dssub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);

                        ParameterValuesForChecklist obj = new ParameterValuesForChecklist();
                        List<Dictionary<string, object>> listMain = ScriptEngine.GetTableRows(dsmain.Tables[0]);
                        List<Dictionary<string, object>> listSub = ScriptEngine.GetTableRows(dssub.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.mainCat = listMain;
                        obj.subCat = listSub;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Redflag CheckList:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag main categ.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagMainCateg")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagMainCateg(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslabel = new DataSet();
                DataSet dsmain = new DataSet();
                DataSet dssub = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string mainCatId = Convert.ToString(request.SelectToken("mainCatId"));
                    string checklistId = Convert.ToString(request.SelectToken("checklistId"));
                    string redFlagId = Convert.ToString(request.SelectToken("redFlagId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    if (String.IsNullOrEmpty(checklistId) || String.IsNullOrEmpty(mainCatId))
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(mode))
                        {
                            mode = "0";
                        }
                        string[] paraminsert = { "LANGUAGEID", "USERID", "MODE" };
                        object[] pValuesinsert = { languageId, userId, mode };

                        strquery = ScriptEngine.GetValue("GETCUSTOMLABELBYID");
                        dslabel = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        string[] paraminsert2 = { "LANGUAGECODE", "USERID", "MODE", "CHECKLISTSETUPID", "MAINCATEGORYID", "REDFLAGID" };
                        object[] pValuesinsert2 = { languageId, userId, mode, checklistId, mainCatId, redFlagId };
                        strquery = ScriptEngine.GetValue("GETSUBCATEGORYLISTFORMAINCATEGORYREDFLAG");
                        dssub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        ParameterValuesForMainCat obj = new ParameterValuesForMainCat();
                        if (dssub.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> listSub = ScriptEngine.GetTableRows(dssub.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.subCat = listSub;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Redflag MainCategory:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/insert")]
        [APIAuthAttribute]
        public HttpResponseMessage insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string flagName = Convert.ToString(request.SelectToken("flagName"));
                    string observeId = Convert.ToString(request.SelectToken("observeId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subareaId = Convert.ToString(request.SelectToken("subareaId"));
                    string mainCatID = Convert.ToString(request.SelectToken("mainCatID"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string subCatID = Convert.ToString(request.SelectToken("subCatID"));
                    string matric = Convert.ToString(request.SelectToken("matric"));
                    string trigger = Convert.ToString(request.SelectToken("trigger"));
                    string limit = Convert.ToString(request.SelectToken("limit"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    if (String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(flagName) || String.IsNullOrEmpty(observeId) || String.IsNullOrEmpty(areaId) || String.IsNullOrEmpty(subareaId) ||
                        String.IsNullOrEmpty(checkListId) || String.IsNullOrEmpty(mainCatID) || String.IsNullOrEmpty(subCatID) || String.IsNullOrEmpty(matric) || String.IsNullOrEmpty(trigger) ||
                        String.IsNullOrEmpty(limit) || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTREDFLAG");
                        string[] paraminsert = { "COMPANYID", "REDFLAGNAME", "SITEID", "MAINCATEGORYID", "SUBCATEGORYID", "AREAID", "SUBAREAID", "OBSERVERID", "METRIC", "TRIGGER", "VALUE", "CARDVERSIONID", "STATUS", "CREATEDBY" };
                        object[] pValuesinsert = { compId, flagName, siteId, mainCatID, subCatID, areaId, subareaId, observeId, matric, trigger, limit, checkListId, status, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTREDFLAGSUCESS";
                            obj1.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTREDFLAGNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Redflag Insert:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the red flag data edit.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/getRedFlagDataEdit")]
        [APIAuthAttribute]
        public HttpResponseMessage getRedFlagDataEdit(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsData = new DataSet();
                DataSet dsmain = new DataSet();
                DataSet dssub = new DataSet();
                DataSet dssubarea = new DataSet();
                DataSet dschklstobs = new DataSet();
                DataSet dsredflag = new DataSet();
                DataSet dschklstflg = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string redFlagId = Convert.ToString(request.SelectToken("redFlagId"));
                    if (String.IsNullOrEmpty(redFlagId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert3 = { "REDFLAGID" };
                        object[] pValuesinsert3 = { redFlagId };
                        strquery = ScriptEngine.GetValue("GETREDFLAGGENERALINFO");
                        dsData = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert3, pValuesinsert3);

                        string siteid = dsData.Tables[0].Rows[0]["SITEID"].ToString();
                        string[] paraminsert = { "MODE", "REDFLAGID", "SITEID" };
                        object[] pValuesinsert = { "1", redFlagId, siteid };

                        strquery = ScriptEngine.GetValue("GETCHECKLISTOBSERVERSFORREDFLAG");
                        dschklstobs = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETREDFLAGAREALIST");
                        dsredflag = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETCHECKLISTSETUPFORREDFLAG");
                        dschklstflg = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        string chksetupid = dsData.Tables[0].Rows[0]["CHECKLISTSETUPID"].ToString();
                        string mainCat = dsData.Tables[0].Rows[0]["MAINCATEGORYID"].ToString();
                        string areaId = dsData.Tables[0].Rows[0]["AREAID"].ToString();

                        string[] paraminsert1 = { "MODE", "REDFLAGID", "SITEID", "CHECKLISTSETUPID", "LANGUAGEID", "MAINCATEGORYID", "LANGUAGECODE", "AREAID" };
                        object[] pValuesinsert1 = { "1", redFlagId, siteid, chksetupid, languageId, mainCat, languageId, areaId };

                        strquery = ScriptEngine.GetValue("GETMAINCATEGORYFORSETUP");
                        dsmain = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETSUBCATEGORYLISTFORMAINCATEGORYREDFLAG");
                        dssub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETSUBAREALISTSFORCOMBO");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        ParameterValuesForRedEdit obj = new ParameterValuesForRedEdit();
                        List<Dictionary<string, object>> listdata = ScriptEngine.GetTableRows(dsData.Tables[0]);
                        List<Dictionary<string, object>> listmain = ScriptEngine.GetTableRows(dsmain.Tables[0]);
                        List<Dictionary<string, object>> listsub = ScriptEngine.GetTableRows(dssub.Tables[0]);
                        List<Dictionary<string, object>> listsubarea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                        List<Dictionary<string, object>> listchkobs = ScriptEngine.GetTableRows(dschklstobs.Tables[0]);
                        List<Dictionary<string, object>> listredflag = ScriptEngine.GetTableRows(dsredflag.Tables[0]);
                        List<Dictionary<string, object>> listchklstflg = ScriptEngine.GetTableRows(dschklstflg.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.generalData = listdata;
                        obj.observer = listchkobs;
                        obj.area = listredflag;
                        obj.subarea = listsubarea;
                        obj.checklist = listchklstflg;
                        obj.maincat = listmain;
                        obj.subcat = listsub;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Redflag Data for Edit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/RedFlag/update")]
        [APIAuthAttribute]
        public HttpResponseMessage update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string redFlagId = Convert.ToString(request.SelectToken("redId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string flagName = Convert.ToString(request.SelectToken("flagName"));
                    string observeId = Convert.ToString(request.SelectToken("observeId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subareaId = Convert.ToString(request.SelectToken("subareaId"));
                    string mainCatID = Convert.ToString(request.SelectToken("mainCatID"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string subCatID = Convert.ToString(request.SelectToken("subCatID"));
                    string matric = Convert.ToString(request.SelectToken("matric"));
                    string trigger = Convert.ToString(request.SelectToken("trigger"));
                    string limit = Convert.ToString(request.SelectToken("limit"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    if (String.IsNullOrEmpty(redFlagId) || String.IsNullOrEmpty(flagName) || String.IsNullOrEmpty(observeId) || String.IsNullOrEmpty(areaId) || String.IsNullOrEmpty(subareaId) ||
                        String.IsNullOrEmpty(checkListId) || String.IsNullOrEmpty(mainCatID) || String.IsNullOrEmpty(subCatID) || String.IsNullOrEmpty(matric) || String.IsNullOrEmpty(trigger) ||
                        String.IsNullOrEmpty(limit) || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEREDFLAG");
                        string[] paraminsert = { "REDFLAGID", "REDFLAGNAME", "MAINCATEGORYID", "SUBCATEGORYID", "AREAID", "SUBAREAID", "OBSERVERID", "METRIC", "TRIGGER", "VALUE", "CARDVERSIONID", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { redFlagId, flagName, mainCatID, subCatID, areaId, subareaId, observeId, matric, trigger, limit, checkListId, status, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTREDFLAGUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTREDFLAGNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When  Redflag Update:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId, string areaId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID", "AREAID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId, areaId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion
        
    }
}

