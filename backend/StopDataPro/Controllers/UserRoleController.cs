﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// UserRoleController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class UserRoleController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRoleController"/> class.
        /// </summary>
        public UserRoleController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(UserRoleController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string ROLENAME = Convert.ToString(request.SelectToken("roleName"));
                    string COPYFROM = Convert.ToString(request.SelectToken("copyFrom"));
                    string DESCRIPTION = Convert.ToString(request.SelectToken("description"));
                    if (String.IsNullOrEmpty(ROLENAME) || String.IsNullOrEmpty(COPYFROM))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (COPYFROM == "0" || COPYFROM == "")
                        {
                            COPYFROM = "-1";
                        }
                        strquery = ScriptEngine.GetValue("INSERTROLES");
                        string[] paraminsert = { "COMPANYID", "ROLENAME", "DESCRIPTION", "STATUS", "CREATEDBY", "COPYFROM" };
                        object[] pValuesinsert = { COMPANYID, ROLENAME.Trim(), DESCRIPTION.Trim(), status, userid, COPYFROM };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            StatusMsgId obj = new StatusMsgId();
                            obj.status = true;
                            obj.message = "ALTROLECREATED";
                            obj.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLECREATE";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert Role:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/delete")]
        [APIAuthAttribute]
        public HttpResponseMessage delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            StatusMsgIUR obj2 = new StatusMsgIUR();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(roleID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEROLES");
                        string[] paraminsert = { "ROLEID", "UPDATEDBY" };
                        object[] pValuesinsert = { roleID, userid, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult > 1)
                        {
                            obj2.defaultRole = 0;
                            obj2.status = true;
                            obj2.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else if (iresult == 1)
                        {
                            obj2.defaultRole = 1;
                            obj2.status = false;
                            obj2.message = "ALTDELDEFROLETYPE";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Role:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the role.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/updateRole")]
        [APIAuthAttribute]
        public HttpResponseMessage updateRole(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string ROLENAME = Convert.ToString(request.SelectToken("roleName"));
                    string DESCRIPTION = Convert.ToString(request.SelectToken("description"));
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEROLES");
                        string[] paraminsert = { "ROLEID", "ROLENAME", "DESCRIPTION", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { ROLEID, ROLENAME.Trim(), DESCRIPTION.Trim(), status, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTROLEPERMISSION";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEEXIST";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEEDITPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Role:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/updateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage updateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            StatusMsgIUR obj2 = new StatusMsgIUR();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(roleID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEROLESSTATUS");
                        string[] paraminsert = { "ROLEID", "UPDATEDBY" };
                        object[] pValuesinsert = { roleID, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult > 0 && iresult != 2) 
                        {
                            obj2.defaultRole = 0;
                            obj2.status = true;
                            obj2.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else if (iresult == 2)
                        {
                            obj2.defaultRole = 1;
                            obj2.status = false;
                            obj2.message = "ALTCHGSTATUSDEFROLE";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj2);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When change status Role:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the role data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/getRoleData")]
        [APIAuthAttribute]
        public HttpResponseMessage getRoleData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsDefaultRole = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string defaultRole = Convert.ToString(request.SelectToken("defaultRole"));

                    if (!String.IsNullOrEmpty(ROLEID))
                    {
                        strquery = ScriptEngine.GetValue("GETROLEINFO");
                        string[] paraminsert = { "ROLEID" };
                        object[] pValuesinsert = { ROLEID };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else if (!String.IsNullOrEmpty(defaultRole))
                    {
                        strquery = ScriptEngine.GetValue("GETDEFAULTROLE");
                        dsDefaultRole = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                        if (dsDefaultRole.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(dsDefaultRole.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETROLESLIST");
                        string[] paraminsert = { "", "" };
                        object[] pValuesinsert = { };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterValuesRoleList obj = new ParameterValuesRoleList();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Role Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the role assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/getRoleAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage getRoleAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ROLEID))
                        {
                            strquery = ScriptEngine.GetValue("GETUSERSLISTROLES");
                            string[] paraminsert = { "ROLEID", "USERID" };
                            object[] pValuesinsert = { ROLEID, userid };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            DataTable dtTokenValAll = ds.Tables[1].Copy();
                            dtTokenValAll.Merge(ds.Tables[0]);
                            ParameterValuesRole obj = new ParameterValuesRole();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(dtTokenValAll);
                            if (ds.Tables[2].Rows.Count == 0)
                            {
                                DataRow dr = ds.Tables[2].NewRow();
                                dr[0] = "";
                                ds.Tables[2].Rows.Add(dr);
                            }
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[2]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.avail = lstPersons;
                            obj.assign = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Role assign Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the role assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/UpdateRoleAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateRoleAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();

                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string selectuserid = Convert.ToString(request.SelectToken("userIds"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEROLEASSIGNMENT");
                        string[] paraminsert = { "USERID", "ROLEID", "UPDATEDBY" };
                        object[] pValuesinsert = { selectuserid, ROLEID, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTROLEUSER";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEUSERFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Role assing Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the role report.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/getRoleReport")]
        [APIAuthAttribute]
        public HttpResponseMessage getRoleReport(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ROLEID))
                        {
                            strquery = ScriptEngine.GetValue("GETREPORTLISTROLES");
                            string[] paraminsert = { "ROLEID" };
                            object[] pValuesinsert = { ROLEID };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if (ds.Tables[0].Rows.Count != 0 || ds.Tables[1].Rows.Count != 0)
                        {
                            DataTable dtTokenValAll = ds.Tables[1].Copy();
                            dtTokenValAll.Merge(ds.Tables[0]);
                            if (ds.Tables[2].Rows.Count == 0)
                            {
                                DataRow dr = ds.Tables[2].NewRow();
                                dr[0] = "";
                                ds.Tables[2].Rows.Add(dr);
                            }
                            ParameterValuesRole obj = new ParameterValuesRole();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(dtTokenValAll);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[2]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.avail = lstPersons;
                            obj.assign = lstPersons1;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Role Report:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the role report.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/updateRoleReport")]
        [APIAuthAttribute]
        public HttpResponseMessage updateRoleReport(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string reportID = Convert.ToString(request.SelectToken("reportIds"));
                    if (String.IsNullOrEmpty(ROLEID) && String.IsNullOrEmpty(reportID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEROLEREPORTASSIGMENT");
                        string[] paraminsert = { "ROLEID", "UPDATEDBY", "REPORTID" };
                        object[] pValuesinsert = { ROLEID, userid, reportID };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTROLEREPORT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEREPORTFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Role Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the role permission.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/getRolePermission")]
        [APIAuthAttribute]
        public HttpResponseMessage getRolePermission(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(ROLEID))
                        {
                            strquery = ScriptEngine.GetValue("GETROLEPERMISSIONALL");
                            string[] paraminsert = { "ROLEID" };
                            object[] pValuesinsert = { ROLEID };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Role permission Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the role permission.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserRole/updateRolePermission")]
        [APIAuthAttribute]
        public HttpResponseMessage updateRolePermission(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string ROLEID = Convert.ToString(request.SelectToken("roleId"));
                    string perId = Convert.ToString(request.SelectToken("permissionId"));
                    if (String.IsNullOrEmpty(ROLEID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEROLEPERMISSION");
                        string[] paraminsert = { "PERMISSIONID", "ROLEID", "UPDATEDBY" };
                        object[] pValuesinsert = { perId, ROLEID, userid };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTROLEPERMISSION";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTROLEPERMISSIONFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update permission Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
        
    }
}
