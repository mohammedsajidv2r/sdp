﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;
using StopDataPro.Services.Models;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// SiteAreaController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class SiteAreaController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteAreaController"/> class.
        /// </summary>
        public SiteAreaController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(SiteAreaController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the infoconfig.
        /// </summary>
        /// <value>
        /// The infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action methods

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId statusObj = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];

                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                //var dtTokenVal = BaseAdapter.TokenCheck();
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    statusObj.status = false;
                    statusObj.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
                }
                else
                {
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string siteId = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string ROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaName = Convert.ToString(request.SelectToken("areaName"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string subarea = Convert.ToString(request.SelectToken("subareaIds"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    if (String.IsNullOrEmpty(areaName))
                    {
                        statusObj.status = false;
                        statusObj.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTAREAINFO");
                        string[] paraminsert = { "COMPANYID", "AREANAME", "SITEID", "STATUS", "CREATEDBY" };
                        object[] pValuesinsert = { COMPANYID, areaName, siteId, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("UPDATEAREASITES");
                            string[] paraminsert1 = { "AREAID", "SITEID", "USERID", "ROLEID" };
                            object[] pValuesinsert1 = { iresult, siteIds, CREATEDBY, ROLEID };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            if (subAreaEnable == "1")
                            {
                                strquery = ScriptEngine.GetValue("UPDATEAREASUBAREAS");
                                string[] paraminsertSubarea = { "AREAID", "SUBAREAID", "USERID" };
                                object[] pValuesinsertSubarea = { iresult, subarea, CREATEDBY, ROLEID };
                                int iresultsub = BaseAdapter.ExecuteInlineQueries(strquery, paraminsertSubarea, pValuesinsertSubarea);
                            }
                            statusObj.status = true;
                            statusObj.id = iresult;
                            statusObj.message = "ALTAREACREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
                        }
                        else if (iresult == -1)
                        {
                            statusObj.status = false;
                            statusObj.message = "ALTAREAEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
                        }
                        else
                        {
                            statusObj.status = false;
                            statusObj.message = "ALTAREACREATEPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When insert Area:" + ex.StackTrace);
                statusObj.status = false;
                statusObj.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, statusObj);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU statusMsg = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    statusMsg.status = false;
                    statusMsg.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
                }
                else
                {
                    string areaName = Convert.ToString(request.SelectToken("areaName"));
                    string areaID = Convert.ToString(request.SelectToken("areaId"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string ROLEID = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string siteIds = Convert.ToString(request.SelectToken("siteIds"));
                    string subarea = Convert.ToString(request.SelectToken("subareaIds"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    if (String.IsNullOrEmpty(areaID) || String.IsNullOrEmpty(areaName))
                    {
                        statusMsg.status = false;
                        statusMsg.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEAREAINFO");
                        string[] paraminsert = { "AREANAME", "AREAID", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { areaName, areaID, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("UPDATEAREASITES");
                            string[] paraminsert1 = { "AREAID", "SITEID", "USERID", "ROLEID" };
                            object[] pValuesinsert1 = { areaID, siteIds, CREATEDBY, ROLEID };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            if (subAreaEnable == "1")
                            {
                                strquery = ScriptEngine.GetValue("UPDATEAREASUBAREAS");
                                string[] paraminsertSubarea = { "AREAID", "SUBAREAID", "USERID" };
                                object[] pValuesinsertSubarea = { areaID, subarea, CREATEDBY, ROLEID };
                                int iresultsub = BaseAdapter.ExecuteInlineQueries(strquery, paraminsertSubarea, pValuesinsertSubarea);
                            }
                            statusMsg.status = true;
                            statusMsg.message = "ALTAREAUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
                        }
                        else if (iresult == -1)
                        {
                            statusMsg.status = false;
                            statusMsg.message = "ALTAREAEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
                        }
                        else
                        {
                            statusMsg.status = false;
                            statusMsg.message = "ALTAREAUPDATEPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When update Area:" + ex.StackTrace);
                statusMsg.status = false;
                statusMsg.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, statusMsg);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU statusMsgIU = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    statusMsgIU.status = false;
                    statusMsgIU.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                }
                else
                {
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(areaId))
                    {
                        statusMsgIU.status = false;
                        statusMsgIU.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEAREA");
                        string[] paraminsert = { "USERID", "AREAID" };
                        object[] pValuesinsert = { userid, areaId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            statusMsgIU.status = true;
                            statusMsgIU.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                        }
                        else
                        {
                            statusMsgIU.status = false;
                            statusMsgIU.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When delete Area:" + ex.StackTrace);
                statusMsgIU.status = false;
                statusMsgIU.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU statusMsgIU = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    statusMsgIU.status = false;
                    statusMsgIU.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                }
                else
                {
                    string AREAID = Convert.ToString(request.SelectToken("areaId"));
                    string userID = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(AREAID))
                    {
                        statusMsgIU.status = false;
                        statusMsgIU.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESTATUSAREAS");
                        string[] paraminsert = { "AREAID", "USERID" };
                        object[] pValuesinsert = { AREAID, userID };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            statusMsgIU.status = true;
                            statusMsgIU.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                        }
                        else
                        {
                            statusMsgIU.status = false;
                            statusMsgIU.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When change status Area:" + ex.StackTrace);
                statusMsgIU.status = false;
                statusMsgIU.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
            }
            return response;
        }

        /// <summary>
        /// Gets the site data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/GetSiteData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetSiteData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterData parameterData = new ParameterData();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;

                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAssign = new DataSet();
                DataSet dsAssignSub = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    parameterData.status = false;
                    parameterData.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string areaID = Convert.ToString(request.SelectToken("areaId"));
                    string AREATYPE = Convert.ToString(request.SelectToken("areaType"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    if (!String.IsNullOrEmpty(areaID))
                    {
                        ParameterAreaAssign obj = new ParameterAreaAssign();
                        strquery = ScriptEngine.GetValue("GETAREAGENERALINFO");
                        string[] paraminsert = { "AREAID" };
                        object[] pValuesinsert = { areaID };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETAREASITESASSGN");
                        string[] paraminsert1 = { "AREAID", "ROLEID", "USERID" };
                        object[] pValuesinsert1 = { areaID, roleId, USERID };
                        dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        DataTable dtAvail = dsAssign.Tables[0].Clone();
                        DataTable dtsite = new DataTable();
                        try
                        {
                            string[] siteavail = dsAssign.Tables[1].Rows[0]["siteIds"].ToString().Split(',');
                            for (int i = 0; i < dsAssign.Tables[0].Rows.Count; i++)
                            {
                                int j = 0;
                                for (j = 0; j < siteavail.Length; j++)
                                {
                                    if (siteavail[j] == dsAssign.Tables[0].Rows[i]["SITEID"].ToString())
                                    {
                                        DataRow dr = dtAvail.NewRow();
                                        dr[0] = dsAssign.Tables[0].Rows[i]["SITEID"].ToString();
                                        dr[1] = dsAssign.Tables[0].Rows[i]["SITENAME"].ToString();
                                        dtAvail.Rows.Add(dr);
                                        DataRow dr1 = dsAssign.Tables[0].Rows[i];
                                        dr1.Delete();
                                        dsAssign.Tables[0].AcceptChanges();
                                    }
                                }
                                j = 0;
                            }
                            dtsite = dtAvail.Copy();
                            dtsite.Merge(dsAssign.Tables[0]);
                        }
                        catch (Exception ex)
                        {
                            dtsite = dsAssign.Tables[0];
                        }

                        DataTable dtSubArea = new DataTable();
                        if (subAreaEnable == "1")
                        {
                            strquery = ScriptEngine.GetValue("GETSUBAREAASSIGN");
                            dsAssignSub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                            DataTable dtSubAvail = dsAssignSub.Tables[0].Clone();
                            try
                            {
                                string[] siteavail = dsAssignSub.Tables[1].Rows[0]["subAreaIds"].ToString().Split(',');
                                for (int i = 0; i < dsAssignSub.Tables[0].Rows.Count; i++)
                                {
                                    int j = 0;
                                    for (j = 0; j < siteavail.Length; j++)
                                    {
                                        if (siteavail[j] == dsAssignSub.Tables[0].Rows[i]["SUBAREAID"].ToString())
                                        {
                                            DataRow dr = dtSubAvail.NewRow();
                                            dr[0] = dsAssignSub.Tables[0].Rows[i]["SUBAREAID"].ToString();
                                            dr[1] = dsAssignSub.Tables[0].Rows[i]["SUBAREANAME"].ToString();
                                            dtSubAvail.Rows.Add(dr);
                                            DataRow dr1 = dsAssignSub.Tables[0].Rows[i];
                                            dr1.Delete();
                                            dsAssignSub.Tables[0].AcceptChanges();
                                        }
                                    }
                                    j = 0;
                                }
                                dtSubArea = dtSubAvail.Copy();
                                dtSubArea.Merge(dsAssignSub.Tables[0]);
                            }
                            catch (Exception ex)
                            {
                                dtSubArea = dsAssignSub.Tables[0];
                            }
                            if (dsAssignSub.Tables[1].Rows.Count == 0)
                            {
                                DataRow dr = dsAssignSub.Tables[1].NewRow();
                                dr[0] = "";
                                dsAssignSub.Tables[1].Rows.Add(dr);
                            }
                            obj.subAreaAvailable = ScriptEngine.GetTableRows(dtSubArea);
                            obj.subAreaAssign = ScriptEngine.GetTableRows(dsAssignSub.Tables[1])[0]["subAreaIds"];
                        }
                        else
                        {
                            obj.subAreaAvailable = new string[] { };
                            obj.subAreaAssign = "";
                        }
                        if (dsAssign.Tables[1].Rows.Count == 0)
                        {
                            DataRow dr = dsAssign.Tables[1].NewRow();
                            dr[0] = "";
                            dsAssign.Tables[1].Rows.Add(dr);
                        }

                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstDatas = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dtsite);
                            List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssign.Tables[1]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.Data = lstDatas[0];
                            obj.siteAvailable = lstAvail;
                            obj.siteAssign = lstAssign[0]["siteIds"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            parameterData.status = false;
                            parameterData.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETAREALISTING");
                        string[] paraminsert = { "USERID", "AREATYPE" };
                        object[] pValuesinsert = { USERID, AREATYPE };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterData ob = new ParameterData();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            ob.status = true;
                            ob.message = "Success";
                            ob.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, ob);
                        }
                        else
                        {
                            parameterData.status = false;
                            parameterData.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When get Area Data:" + ex.StackTrace);
                parameterData.status = false;
                parameterData.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
            }
            return response;
        }

        /// <summary>
        /// Adds the area data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/addAreaData")]
        [APIAuthAttribute]
        public HttpResponseMessage addAreaData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            ParameterData parameterData = new ParameterData();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAssign = new DataSet();
                DataSet dsAssignSub = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    parameterData.status = false;
                    parameterData.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();

                    strquery = ScriptEngine.GetValue("GETAREASITESASSGN");
                    string[] paraminsert1 = { "AREAID", "ROLEID", "USERID" };
                    object[] pValuesinsert1 = { "", roleId, USERID };
                    dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                    
                    ParameterArea ob = new ParameterArea();
                    if (subAreaEnable =="1")
                    {
                        strquery = ScriptEngine.GetValue("GETSUBAREAASSIGN");
                        dsAssignSub = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        ob.subArea = ScriptEngine.GetTableRows(dsAssignSub.Tables[0]); 
                    }
                    else
                    {
                        ob.subArea = new string[] { };
                    }
                    if (dsAssign.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstsite = ScriptEngine.GetTableRows(dsAssign.Tables[0]);
                        ob.status = true;
                        ob.message = "Success";
                        ob.site = lstsite;
                        ob.defaultSite = DEFAULTSITEID;
                        response = Request.CreateResponse(HttpStatusCode.OK, ob);
                    }
                    else
                    {
                        parameterData.status = false;
                        parameterData.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When get data on Add click:" + ex.StackTrace);
                parameterData.status = false;
                parameterData.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, parameterData);
            }
            return response;

        }

        /// <summary>
        /// Updates the area assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteArea/updateAreaAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateAreaAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU statusMsgIU = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();

                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    statusMsgIU.status = false;
                    statusMsgIU.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string assignIds = Convert.ToString(request.SelectToken("assignIds"));
                    string AREAID = Convert.ToString(request.SelectToken("areaId"));
                    string assignType = Convert.ToString(request.SelectToken("assignType"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    if (String.IsNullOrEmpty(AREAID))
                    {
                        statusMsgIU.status = false;
                        statusMsgIU.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                    }
                    else
                    {
                        int iresult = 0;
                        if (assignType == "1")
                        {
                            strquery = ScriptEngine.GetValue("UPDATEAREASITES");
                            string[] paraminsert = { "AREAID", "SITEID", "USERID", "ROLEID" };
                            object[] pValuesinsert = { AREAID, assignIds, USERID, roleId };
                            iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        }
                        else if (assignType == "2" && subAreaEnable=="1")
                        {
                            strquery = ScriptEngine.GetValue("UPDATEAREASUBAREAS");
                            string[] paraminsert = { "AREAID", "SUBAREAID", "USERID", "ROLEID" };
                            object[] pValuesinsert = { AREAID, assignIds, USERID, roleId };
                            iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        }
                        if (iresult != 0 && iresult > 0)
                        {
                            statusMsgIU.status = true;
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                            statusMsgIU.message = "ALTAREAASSGNSUCCESS";
                        }
                        else
                        {
                            statusMsgIU.status = false;
                            statusMsgIU.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error("Exception Occured When update Area assign:" + ex.StackTrace);
                statusMsgIU.status = false;
                statusMsgIU.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, statusMsgIU);
            }
            return response;
        }

        #endregion  
    }
}