﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// InboxController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class InboxController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InboxController"/> class.
        /// </summary>
        public InboxController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(InboxController));
        }

        #region Action Method

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the inbox data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Inbox/getInboxData")]
        public HttpResponseMessage getInboxData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                //DataSet dsProfile = new DataSet();
                DataSet dsemail = new DataSet();
                //DataSet dsschedul = new DataSet();
                //DataSet dsallEmail = new DataSet();
                //DataSet dsUnread = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    ParameterInboxList pv = new ParameterInboxList();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string dformat = dtTokenVal.Rows[0]["DATEFORMAT"].ToString();
                    strquery = ScriptEngine.GetValue("GETUSERMAILINBOX");

                    string[] paramrptinfo = { "USERID", "DFORMAT" };

                    ////unread
                    //object[] pValuesrptinfo1 = { userId, dformat, "0", "" };
                    //dsUnread = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo1);

                    //email
                    object[] pValuesrptinfoEmail = { userId, dformat };
                    dsemail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfoEmail);

                    ////schedule
                    //object[] pValuesrptinfoSch = { userId, dformat, "", "2" };
                    //dsschedul = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfoSch);

                    ////all mails
                    //object[] pValuesrptinfoMail = { userId, dformat, "", "" };
                    //dsallEmail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfoMail);

                    //dsUnread.Tables[0].Columns.Remove("BODY");
                    //dsUnread.Tables[0].Columns.Remove("BODY1");
                    //dsemail.Tables[0].Columns.Remove("BODY");
                    //dsemail.Tables[0].Columns.Remove("BODY1");
                    //dsschedul.Tables[0].Columns.Remove("BODY");
                    //dsschedul.Tables[0].Columns.Remove("BODY1");
                    //dsallEmail.Tables[0].Columns.Remove("BODY");
                    //dsallEmail.Tables[0].Columns.Remove("BODY1");

                    pv.status = true;
                    pv.message = "Success";
                    pv.unRead = ScriptEngine.GetTableRows(dsemail.Tables[0]);
                    pv.emails = ScriptEngine.GetTableRows(dsemail.Tables[1]);
                    pv.scheduleReport = ScriptEngine.GetTableRows(dsemail.Tables[2]);
                    pv.allEmails = ScriptEngine.GetTableRows(dsemail.Tables[3]);
                    response = Request.CreateResponse(HttpStatusCode.OK, pv);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Inbox Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the inbox data information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Inbox/getInboxDataInfo")]
        public HttpResponseMessage getInboxDataInfo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsProfile = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    StatusMsgData pv = new StatusMsgData();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string dformat = dtTokenVal.Rows[0]["DATEFORMAT"].ToString();
                    string inboxId = Convert.ToString(request.SelectToken("inboxId"));
                    strquery = ScriptEngine.GetValue("GETUSERMAILINBOXINFO");
                    string[] paramrptinfo = { "USERID", "DFORMAT", "INBOXID" };
                    object[] pValuesrptinfo = { userId, dformat, inboxId };
                    dsProfile = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    if (dsProfile.Tables[0].Rows.Count != 0)
                    {
                        pv.status = true;
                        pv.message = "Success";
                        pv.data = ScriptEngine.GetTableRows(dsProfile.Tables[0]);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                    }
                    response = Request.CreateResponse(HttpStatusCode.OK, pv);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get User Inbox Data Info:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the inbox.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Inbox/deleteInbox")]
        public HttpResponseMessage deleteInbox(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                
                string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                string inboxId = Convert.ToString(request.SelectToken("inboxId"));
                strquery = ScriptEngine.GetValue("DELETEEMAILINBOX");
                string[] paramrptinfo = { "INBOXID" };
                object[] pValuesrptinfo = { inboxId };
                int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                if (iresult > 0)
                {
                    obj1.status = true;
                    obj1.message = "ALTDELSUCCESS";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    obj1.status = false;
                    obj1.message = "LBLTECHERROR";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete Inbox Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
        
    }
}
