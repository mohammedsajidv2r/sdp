﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// CategoryController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class CategoryController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CategoryController"/> class.
        /// </summary>
        public CategoryController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(CategoryController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the category data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/getCategoryData")]
        [APIAuthAttribute]
        public HttpResponseMessage getCategoryData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETMAINCATEGORYLIST");
                    string[] paraminsert = { "LANGUAGECODE" };
                    object[] pValuesinsert = { LANGUAGECODE };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterValuesTarget obj = new ParameterValuesTarget();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.GeneralData = lstPersons;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Category Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/insertCategory")]
        [APIAuthAttribute]
        public HttpResponseMessage insertCategory(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string mainCategoryName = Convert.ToString(request.SelectToken("mainCategoryName"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    if (String.IsNullOrEmpty(mainCategoryName) || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else if (Convert.ToInt16(status) > 1 || Convert.ToInt16(status) < 0)
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTMAINCATEGORY");
                        string[] paraminsert = { "COMPANYID", "CREATEDBY", "MAINCATEGORYNAME", "STATUS" };
                        object[] pValuesinsert = { COMPANYID, USERID, mainCategoryName, status };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTMAINCATEGORYCREATED";
                            obj1.id = iresult;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAINCATEGORYNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAINCATEGORYNOTCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Add Category Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Views the category information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/viewCategoryInfo")]
        [APIAuthAttribute]
        public HttpResponseMessage viewCategoryInfo(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    if (String.IsNullOrEmpty(mainCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETCATEGORYGENERALINFO");
                        string[] paraminsert = { "LANGUAGECODE", "MAINCATEGORYID" };
                        object[] pValuesinsert = { LANGUAGECODE, mainCategoryId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            DataTable dAvailSort = ds.Tables[2].Copy();
                            dAvailSort.Merge(ds.Tables[1]);
                            if (ds.Tables[3].Rows.Count == 0)
                            {
                                DataRow dr = ds.Tables[3].NewRow();
                                dr[0] = "";
                                ds.Tables[3].Rows.Add(dr);
                            }
                            ParameterListCatg obj = new ParameterListCatg();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(dAvailSort);
                            List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds.Tables[3]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.GeneralData = lstPersons[0];
                            obj.subCategory = lstPersons1;
                            obj.subCategoryId = lstPersons2[0]["SUBCATEGORYIDs"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When View Category Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the sub category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/InsertSubCategory")]
        [APIAuthAttribute]
        public HttpResponseMessage InsertSubCategory(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string subCategoryName = Convert.ToString(request.SelectToken("subCategoryName"));
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    if (String.IsNullOrEmpty(subCategoryName) || String.IsNullOrEmpty(mainCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTSUBCATEGORY");
                        string[] paraminsert = { "COMPANYID", "CREATEDBY", "SUBCATEGORYNAME", "MAINCATEGORYID" };
                        object[] pValuesinsert = { COMPANYID, USERID, subCategoryName, mainCategoryId };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("GETSUBCATEGORYASSIGNMENTS");
                            ParameterListSubCatg obj = new ParameterListSubCatg();
                            string[] paraminsert1 = { "MAINCATEGORYID", "LANGUAGECODE" };
                            object[] pValuesinsert1 = { mainCategoryId, LANGUAGECODE };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                if (ds.Tables[1].Rows.Count == 0)
                                {
                                    DataRow dr = ds.Tables[1].NewRow();
                                    dr[0] = "";
                                    ds.Tables[1].Rows.Add(dr);
                                }
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                obj.subCategory = lstPersons;
                                obj.subCategoryId = lstPersons1[0]["SUBCATEGORYIDs"];
                            }
                            obj.status = true;
                            obj.message = "ALTSUBCATEGORYCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSUBCATEGORYNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSUBCATEGORYNOTCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Add SubCategory Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the sub category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/deleteSubCategory")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteSubCategory(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    string subCategoryIds = Convert.ToString(request.SelectToken("subCategoryIds"));
                    if (String.IsNullOrEmpty(mainCategoryId) || String.IsNullOrEmpty(subCategoryIds))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESUBCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID", "SUBCATEGORYID" };
                        object[] pValuesinsert = { USERID, mainCategoryId, subCategoryIds };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete SubCategory Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/updateCategory")]
        [APIAuthAttribute]
        public HttpResponseMessage updateCategory(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    string mainCategoryName = Convert.ToString(request.SelectToken("mainCategoryName"));
                    if (String.IsNullOrEmpty(mainCategoryId) || String.IsNullOrEmpty(mainCategoryName))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATEMAINCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID", "MAINCATEGORYNAME", "LANGUAGEID" };
                        object[] pValuesinsert = { USERID, mainCategoryId, mainCategoryName, LANGUAGECODE };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult == 3)
                        {
                            obj1.status = true;
                            obj1.message = "ALTMAINCATEGORYUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == 1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAINCATEGORYNAMEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTMAINCATEGORYNOTUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update SubCategory Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the sub category assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/updateSubCategoryAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateSubCategoryAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string LANGUAGECODE = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    string subCategoryIds = Convert.ToString(request.SelectToken("subCategoryIds"));
                    if (String.IsNullOrEmpty(mainCategoryId) || String.IsNullOrEmpty(subCategoryIds))
                    {
                        obj1.status = false;
                        obj1.message = "ALTASGNSUBCTG";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESUBCATEGORYASSIGNMENTS");
                        string[] paraminsert = { "USERID", "MAINCATEGORYID", "SUBCATEGORYID" };
                        object[] pValuesinsert = { USERID, mainCategoryId, subCategoryIds };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            strquery = ScriptEngine.GetValue("GETSUBCATEGORYASSIGNMENTS");
                            ParameterListSubCatg obj = new ParameterListSubCatg();
                            string[] paraminsert1 = { "MAINCATEGORYID", "LANGUAGECODE" };
                            object[] pValuesinsert1 = { mainCategoryId, LANGUAGECODE };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                if (ds.Tables[1].Rows.Count == 0)
                                {
                                    DataRow dr = ds.Tables[1].NewRow();
                                    dr[0] = "";
                                    ds.Tables[1].Rows.Add(dr);
                                }
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                obj.subCategory = lstPersons;
                                obj.subCategoryId = lstPersons1[0]["SUBCATEGORYIDs"];
                            }
                            obj.status = true;
                            obj.message = "ALTSUBCATASSIGNSUCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update SubCategory Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/deleteCategory")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteCategory(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    if (String.IsNullOrEmpty(mainCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEMAINCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID" };
                        object[] pValuesinsert = { USERID, mainCategoryId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete Category Data" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ChecklistConfig/updateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage updateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    if (String.IsNullOrEmpty(mainCategoryId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESTATUSMAINCATEGORY");
                        string[] paraminsert = { "UPDATEDBY", "MAINCATEGORYID" };
                        object[] pValuesinsert = { USERID, mainCategoryId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Category status:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        [HttpPost]
        [Route("api/ChecklistConfig/getCatTranslationData")]
        [APIAuthAttribute]
        public HttpResponseMessage getCatTranslationData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = Convert.ToString(request.SelectToken("languageId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    strquery = ScriptEngine.GetValue("GETLANGUAGES");
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterValuesLangTarget obj = new ParameterValuesLangTarget();
                        List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                        if (languageId == "")
                        {
                            languageId = ds.Tables[0].Rows[0]["LANGUAGEID"].ToString();
                        }
                        if (mode == "")
                        {
                            mode = "0";
                        }
                        strquery = ScriptEngine.GetValue("GETMAINCATEGORYFORTRANSLATION");
                        string[] paraminsert = { "LANGUAGEID", "MODE" };
                        object[] pValuesinsert = { languageId, mode };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj.status = true;
                            if (mainCategoryId == "")
                            {
                                mainCategoryId = ds.Tables[0].Rows[0]["MAINCATEGORYID"].ToString();
                            }
                            strquery = ScriptEngine.GetValue("GETCATEGORYTRANSLATION");
                            string[] paraminsert1 = { "LANGUAGECODE", "MAINCATEGORYID" };
                            object[] pValuesinsert1 = { languageId, mainCategoryId };
                            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj.message = "Success";
                                obj.Language = lstPersons;
                                obj.Category = lstPersons1;
                                obj.Translation = lstPersons2;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Translation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }
        
        [HttpPost]
        [Route("api/ChecklistConfig/updateTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage updateTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string entityIds = Convert.ToString(request.SelectToken("entityIds"));
                    string languageId = Convert.ToString(request.SelectToken("languageId"));
                    string mainCategoryId = Convert.ToString(request.SelectToken("mainCategoryId"));
                    bool pipeLineValid = true;
                    if (String.IsNullOrEmpty(mainCategoryId) || String.IsNullOrEmpty(languageId) || String.IsNullOrEmpty(entityIds))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(entityIds);
                        string entity = "";
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dtTokenVals.Rows.Count; i++)
                        {
                            string id = dtTokenVals.Rows[i]["eid"].ToString();
                            string value = dtTokenVals.Rows[i]["eval"].ToString();
                            if (value.Contains("|"))
                            {
                                pipeLineValid = false;
                                break;
                            }

                            if (value.Contains("'"))
                            {
                                value = value.Replace("'","''");
                            }
                            if (value.Contains(","))
                            {
                                value = value.Replace(",", "@m*m@");
                            }

                            entity = mainCategoryId + "," + id + "," + value + "|";
                            sb.Append(entity);
                        }
                        if (pipeLineValid == true)
                        {
                            strquery = ScriptEngine.GetValue("UPDATECATEGORYTRANSLATION");
                            string[] paraminsert = { "UPDATEDBY", "ENTITYID", "PARENTID", "LANGUAGEID" };
                            object[] pValuesinsert = { USERID, sb.ToString(), mainCategoryId, languageId };
                            int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            if (iresult != 0 && iresult > 0)
                            {
                                obj1.status = true;
                                obj1.message = "ALTSELECTEDITEMTRANSLATEDSUCESS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "ALTSELECTEDITEMTRANSLATEDFAIL";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTVALIDATETRANSTEXT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Translation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
        
    }
}

