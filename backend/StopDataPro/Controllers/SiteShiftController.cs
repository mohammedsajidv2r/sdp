﻿using DataPro;
using log4net;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    public class SiteShiftController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SiteShiftController"/> class.
        /// </summary>
        public SiteShiftController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(SiteShiftController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Actino Method

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [APIAuthAttribute]
        public HttpResponseMessage Insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal  = ScriptEngine.TokenCheck();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string COMPANYID = dtTokenVal .Rows[0]["COMPANYID"].ToString();
                    string CREATEDBY = dtTokenVal .Rows[0]["USERID"].ToString();
                    string roleid = dtTokenVal .Rows[0]["DEFAULTROLEID"].ToString();
                    string siteId = dtTokenVal .Rows[0]["DEFAULTSITEID"].ToString();
                    string shiftName = Convert.ToString(request.SelectToken("shiftName"));
                    string siteid = Convert.ToString(request.SelectToken("siteIds"));
                    int status = Convert.ToInt32(request.SelectToken("status"));
                    if (String.IsNullOrEmpty(shiftName))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTSHIFTINFO");
                        string[] paraminsert = { "COMPANYID", "SHIFTNAME", "SITEID", "STATUS", "CREATEDBY" };
                        object[] pValuesinsert = { COMPANYID, shiftName, siteId, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            int shiftid = iresult;
                            strquery = ScriptEngine.GetValue("UPDATESHIFTSITES");
                            string[] paraminsert1 = { "SHIFTID", "SITEID", "USERID", "ROLEID" };
                            object[] pValuesinsert1 = { shiftid, siteid, CREATEDBY, roleid };
                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);

                            obj1.status = true;
                            obj1.id = iresult;
                            obj1.message = "ALTSHIFTCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSHIFTEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSHIFTCREATEPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert Shfit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/Update")]
        [APIAuthAttribute]
        public HttpResponseMessage Update(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string roleid = dtTokenVal .Rows[0]["DEFAULTROLEID"].ToString();
                    string shiftName = Convert.ToString(request.SelectToken("shiftName"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    int status = Convert.ToInt32(request.SelectToken("status"));

                    string siteid = Convert.ToString(request.SelectToken("siteIds"));
                    string CREATEDBY = dtTokenVal .Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(shiftId) || String.IsNullOrEmpty(shiftName))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESHIFTINFO");
                        string[] paraminsert = { "SHIFTNAME", "SHIFTID", "STATUS", "UPDATEDBY" };
                        object[] pValuesinsert = { shiftName, shiftId, status, CREATEDBY };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult == 2)
                        {
                            strquery = ScriptEngine.GetValue("UPDATESHIFTSITES");
                            string[] paraminsert1 = { "SHIFTID", "SITEID", "USERID", "ROLEID" };
                            object[] pValuesinsert1 = { shiftId, siteid, CREATEDBY, roleid };

                            int iresult1 = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert1, pValuesinsert1);
                            obj1.status = true;
                            obj1.message = "ALTSHIFTUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == 1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTSHIFTEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSHIFTUPDATEPROB";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Shfit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/Delete")]
        [APIAuthAttribute]
        public HttpResponseMessage Delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string userID = dtTokenVal .Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(shiftId) || String.IsNullOrEmpty(userID))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandator" +
                            "y Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESHIFT");
                        string[] paraminsert = { "USERID", "SHIFTID" };
                        object[] pValuesinsert = { userID, shiftId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Shfit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/UpdateStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage UpdateStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string userID = dtTokenVal .Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(shiftId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("CHANGESTATUSSHIFTS");
                        string[] paraminsert = { "SHIFTID", "USERID" };
                        object[] pValuesinsert = { shiftId, userID };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update status Shfit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/GetSiteData")]
        [APIAuthAttribute]
        public HttpResponseMessage GetSiteData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAssign = new DataSet();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string USERID = dtTokenVal .Rows[0]["USERID"].ToString();
                    string shiftType = Convert.ToString(request.SelectToken("shiftType"));
                    string roleId = dtTokenVal .Rows[0]["DEFAULTROLEID"].ToString();
                    DataSet ds = new DataSet();
                    if (!String.IsNullOrEmpty(shiftId))
                    {
                        strquery = ScriptEngine.GetValue("GETSHIFTGENERALINFO");
                        string[] paraminsert = { "SHIFTID" };
                        object[] pValuesinsert = { shiftId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETSHIFTSITESASSGN");
                        string[] paraminsert1 = { "SHIFTID", "ROLEID", "USERID" };
                        object[] pValuesinsert1 = { shiftId, roleId, USERID };

                        dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        DataTable dAvailSort = dsAssign.Tables[1].Copy();
                        dAvailSort.Merge(dsAssign.Tables[0]);
                        if (dsAssign.Tables[2].Rows.Count == 0)
                        {
                            DataRow dr = dsAssign.Tables[2].NewRow();
                            dr[0] = "";
                            dsAssign.Tables[2].Rows.Add(dr);
                        }
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterSubAreaAssign obj = new ParameterSubAreaAssign();
                            List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstAvail = ScriptEngine.GetTableRows(dAvailSort);
                            List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssign.Tables[2]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.Data = lstData[0];
                            obj.available = lstAvail;
                            obj.assign = lstAssign[0]["siteIds"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETSHIFTLISTING");
                        string[] paraminsert = { "USERID", "SHIFTTYPE" };
                        object[] pValuesinsert = { USERID, shiftType };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterData ob = new ParameterData();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            ob.status = true;
                            ob.message = "Success";
                            ob.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, ob);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Shfit Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Adds the shift data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/addShiftData")]
        [APIAuthAttribute]
        public HttpResponseMessage addShiftData(JObject request)
         {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAssign = new DataSet();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string USERID = dtTokenVal .Rows[0]["USERID"].ToString();
                    string roleId = dtTokenVal .Rows[0]["DEFAULTROLEID"].ToString();
                    string DEFAULTSITEID = dtTokenVal .Rows[0]["DEFAULTSITEID"].ToString();
                    ParameterShift ob = new ParameterShift();
                    strquery = ScriptEngine.GetValue("GETSHIFTSITESASSGN");
                    string[] paraminsert1 = { "SHIFTID", "ROLEID", "USERID" };
                    object[] pValuesinsert1 = { "", roleId, USERID };

                    dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                    if (dsAssign.Tables[0].Rows.Count != 0)
                    {
                        List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssign.Tables[0]);
                        ob.status = true;
                        ob.message = "Success";
                        ob.available = lstAssign;
                        ob.defaultSite = DEFAULTSITEID;
                        response = Request.CreateResponse(HttpStatusCode.OK, ob);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get data on Add click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;

        }

        /// <summary>
        /// Updates the shift assign data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/SiteShift/updateShiftAssignData")]
        [APIAuthAttribute]
        public HttpResponseMessage updateShiftAssignData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal .Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string assignIds = Convert.ToString(request.SelectToken("assignIds"));
                    string roleId = dtTokenVal .Rows[0]["DEFAULTROLEID"].ToString();
                    string userID = dtTokenVal .Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(shiftId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESHIFTSITES");
                        string[] paraminsert = { "SHIFTID", "SITEID", "USERID", "ROLEID" };
                        object[] pValuesinsert = { shiftId, assignIds, userID, roleId };

                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTSHIFTASSGNSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTSHIFTASSGNFAIL";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Shfit Assign:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion

    }
}
