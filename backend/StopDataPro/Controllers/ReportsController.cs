﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// ReportsController
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ReportsController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportsController"/> class.
        /// </summary>
        public ReportsController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ReportsController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Siteses the report.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/SitesReport")]
        [APIAuthAttribute]
        public HttpResponseMessage SitesReport(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgSite obj1 = new StatusMsgSite();
            DataSet ds = new DataSet();
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for check token to db
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                //for read config get connectionstring
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();

                    int reportId = Convert.ToInt32(request.SelectToken("reportId"));
                    int DESIGNID = Convert.ToInt32(request.SelectToken("DESIGNID"));
                    if (DESIGNID == 0)
                    {
                        DESIGNID = Convert.ToInt32(request.SelectToken("designId"));
                    }
                    string YEAR = Convert.ToString(request.SelectToken("YEAR"));
                    string SAFECHECK = "0";
                    string METRICSVALUE = "0";
                    string OBSDATE = Convert.ToString(request.SelectToken("OBSDATE"));
                    //DataPro configuration setting 
                    Dictionary<string, object> scriptengine_data;
                    StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                    string strsettings = srSettings.ReadToEnd();
                    scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                    srSettings.Close();
                    Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                    Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                    dctfulldata = scriptengine_data;
                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "dataproreportservice").Value;
                    string DRS_WSNAME = dctpartdata["WSNAME"].ToString();
                    string DRS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string DRS_WSURL = dctpartdata["WSURL"].ToString();
                    string DRS_WSURL1 = dctpartdata["WSURL1"].ToString();
                    string url = AppInfo.getUrlHost(httpUrl);
                    string APPURL = "";
                    if (url.Contains("http"))
                    { APPURL = url.Replace("/", "a_b"); }
                    else
                    {
                        string urll = "http://" + url;
                        APPURL = urll.Replace("/", "a_b");
                    }
                    string URL = DRS_WSURL.Replace("dataproreportservice.asmx", "").Replace("/", "a_b");
                    string CREATEDDATE = Convert.ToString(request.SelectToken("CREATEDDATE"));     //Entered Date
                    string RESDATEREQ = Convert.ToString(request.SelectToken("RESDATEREQ"));       //action due date
                    string RESPONDEDDATE = Convert.ToString(request.SelectToken("RESPONDEDDATE")); //responded date
                    //site filter
                    string SITEID = Convert.ToString(request.SelectToken("SITEID"));
                    string AREAID = Convert.ToString(request.SelectToken("AREAID"));
                    string SUBAREAID = Convert.ToString(request.SelectToken("SUBAREAID"));
                    string SHIFTID = Convert.ToString(request.SelectToken("SHIFTID"));
                    string OBSERVERID = Convert.ToString(request.SelectToken("OBSERVERID"));
                    string CUSTOMFIELD1 = Convert.ToString(request.SelectToken("CUSTOMFIELD1"));
                    string CUSTOMFIELD2 = Convert.ToString(request.SelectToken("CUSTOMFIELD2"));
                    string CUSTOMFIELD3 = Convert.ToString(request.SelectToken("CUSTOMFIELD3"));
                    string REDFLAGID = Convert.ToString(request.SelectToken("REDFLAGID"));
                    //groups
                    string GROUP1 = Convert.ToString(request.SelectToken("GROUP1"));
                    string GROUP2 = Convert.ToString(request.SelectToken("GROUP2"));
                    string GROUP3 = Convert.ToString(request.SelectToken("GROUP3"));
                    string GROUP4 = Convert.ToString(request.SelectToken("GROUP4"));
                    string GROUP5 = Convert.ToString(request.SelectToken("GROUP5"));
                    string GROUP6 = Convert.ToString(request.SelectToken("GROUP6"));
                    string GROUP7 = Convert.ToString(request.SelectToken("GROUP7"));
                    string GROUP8 = Convert.ToString(request.SelectToken("GROUP8"));
                    string GROUP9 = Convert.ToString(request.SelectToken("GROUP9"));
                    string GROUP10 = Convert.ToString(request.SelectToken("GROUP10"));
                    string GROUP11 = Convert.ToString(request.SelectToken("GROUP11"));
                    string GROUP12 = Convert.ToString(request.SelectToken("GROUP12"));
                    //category
                    string SETUPID = Convert.ToString(request.SelectToken("SETUPID"));   // //checklistsetup
                    string MAINCATEGORYID = Convert.ToString(request.SelectToken("MAINCATEGORYID"));
                    string SUBCATEGORYID = Convert.ToString(request.SelectToken("SUBCATEGORYID"));
                    //optional 
                    string USERLEVELID = Convert.ToString(request.SelectToken("USERLEVELID"));
                    string CACARDSTATUS = Convert.ToString(request.SelectToken("CACARDSTATUS"));
                    string OBSSTATUSID = Convert.ToString(request.SelectToken("OBSSTATUSID"));
                    string INCLUDECHECKLISTVALUE = Convert.ToString(request.SelectToken("INCLUDECHECKLISTVALUE")); //includecheklistwithoutgroupassign
                    string OBSZEROCHECKLISTVALUE = Convert.ToString(request.SelectToken("OBSZEROCHECKLISTVALUE"));  //hide observer with zero checklist
                    string OBSWITHZEROTARGET = Convert.ToString(request.SelectToken("OBSWITHZEROTARGET"));   //include observer with zeror target
                    string TARGETSTATUS = Convert.ToString(request.SelectToken("TARGETSTATUS"));
                    string metrics = Convert.ToString(request.SelectToken("METRICSVALUE"));  //for red flag
                    string INCLUDEHISTDATAVALUE = Convert.ToString(request.SelectToken("INCLUDEHISTDATAVALUE"));  //include historial data
                    string INCOMPOBSERVATION = Convert.ToString(request.SelectToken("INCOMPOBSERVATION"));    //display incomplete observation   
                    string PRIORITYVALUE = Convert.ToString(request.SelectToken("PRIORITYVALUE"));
                    string UACACARDSTATUS = Convert.ToString(request.SelectToken("UACACARDSTATUS"));
                    string CHARTYPE = Convert.ToString(request.SelectToken("CHARTTYPE"));    //TRADELINE
                    string ROLEID = Convert.ToString(request.SelectToken("ROLEID"));      //for userobserlist
                    string STARTOFWEEK = Convert.ToString(request.SelectToken("STARTOFWEEK"));     //target vs activity
                    string CARDSTATUSID = Convert.ToString(request.SelectToken("CARDSTATUSID"));         //CA Report
                    string INCLUDECOMMENTS = Convert.ToString(request.SelectToken("INCLUDECOMMENTS")); //DUMP REPORT
                    //group by
                    string CHKSUMGROUP1 = Convert.ToString(request.SelectToken("CHKSUMGROUP1"));  //
                    string CHKSUMGROUP2 = Convert.ToString(request.SelectToken("CHKSUMGROUP2"));
                    string CHKSUMGROUP3 = Convert.ToString(request.SelectToken("CHKSUMGROUP3"));
                    string strquery = ScriptEngine.GetValue("GETREPORTS_INFO");
                    string[] paramrptinfo = { "REPORTID" };
                    object[] pValuesrptinfo = { reportId };
                    DataSet dtTokenVals = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                    dtTokenVals.Tables[0].Columns.Remove("RPTDESIGN");
                    dtTokenVals.Tables[0].Columns.Remove("ISQUERY");
                    dtTokenVals.Tables[0].Columns.Remove("DUMPREPORTRUNDATE");
                    DataTable dtTokenValNew = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo).Tables[0].Select(" DESIGNID = " + DESIGNID + "").CopyToDataTable();
                    obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);

                    Log.InfoFormat("check ReportId:" + Convert.ToString(dtTokenValNew.Rows[0]["REPORTID"]));
                    Log.InfoFormat("check desingId:" + Convert.ToString(dtTokenValNew.Rows[0]["DESIGNID"]));
                    #region Sites Report  ||  reportId == 3 ||designId 24,25,26,27,5
                    if (reportId == 3)
                    {

                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "USERLEVELID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, USERLEVELID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12 };
                        ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterValuesSite pv = new ParameterValuesSite();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            if (DESIGNID == 5)
                            {
                                pv.status = true;
                                pv.message = "Success";
                                pv.siteChart = lstPersons;
                                pv.siteDetails = ScriptEngine.GetTableRows(ds.Tables[1]);
                                pv.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, pv);
                            }
                            else
                            {
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    #endregion

                    #region Monthly Safe/Unsafe Stats || reportId == 4 desingid 6,31,32
                    else if (reportId == 4)
                    {
                        if (DESIGNID == 6)
                        {
                            METRICSVALUE = "0";
                        }
                        else if (DESIGNID == 31)
                        {
                            METRICSVALUE = "1";
                        }
                        else if (DESIGNID == 32)
                        {
                            METRICSVALUE = "2";
                        }
                        else
                        {
                            METRICSVALUE = "0";
                        }
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "OBSERVERID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "MAINCATEGORYID", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "METRICSVALUE" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, OBSERVERID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, MAINCATEGORYID, OBSSTATUSID, INCLUDECHECKLISTVALUE, METRICSVALUE };
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ParameterValuesSubchekclist ob = new ParameterValuesSubchekclist();
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds.Tables[2]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons;
                                ob.sitelist = lstPersons1;
                                ob.mainCategory = lstPersons2;
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region indexreport   || reportId == 5 desingid 8
                    if (reportId == 5)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "SETUPID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "MAINCATEGORYID", "INCLUDECHECKLISTVALUE", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, SETUPID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, MAINCATEGORYID, INCLUDECHECKLISTVALUE, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                if (subAreaEnable == "0")
                                {
                                    ds.Tables[0].Columns.Remove("SUBAREANAME");
                                }
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Target Vs Activity - Monthly  || reportId == 6 designId 9,10
                    if (reportId == 6)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "OBSDATE", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "OBSZEROCHECKLISTVALUE", "TARGETSTATUS", "OBSWITHZEROTARGET", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, OBSDATE, OBSSTATUSID, INCLUDECHECKLISTVALUE, OBSZEROCHECKLISTVALUE, TARGETSTATUS, OBSWITHZEROTARGET, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion


                    Log.InfoFormat("check period Report exists");
                    #region Target Vs Activity - Period  || reportId == 23 designId 47,48
                    if (reportId == 23)
                    {
                        Log.InfoFormat("period reportId:" + reportId);
                        Log.InfoFormat("period designId:" + DESIGNID);
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "OBSDATE", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "OBSZEROCHECKLISTVALUE", "TARGETSTATUS", "OBSWITHZEROTARGET", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, OBSDATE, OBSSTATUSID, INCLUDECHECKLISTVALUE, OBSZEROCHECKLISTVALUE, TARGETSTATUS, OBSWITHZEROTARGET, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);

                                Log.InfoFormat("period datacount:" + lstPersons.Count);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                Log.InfoFormat("No data of period report");
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Site new Observation Stats  || reportId == 7  DESIGNID 11,29,30
                    if (reportId == 7)
                    {
                        if (DESIGNID == 11)
                        {
                            METRICSVALUE = "0";
                        }
                        else if (DESIGNID == 29)
                        {
                            METRICSVALUE = "1";
                        }
                        else if (DESIGNID == 30)
                        {
                            METRICSVALUE = "2";
                        }
                        else
                        {
                            METRICSVALUE = "0";
                        }
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "METRICSVALUE", "YEAR" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, OBSSTATUSID, INCLUDECHECKLISTVALUE, METRICSVALUE, YEAR };
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesSiteObservation ob = new ParameterValuesSiteObservation();
                            ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                string cuposition = ds.Tables[1].Rows[0]["CUSPOSITION"].ToString();
                                if (cuposition == "")
                                {
                                    ds.Tables[1].Columns.RemoveAt(21);
                                    ds.Tables[1].Columns.RemoveAt(20);
                                    ds.Tables[1].Columns.RemoveAt(19);
                                }
                                else if (cuposition == "19")
                                {
                                    ds.Tables[1].Columns.RemoveAt(21);
                                    ds.Tables[1].Columns.RemoveAt(20);
                                }
                                else if (cuposition == "20")
                                {
                                    ds.Tables[1].Columns.RemoveAt(21);
                                    ds.Tables[1].Columns.RemoveAt(19);
                                }
                                else if (cuposition == "21")
                                {
                                    ds.Tables[1].Columns.RemoveAt(20);
                                    ds.Tables[1].Columns.RemoveAt(19);
                                }
                                else if (cuposition == "19,20" || cuposition == "20,19")
                                {
                                    ds.Tables[1].Columns.RemoveAt(21);
                                }
                                else if (cuposition == "20,21" || cuposition == "21,20")
                                {
                                    ds.Tables[1].Columns.RemoveAt(19);
                                }
                                else if (cuposition == "21,19" || cuposition == "19,21")
                                {
                                    ds.Tables[1].Columns.RemoveAt(20);
                                }


                                DataView view = new DataView(ds.Tables[1]);
                                DataTable distinctValues = view.ToTable(true, "SITENAME", "SITEID");
                                DataView view1 = new DataView(ds.Tables[1]);
                                view1.Sort = "YEAR" + " ASC";
                                DataTable distinctValues1 = view1.ToTable(true, "YEAR");
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                List<Dictionary<string, object>> lstSite = ScriptEngine.GetTableRows(distinctValues);
                                List<Dictionary<string, object>> lstYear = ScriptEngine.GetTableRows(distinctValues1);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons1;
                                ob.Total = lstPersons[0];
                                ob.sites = lstSite;
                                ob.year = lstYear;
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region redflag   || reportId == 8 desingid 12
                    if (reportId == 8)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            METRICSVALUE = "";
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "REDFLAGID", "OBSDATE", "METRICSVALUE", "URL", "APPURL" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, REDFLAGID, OBSDATE, metrics, URL, APPURL };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Checklist Report (By Metrics) || reportId == 9 design- 13,14,15
                    else if (reportId == 9)
                    {
                        if (DESIGNID == 13)
                        {
                            SAFECHECK = "0";
                        }
                        else if (DESIGNID == 14)
                        {
                            SAFECHECK = "1";
                        }
                        else if (DESIGNID == 15)
                        {
                            SAFECHECK = "2";
                        }
                        else
                        {
                            SAFECHECK = "0";
                        }
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SAFECHECK", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "INCLUDECHECKLISTVALUE" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SAFECHECK, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, INCLUDECHECKLISTVALUE };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesCA ob = new ParameterValuesCA();
                            ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                string cuposition = ds.Tables[1].Rows[0]["CUSPOSITION"].ToString();
                                if (cuposition == "")
                                {
                                    ds.Tables[1].Columns.RemoveAt(17);
                                    ds.Tables[1].Columns.RemoveAt(16);
                                    ds.Tables[1].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "15")
                                {
                                    ds.Tables[1].Columns.RemoveAt(17);
                                    ds.Tables[1].Columns.RemoveAt(16);
                                }
                                else if (cuposition == "16")
                                {
                                    ds.Tables[1].Columns.RemoveAt(17);
                                    ds.Tables[1].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "17")
                                {
                                    ds.Tables[1].Columns.RemoveAt(16);
                                    ds.Tables[1].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "15,16" || cuposition == "16,15")
                                {
                                    ds.Tables[1].Columns.RemoveAt(17);
                                }
                                else if (cuposition == "16,17" || cuposition == "17,16")
                                {
                                    ds.Tables[1].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "17,15" || cuposition == "15,17")
                                {
                                    ds.Tables[1].Columns.RemoveAt(16);
                                }


                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons1;
                                ob.sitelist = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Checklist Report (By Category) || reportId == 10 DESIGNID 16,17,18
                    else if (reportId == 10)
                    {
                        if (DESIGNID == 16)
                        {
                            SAFECHECK = "0";
                        }
                        else if (DESIGNID == 17)
                        {
                            SAFECHECK = "1";
                        }
                        else if (DESIGNID == 18)
                        {
                            SAFECHECK = "2";
                        }
                        else
                        {
                            SAFECHECK = "0";
                        }
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "SAFECHECK", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "MAINCATEGORYID", "INCLUDECHECKLISTVALUE" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, SAFECHECK, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, MAINCATEGORYID, INCLUDECHECKLISTVALUE };
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);

                            if (SAFECHECK == "2")
                            {
                                ParameterValuesSubchekclist ob = new ParameterValuesSubchekclist();
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                if (ds.Tables[0].Rows.Count != 0)
                                {
                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    List<Dictionary<string, object>> lstPersons3 = ScriptEngine.GetTableRows(ds.Tables[2]);
                                    ob.status = true;
                                    ob.message = "Success";
                                    ob.Data = lstPersons;
                                    ob.sitelist = lstPersons1;
                                    ob.mainCategory = lstPersons3;
                                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                                }
                                else
                                {
                                    obj1.status = false;
                                    obj1.message = "FMKNORECS";
                                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                }
                            }
                            else
                            {
                                ParameterValuesCA ob = new ParameterValuesCA();
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                if (ds.Tables[0].Rows.Count != 0)
                                {
                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    ob.status = true;
                                    ob.message = "Success";
                                    ob.Data = lstPersons;
                                    ob.sitelist = lstPersons1;
                                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                                }
                                else
                                {
                                    obj1.status = false;
                                    obj1.message = "FMKNORECS";
                                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                    Log.InfoFormat("response:" + response);
                                }
                            }
                        }
                    }
                    #endregion

                    #region Pareto Chart || reportId == 11  DESIGNID 19
                    else if (reportId == 11)
                    {
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "OBSERVERID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "SUBAREAID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "MAINCATEGORYID", "OBSSTATUSID", "INCLUDECHECKLISTVALUE" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, OBSERVERID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, SUBAREAID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, MAINCATEGORYID, OBSSTATUSID, INCLUDECHECKLISTVALUE };
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);

                            DataView view = new DataView(ds.Tables[1]);
                            DataTable distinctValues = view.ToTable(true, "SITENAME", "SITEID");

                            ParameterValuesCA ob = new ParameterValuesCA();
                            ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(distinctValues);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons;
                                ob.sitelist = lstPersons1;
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region CategoryComment  || reportId == 13 desingid 21
                    if (reportId == 13)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "OBSERVERID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "MAINCATEGORYID", "SETUPID", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "URL", "APPURL" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, OBSERVERID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, MAINCATEGORYID, SETUPID, OBSSTATUSID, INCLUDECHECKLISTVALUE, URL, APPURL };
                            DataSet ds1 = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds1.Tables[0].Rows.Count != 0)
                            {
                                ds1.Tables[0].DefaultView.Sort = "SITENAME";
                                DataTable dt1 = ds1.Tables[0].DefaultView.ToTable();
                                ds.Tables.Add(dt1);
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Checklist Count || reportId == 14 DESIGNID 22,28
                    else if (reportId == 14)
                    {
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SHIFTID", "OBSERVERID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "SUBAREAID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "INCLUDECHECKLISTVALUE", "CREATEDDATE", "OBSSTATUSID", "OBSZEROCHECKLISTVALUE", "INCLUDEHISTDATAVALUE", "INCOMPOBSERVATION" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SHIFTID, OBSERVERID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, SUBAREAID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, INCLUDECHECKLISTVALUE, CREATEDDATE, OBSSTATUSID, OBSZEROCHECKLISTVALUE, INCLUDEHISTDATAVALUE, INCOMPOBSERVATION };
                        if (String.IsNullOrEmpty(OBSDATE) && String.IsNullOrEmpty(CREATEDDATE))
                        {
                            obj1.status = false;
                            obj1.message = "Observation Date or Entered Date is Mandatory Parameter";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesCA ob = new ParameterValuesCA();
                            ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                if (DESIGNID == 22)
                                {
                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    ob.status = true;
                                    ob.message = "Success";
                                    ob.Data = lstPersons;
                                    ob.sitelist = lstPersons1;

                                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                                }
                                else if (DESIGNID == 28)
                                {
                                    string cuposition = ds.Tables[0].Rows[0]["CUSPOSITION"].ToString();
                                    if (cuposition == "")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(14);
                                        ds.Tables[0].Columns.RemoveAt(13);
                                        ds.Tables[0].Columns.RemoveAt(12);
                                    }
                                    else if (cuposition == "12")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(14);
                                        ds.Tables[0].Columns.RemoveAt(13);
                                    }
                                    else if (cuposition == "13")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(14);
                                        ds.Tables[0].Columns.RemoveAt(12);
                                    }
                                    else if (cuposition == "14")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(13);
                                        ds.Tables[0].Columns.RemoveAt(12);
                                    }
                                    else if (cuposition == "13,14" || cuposition == "14,13")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(12);
                                    }
                                    else if (cuposition == "12,13" || cuposition == "13,12")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(14);
                                    }
                                    else if (cuposition == "12,14" || cuposition == "14,12")
                                    {
                                        ds.Tables[0].Columns.RemoveAt(13);
                                    }

                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    obj1.status = true;
                                    obj1.message = "Success";
                                    obj1.Data = lstPersons;
                                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                }
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Corrective Action Report || reportId == 15 DESIGN ID 23,24
                    else if (reportId == 15)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "OBSERVERID", "MAINCATEGORYID", "SUBCATEGORYID", "SETUPID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "PRIORITYVALUE", "CARDSTATUSID", "RESDATEREQ", "RESPONDEDDATE", "CACARDSTATUS", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, OBSERVERID, MAINCATEGORYID, SUBCATEGORYID, SETUPID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, OBSSTATUSID, INCLUDECHECKLISTVALUE, PRIORITYVALUE, CARDSTATUSID, RESDATEREQ, RESPONDEDDATE, CACARDSTATUS, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (DESIGNID == 44)
                            {
                                if (ds.Tables[0].Rows.Count != 0)
                                {
                                    ParameterValuesDetail ob = new ParameterValuesDetail();
                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    ob.status = true;
                                    ob.message = "Success";
                                    ob.Details = lstPersons;
                                    ob.sitelist = lstPersons1;
                                    ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                                }
                                else
                                {
                                    obj1.status = false;
                                    obj1.message = "FMKNORECS";
                                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                }
                            }
                            else if (DESIGNID == 23)
                            {
                                if (ds.Tables[0].Rows.Count != 0)
                                {
                                    ParameterValuesGraphically ob = new ParameterValuesGraphically();
                                    List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    ob.status = true;
                                    ob.message = "Success";
                                    ob.Graphically = lstPersons;
                                    ob.sitelist = lstPersons1;
                                    ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                    response = Request.CreateResponse(HttpStatusCode.OK, ob);
                                }
                                else
                                {
                                    obj1.status = false;
                                    obj1.message = "FMKNORECS";
                                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                                }
                            }
                        }
                    }
                    #endregion

                    #region Checklist Count Summary  || reportId == 16  desing id 33,34,35
                    if (reportId == 16)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (String.IsNullOrEmpty(CHKSUMGROUP1) && String.IsNullOrEmpty(CHKSUMGROUP2) && String.IsNullOrEmpty(CHKSUMGROUP3))
                        {
                            obj1.status = false;
                            obj1.message = "Group by filter is Mandatory Parameter";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "OBSDATE", "CREATEDDATE", "OBSSTATUSID", "INCLUDEHISTDATAVALUE", "INCLUDECHECKLISTVALUE", "CHKSUMGROUP1", "CHKSUMGROUP2", "CHKSUMGROUP3", "OBSZEROCHECKLISTVALUE", "DESIGNID" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, OBSDATE, CREATEDDATE, OBSSTATUSID, INCLUDEHISTDATAVALUE, INCLUDECHECKLISTVALUE, CHKSUMGROUP1, CHKSUMGROUP2, CHKSUMGROUP3, OBSZEROCHECKLISTVALUE, DESIGNID };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region TradeLine || reportId == 17 design 36,37,38
                    else if (reportId == 17)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            if (DESIGNID == 37)
                            {
                                CHARTYPE = "2";
                            }
                            else if (DESIGNID == 38)
                            {
                                CHARTYPE = "3";
                            }
                            else
                            {
                                CHARTYPE = "1";
                            }
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "SETUPID", "MAINCATEGORYID", "CHARTTYPE", "METRICSVALUE", "OBSSTATUSID", "INCLUDECHECKLISTVALUE" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, SETUPID, MAINCATEGORYID, CHARTYPE, METRICSVALUE, OBSSTATUSID, INCLUDECHECKLISTVALUE };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                ParameterValuesDetail ob = new ParameterValuesDetail();
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Details = lstPersons;
                                ob.sitelist = lstPersons1;
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region UserObserveList  || reportId == 18 desingid 39
                    if (reportId == 18)
                    {
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "OBSSTATUSID", "USERLEVELID", "ROLEID" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, OBSSTATUSID, USERLEVELID, ROLEID };
                        ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    #endregion

                    #region Target Vs Activity - Weekly  || reportId == 19 desingid 40,41
                    if (reportId == 19)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "AREAID", "SUBAREAID", "OBSERVERID", "SHIFTID", "SETUPID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "OBSDATE", "OBSSTATUSID", "OBSZEROCHECKLISTVALUE", "INCLUDECHECKLISTVALUE", "TARGETSTATUS", "OBSWITHZEROTARGET", "STARTOFWEEK", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, AREAID, SUBAREAID, OBSERVERID, SHIFTID, SETUPID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, OBSDATE, OBSSTATUSID, OBSZEROCHECKLISTVALUE, INCLUDECHECKLISTVALUE, TARGETSTATUS, OBSWITHZEROTARGET, STARTOFWEEK, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                ParameterValuesDataDump ob = new ParameterValuesDataDump();
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                obj1.status = true;
                                obj1.message = "Success";
                                obj1.Data = lstPersons;
                                obj1.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion


                    #region Stats By Category || reportId == 20 design42,43
                    else if (reportId == 20)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "SETUPID", "MAINCATEGORYID", "SUBCATEGORYID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "OBSERVERID", "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "SAFECHECK", "YEAR", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, SUBAREAID, SHIFTID, SETUPID, MAINCATEGORYID, SUBCATEGORYID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, OBSERVERID, OBSSTATUSID, INCLUDECHECKLISTVALUE, SAFECHECK, YEAR, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3 };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                ParameterValuesStats ob = new ParameterValuesStats();
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                List<Dictionary<string, object>> lstPersons2 = ScriptEngine.GetTableRows(ds.Tables[2]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons;
                                ob.sitelist = lstPersons1;
                                ob.year = lstPersons2;
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion

                    #region Data Dump Report  || reportId == 21
                    if (reportId == 21)
                    {
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (String.IsNullOrEmpty(SETUPID))
                        {
                            obj1.status = false;
                            obj1.message = "Checklist Setup is Mandatory Parameter";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "SETUPID", "OBSDATE", "INCLUDECOMMENTS" };
                            object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, SETUPID, OBSDATE, INCLUDECOMMENTS };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            ParameterValuesDataDump ob = new ParameterValuesDataDump();
                            if (ds.Tables[0].Rows.Count != 0)
                            {

                                if (subAreaEnable == "0")
                                {
                                    if (ds.Tables[0].Columns[0].ColumnName == "STATUS")
                                    {
                                    }
                                    else
                                    {
                                        ds.Tables[0].Columns.Remove("SUBAREANAME");
                                    }
                                }
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons1;
                                if (ds.Tables[0].Rows[0]["STATUS"].ToString() != "0")
                                {
                                    ob.DataStatus = lstPersons;
                                }
                                else
                                {
                                    ob.DataStatus = new string[] { };
                                }
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                ob.status = false;
                                ob.message = "FMKNORECS";
                                ob.Data = new string[] { };
                                ob.DataStatus = new string[] { };
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                        }
                    }
                    #endregion

                    #region Unsafe Acts – Corrective Actions Coverage Report || reportId == 22 || designid =46
                    else if (reportId == 22)
                    {
                        string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "OBSERVERID", "SHIFTID", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "SUBAREAID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12", "OBSSTATUSID", "SETUPID", "MAINCATEGORYID", "INCLUDECHECKLISTVALUE", "RESDATEREQ", "RESPONDEDDATE", "PRIORITYVALUE", "CARDSTATUSID", "UACACARDSTATUS", "CACARDSTATUS" };
                        object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, OBSDATE, SITEID, AREAID, OBSERVERID, SHIFTID, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, SUBAREAID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12, OBSSTATUSID, SETUPID, MAINCATEGORYID, INCLUDECHECKLISTVALUE, RESDATEREQ, RESPONDEDDATE, PRIORITYVALUE, CARDSTATUSID, UACACARDSTATUS, CACARDSTATUS };
                        if (String.IsNullOrEmpty(OBSDATE))
                        {
                            obj1.status = false;
                            obj1.message = "ALTCHECKLISTCOUNTREPORTALERT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenValNew.Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            if (ds.Tables[0].Rows.Count != 0)
                            {
                                ParameterValuesCA ob = new ParameterValuesCA();
                                List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                                List<Dictionary<string, object>> lstPersons1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                ob.status = true;
                                ob.message = "Success";
                                ob.Data = lstPersons;
                                ob.sitelist = lstPersons1;
                                ob.designData = ScriptEngine.GetTableRows(dtTokenVals.Tables[0]);
                                response = Request.CreateResponse(HttpStatusCode.OK, ob);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "FMKNORECS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Report Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the report list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getReportList")]
        [APIAuthAttribute]
        public HttpResponseMessage getReportList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsVole = new DataSet();
                DataSet dsTrend = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    strquery = ScriptEngine.GetValue("GETREPORTS");
                    string[] paraminsert = { "REPORTTYPE", "ROLEID", };
                    //volume
                    object[] pValuesinsert = { "1", defaultRoleId };
                    dsVole = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    //trend
                    object[] pValuesinsert1 = { "2", defaultRoleId };
                    dsTrend = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert1);
                    dsVole.Tables[0].Merge(dsTrend.Tables[0]);
                    dsVole.Tables[0].Columns.Add("Flag", typeof(int));
                    for (int i = 0; i < dsVole.Tables[0].Rows.Count; i++)
                    {
                        string repid = dsVole.Tables[0].Rows[i]["REPORTID"].ToString();
                        if (repid == "22" || repid == "10" || repid == "4" || repid == "11" || repid == "17" || repid == "15")
                        {
                            dsVole.Tables[0].Rows[i]["Flag"] = 1;
                        }
                        else
                        {
                            dsVole.Tables[0].Rows[i]["Flag"] = 2;
                        }
                    }
                    StatusMsgData obj = new StatusMsgData();
                    if (dsVole.Tables[0].Rows.Count > 0)
                    {
                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(dsVole.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.data = list;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Report List:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the report filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getReportFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getReportFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsSave = new DataSet();
                DataSet dsFilter = new DataSet();
                ParameterValueFilterRep obj = new ParameterValueFilterRep();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string langId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string filterCriteriaId = Convert.ToString(request.SelectToken("filterCriteriaId"));
                    if (String.IsNullOrEmpty(reportId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "REPORTID", "USERID", "LANGUAGEID", "FILTERCRITERIAID" };
                        object[] pValuesinsert = { reportId, userId, langId, filterCriteriaId };
                        strquery = ScriptEngine.GetValue("GETSAVEDFILTERCRITERIA");
                        dsSave = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        strquery = ScriptEngine.GetValue("GETREPORTOPTIONSFILTER");
                        dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        string filSiteId = "";
                        string filAreaId = "";
                        string filSetUpId = "";
                        string groupBy1 = "";
                        string groupBy2 = "";
                        string groupBy3 = "";
                        #region for filter setting data
                        DataTable dtTokenValNewF = new DataTable();
                        string[] group1split = new string[] { };
                        string[] group11 = new string[] { };
                        string[] g = new string[] { };
                        StringBuilder sbb = new StringBuilder();
                        try
                        {
                            int row = 0;
                            for (int i = 0; i < dsFilter.Tables[2].Rows.Count; i++)
                            {
                                int gg = 1;
                                group1split = dsFilter.Tables[2].Rows[i]["RESULT"].ToString().Split(':');
                                group11 = Regex.Split(group1split[1], @"\*\~\*");
                                dtTokenValNewF.Columns.Add(group1split[0], typeof(String));
                                if (dtTokenValNewF.Rows.Count == 0)
                                {
                                    DataRow dr = dtTokenValNewF.NewRow();
                                    dtTokenValNewF.Rows.Add(dr);
                                }
                                foreach (string aa in group11)
                                {
                                    if (gg == group11.Length)
                                    {
                                        if (aa.Split('~')[0].ToString() == "DateRange")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else if (aa.Split('~')[0].ToString() == "LastMonth")
                                        {
                                            sbb.Append(aa.Split('~')[4].ToString());
                                        }
                                        else
                                        {
                                            sbb.Append(aa.Split('~')[0].ToString());
                                        }
                                    }
                                    else
                                    {
                                        sbb.Append(aa.Split('~')[0].ToString() + ",");
                                    }
                                    gg++;
                                }
                                if ("SITEID" == group1split[0])
                                {
                                    filSiteId = sbb.ToString();
                                }
                                if ("AREAID" == group1split[0])
                                {
                                    filAreaId = sbb.ToString();
                                }
                                if ("SETUPID" == group1split[0])
                                {
                                    filSetUpId = sbb.ToString();
                                }
                                if ("CHKSUMGROUP1" == group1split[0])
                                {
                                    groupBy1 = sbb.ToString();
                                }
                                if ("CHKSUMGROUP2" == group1split[0])
                                {
                                    groupBy2 = sbb.ToString();
                                }
                                if ("CHKSUMGROUP3" == group1split[0])
                                {
                                    groupBy3 = sbb.ToString();
                                }
                                dtTokenValNewF.Rows[0][group1split[0]] = sbb.ToString();
                                row++;
                                sbb.Clear();
                            }
                            obj.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF)[0];
                        }
                        catch (Exception ex)
                        {
                            obj.filterSetting = ScriptEngine.GetTableRows(dtTokenValNewF);
                        }
                        #endregion

                        int count = dsFilter.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));
                        for (int i = 0; i < count; i++)
                        {
                            DataSet dsfilter = new DataSet();
                            string query = dsFilter.Tables[0].Rows[i]["QUERY"].ToString();
                            if (query != "DATE")
                                if (filterCriteriaId == "0")
                                {
                                    dsfilter = getValueTable(query, userId, langId, defaultRoleId, "", "", "", "", "", "");
                                }
                                else
                                {
                                    dsfilter = getValueTable(query, userId, langId, defaultRoleId, filSiteId, filAreaId, filSetUpId, groupBy1, groupBy2, groupBy3);
                                }
                            DataRow dr = dtTokenValNew.NewRow();
                            dr[0] = dsFilter.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsFilter.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsFilter.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsFilter.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            if (query != "DATE")
                                dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            else
                                dr[4] = "";
                            dtTokenValNew.Rows.Add(dr);
                        }
                        DataTable dtTokenValoptional = new DataTable();
                        DataTable dtGroupValoptional = new DataTable();
                        DataTable dtTokenValDate = new DataTable();
                        DataTable dtTokenValgroupby = new DataTable();
                        DataTable dtTokenValcategory = new DataTable();
                        DataTable charttype = new DataTable();
                        if (reportId == "17")
                        {
                            try
                            {
                                charttype = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex) { }
                        }
                        else
                        {
                            try
                            {
                                dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                            }
                            catch (Exception ex) { }
                        }
                        try
                        {
                            dtTokenValDate = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "4" + "").CopyToDataTable();
                        }
                        catch (Exception ex) { }
                        try
                        {
                            dtTokenValgroupby = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "6" + "").CopyToDataTable();
                        }
                        catch (Exception ex) { }
                        try
                        {
                            dtTokenValcategory = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "3" + "").CopyToDataTable();
                        }
                        catch (Exception ex) { }
                        try
                        {
                            dtGroupValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "2" + "").CopyToDataTable();
                        }
                        catch (Exception ex) { }
                        DataTable dtTokenValfilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                        defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                        strquery = ScriptEngine.GetValue("GETREPORTS");
                        string[] paraminsert1 = { "REPORTTYPE", "ROLEID", };
                        object[] pValuesinsert1 = { "1", defaultRoleId };
                        DataSet dsVole = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        object[] pValuesinsert11 = { "2", defaultRoleId };
                        DataSet dsTrend = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert11);
                        dsVole.Tables[0].Merge(dsTrend.Tables[0]);
                        DataTable dtTokenValrep = dsVole.Tables[0].Select("REPORTID= " + reportId + "").CopyToDataTable();
                        strquery = ScriptEngine.GetValue("GETREPORTS_INFO");
                        DataSet dsReportInfo = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(dsSave.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.savedFilter = list;
                        obj.filterList = ScriptEngine.GetTableRows(dtTokenValfilter);
                        obj.filterOptional = ScriptEngine.GetTableRows(dtTokenValoptional);
                        obj.filterLabel = ScriptEngine.GetTableRows(dsFilter.Tables[1]);
                        obj.reportDetail = ScriptEngine.GetTableRows(dtTokenValrep)[0];
                        obj.filterDate = ScriptEngine.GetTableRows(dtTokenValDate);
                        obj.filterGroupBy = ScriptEngine.GetTableRows(dtTokenValgroupby);
                        obj.filterCategories = ScriptEngine.GetTableRows(dtTokenValcategory);
                        obj.filterChartType = ScriptEngine.GetTableRows(charttype);
                        obj.reportDesignId = int.Parse(dsReportInfo.Tables[0].Rows[0]["DESIGNID"].ToString());
                        obj.filterGroup = ScriptEngine.GetTableRows(dtGroupValoptional);
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Report Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/deleteFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string filterId = Convert.ToString(request.SelectToken("filterCriteriaId"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    if (String.IsNullOrEmpty(reportId) || String.IsNullOrEmpty(filterId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESAVEDREPORTSELECTION");
                        string[] paraminsert = { "USERID", "REPORTID", "FILTERID" };
                        object[] pValuesinsert = { userId, reportId, filterId };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "LBLFILTERRESET";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When delete Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Saves the filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/saveFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage saveFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string filterCriteriaId = Convert.ToString(request.SelectToken("filterCriteriaId"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string filterName = Convert.ToString(request.SelectToken("filterName"));
                    string optionId = Convert.ToString(request.SelectToken("options"));
                    if (String.IsNullOrEmpty(reportId) || String.IsNullOrEmpty(filterCriteriaId) || String.IsNullOrEmpty(filterName) || String.IsNullOrEmpty(optionId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(optionId);
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dtTokenVals.Columns.Count; i++)
                        {
                            string columnaName = dtTokenVals.Columns[i].ToString();
                            string columnid = dtTokenVals.Rows[0][columnaName].ToString();
                            string[] idsplit = columnid.Split(',');
                            foreach (string ids in idsplit)
                            {
                                sb.Append(columnaName + "~" + ids + ",");
                            }
                        }
                        strquery = ScriptEngine.GetValue("UPDATEREPORTFILTER");
                        string[] paraminsert = { "USERID", "REPORTID", "FILTERCRITERIAID", "FILTERNAME", "OPTIONID" };
                        object[] pValuesinsert = { userId, reportId, filterCriteriaId, filterName, sb.ToString() };
                        int iresult = 0;
                        iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;

                            if (filterCriteriaId == "0")
                            {
                                obj1.message = "ALTREPORTFILTERCREATESUCCESS";
                            }
                            else
                            {
                                obj1.message = "ALTREPORTFILTERUPDATESUCCESS";
                            }
                            obj1.id = iresult;
                        }
                        else if (iresult == -1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTVALIDFILTERNAME2";
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When save Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the edit report data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getEditReportData")]
        [APIAuthAttribute]
        public HttpResponseMessage getEditReportData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dssubarea = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    if (String.IsNullOrEmpty(reportId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string[] paraminsert = { "REPORTID", "USERID", "LANGUAGEID" };
                        object[] pValuesinsert = { reportId, userId, languageId };
                        strquery = ScriptEngine.GetValue("GETREPORTOPTIONSEDIT");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        ParameterValueFilter obj = new ParameterValueFilter();
                        if (dssubarea.Tables[0].Rows.Count > 0)
                        {
                            string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                            strquery = ScriptEngine.GetValue("GETREPORTSDETAIL");
                            string[] paraminsert1 = { "REPORTID", "ROLEID", };
                            object[] pValuesinsert1 = { reportId, defaultRoleId };
                            DataSet dsReport = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.data = list;
                            obj.reportName = dsReport.Tables[0].Rows[0]["REPORTNAME"].ToString();
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Edit Report data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the edit report data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/updateEditReportData")]
        [APIAuthAttribute]
        public HttpResponseMessage updateEditReportData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string optionId = Convert.ToString(request.SelectToken("optionId"));
                    strquery = ScriptEngine.GetValue("UPDATEREPORTOPTIONMASTER");
                    string[] paraminsert = { "USERID", "REPORTID", "OPTIONID" };
                    object[] pValuesinsert = { userId, reportId, optionId };
                    int iresult = 0;
                    iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                    if (iresult != 0 && iresult > 0)
                    {
                        obj1.status = true;
                        obj1.message = "Selected filter criteria has been Updated successfully";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "LBLTECHERROR";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Edit Report data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the sub report data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getSubReportData")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubReportData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dssubarea = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string subReportId = Convert.ToString(request.SelectToken("subReportId"));
                    string reportId = Convert.ToString(request.SelectToken("reportId"));
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string USERID = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    int DESIGNID = Convert.ToInt32(request.SelectToken("designId"));
                    string YEAR = Convert.ToString(request.SelectToken("YEAR"));
                    string MONTH = Convert.ToString(request.SelectToken("MONTH"));
                    string SAFECHECK = "0";
                    string METRICSVALUE = "0";
                    //DataPro configuration setting 
                    Dictionary<string, object> scriptengine_data;
                    StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                    string strsettings = srSettings.ReadToEnd();
                    scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                    srSettings.Close();
                    Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                    Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                    dctfulldata = scriptengine_data;
                    dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "dataproreportservice").Value;
                    string DRS_WSNAME = dctpartdata["WSNAME"].ToString();
                    string DRS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string DRS_WSURL = dctpartdata["WSURL"].ToString();
                    string DRS_WSURL1 = dctpartdata["WSURL1"].ToString();
                    string url = AppInfo.getUrlHost(httpUrl);
                    string APPURL = "";
                    if (url.Contains("http"))
                    {
                        APPURL = url.Replace("/", "a_b");
                    }
                    else
                    {
                        string urll = "http://" + url;
                        APPURL = urll.Replace("/", "a_b");
                    }
                    string URL = DRS_WSURL.Replace("dataproreportservice.asmx", "").Replace("/", "a_b");
                    //date
                    string OBSDATE = Convert.ToString(request.SelectToken("OBSDATE"));
                    string CREATEDDATE = Convert.ToString(request.SelectToken("CREATEDDATE"));  //Entered Date
                    string RESDATEREQ = Convert.ToString(request.SelectToken("RESDATEREQ"));   //action due date
                    string RESPONDEDDATE = Convert.ToString(request.SelectToken("RESPONDEDDATE")); //responded date
                    //site filter
                    string SITEID = Convert.ToString(request.SelectToken("SITEID"));
                    string AREAID = Convert.ToString(request.SelectToken("AREAID"));
                    string SUBAREAID = Convert.ToString(request.SelectToken("SUBAREAID"));
                    string SHIFTID = Convert.ToString(request.SelectToken("SHIFTID"));
                    string OBSERVERID = Convert.ToString(request.SelectToken("OBSERVERID"));
                    string CUSTOMFIELD1 = Convert.ToString(request.SelectToken("CUSTOMFIELD1"));
                    string CUSTOMFIELD2 = Convert.ToString(request.SelectToken("CUSTOMFIELD2"));
                    string CUSTOMFIELD3 = Convert.ToString(request.SelectToken("CUSTOMFIELD3"));
                    string REDFLAGID = Convert.ToString(request.SelectToken("REDFLAGID"));
                    //groups
                    string GROUP1 = Convert.ToString(request.SelectToken("GROUP1"));
                    string GROUP2 = Convert.ToString(request.SelectToken("GROUP2"));
                    string GROUP3 = Convert.ToString(request.SelectToken("GROUP3"));
                    string GROUP4 = Convert.ToString(request.SelectToken("GROUP4"));
                    string GROUP5 = Convert.ToString(request.SelectToken("GROUP5"));
                    string GROUP6 = Convert.ToString(request.SelectToken("GROUP6"));
                    string GROUP7 = Convert.ToString(request.SelectToken("GROUP7"));
                    string GROUP8 = Convert.ToString(request.SelectToken("GROUP8"));
                    string GROUP9 = Convert.ToString(request.SelectToken("GROUP9"));
                    string GROUP10 = Convert.ToString(request.SelectToken("GROUP10"));
                    string GROUP11 = Convert.ToString(request.SelectToken("GROUP11"));
                    string GROUP12 = Convert.ToString(request.SelectToken("GROUP12"));
                    //category
                    string SETUPID = Convert.ToString(request.SelectToken("SETUPID"));   // //checklistsetup
                    string MAINCATEGORYID = Convert.ToString(request.SelectToken("MAINCATEGORYID"));
                    string SUBCATEGORYID = Convert.ToString(request.SelectToken("SUBCATEGORYID"));
                    //optional 
                    string USERLEVELID = Convert.ToString(request.SelectToken("USERLEVELID"));
                    string CACARDSTATUS = Convert.ToString(request.SelectToken("CACARDSTATUS"));
                    string OBSSTATUSID = Convert.ToString(request.SelectToken("OBSSTATUSID"));
                    string INCLUDECHECKLISTVALUE = Convert.ToString(request.SelectToken("INCLUDECHECKLISTVALUE")); //includecheklistwithoutgroupassign
                    string OBSZEROCHECKLISTVALUE = Convert.ToString(request.SelectToken("OBSZEROCHECKLISTVALUE"));  //hide observer with zero checklist
                    string OBSWITHZEROTARGET = Convert.ToString(request.SelectToken("OBSWITHZEROTARGET"));   //include observer with zeror target
                    string TARGETSTATUS = Convert.ToString(request.SelectToken("TARGETSTATUS"));
                    string metrics = Convert.ToString(request.SelectToken("METRICSVALUE"));  //for red flag
                    string INCLUDEHISTDATAVALUE = Convert.ToString(request.SelectToken("INCLUDEHISTDATAVALUE"));  //include historial data
                    string INCOMPOBSERVATION = Convert.ToString(request.SelectToken("INCOMPOBSERVATION"));    //display incomplete observation   
                    string PRIORITYVALUE = Convert.ToString(request.SelectToken("PRIORITYVALUE"));
                    string UACACARDSTATUS = Convert.ToString(request.SelectToken("UACACARDSTATUS"));
                    string CHARTYPE = Convert.ToString(request.SelectToken("CHARTTYPE"));    //TRADELINE
                    string ROLEID = Convert.ToString(request.SelectToken("ROLEID"));      //for userobserlist
                    string STARTOFWEEK = Convert.ToString(request.SelectToken("STARTOFWEEK"));     //target vs activity
                    string CARDSTATUSID = Convert.ToString(request.SelectToken("CARDSTATUSID"));         //CA Report
                    string INCLUDECOMMENTS = Convert.ToString(request.SelectToken("INCLUDECOMMENTS")); //DUMP REPORT
                    //group by
                    string CHKSUMGROUP1 = Convert.ToString(request.SelectToken("CHKSUMGROUP1"));  //
                    string CHKSUMGROUP2 = Convert.ToString(request.SelectToken("CHKSUMGROUP2"));
                    string CHKSUMGROUP3 = Convert.ToString(request.SelectToken("CHKSUMGROUP3"));
                    string RESPONSESTATUS = Convert.ToString(request.SelectToken("RESPONSESTATUS"));
                    string RESPONSESTATUSGENERAL = Convert.ToString(request.SelectToken("RESPONSESTATUSGENERAL"));
                    string SUBRPTPARAM = Convert.ToString(request.SelectToken("SUBRPTPARAM"));
                    string ATTACHMENTS = Convert.ToString(request.SelectToken("ATTACHMENTS"));
                    string IMAGEPATH = Convert.ToString(request.SelectToken("IMAGEPATH"));
                    if (String.IsNullOrEmpty(subReportId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETSUBREPORTSINFO");
                        string[] paramrptinfo = { "SUBREPORTID" };
                        object[] pValuesrptinfo = { subReportId };
                        DataSet dtTokenVals = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                        DataSet ds = new DataSet();
                        if (subReportId == "7" || subReportId == "10" || subReportId == "11")  //for Monthly Safe/Unsafe Stats
                        {
                            if (String.IsNullOrEmpty(MONTH))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add Month Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                if (subReportId == "10")
                                {
                                    METRICSVALUE = "1";
                                }
                                else if (subReportId == "11")
                                {
                                    METRICSVALUE = "2";
                                }
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID", "OBSERVERID", "MAINCATEGORYID", "SETUPID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "METRICSVALUE","MONTH", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID, OBSERVERID, MAINCATEGORYID, SETUPID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE, METRICSVALUE, MONTH, CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3};
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            }
                        }
                        if (subReportId == "3" || subReportId == "4")  //Checklist Report (By Category)
                        {
                            if (subReportId == "3")
                            {
                                SAFECHECK = "0";
                            }
                            else if (subReportId == "4")
                            {
                                SAFECHECK = "1";
                            }
                            else
                            {
                                SAFECHECK = "0";
                            }
                            string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","SETUPID", "MAINCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","SAFECHECK"};
                            object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID, SETUPID ,MAINCATEGORYID,   GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                             INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3,SAFECHECK};
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);

                            string cuposition = ds.Tables[0].Rows[0]["CUSPOSITION"].ToString();
                            if (cuposition == "")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(20);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "19")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(20);
                            }
                            else if (cuposition == "20")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "21")
                            {
                                ds.Tables[0].Columns.RemoveAt(20);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "19,20" || cuposition == "20,19")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                            }
                            else if (cuposition == "20,21" || cuposition == "21,20")
                            {
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "21,19" || cuposition == "19,21")
                            {
                                ds.Tables[0].Columns.RemoveAt(20);
                            }

                            if (subAreaEnable == "0")
                            {
                                ds.Tables[0].Columns.Remove("SUBAREAID");
                                ds.Tables[0].Columns.Remove("SUBAREANAME");
                            }
                        }
                        if (subReportId == "5")  //Checklist Report (By Category) sub category
                        {
                            SAFECHECK = "2";
                            string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","SETUPID", "MAINCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","SAFECHECK","SUBCATEGORYID"};
                            object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID, SETUPID ,MAINCATEGORYID,   GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                             INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3,SAFECHECK,SUBCATEGORYID};
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);

                            string cuposition = ds.Tables[0].Rows[0]["CUSPOSITION"].ToString();
                            if (cuposition == "")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(20);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "19")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(20);
                            }
                            else if (cuposition == "20")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "21")
                            {
                                ds.Tables[0].Columns.RemoveAt(20);
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "19,20" || cuposition == "20,19")
                            {
                                ds.Tables[0].Columns.RemoveAt(21);
                            }
                            else if (cuposition == "20,21" || cuposition == "21,20")
                            {
                                ds.Tables[0].Columns.RemoveAt(19);
                            }
                            else if (cuposition == "21,19" || cuposition == "19,21")
                            {
                                ds.Tables[0].Columns.RemoveAt(20);
                            }

                            if (subAreaEnable == "0")
                            {
                                ds.Tables[0].Columns.Remove("SUBAREAID");
                                ds.Tables[0].Columns.Remove("SUBAREANAME");
                            }
                        }
                        if (subReportId == "1")  //Pareto Chart - Subcategory
                        {
                            if (String.IsNullOrEmpty(MAINCATEGORYID))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add MainCategory Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID", "MAINCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID,   GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3};
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            }
                        }
                        if (subReportId == "2")  //Pareto Chart - Subcategory Detail   //SUBCATEGORYID
                        {
                            if (String.IsNullOrEmpty(MAINCATEGORYID) && String.IsNullOrEmpty(SUBCATEGORYID))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add MainCategory & SubCategory Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID", "MAINCATEGORYID","SUBCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID, SUBCATEGORYID,  GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3};
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                                string cuposition = ds.Tables[0].Rows[0]["CUSPOSITION"].ToString();
                                if (cuposition == "")
                                {
                                    ds.Tables[0].Columns.RemoveAt(13);
                                    ds.Tables[0].Columns.RemoveAt(12);
                                    ds.Tables[0].Columns.RemoveAt(11);
                                }
                                else if (cuposition == "11")
                                {
                                    ds.Tables[0].Columns.RemoveAt(13);
                                    ds.Tables[0].Columns.RemoveAt(12);
                                }
                                else if (cuposition == "12")
                                {
                                    ds.Tables[0].Columns.RemoveAt(13);
                                    ds.Tables[0].Columns.RemoveAt(11);
                                }
                                else if (cuposition == "13")
                                {
                                    ds.Tables[0].Columns.RemoveAt(12);
                                    ds.Tables[0].Columns.RemoveAt(11);
                                }
                                else if (cuposition == "11,12" || cuposition == "12,11")
                                {
                                    ds.Tables[0].Columns.RemoveAt(13);
                                }
                                else if (cuposition == "12,13" || cuposition == "13,12")
                                {
                                    ds.Tables[0].Columns.RemoveAt(11);
                                }
                                else if (cuposition == "13,11" || cuposition == "11,13")
                                {
                                    ds.Tables[0].Columns.RemoveAt(12);
                                }

                                if (subAreaEnable == "0")
                                {
                                    ds.Tables[0].Columns.Remove("SUBAREANAME");
                                }
                            }
                        }
                        if (subReportId == "9")  //Category Comments Attachments
                        {
                            if (String.IsNullOrEmpty(ATTACHMENTS))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add Attachment Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID", "MAINCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","ATTACHMENTS", "IMAGEPATH", "URL", "APPURL"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID, GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3,ATTACHMENTS, IMAGEPATH, URL, APPURL };
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            }
                        }
                        if (subReportId == "12")  //Checklist Count
                        {
                            string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID",   "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","OBSZEROCHECKLISTVALUE", "CREATEDDATE", "INCLUDEHISTDATAVALUE", "INCOMPOBSERVATION"};
                            object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,  GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3,OBSZEROCHECKLISTVALUE, CREATEDDATE, INCLUDEHISTDATAVALUE, INCOMPOBSERVATION};
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                        }
                        if (subReportId == "6")  //Corrective Action Detail
                        {
                            string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID", "MAINCATEGORYID","SUBCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","PRIORITYVALUE",   "CARDSTATUSID", "RESDATEREQ", "RESPONDEDDATE" , "RESPONSESTATUS" , "CACARDSTATUS" };
                            object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID, SUBCATEGORYID,  GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, PRIORITYVALUE,  CARDSTATUSID, RESDATEREQ, RESPONDEDDATE , RESPONSESTATUS , CACARDSTATUS };
                            DataSet ds1 = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                            try
                            {
                                ds1.Tables[0].DefaultView.Sort = "SITENAME";
                                DataTable dtTokenVal1 = ds1.Tables[0].DefaultView.ToTable();
                                ds.Tables.Add(dtTokenVal1);
                            }
                            catch (Exception ex) { }
                        }
                        if (subReportId == "14")  //Corrective Action Detail General
                        {
                            if (String.IsNullOrEmpty(RESPONSESTATUSGENERAL))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add Response General Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID", "MAINCATEGORYID","SUBCATEGORYID",  "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","PRIORITYVALUE",   "CARDSTATUSID", "RESDATEREQ", "RESPONDEDDATE" , "RESPONSESTATUS" , "CACARDSTATUS" , "RESPONSESTATUSGENERAL"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID, SUBCATEGORYID,  GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, PRIORITYVALUE,  CARDSTATUSID, RESDATEREQ, RESPONDEDDATE , RESPONSESTATUS , CACARDSTATUS , RESPONSESTATUSGENERAL};
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                                if (subAreaEnable == "0")
                                {
                                    ds.Tables[0].Columns.Remove("SUBAREANAME");
                                }
                            }
                        }
                        if (subReportId == "13")  //Checklist Count Summary
                        {
                            string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID",   "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE",  "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3","OBSZEROCHECKLISTVALUE", "CREATEDDATE", "INCLUDEHISTDATAVALUE","CHKSUMGROUP1","CHKSUMGROUP2","CHKSUMGROUP3","DESIGNID","SUBRPTPARAM"};
                            object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID , GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3, OBSZEROCHECKLISTVALUE, CREATEDDATE, INCLUDEHISTDATAVALUE,CHKSUMGROUP1,CHKSUMGROUP2,CHKSUMGROUP3,"33",SUBRPTPARAM };
                            ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);

                            if (!SUBRPTPARAM.Contains("GROUP"))
                            {
                                string cuposition = ds.Tables[0].Rows[0]["CUSPOSITION"].ToString();
                                if (cuposition == "")
                                {
                                    ds.Tables[0].Columns.RemoveAt(17);
                                    ds.Tables[0].Columns.RemoveAt(16);
                                    ds.Tables[0].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "15")
                                {
                                    ds.Tables[0].Columns.RemoveAt(17);
                                    ds.Tables[0].Columns.RemoveAt(16);
                                }
                                else if (cuposition == "16")
                                {
                                    ds.Tables[0].Columns.RemoveAt(17);
                                    ds.Tables[0].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "17")
                                {
                                    ds.Tables[0].Columns.RemoveAt(16);
                                    ds.Tables[0].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "15,16" || cuposition == "16,15")
                                {
                                    ds.Tables[0].Columns.RemoveAt(17);
                                }
                                else if (cuposition == "16,17" || cuposition == "17,16")
                                {
                                    ds.Tables[0].Columns.RemoveAt(15);
                                }
                                else if (cuposition == "17,15" || cuposition == "15,17")
                                {
                                    ds.Tables[0].Columns.RemoveAt(16);
                                }
                            }
                            if (subAreaEnable == "0")
                            {
                                ds.Tables[0].Columns.Remove("SUBAREAID");
                                ds.Tables[0].Columns.Remove("SUBAREANAME");
                            }
                        }
                        if (subReportId == "15")  //Unsafe Acts – Corrective Actions Coverage Detail report
                        {
                            if (String.IsNullOrEmpty(RESPONSESTATUS))
                            {
                                obj1.status = false;
                                obj1.message = "Please Add Response Status Parameter";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = {  "COMPANYID", "USERID", "LANGUAGEID", "OBSDATE", "SITEID", "AREAID", "SUBAREAID", "SHIFTID","OBSERVERID","SETUPID","MAINCATEGORYID","SUBCATEGORYID","GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10", "GROUP11", "GROUP12",
                            "OBSSTATUSID", "INCLUDECHECKLISTVALUE", "CUSTOMFIELD1", "CUSTOMFIELD2", "CUSTOMFIELD3", "PRIORITYVALUE",
                                                                            "CARDSTATUSID", "RESDATEREQ", "RESPONDEDDATE" , "RESPONSESTATUS" , "CACARDSTATUS" ,"UACACARDSTATUS"};
                                object[] pValuesinsert = { COMPANYID,   USERID,   LANGUAGEID,  OBSDATE,   SITEID, AREAID, SUBAREAID, SHIFTID,OBSERVERID, SETUPID ,MAINCATEGORYID, SUBCATEGORYID,  GROUP1, GROUP2, GROUP3, GROUP4, GROUP5, GROUP6, GROUP7, GROUP8, GROUP9, GROUP10, GROUP11, GROUP12,
                            OBSSTATUSID, INCLUDECHECKLISTVALUE,  CUSTOMFIELD1, CUSTOMFIELD2, CUSTOMFIELD3,PRIORITYVALUE, CARDSTATUSID, RESDATEREQ, RESPONDEDDATE , RESPONSESTATUS , CACARDSTATUS, UACACARDSTATUS  };
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                                if (subAreaEnable == "0")
                                {
                                    ds.Tables[0].Columns.Remove("SUBAREANAME");
                                }
                            }
                        }
                        if (subReportId == "8")  //redfladid
                        {
                            if (String.IsNullOrEmpty(REDFLAGID))
                            {
                                obj1.status = false;
                                obj1.message = "Please Select Red Flag Id";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                string[] paraminsert = { "COMPANYID", "USERID", "LANGUAGEID", "SITEID", "REDFLAGID", "OBSDATE", "METRICSVALUE", "URL", "APPURL" };
                                object[] pValuesinsert = { COMPANYID, USERID, LANGUAGEID, SITEID, REDFLAGID, OBSDATE, metrics, URL, APPURL };
                                ds = BaseAdapter.ExecuteStoreProc_Dataset(Convert.ToString(dtTokenVals.Tables[0].Rows[0]["QUERYVALUE"]), paraminsert, pValuesinsert);
                                if (subAreaEnable == "0")
                                {
                                    ds.Tables[0].Columns.Remove("SUBAREANAME");
                                }
                            }
                        }
                        StatusMsgData obj = new StatusMsgData();
                        ParameterValuesChkSummary pvcchksummary = new ParameterValuesChkSummary();
                        if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                        {
                            List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                            if (subReportId == "13")
                            {
                                pvcchksummary.data = list;
                                try
                                {
                                    List<Dictionary<string, object>> list1 = ScriptEngine.GetTableRows(ds.Tables[1]);
                                    pvcchksummary.headingValue = list1[0];
                                }
                                catch (Exception ex)
                                {
                                    pvcchksummary.headingValue = new string[] { };
                                }
                                pvcchksummary.data = list;
                                pvcchksummary.status = true;
                                response = Request.CreateResponse(HttpStatusCode.OK, pvcchksummary);
                            }
                            else
                            {
                                obj.data = list;
                                obj.status = true;
                                obj.message = "Success";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Sub Report data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getSiteFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSiteFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));

                    strquery = ScriptEngine.GetValue("WAAREAS");
                    DataSet dsAllFilter = new DataSet();
                    string[] paraminsert = { "USERID", "SITEID", "ROLEID", "LANGUAGEID" };
                    object[] pValuesinsert = { userId, siteId, defaultRoleId, languageId };
                    dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WOSHIFTS");
                    DataSet dsShift = new DataSet();
                    dsShift = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WOSSITEOBSERVERS");
                    DataSet dsObs = new DataSet();
                    dsObs = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WASETUPS");
                    DataSet dsSetup = new DataSet();
                    dsSetup = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WACCESSCUSTOMFIELD1");
                    DataSet dsCustom1 = new DataSet();
                    dsCustom1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WACCESSCUSTOMFIELD2");
                    DataSet dsCustom2 = new DataSet();
                    dsCustom2 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    strquery = ScriptEngine.GetValue("WACCESSCUSTOMFIELD3");
                    DataSet dsCustom3 = new DataSet();
                    dsCustom3 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    ParameterValuesArea obj = new ParameterValuesArea();
                    obj.status = true;
                    obj.message = "Success";
                    obj.area = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                    obj.shift = ScriptEngine.GetTableRows(dsShift.Tables[0]);
                    obj.observer = ScriptEngine.GetTableRows(dsObs.Tables[0]);
                    obj.checkList = ScriptEngine.GetTableRows(dsSetup.Tables[0]);
                    obj.customField1 = ScriptEngine.GetTableRows(dsCustom1.Tables[0]);
                    obj.customField2 = ScriptEngine.GetTableRows(dsCustom2.Tables[0]);
                    obj.customField3 = ScriptEngine.GetTableRows(dsCustom3.Tables[0]);
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
                }

            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Site Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the sub area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getSubAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WACCESSSUBAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "AREAID" };
                        object[] pValuesinsert2 = { areaId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        ParameterValuesSubArea obj = new ParameterValuesSubArea();
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            obj.status = true;
                            obj.message = "Success";
                            obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj.status = true;
                            obj.message = "FMKNORECS";
                            obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get SubArea Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the group by filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getGroupByFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getGroupByFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string CHKSUMGROUP1 = Convert.ToString(request.SelectToken("group1"));
                    string CHKSUMGROUP2 = Convert.ToString(request.SelectToken("group2"));
                    string CHKSUMGROUP3 = Convert.ToString(request.SelectToken("group3"));
                    if (String.IsNullOrEmpty(CHKSUMGROUP1) && String.IsNullOrEmpty(CHKSUMGROUP2) && String.IsNullOrEmpty(CHKSUMGROUP3))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataSet dsChkSum1 = new DataSet();
                        DataSet dsChkSum2 = new DataSet();
                        DataSet dsChkSum3 = new DataSet();
                        string[] paraminsert2 = { "CHKSUMGROUP1", "CHKSUMGROUP2", "CHKSUMGROUP3", "LANGUAGEID" };
                        object[] pValuesinsert2 = { CHKSUMGROUP1, CHKSUMGROUP2, CHKSUMGROUP3, languageId };

                        strquery = ScriptEngine.GetValue("GETCHKCOUNTSUMMARYGROUP1");
                        dsChkSum1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2); //group2 group3

                        strquery = ScriptEngine.GetValue("GETCHKCOUNTSUMMARYGROUP2");
                        dsChkSum2 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);  //group1 group3

                        strquery = ScriptEngine.GetValue("GETCHKCOUNTSUMMARYGROUP3");
                        dsChkSum3 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);  //group1 group2

                        ParameterValuesGroupBy obj = new ParameterValuesGroupBy();
                        obj.status = true;
                        obj.message = "Success";
                        obj.group1 = ScriptEngine.GetTableRows(dsChkSum1.Tables[0]);
                        obj.group2 = ScriptEngine.GetTableRows(dsChkSum2.Tables[0]);
                        obj.group3 = ScriptEngine.GetTableRows(dsChkSum3.Tables[0]);
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Group By Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the check list filter main catg.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/Reports/getCheckListFilterMainCatg")]
        [APIAuthAttribute]
        public HttpResponseMessage getCheckListFilterMainCatg(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //for read appinfo config get connectionstring
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                //for check token to db
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string chkListSetupId = Convert.ToString(request.SelectToken("chkListSetupId"));
                    if (String.IsNullOrEmpty(chkListSetupId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataSet dsChkSum1 = new DataSet();
                        string[] paraminsert2 = { "USERID", "SETUPID" };
                        object[] pValuesinsert2 = { userId, chkListSetupId };
                        strquery = ScriptEngine.GetValue("WAMAINCATEGORY");
                        dsChkSum1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2); //group2 group3
                        ParameterValueCheckList obj = new ParameterValueCheckList();
                        if (dsChkSum1.Tables[0].Rows.Count > 0)
                        {
                            obj.status = true;
                            obj.message = "Success";
                            obj.mainCat = ScriptEngine.GetTableRows(dsChkSum1.Tables[0]);
                        }
                        else
                        {
                            obj.status = true;
                            obj.message = "FMKNORECS";
                            obj.mainCat = ScriptEngine.GetTableRows(dsChkSum1.Tables[0]);
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Checklist Filter data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="siteId">The site identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <param name="SETUPID">The setupid.</param>
        /// <param name="groupBy1">The group by1.</param>
        /// <param name="groupBy2">The group by2.</param>
        /// <param name="groupBy3">The group by3.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string roleId, string siteId, string areaId, string SETUPID, string groupBy1, string groupBy2, string groupBy3)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "ROLEID", "SITEID", "AREAID", "SETUPID", "CHKSUMGROUP1", "CHKSUMGROUP2", "CHKSUMGROUP3" };
            object[] pValuesinsert = { userid, languageId, roleId, siteId, areaId, SETUPID, groupBy1, groupBy2, groupBy3 };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion

    }
}