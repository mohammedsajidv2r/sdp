﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// CorrectiveAction Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class CorrectiveActionController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CorrectiveActionController"/> class.
        /// </summary>
        public CorrectiveActionController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(CorrectiveActionController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method
        /// <summary>
        /// Gets the corrective actions.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getCorrectiveActions")]
        [APIAuthAttribute]
        public HttpResponseMessage getCorrectiveActions(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery, userid = "", compId = "";
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds1 = new DataSet();
                DataSet dsAvail = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    compId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string group1 = Convert.ToString(request.SelectToken("group1"));
                    string group2 = Convert.ToString(request.SelectToken("group2"));
                    string group3 = Convert.ToString(request.SelectToken("group3"));
                    string group4 = Convert.ToString(request.SelectToken("group4"));
                    string group5 = Convert.ToString(request.SelectToken("group5"));
                    string group6 = Convert.ToString(request.SelectToken("group6"));
                    string group7 = Convert.ToString(request.SelectToken("group7"));
                    string group8 = Convert.ToString(request.SelectToken("group8"));
                    string group9 = Convert.ToString(request.SelectToken("group9"));
                    string group10 = Convert.ToString(request.SelectToken("group10"));
                    string filSiteId = Convert.ToString(request.SelectToken("filSiteId"));
                    string filAreaId = Convert.ToString(request.SelectToken("filAreaId"));
                    string filSubAreaId = Convert.ToString(request.SelectToken("filSubAreaId"));
                    string filShiftId = Convert.ToString(request.SelectToken("filShiftId"));
                    string filRespPerson = Convert.ToString(request.SelectToken("filRespPerson"));
                    string filActionOwner = Convert.ToString(request.SelectToken("filActionOwner"));
                    string filResDateReqTo = Convert.ToString(request.SelectToken("filResDateReqTo"));
                    string filResDateReqFrom = Convert.ToString(request.SelectToken("filResDateReqFrom"));
                    string filRespondedtTokenValo = Convert.ToString(request.SelectToken("filRespondedTo"));
                    string filRespondedFrom = Convert.ToString(request.SelectToken("filRespondedFrom"));
                    string filCardStatusId = Convert.ToString(request.SelectToken("filCardStatusId"));
                    string filCorractiveType = Convert.ToString(request.SelectToken("filCorractiveType"));
                    string cleraFilter = Convert.ToString(request.SelectToken("clearFilter"));
                    string comingDue = Convert.ToString(request.SelectToken("caType"));
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    string siteId = string.Empty;
                    string siteName = string.Empty;
                    string areaId = "0";
                    strquery = ScriptEngine.GetValue("GETMYCORRACTIVEACTIONFILTER");
                    string[] paramrptinfo = { "USERID", "LANGUAGEID" };
                    object[] pValuesrptinfo = { userid, languageId };
                    dsAvail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);
                    string[] data = dsAvail.Tables[2].Rows[0]["RESULT"].ToString().Split('!');
                    siteId = data[0].Replace("~~", "").Replace("SITEID:", "");
                    siteName = data[1].Replace("~~", "");
                    if (filSiteId != "")
                    {
                        filSiteId = filSiteId;
                    }
                    else if (cleraFilter == "0")
                    {
                        filSiteId = "";
                    }
                    else
                    {
                        filSiteId = siteId;
                    }
                    if (dsAvail.Tables[0].Rows.Count != 0)
                    {
                        Log.InfoFormat(comingDue);
                        if (comingDue == "1" || comingDue == "2")
                        {
                            filSiteId = siteId;
                            filCardStatusId = "1";
                        }
                        int count = dsAvail.Tables[0].Rows.Count;
                        DataTable dtTokenValNew = new DataTable();
                        dtTokenValNew.Columns.Add("Name", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONID", typeof(String));
                        dtTokenValNew.Columns.Add("OPTIONSCATEGORYID", typeof(String));
                        dtTokenValNew.Columns.Add("REPORTPARAM", typeof(String));
                        dtTokenValNew.Columns.Add("Data", typeof(String));
                        for (int i = 0; i < count; i++)
                        {
                            DataSet dsfilter = new DataSet();
                            string query = dsAvail.Tables[0].Rows[i]["QUERY"].ToString();
                            if (query != "DATE")
                                dsfilter = getValueTable(query, userid, languageId, siteId, roleId, areaId);
                            DataRow dr = dtTokenValNew.NewRow();
                            dr[0] = dsAvail.Tables[0].Rows[i]["OPTIONNAME"].ToString();
                            dr[1] = dsAvail.Tables[0].Rows[i]["OPTIONID"].ToString();
                            dr[2] = dsAvail.Tables[0].Rows[i]["OPTIONSCATEGORYID"].ToString();
                            dr[3] = dsAvail.Tables[0].Rows[i]["REPORTPARAM"].ToString();
                            if (query != "DATE")
                                dr[4] = JsonConvert.SerializeObject(dsfilter.Tables[0]);
                            else
                                dr[4] = "";
                            dtTokenValNew.Rows.Add(dr);
                        }
                        DataTable dtTokenValoptional = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "5" + "").CopyToDataTable();
                        DataTable dtTokenValfilterr = dtTokenValNew.Select("OPTIONSCATEGORYID <> " + "5" + "").CopyToDataTable();
                        //DataTable dtTokenValfilter = dtTokenValfilterr.Select("OPTIONSCATEGORYID <> " + "4" + " AND OPTIONSCATEGORYID <> " + 1 + "").CopyToDataTable();
                        DataTable dtTokenValfilter = dtTokenValfilterr.Select("OPTIONSCATEGORYID <> " + "4" + " AND OPTIONSCATEGORYID <> " + 1 + "").Count() > 0 ? dtTokenValfilterr.Select("OPTIONSCATEGORYID <> " + "4" + " AND OPTIONSCATEGORYID <> " + 1 + "").CopyToDataTable() : dtTokenValfilterr.Clone();
                        DataTable dtTokenValSiteFilter = dtTokenValNew.Select("OPTIONSCATEGORYID = " + "1" + "").CopyToDataTable();
                        DataTable dtTokenValSite = dtTokenValSiteFilter.Select("OPTIONID = " + "1" + "").CopyToDataTable();
                        DataTable dtTokenValArea = dtTokenValSiteFilter.Select("OPTIONID = " + "2" + "").CopyToDataTable();
                        DataTable dtTokenValShiftArea = dtTokenValSiteFilter.Select("OPTIONID = " + "4" + "").CopyToDataTable();
                        strquery = ScriptEngine.GetValue("GETMYCORRACTIVEACTIONLISTING");
                        string[] param1 = { "USERID", "GROUP1", "GROUP2", "GROUP3", "GROUP4", "GROUP5", "GROUP6", "GROUP7", "GROUP8", "GROUP9", "GROUP10"
                        ,"SITEID","AREAID","SUBAREAID","SHIFTID","RESPONSIBLEPERSON","ACTOWNER","RESDATEREQTO","RESDATEREQFROM","RESPONDEDDATETO","RESPONDEDDATEFROM"
                        ,"CARDSTATUSID","CORRACTYPE", "COMINGDUE"};
                        object[] pValues = { userid, group1,group2,group3,group4,group5,group6,group7,group8,group9,group10
                                ,filSiteId,filAreaId,filSubAreaId,filShiftId,filRespPerson,filActionOwner,filResDateReqTo,filResDateReqFrom,filRespondedtTokenValo,filRespondedFrom
                        ,filCardStatusId,filCorractiveType,comingDue};
                        ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, param1, pValues);
                        ParameterCAList obj = new ParameterCAList();
                        List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
                        DataTable dtTokenValSubArea = new DataTable();

                        if (subAreaEnable == "1")
                        {
                            dtTokenValSubArea = dtTokenValSiteFilter.Select("OPTIONID = " + "53" + "").CopyToDataTable();
                            obj.subAreafilters = ScriptEngine.GetTableRows(dtTokenValSubArea)[0];
                        }
                        else
                        {
                            obj.subAreafilters = "";
                        }
                        try
                        {

                            if (ds1.Tables[0].Rows.Count > 0)
                            {
                                list = ScriptEngine.GetTableRows(ds1.Tables[0]);
                            }
                            else
                            {
                                list = ScriptEngine.GetTableRows(ds1.Tables[1]);
                            }
                        }
                        catch (Exception ex)
                        { }
                        if (list.Count > 0)
                        {
                            obj.message = "Success";
                            obj.status = true;
                            obj.corractiveAction = list;
                        }
                        else
                        {
                            obj.message = "FMKNORECS";
                            obj.status = true;
                            obj.corractiveAction = new string[] { };
                        }
                        obj.optionCategory = ScriptEngine.GetTableRows(dsAvail.Tables[1]);
                        obj.filters = ScriptEngine.GetTableRows(dtTokenValfilter);
                        obj.siteFilters = ScriptEngine.GetTableRows(dtTokenValSite)[0];
                        obj.areaFilters = ScriptEngine.GetTableRows(dtTokenValArea)[0];
                        obj.shiftfilters = ScriptEngine.GetTableRows(dtTokenValShiftArea)[0];
                        obj.optionalFilters = ScriptEngine.GetTableRows(dtTokenValoptional);
                        obj.defaultSiteId = siteId;
                        obj.defaultSiteName = siteName;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Corrective Action Data:" + ex.StackTrace);
                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                object[] pValuesrptinfo = { compId, userid, "LOAD_MYCAFILTERS", 1, "CORRECTIVEACTION", "LOAD_MYCAFILTERS", ex.Message };
                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/delete")]
        [APIAuthAttribute]
        public HttpResponseMessage delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string corrActionId = Convert.ToString(request.SelectToken("corrActionId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(corrActionId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETECORRACTIVEACTIONCARDS");
                        string[] paraminsert = { "CORRACTIONID", "UPDATEDBY" };
                        object[] pValuesinsert = { corrActionId, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Delete Corrective Action:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/changeStatus")]
        [APIAuthAttribute]
        public HttpResponseMessage changeStatus(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string corrActionId = Convert.ToString(request.SelectToken("caId"));
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    if (String.IsNullOrEmpty(corrActionId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESTATUSCORRECTIVEACTION");
                        string[] paraminsert = { "CORRACTIONID", "UPDATEDBY" };
                        object[] pValuesinsert = { corrActionId, userId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCHANGESTATUSSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Changes Status Corrective Action:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the corr action add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getCorrActionAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getCorrActionAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string OBSAPPROVALPC = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string mode = "0";
                    string cardID = "0";
                    if (String.IsNullOrEmpty(siteId))
                        siteId = DEFAULTSITEID;
                    string[] finalqueries = new string[6];
                    finalqueries[0] = ScriptEngine.GetValue("GETCHECKLISTSITELIST");
                    finalqueries[1] = ScriptEngine.GetValue("GETCHECKLISTOBSERVERS");
                    finalqueries[2] = ScriptEngine.GetValue("GETCUSTOMFIELDSNAME");
                    finalqueries[3] = ScriptEngine.GetValue("GETCUSTOMFIELDSVALUE");
                    finalqueries[4] = ScriptEngine.GetValue("GETCHECKLISTAREALIST");
                    finalqueries[5] = ScriptEngine.GetValue("GETCHECKLISTSHIFTLIST");
                    strquery = string.Join(";", finalqueries).ToString();
                    string[] paraminsert = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID" };
                    object[] pValuesinsert = { userId, mode, OBSAPPROVALPC, siteId, cardID, languageId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterCAAddList obj = new ParameterCAAddList();
                        obj.status = true;
                        obj.message = "Success";
                        obj.defaultSiteId = int.Parse(DEFAULTSITEID);
                        obj.sites = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.observer = ScriptEngine.GetTableRows(ds.Tables[1]);
                        obj.fieldName = ScriptEngine.GetTableRows(ds.Tables[3]);
                        obj.fieldValue = ScriptEngine.GetTableRows(ds.Tables[4]);
                        obj.area = ScriptEngine.GetTableRows(ds.Tables[5]);
                        obj.shifts = ScriptEngine.GetTableRows(ds.Tables[6]);
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Corrective Action data on Add Click:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the site ca data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getSiteCAData")]
        [APIAuthAttribute]
        public HttpResponseMessage getSiteCAData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string OBSAPPROVALPC = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string mode = "0";
                    string cardID = "0";
                    if (String.IsNullOrEmpty(siteId))
                        siteId = DEFAULTSITEID;
                    string[] finalqueries = new string[3];
                    finalqueries[0] = ScriptEngine.GetValue("GETCHECKLISTOBSERVERS");
                    finalqueries[1] = ScriptEngine.GetValue("GETCHECKLISTAREALIST");
                    finalqueries[2] = ScriptEngine.GetValue("GETCHECKLISTSHIFTLIST");
                    strquery = string.Join(";", finalqueries).ToString();
                    string[] paraminsert = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID" };
                    object[] pValuesinsert = { userId, mode, OBSAPPROVALPC, siteId, cardID, languageId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterCASiteFilter obj = new ParameterCASiteFilter();
                        obj.status = true;
                        obj.message = "Success";
                        obj.observer = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.area = ScriptEngine.GetTableRows(ds.Tables[2]);
                        obj.shifts = ScriptEngine.GetTableRows(ds.Tables[3]);
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Site Corrective Action Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the ca area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getCAArea")]
        [APIAuthAttribute]
        public HttpResponseMessage getCAArea(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dssubarea = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteIdd = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string SITEID = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(mode))
                        { mode = "0"; }
                        string[] paraminsert = { "MODE", "AREAID", };
                        object[] pValuesinsert = { mode, areaId };
                        strquery = ScriptEngine.GetValue("GETOFFLINESUBAREALIST");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        ParameterValuesSubArea obj = new ParameterValuesSubArea();
                        if (dssubarea.Tables[0].Rows.Count > 0)
                        {
                            List<Dictionary<string, object>> listsubarea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = listsubarea;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            obj1.Data = new string[] { };
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get Area Corrective Action Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the corrective action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/insertCorrectiveAction")]
        [APIAuthAttribute]
        public HttpResponseMessage insertCorrectiveAction(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string obsDate = Convert.ToString(request.SelectToken("obsDate"));
                    string observerId = Convert.ToString(request.SelectToken("observerId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string customFieldvalueId = Convert.ToString(request.SelectToken("customFieldvalueId"));

                    if (String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(obsDate) || String.IsNullOrEmpty(observerId) || String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTCORRACTIVEACTION");
                        string[] paraminsert = { "CUSTOMFIELDVALUEID", "COMPANYID", "SELOBSERVERID", "CREATEDBY", "OBSERVATIONDATE"
                        , "SHIFTID", "AREAID", "SUBAREAID", "SITEID"};
                        object[] pValuesinsert = { customFieldvalueId, companyId,observerId,userId,obsDate,shiftId
                                ,areaId,subAreaId,siteId };
                        int result = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (result > 0)
                        {
                            ParameterInsertCA obj = new ParameterInsertCA();
                            obj.status = true;
                            obj.message = "ALTCORRACTIVESELCARDCREATED";
                            obj.corrActionId = result;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Insert Corrective Action Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the corrective action for edit.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getCorrectiveActionForEdit")]
        [APIAuthAttribute]
        public HttpResponseMessage getCorrectiveActionForEdit(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAction = new DataSet();
                DataSet dsAvail = new DataSet();
                DataSet dsTab = new DataSet();
                DataSet dsTab1 = new DataSet();
                DataSet dsGeneral = new DataSet();
                DataSet dsGeneral1 = new DataSet();
                DataSet dsObserver = new DataSet();
                DataSet dsObserverCmt = new DataSet();
                DataSet dsResp = new DataSet();
                DataSet dssubarea = new DataSet();
                DataSet dschecklst = new DataSet();

                //DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();


                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userid = dtTokenVal.Rows[0]["USERID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string roleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string caId = Convert.ToString(request.SelectToken("caId"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string stropCardtTokenValype = Convert.ToString(request.SelectToken("stopCardType"));
                    string checkListSetUpId = Convert.ToString(request.SelectToken("checkListSetUpId"));

                 //   string cardReqId = Convert.ToString(request.SelectToken("cardReqId"));
                    if (String.IsNullOrEmpty(stropCardtTokenValype))
                    {
                        stropCardtTokenValype = "2";
                    }
              
                    string siteId = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string mode = "1";
                    string modActionOwner = "1";
                    if (caId == "0")
                    {
                        modActionOwner = "0";
                    }

                    strquery = ScriptEngine.GetValue("GETCORRACTIVEACTIONGENERALINFO");
                    string[] paraminsert = { "CARDID", "USERID", "MODE" };
                    object[] pValuesinsert = { cardId, userid, mode };
                    dsGeneral = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    siteId = dsGeneral.Tables[0].Rows[0]["SITEID"].ToString();

                    strquery = ScriptEngine.GetValue("GETACTIONOWNER");
                    string[] param1 = { "MODE", "SITEID", "CORRACTIONID", "LANGUAGECODE" };
                    object[] pValues = { modActionOwner, siteId, caId, languageId };
                    dsAction = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, param1, pValues);


                    ParameterValuesForEdit obj = new ParameterValuesForEdit();
                    string CHECKLISTSETUPID = "";
                    if (caId == "0")
                    {
                        strquery = ScriptEngine.GetValue("GETAVAILASSIGNCATEGORYFORCORRACTIION");
                        string[] param11 = { "LANGUAGEID", "SITEID", "STOPCARDTYPE", "MODE", "CARDID" };
                        object[] pValues11 = { languageId, siteId, stropCardtTokenValype, "0", cardId };
                        dsTab = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, param11, pValues11);
                        CHECKLISTSETUPID = dsGeneral.Tables[0].Rows[0]["CHECKLISTSETUPID"].ToString();
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETCORRACTIVEACTIONTABINFO");
                        string[] param = { "CORRACTIONID", "LANGUAGECODE", "SITEID", "USERID" };
                        object[] pValues1 = { caId, languageId, siteId, userid };
                        dsTab1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, param, pValues1);
                        CHECKLISTSETUPID = dsTab1.Tables[0].Rows[0]["CHECKLISTSETUPID"].ToString();
                    }
                    string[] finalqueries = new string[5];
                    finalqueries[0] = ScriptEngine.GetValue("GETCHECKLISTOBSERVERS");
                    finalqueries[1] = ScriptEngine.GetValue("GETCUSTOMFIELDSNAME");
                    finalqueries[2] = ScriptEngine.GetValue("GETCUSTOMFIELDSVALUE");
                    finalqueries[3] = ScriptEngine.GetValue("GETCHECKLISTAREALIST");
                    finalqueries[4] = ScriptEngine.GetValue("GETCHECKLISTSHIFTLIST");
                    strquery = string.Join(";", finalqueries).ToString();
                    string[] paraminsert1 = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID", "CORRACTIONID" };
                    object[] pValuesinsert1 = { userid, mode, "0", siteId, cardId, languageId, caId };
                    dsGeneral1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                    string[] paraminsertRes = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID", "CORRACTIONID" };
                    object[] pValuesinsertRes = { userid, modActionOwner, "0", siteId, cardId, languageId, caId };
                    strquery = ScriptEngine.GetValue("GETREPONSIBLEPERSONFORCORRACTION");
                    dsResp = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsertRes, pValuesinsertRes);

                    DataTable dtResAvail = new DataTable();
                    if (modActionOwner == "1")
                    {
                        dtResAvail = dsResp.Tables[0].Copy();
                        dtResAvail.Merge(dsResp.Tables[1]);
                    }
                    else
                    {
                        dtResAvail = dsResp.Tables[0].Copy();
                    }


                    if (CHECKLISTSETUPID != "")
                    {
                        strquery = ScriptEngine.GetValue("GETSTOPCARDOBSERVATION");
                        string[] paraminsert2 = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID", "CHECKLISTSETUPID" };
                        object[] pValuesinsert2 = { userid, mode, obsApprovalPc, siteId, cardId, languageId, CHECKLISTSETUPID };
                        dsObserver = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                    }

                    if (dsGeneral.Tables[0].Rows.Count > 0)
                    {
                        strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                        string[] paraminsert11 = { "LANGUAGECODE" };
                        object[] pValuesinsert11 = { languageId };
                        dschecklst = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert11, pValuesinsert11);
                        string subareid = dsGeneral.Tables[0].Rows[0]["AREAID"].ToString();
                        string[] paraminsertsub = { "MODE", "AREAID", };
                        object[] pValuesinsertsub = { mode, subareid };
                        strquery = ScriptEngine.GetValue("GETOFFLINESUBAREALIST");
                        dssubarea = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsertsub, pValuesinsertsub);
                        obj.subArea = ScriptEngine.GetTableRows(dssubarea.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.actionOwner = ScriptEngine.GetTableRows(dsAction.Tables[0]);
                        if (caId == "0")
                        {
                            obj.caAvail = ScriptEngine.GetTableRows(dsTab.Tables[0]);
                        }
                        else
                        {
                            try
                            {
                                DataTable dtAvail = dsTab1.Tables[2].Copy();
                                dtAvail.Merge(dsTab1.Tables[1]);
                                obj.caAvail = ScriptEngine.GetTableRows(dtAvail);

                            }
                            catch (Exception ex)
                            {
                                if (dsTab1.Tables[1].Rows[0]["Column1"].ToString() == "")
                                {
                                    obj.caAvail = new string[] { };
                                }
                            }
                        }
                        try
                        {
                            //string[] resperId = Regex.Split(dsTab1.Tables[0].Rows[0]["SELPERSON"].ToString(), @"\D+");
                            //resperId = resperId.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                            //string resId = String.Join(",", resperId.ToArray());
                            //dsTab1.Tables[0].Rows[0]["SELPERSON"] = resId.ToString();
                            obj.caData = ScriptEngine.GetTableRows(dsTab1.Tables[0])[0];
                            obj.caAssing = ScriptEngine.GetTableRows(dsTab1.Tables[3])[0]["categoryId"];
                            obj.caCategory = ScriptEngine.GetTableRows(dsTab1.Tables[4])[0];
                        }
                        catch (Exception ex)
                        {
                            obj.caData = "";
                            obj.caAssing = "";
                            obj.caCategory = "";
                        }
                        try
                        {
                            obj.observerMainCat = ScriptEngine.GetTableRows(dsObserver.Tables[0]);
                            obj.observerSubCat = ScriptEngine.GetTableRows(dsObserver.Tables[1]);

                            //for get areaname
                            #region
                            ///-----------ADDED FOR ADD/SHOW NEW FIELDS-------------------
                            int UPDATEREQFLAGAPP = 0;

                            string[] parameterinsert1 = { "USERID", "LANGUAGEID", "CARDID", "ROLEID", "OBSAPPROVALPC" };
                            object[] paraValuesinsert1 = { userid, languageId, cardId, defaultRoleId, UPDATEREQFLAGAPP };
                            strquery = ScriptEngine.GetValue("GETSTOPCARDCHECKLISTINFO");
                            DataSet ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parameterinsert1, paraValuesinsert1);
                            string siteId1 = Convert.ToString(ds.Tables[0].Rows[0]["SITEID"]);
                            string areaId = Convert.ToString(ds.Tables[0].Rows[0]["AREAID"]);
                            string checkListSetUpId1 = Convert.ToString(ds.Tables[0].Rows[0]["CHECKLISTSETUPID"]);
                            string[] parammeterinsert = { "USERID", "LANGUAGEID", "CARDID", "ROLEID", "OBSAPPROVALPC", "SITEID", "MODE", "AREAID", "CHECKLISTSETUPID" };
                            object[] parammeterValuesinsert = { userid, languageId, cardId, defaultRoleId, UPDATEREQFLAGAPP, siteId1, "1", areaId, checkListSetUpId1 };
                            strquery = ScriptEngine.GetValue("GETSTOPCARDCHECKLISTAREAOBSERVERSHIFTINFO");
                            ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, parammeterinsert, parammeterValuesinsert);
                            DataTable dtAvail1 = ds1.Tables[1].Copy();
                            dtAvail1.Merge(ds1.Tables[0]);
                            //if (ds.Tables[0].Rows.Count != 0)
                            //{

                            DataTable daraea = ds1.Tables[3].Select("AREAID = " + dsGeneral.Tables[0].Rows[0]["AREAID"].ToString() + "").CopyToDataTable();
                            obj.areaName = daraea.Rows[0]["AREANAME"].ToString();
                            DataTable dshift = ds1.Tables[4].Select("SHIFTID = " + dsGeneral.Tables[0].Rows[0]["SHIFTID"].ToString() + "").CopyToDataTable();
                            obj.shiftName = dshift.Rows[0]["SHIFTNAME"].ToString();

                            obj.observerId = ScriptEngine.GetTableRows(ds1.Tables[2])[0]["USERID"];
                            obj.setupName = ScriptEngine.GetTableRows(ds.Tables[0])[0]["SETUPNAME"].ToString();

                            DataTable dobserver = new DataTable();
                            string observId = ds1.Tables[2].Rows[0]["USERID"].ToString();
                            System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            string[] obsersplit = observId.Split(',');
                            int obseverCount = 1;
                            for (int i = 0; i < obsersplit.Length; i++)
                            {
                                DataTable dtTokenValOb = dtAvail1.Select("USERID = " + obsersplit[i] + "").CopyToDataTable();
                                if (obseverCount == obsersplit.Length)
                                {
                                    string[] uname = dtTokenValOb.Rows[0]["USERNAME"].ToString().Split(',');
                                    sb.Append(uname[0] + "" + uname[1]);
                                }
                                else
                                {
                                    string[] uname = dtTokenValOb.Rows[0]["USERNAME"].ToString().Split(',');
                                    sb.Append(uname[0] + "" + uname[1] + "," + " ");
                                }
                                obseverCount++;
                            }
                            obj.observerName = sb.ToString();
                            //}
                            //----------------end of new code
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            obj.observerMainCat = new string[] { };
                            obj.observerSubCat = new string[] { };
                            obj.areaName = "";
                            obj.setupName = "";
                            obj.shiftName = "";
                            obj.observerName = "";
                        }
                        try
                        {
                            obj.observerId = ScriptEngine.GetTableRows(dsGeneral1.Tables[2])[0]["USERID"];
                        }
                        catch (Exception ex)
                        {
                            obj.observerId = "";
                        }
                        DataTable dtAvailUser = dsGeneral1.Tables[1].Copy();
                        dtAvailUser.Merge(dsGeneral1.Tables[0]);
                        obj.general = ScriptEngine.GetTableRows(dsGeneral.Tables[0])[0];
                        obj.resPerson = ScriptEngine.GetTableRows(dtResAvail);
                        obj.observer = ScriptEngine.GetTableRows(dtAvailUser);
                        obj.fieldName = ScriptEngine.GetTableRows(dsGeneral1.Tables[3]);
                        obj.fieldValue = ScriptEngine.GetTableRows(dsGeneral1.Tables[4]);
                        obj.area = ScriptEngine.GetTableRows(dsGeneral1.Tables[5]);
                        obj.shifts = ScriptEngine.GetTableRows(dsGeneral1.Tables[6]);
                        obj.checkListConfig = ScriptEngine.GetTableRows(dschecklst.Tables[0])[0];

                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Corrective Action Edit Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observation comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/getObservationComment")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservationComment(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsAllFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string OBSAPPROVALPC = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatId = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatId = Convert.ToString(request.SelectToken("subCatId"));
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(mainCatId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETCATEGORYCOMMENTS");
                        string[] paraminsert2 = { "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "LANGUAGEID", "OBSAPPROVALPC" };
                        object[] pValuesinsert2 = { cardId, mainCatId, subCatId, languageId, OBSAPPROVALPC };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);

                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterValuesObserComment obj = new ParameterValuesObserComment();
                            obj.status = true;
                            obj.message = "Success";
                            obj.details = ScriptEngine.GetTableRows(dsAllFilter.Tables[0])[0];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Corrective Action Observation Comment Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the corrective action general.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/updateCorrectiveActionGeneral")]
        [APIAuthAttribute]
        public HttpResponseMessage updateCorrectiveActionGeneral(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string obsDate = Convert.ToString(request.SelectToken("obsDate"));
                    string observerId = Convert.ToString(request.SelectToken("observerId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string customFieldvalueId = Convert.ToString(request.SelectToken("customFieldvalueId"));
                    string safeComments = Convert.ToString(request.SelectToken("safeComments"));
                    string unSafeComments = Convert.ToString(request.SelectToken("unSafeComments"));
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(obsDate) || String.IsNullOrEmpty(observerId) || String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATECORRACTIVEACTION");
                       string[] paraminsert = { "CARDID","CUSTOMFIELDVALUEID", "SELOBSERVERID", "UPDATEDBY", "OBSERVATIONDATE"
                        , "SHIFTID", "AREAID", "SUBAREAID", "SITEID","SAFECOMMENTS","UNSAFECOMMENTS"};
                        object[] pValuesinsert = {cardId, customFieldvalueId, observerId,userId,obsDate,shiftId
                                ,areaId,subAreaId,siteId, safeComments,unSafeComments};
                        int result = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (result > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCORRACTIVESELCARDUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update Corrective Action General Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the corrective action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/CorrectiveAction/updateCorrectiveAction")]
        [APIAuthAttribute]
        public HttpResponseMessage updateCorrectiveAction(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string obsApprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string updateCorractiveactiondata = Convert.ToString(request.SelectToken("updateCorractiveActionData"));
                    string otherRespperson = Convert.ToString(request.SelectToken("otherRespPerson"));
                    string respPerson = Convert.ToString(request.SelectToken("respPerson"));
                    string corractionId = Convert.ToString(request.SelectToken("corractionId"));
                    string responseDate = Convert.ToString(request.SelectToken("responseDate"));
                    string responseRequireDate = Convert.ToString(request.SelectToken("responseRequireDate"));
                    string priority = Convert.ToString(request.SelectToken("priority"));
                    string otherEmailId = Convert.ToString(request.SelectToken("otherEmailId"));
                    string actionPlanned = Convert.ToString(request.SelectToken("actionPlanned"));
                    string actionPrerformed = Convert.ToString(request.SelectToken("actionPerformed"));
                    string cardStatus = Convert.ToString(request.SelectToken("cardStatus"));
                    string owner = Convert.ToString(request.SelectToken("owner"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string assingCategory = Convert.ToString(request.SelectToken("assignCategory"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string checkListsetupid = Convert.ToString(request.SelectToken("checkListSetupId"));

                    //DataPro configuration setting 
                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                    string strAPPID = dctpartdata["APPID"].ToString();
                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                    string ES_WSURL = dctpartdata["WSURL"].ToString();
                    DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(updateCorractiveactiondata);
                    if (String.IsNullOrEmpty(actionPlanned) || String.IsNullOrEmpty(assingCategory)
                        || String.IsNullOrEmpty(owner) || String.IsNullOrEmpty(responseRequireDate)
                        || String.IsNullOrEmpty(cardStatus) || String.IsNullOrEmpty(priority) || String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(corractionId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        if (corractionId == "0")
                        {
                            strquery = ScriptEngine.GetValue("INSERTCORRACTIVEACTIONCATEGORYASSIGN");
                            string[] paraminsert = { "OTHERRESPONSIBLEPERSON", "RESPONSEDATE", "RESPONSEREQUIREDDATE", "PRIORITY", "OTHEREMAILID"
                        , "ACTIONPLANNED", "ACTIONPERFORMED", "CARDSTATUS", "OWNER", "CREATEDBY","RESPONSIBLEPERSON","ASSIGNEDCATEGORYLIST","COMPANYID"
                        ,"CARDID","CAPAFROMTAB"};
                            object[] pValuesinsert = { otherRespperson, responseDate,responseRequireDate,priority,otherEmailId,actionPlanned
                                ,actionPrerformed,cardStatus,owner,userId,respPerson,assingCategory,companyId ,cardId,"1" };
                            dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        else
                        {
                            string xml = "<modified>"
                       + "<respperson>" + dtTokenVals.Rows[0]["RESPPERSON"].ToString() + "</respperson>"
                       + "<respdate>" + dtTokenVals.Rows[0]["RESPDATE"].ToString() + "</respdate>"
                       + "<respreqdate>" + dtTokenVals.Rows[0]["RESPREQDATE"].ToString() + "</respreqdate>"
                       + "<priority>" + dtTokenVals.Rows[0]["PRIORITY"].ToString() + "</priority>"
                       + "<othermailid>" + dtTokenVals.Rows[0]["OTHEREMAILID"].ToString() + "</othermailid>"
                       + "<planned>" + dtTokenVals.Rows[0]["PLANNED"].ToString() + "</planned>"
                       + "<performed>" + dtTokenVals.Rows[0]["PERFORMED"].ToString() + "</performed>"
                       + "<castatus>" + dtTokenVals.Rows[0]["CASTATUS"].ToString() + "</castatus>"
                       + "<actowner>" + dtTokenVals.Rows[0]["ACTOWNER"].ToString() + "</actowner>"
                       + "<categoryassigned>" + dtTokenVals.Rows[0]["CATEGORYASSIGNED"].ToString() + "</categoryassigned>"
                       + "</modified>";
                            strquery = ScriptEngine.GetValue("UPDATECORRACTIVEACTIONCATEGORYASSIGN");
                            string[] paraminsert = { "UPDATECORRECTIVEACTIONDATA", "OTHERRESPONSIBLEPERSON", "RESPONSEDATE", "RESPONSEREQUIREDDATE", "PRIORITY", "OTHEREMAILID"
                        , "ACTIONPLANNED", "ACTIONPERFORMED", "CARDSTATUS", "OWNER", "UPDATEDBY", "CORRACTIONID","RESPONSIBLEPERSON","ASSIGNEDCATEGORYLIST","COMPANYID"
                        ,"CARDID"};
                            object[] pValuesinsert = { xml, otherRespperson, responseDate,responseRequireDate,priority,otherEmailId,actionPlanned
                                ,actionPrerformed,cardStatus,owner,userId,corractionId,respPerson,assingCategory,companyId ,cardId };
                            dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        Log.InfoFormat("dsFilter.Tables[0].Rows.Count" + dsFilter.Tables[0].Rows.Count);
                        if (dsFilter.Tables[0].Rows.Count != 0)
                        {
                            ParameterUpdateCA obj = new ParameterUpdateCA();
                            DataSet dsmaildetails = new DataSet();
                            string str = "";
                            if (corractionId == "0")
                            {
                                obj.corractionId = int.Parse(dsFilter.Tables[0].Rows[0][1].ToString());
                                string[] paramcorrectiveaction = { "CORRACTIONID" };
                                object[] pValuecorrectiveaction = { obj.corractionId };
                                strquery = ScriptEngine.GetValue("MAILCORRECTIVEACTION");
                                dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcorrectiveaction, pValuecorrectiveaction);
                                Log.InfoFormat("==CorrectiveAction for Assigned=");
                            }
                            else
                            {
                                obj.corractionId = int.Parse(corractionId);
                                string[] paramcorrectiveaction = { "CORRACTIONID", "MODID" };
                                object[] pValuecorrectiveaction = { obj.corractionId, dsFilter.Tables[0].Rows[0][1].ToString() };
                                strquery = ScriptEngine.GetValue("MAILCORRECTIVEACTIONUPDATE");
                                dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcorrectiveaction, pValuecorrectiveaction);
                                Log.InfoFormat("==CorrectiveAction for Update=");
                            }
                            try
                            {
                                if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                {
                                    Log.InfoFormat("==CorrectiveAction Email Process Start==");
                                    string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl;
                                    int ischeduleid;
                                    DateTime senddate;
                                    object objEmail;
                                    for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                    {
                                        ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                        strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                        strreplyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                        strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                        strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                        strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                        strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                        strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                        senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                        strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                        str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + companyId + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                        Log.InfoFormat("JSON String:" + str);
                                        //mail.SendNotificationDetails(str);
                                        object[] objSendNotificationDetails = new object[1];
                                        objSendNotificationDetails[0] = str;
                                        CallWebServices CWS = new CallWebServices();
                                        objEmail = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                        Log.InfoFormat("WebService Return Value:" + objEmail.ToString());
                                    }
                                }
                                else
                                {
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for Corrective Action:" + ex.StackTrace);
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { companyId, userId, "Corrective Actions Updated", 1, "MAILCORRECTIVEACTIONUPDATE", str, ex.Message };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                            }
                            obj.status = true;
                            obj.message = "ALTCORRACTIVESELCARDUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Update Corrective Action Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="siteId">The site identifier.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <param name="areaId">The area identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string siteId, string roleId, string areaId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID", "AREAID", "MODE", "CORRACTIONID" };
            object[] pValuesinsert = { userid, languageId, siteId, roleId, areaId, "0", "0" };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion

    }
}

