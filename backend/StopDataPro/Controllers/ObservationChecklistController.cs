﻿using DataPro;
using DataPro.APPMOD.ADMIN;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    /// <summary>
    /// ObservationChecklist Controller
    /// </summary>
    /// <seealso cref="System.Web.Http.ApiController" />
    public class ObservationChecklistController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ObservationChecklistController"/> class.
        /// </summary>
        public ObservationChecklistController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.AppInfo = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(ObservationChecklistController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the AppInfo.
        /// </summary>
        /// <value>
        /// The AppInfo.
        /// </value>
        public AppInfo AppInfo { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }

        #endregion

        #region Action Method

        /// <summary>
        /// Gets the observation list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getObservationList")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservationList(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));
               
                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string subAreaEnable = dtTokenVal.Rows[0]["ENABLESUBAREAS"].ToString();
                    strquery = ScriptEngine.GetValue("GETDEFAULTCHEKLISTFILTER");
                    string[] paraminsert1 = { "USERID" };
                    object[] pValuesinsert1 = { userId };

                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
              
                    string DEFAULTSITEID = ds.Tables[0].Rows[0]["DEFAULTSITEID"].ToString();
                    string OBSLISTINGXMONTHSVAL = ds.Tables[0].Rows[0]["OBSLISTINGXMONTHSVAL"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string observerId = Convert.ToString(request.SelectToken("observerId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    string fromDate = Convert.ToString(request.SelectToken("fromDate"));
                    string toDate = Convert.ToString(request.SelectToken("toDate"));
                    string statusId = Convert.ToString(request.SelectToken("statusId"));
                    string displayIncomplete = Convert.ToString(request.SelectToken("displayIncomplete"));
                    string unsafeChecklist = Convert.ToString(request.SelectToken("unsafeChecklist"));
                    string unsafeChecklistCA = Convert.ToString(request.SelectToken("unsafeChecklistCA"));
                    string approvalStatus = Convert.ToString(request.SelectToken("approvalStatus"));
                    string filterDateType = Convert.ToString(request.SelectToken("filterDateType"));
                    string userTypeId = Convert.ToString(request.SelectToken("userType"));
                    string todate1 = toDate;
                    string fromDate1 = fromDate;
                    if (String.IsNullOrEmpty(siteId))
                    {
                       string[] newArray = DEFAULTSITEID.Split('!');
                       siteId = newArray[0].Replace("~~", "");
                    }
                    if (String.IsNullOrEmpty(fromDate) || String.IsNullOrEmpty(toDate))
                    {
                        string[] newArray = OBSLISTINGXMONTHSVAL.Split('-');
                        fromDate = newArray[0].Trim();
                        toDate = newArray[1].Trim();
                        filterDateType = "FTDATE";
                    }
                    if (todate1 == "1" || todate1 == "0")
                    {
                        fromDate = fromDate1;
                        toDate = todate1;
                    }
                    strquery = ScriptEngine.GetValue("GETFILTERBASEDSTOPCARDLISTING");
                    string[] paraminsert = { "USERID",  "SITEID", "AREAID", "SUBAREAID", "SHIFTID" , "OBSERVERID", "CHECKLISTSETUPID","FROMDATE", "TODATE",
                       "STATUSID", "INCOMPLETEOBSERVATIONID", "UNSAFEOBS","UNSAFEOBSWITHOUTCA", "OBSAPPROVALPC","APPROVALSTATUS","FILTERDATETYPE", };
                    object[] pValuesinsert = { userId, siteId, areaId, subAreaId,shiftId, observerId, checkListId, fromDate,toDate,
                        statusId, displayIncomplete,unsafeChecklist,unsafeChecklistCA,obsApprovalPc,approvalStatus,filterDateType };
             
                    dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
               
                    ParameterObsList obj = new ParameterObsList();
                   
                    string[] finalqueries = new string[8];
                    finalqueries[0] = ScriptEngine.GetValue("WASITES");
                    finalqueries[1] = ScriptEngine.GetValue("WAAREAS");
                    finalqueries[2] = ScriptEngine.GetValue("WACCESSSUBAREAS");
                    finalqueries[3] = ScriptEngine.GetValue("WOSHIFTS");
                    finalqueries[4] = ScriptEngine.GetValue("WOSSITEOBSERVERS");
                    finalqueries[5] = ScriptEngine.GetValue("WASETUPS");
                    finalqueries[6] = ScriptEngine.GetValue("WAACCESSSETUPSTATUS");
                    finalqueries[7] = ScriptEngine.GetValue("WACCESSINCOMPLETEOBS");
                    strquery = string.Join(";", finalqueries).ToString();
                    DataSet dsAllFilter = new DataSet();
                    string[] paraminsert2 = { "USERID", "SITEID", "ROLEID", "AREAID" };
                    object[] pValuesinsert2 = { userId, siteId, defaultRoleId, areaId };
                    dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                  
                    strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                    string[] paraminsert11 = { "LANGUAGECODE" };
                    object[] pValuesinsert11 = { languageId };
                    DataSet dschecklst = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert11, pValuesinsert11);
                    obj.status = true;
                    obj.message = "Success";
                    if (subAreaEnable == "1")
                    {
                        obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[2]);
                    }
                    else
                    {
                        obj.subArea = new string[] { };
                        dsFilter.Tables[0].Columns.Remove("SUBAREANAME");
                    }
                    List<Dictionary<string, object>> list = ScriptEngine.GetTableRows(ds.Tables[0]);
                    List<Dictionary<string, object>> listSite = ScriptEngine.GetTableRows(dsFilter.Tables[0]);
                    obj.defaultFilter = list;
                    obj.list = listSite;
                    obj.sites = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                    obj.area = ScriptEngine.GetTableRows(dsAllFilter.Tables[1]);
                  
                    obj.shifts = ScriptEngine.GetTableRows(dsAllFilter.Tables[3]);
                    obj.observer = ScriptEngine.GetTableRows(dsAllFilter.Tables[4]);
                    obj.checkList = ScriptEngine.GetTableRows(dsAllFilter.Tables[5]);
                    obj.statusData = ScriptEngine.GetTableRows(dsAllFilter.Tables[6]);
                    obj.restAll = ScriptEngine.GetTableRows(dsAllFilter.Tables[7]);
                    obj.checkListConfig = ScriptEngine.GetTableRows(dschecklst.Tables[0])[0];
                    obj.fromDate = ds.Tables[0].Rows[0]["STARTDATEFORMAT"].ToString();
                    obj.toDate = ds.Tables[0].Rows[0]["ENDDATEFORMAT"].ToString();
                    response = Request.CreateResponse(HttpStatusCode.OK, obj);
               
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get ObservationList Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    if (String.IsNullOrEmpty(siteId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WAAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "USERID", "SITEID", "ROLEID" };
                        object[] pValuesinsert2 = { userId, siteId, defaultRoleId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterObsArea obj = new ParameterObsArea();
                            obj.status = true;
                            obj.message = "success";
                            obj.area = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Area filter Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the sub area filter.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getSubAreaFilter")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubAreaFilter(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    if (String.IsNullOrEmpty(areaId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("WACCESSSUBAREAS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "AREAID" };
                        object[] pValuesinsert2 = { areaId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        ParameterObsSubAreaList obj = new ParameterObsSubAreaList();
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            
                            obj.status = true;
                            obj.message = "success";
                            obj.subArea = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj.subArea = new string[] { };
                            obj.status = false;
                            obj.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            //obj1.status = false;
                            //obj1.message = "FMKNORECS";
                            //response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get SubArea filter Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observation add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getObservationAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservationAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dschecklst = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string OBSAPPROVALPC = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string DEFAULTSITEID = dtTokenVal.Rows[0]["DEFAULTSITEID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string mode = "0";
                    string cardID = "0";
                    if (String.IsNullOrEmpty(siteId))
                        siteId = DEFAULTSITEID;
                    string[] finalqueries = new string[4];
                    finalqueries[0] = ScriptEngine.GetValue("GETCHECKLISTSITELIST");
                    finalqueries[1] = ScriptEngine.GetValue("GETCHECKLISTSETUPOBSERVERAREASHIFTLIST");
                    finalqueries[2] = ScriptEngine.GetValue("GETCUSTOMFIELDSNAME");
                    finalqueries[3] = ScriptEngine.GetValue("GETCUSTOMFIELDSVALUE");
                    strquery = string.Join(";", finalqueries).ToString();
                    string[] paraminsert = { "USERID", "MODE", "OBSAPPROVALPC", "SITEID", "CARDID", "LANGUAGEID" };
                    object[] pValuesinsert = { userId, mode, OBSAPPROVALPC, siteId, cardID, languageId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterObserverAddList obj = new ParameterObserverAddList();
                        DataTable dtAvail = new DataTable();
                        try
                        {
                            foreach (DataColumn dc in ds.Tables[3].Columns)
                            {
                                if (dc.ColumnName == "FULLNAME")
                                {
                                    obj.observerId = ScriptEngine.GetTableRows(ds.Tables[3]);
                                    dtAvail = ds.Tables[2].Copy();
                                    obj.area = ScriptEngine.GetTableRows(ds.Tables[4]);
                                    obj.shifts = ScriptEngine.GetTableRows(ds.Tables[5]);
                                    obj.fieldName = ScriptEngine.GetTableRows(ds.Tables[6]);
                                    obj.fieldValue = ScriptEngine.GetTableRows(ds.Tables[7]);
                                }
                                else if (dc.ColumnName == "USERID")
                                {
                                    dtAvail = ds.Tables[3].Copy();
                                    dtAvail.Merge(ds.Tables[2]);
                                    obj.observerId = ScriptEngine.GetTableRows(ds.Tables[4]);
                                    obj.area = ScriptEngine.GetTableRows(ds.Tables[5]);
                                    obj.shifts = ScriptEngine.GetTableRows(ds.Tables[6]);
                                    obj.fieldName = ScriptEngine.GetTableRows(ds.Tables[7]);
                                    obj.fieldValue = ScriptEngine.GetTableRows(ds.Tables[8]);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                        strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                        string[] paraminsert1 = { "LANGUAGECODE" };
                        object[] pValuesinsert1 = { languageId };
                        dschecklst = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        obj.status = true;
                        obj.message = "Success";
                        obj.sites = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.checkList = ScriptEngine.GetTableRows(ds.Tables[1]);
                        obj.observer = ScriptEngine.GetTableRows(dtAvail);
                        obj.checkListConfig = ScriptEngine.GetTableRows(dschecklst.Tables[0])[0];
                        obj.subArea = new string[] { };
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get observation Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the subareadata.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getSubareadata")]
        [APIAuthAttribute]
        public HttpResponseMessage getSubareadata(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string mode = "0";
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    strquery = ScriptEngine.GetValue("GETAREAMANAGERAREALISTFORCHECKLIST");
                    string[] paraminsert = { "USERID", "MODE", "AREAID", "SITEID" };
                    object[] pValuesinsert = { userId, mode, areaId, siteId };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables.Count != 0)
                    {
                        ParameterObsSubArea obj = new ParameterObsSubArea();
                        obj.status = true;
                        obj.message = "Success";
                        obj.subArea = ScriptEngine.GetTableRows(ds.Tables[1]); 
                        obj.user = ScriptEngine.GetTableRows(ds.Tables[0]); 
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Observation Sub Area Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observer add.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getObserverAdd")]
        [APIAuthAttribute]
        public HttpResponseMessage getObserverAdd(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string LANGUAGEID = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string[] finalqueries = new string[2];
                    finalqueries[0] = ScriptEngine.GetValue("GETGROUPTYPEWITHMANDATOTY");
                    finalqueries[1] = ScriptEngine.GetValue("GETGROUPS_USERGROUPS");
                    strquery = string.Join(";", finalqueries).ToString();
                    string[] paraminsert = { "USERID", "LANGUAGEID" };
                    object[] pValuesinsert = { userId, LANGUAGEID };
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        ParameterObsAddList obj = new ParameterObsAddList();
                        obj.status = true;
                        obj.message = "Success";
                        obj.groupType = ScriptEngine.GetTableRows(ds.Tables[0]);
                        obj.groupData = ScriptEngine.GetTableRows(ds.Tables[1]);
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When Get Observer Data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the observer.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/insertObserver")]
        [APIAuthAttribute]
        public HttpResponseMessage insertObserver(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string email = Convert.ToString(request.SelectToken("email"));
                    string firstName = Convert.ToString(request.SelectToken("firstName"));
                    string lastName = Convert.ToString(request.SelectToken("lastName"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string jobId = Convert.ToString(request.SelectToken("jobTitle"));
                    string groupId = Convert.ToString(request.SelectToken("groupId"));
                    if (String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName) || String.IsNullOrEmpty(siteId))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTNEWOBSERVER");
                        string[] paraminsert = { "EMAIL", "COMPANYID", "FIRSTNAME", "LASTNAME", "CREATEDBY", "SITEID", "JOBTITLE", "UPDATEDBY", "GROUPID" };
                        object[] pValuesinsert = { email, COMPANYID, firstName, lastName, userId, siteId, jobId, userId, groupId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables.Count != 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() == "-1")
                            {
                                obj1.status = false;
                                obj1.message = "ALTUSERMAILEXISTS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else if (Convert.ToInt64(ds.Tables[0].Rows[0][0].ToString()) > 0)
                            {
                                ParameterObserveInsert obj = new ParameterObserveInsert();
                                obj.status = true;
                                obj.message = "ALTOBSERVERCREATED";
                                obj.observer = lastName + ", " + firstName;
                                obj.observerId = ds.Tables[0].Rows[0][0].ToString();
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When insert observer data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the checklist observation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/insertChecklistObservation")]
        [APIAuthAttribute]
        public HttpResponseMessage insertChecklistObservation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string obsApprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string customFieldvalueid = Convert.ToString(request.SelectToken("customFieldValueId"));
                    string checkListsetupid = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string selObserverid = Convert.ToString(request.SelectToken("selObserverId"));
                    string observationDate = Convert.ToString(request.SelectToken("observationDate"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaid = Convert.ToString(request.SelectToken("subAreaId"));
                    string length = Convert.ToString(request.SelectToken("length"));
                    string peopleContacted = Convert.ToString(request.SelectToken("peopleContacted"));
                    string peopleObserved = Convert.ToString(request.SelectToken("peopleObserved"));
                    string safeComments = Convert.ToString(request.SelectToken("safeComments"));
                    string subAreaunSafecommentsid = Convert.ToString(request.SelectToken("subAreaUnSafeCommentsId"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string areaManagerid = Convert.ToString(request.SelectToken("areaManagerId"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string unSafecomments = Convert.ToString(request.SelectToken("unSafeComments"));
                    if (String.IsNullOrEmpty(shiftId) || String.IsNullOrEmpty(siteId)
                        || String.IsNullOrEmpty(checkListsetupid) || String.IsNullOrEmpty(selObserverid) || String.IsNullOrEmpty(observationDate)
                         || String.IsNullOrEmpty(areaId) || String.IsNullOrEmpty(length) || String.IsNullOrEmpty(peopleContacted)
                         || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        string UPDATEREQFLAG = "";
                        if (obsApprovalpc == "0")
                        {
                            UPDATEREQFLAG = "4";
                        }
                        else
                        {
                            UPDATEREQFLAG = "0";
                        }
                        strquery = ScriptEngine.GetValue("INSERTSTOPCARDCHECKLIST");
                        string[] paraminsert = { "CUSTOMFIELDVALUEID", "OBSAPPROVALPC", "CHECKLISTSETUPID", "SITEID", "SELOBSERVERID", "OBSERVATIONDATE", "SHIFTID",
                            "AREAID", "SUBAREAID","LENGTH","PEOPLECONTACTED","PEOPLEOBSERVED","SAFECOMMENTS","UNSAFECOMMENTS","STATUS","CREATEDBY","AREAMANAGERID"
                            ,"COMPANYID" };
                        object[] pValuesinsert = {customFieldvalueid, obsApprovalpc, checkListsetupid, siteId, selObserverid, observationDate, shiftId,
                            areaId, subAreaid,length,peopleContacted,peopleObserved,safeComments,unSafecomments,status,userId,areaManagerid
                            ,COMPANYID};
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables.Count != 0)
                        {
                            if (Convert.ToInt64(ds.Tables[0].Rows[0][0].ToString()) > 0)
                            {
                                ParameterCheckObsInsert obj = new ParameterCheckObsInsert();
                                obj.status = true;
                                obj.message = "ALTCHECKLISTGENERALCREATED";
                                obj.cardId = ds.Tables[0].Rows[0][0].ToString();
                                obj.cardReqId = ds.Tables[0].Rows[0][0].ToString() + "-" + UPDATEREQFLAG;
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When insert observatoin data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Deletes the observation checklist.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/deleteObservationChecklist")]
        [APIAuthAttribute]
        public HttpResponseMessage deleteObservationChecklist(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaId = Convert.ToString(request.SelectToken("subAreaId"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string observerId = Convert.ToString(request.SelectToken("observerId"));
                    string checkListId = Convert.ToString(request.SelectToken("checkListId"));
                    string fromDate = Convert.ToString(request.SelectToken("fromDate"));
                    string toDate = Convert.ToString(request.SelectToken("toDate"));
                    string statusId = Convert.ToString(request.SelectToken("statusId"));
                    string displayIncomplete = Convert.ToString(request.SelectToken("displayIncomplete"));
                    string unsafeChecklist = Convert.ToString(request.SelectToken("unsafeChecklist"));
                    string unsafeChecklistCA = Convert.ToString(request.SelectToken("unsafeChecklistCA"));
                    string approvalStatus = Convert.ToString(request.SelectToken("approvalStatus"));
                    string filterDateType = Convert.ToString(request.SelectToken("filterDateType"));
                    string userTypeId = Convert.ToString(request.SelectToken("userType"));
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(obsApprovalPc))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETESTOPCARDCHECKLIST");
                        string[] paraminsert = { "CARDID", "OBSAPPROVALPC", "UPDATEDBY" };
                        object[] pValuesinsert = { cardId, obsApprovalPc, userId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables.Count != 0)
                        {
                            if (ds.Tables[0].Rows[0][0].ToString() != "1")
                            {
                                obj1.status = false;
                                obj1.message = "LBLTECHERROR";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                ParameterObserveDel obj = new ParameterObserveDel();
                                obj.status = true;
                                obj.message = "ALTDELSUCCESS";
                                strquery = ScriptEngine.GetValue("GETDEFAULTCHEKLISTFILTER");
                                string[] paraminsert1 = { "USERID" };
                                object[] pValuesinsert1 = { userId };
                                ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                                string DEFAULTSITEID = ds.Tables[0].Rows[0]["DEFAULTSITEID"].ToString();
                                string OBSLISTINGXMONTHSVAL = ds.Tables[0].Rows[0]["OBSLISTINGXMONTHSVAL"].ToString();
                                if (String.IsNullOrEmpty(siteId))
                                {
                                    string[] newArray = DEFAULTSITEID.Split('!');
                                    siteId = newArray[0].Replace("~~", "");
                                }
                                if (String.IsNullOrEmpty(fromDate) || String.IsNullOrEmpty(toDate))
                                {
                                    string[] newArray = OBSLISTINGXMONTHSVAL.Split('-');
                                    fromDate = newArray[0].Trim();
                                    toDate = newArray[1].Trim();
                                }
                                DataSet dsData = new DataSet();
                                strquery = ScriptEngine.GetValue("GETFILTERBASEDSTOPCARDLISTING");
                                string[] paraminsert2 = { "USERID",  "SITEID", "AREAID", "SUBAREAID", "SHIFTID" , "OBSERVERID", "CHECKLISTSETUPID","FROMDATE", "TODATE",
                       "STATUSID", "INCOMPLETEOBSERVATIONID", "UNSAFEOBS","UNSAFEOBSWITHOUTCA", "OBSAPPROVALPC","APPROVALSTATUS","FILTERDATETYPE", };
                                object[] pValuesinsert2 = { userId, siteId, areaId, subAreaId,shiftId, observerId, checkListId, fromDate,toDate,
                        statusId, displayIncomplete,unsafeChecklist,unsafeChecklistCA,obsApprovalPc,approvalStatus,filterDateType };
                                dsData = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                                if (dsData.Tables[0].Rows.Count != 0)
                                {
                                    obj.list = ScriptEngine.GetTableRows(ds.Tables[0]);
                                    obj.checkList = ScriptEngine.GetTableRows(dsData.Tables[0]);
                                }
                                response = Request.CreateResponse(HttpStatusCode.OK, obj);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When delete observatoin data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the observation list for edit.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getObservationListForEdit")]
        [APIAuthAttribute]
        public HttpResponseMessage getObservationListForEdit(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet ds1 = new DataSet();
                DataSet ds2 = new DataSet();
                DataSet ds3 = new DataSet();
                DataSet ds4 = new DataSet();
                DataSet ds5 = new DataSet();
                DataSet dschecklst = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string checkListSetupId = Convert.ToString(request.SelectToken("checklistSetupId"));
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string cardReqId = Convert.ToString(request.SelectToken("cardReqId"));
                    int UPDATEREQFLAGAPP = 1;
                    string[] cardIdFlag = cardReqId.Split('-');
                    if (cardIdFlag[1] == "4")
                    {
                        UPDATEREQFLAGAPP = 0;
                    }
                    string[] paraminsert1 = { "USERID", "LANGUAGEID", "CARDID", "ROLEID", "OBSAPPROVALPC" };
                    object[] pValuesinsert1 = { userId, languageId, cardId, defaultRoleId, UPDATEREQFLAGAPP };
                    strquery = ScriptEngine.GetValue("GETSTOPCARDCHECKLISTINFO");
                    ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                    siteId = Convert.ToString(ds.Tables[0].Rows[0]["SITEID"]);
                    areaId = Convert.ToString(ds.Tables[0].Rows[0]["AREAID"]);
                    checkListSetupId = Convert.ToString(ds.Tables[0].Rows[0]["CHECKLISTSETUPID"]);
                    string[] paraminsert = { "USERID", "LANGUAGEID", "CARDID", "ROLEID", "OBSAPPROVALPC", "SITEID", "MODE", "AREAID", "CHECKLISTSETUPID" };
                    object[] pValuesinsert = { userId, languageId, cardId, defaultRoleId, UPDATEREQFLAGAPP, siteId, "1", areaId, checkListSetupId };
                    strquery = ScriptEngine.GetValue("GETSTOPCARDCHECKLISTAREAOBSERVERSHIFTINFO");
                    ds1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    DataTable dtAvail = ds1.Tables[1].Copy();
                    dtAvail.Merge(ds1.Tables[0]);
                    strquery = ScriptEngine.GetValue("GETCUSTOMFIELDSNAME");
                    ds2 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    strquery = ScriptEngine.GetValue("GETCUSTOMFIELDSVALUE");
                    ds3 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    strquery = ScriptEngine.GetValue("GETAREAMANAGERAREALISTFORCHECKLIST");
                    ds4 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    strquery = ScriptEngine.GetValue("GETSTOPCARDOBSERVATION");
                    ds5 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    if (ds.Tables[0].Rows.Count != 0)
                    {
                        strquery = ScriptEngine.GetValue("GETGLOBALOPTIONCHECKLISTCONFIG");
                        string[] paraminsert11 = { "LANGUAGECODE" };
                        object[] pValuesinsert11 = { languageId };
                        dschecklst = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert11, pValuesinsert11);
                        ParameterObservationEdit obj = new ParameterObservationEdit();
                        DataTable daraea = ds1.Tables[3].Select("AREAID = " + ds.Tables[0].Rows[0]["AREAID"].ToString() + "").CopyToDataTable();
                        obj.areaName = daraea.Rows[0]["AREANAME"].ToString();
                        DataTable dshift = ds1.Tables[4].Select("SHIFTID = " + ds.Tables[0].Rows[0]["SHIFTID"].ToString() + "").CopyToDataTable();
                        obj.shiftName = dshift.Rows[0]["SHIFTNAME"].ToString();
                        try
                        {
                            DataTable dsubarea = new DataTable();
                            dsubarea = ds4.Tables[1].Select("SUBAREAID = " + ds.Tables[0].Rows[0]["SUBAREAID"] + "").CopyToDataTable();
                            obj.subAreaName = dsubarea.Rows[0]["SUBAREANAME"].ToString();
                        }
                        catch (Exception ex) { }
                        obj.status = true;
                        obj.message = "Success";
                        try
                        {
                            obj.cardData = ScriptEngine.GetTableRows(ds.Tables[0])[0];
                        }
                        catch (Exception ex)
                        {
                            obj.cardData = ScriptEngine.GetTableRows(ds.Tables[0]);
                        }
                        try
                        {
                            obj.observerId = ScriptEngine.GetTableRows(ds1.Tables[2])[0]["USERID"];
                            DataTable dobserver = new DataTable();
                            string observId = ds1.Tables[2].Rows[0]["USERID"].ToString();
                            StringBuilder sb = new StringBuilder();
                            string[] obsersplit = observId.Split(',');
                            int obseverCount = 1;
                            for (int i = 0; i < obsersplit.Length; i++)
                            {
                                DataTable dtTokenValOb = dtAvail.Select("USERID = " + obsersplit[i] + "").CopyToDataTable();
                                if (obseverCount == obsersplit.Length)
                                {
                                    string[] uname = dtTokenValOb.Rows[0]["USERNAME"].ToString().Split(',');
                                    sb.Append(uname[0] + "" + uname[1]);
                                }
                                else
                                {
                                    string[] uname = dtTokenValOb.Rows[0]["USERNAME"].ToString().Split(',');
                                    sb.Append(uname[0] + "" + uname[1] + "," + " ");
                                }
                                obseverCount++;
                            }
                            obj.observerName = sb.ToString();
                        }
                        catch (Exception ex)
                        {
                            obj.observerId = ScriptEngine.GetTableRows(ds1.Tables[1]);
                        }
                        obj.observer = ScriptEngine.GetTableRows(dtAvail);
                        obj.area = ScriptEngine.GetTableRows(ds1.Tables[3]);
                        obj.shifts = ScriptEngine.GetTableRows(ds1.Tables[4]);
                        obj.fieldName = ScriptEngine.GetTableRows(ds2.Tables[0]);
                        obj.fieldValue = ScriptEngine.GetTableRows(ds3.Tables[0]);
                        obj.users = ScriptEngine.GetTableRows(ds4.Tables[0]);
                        obj.subArea = ScriptEngine.GetTableRows(ds4.Tables[1]);
                        obj.mainCat = ScriptEngine.GetTableRows(ds5.Tables[0]);
                        obj.subCat = ScriptEngine.GetTableRows(ds5.Tables[1]);
                        obj.checkListConfig = ScriptEngine.GetTableRows(dschecklst.Tables[0])[0];
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When Get observatoin data for Edit:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the checklist observation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/updateChecklistObservation")]
        [APIAuthAttribute]
        public HttpResponseMessage updateChecklistObservation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string COMPANYID = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string customFieldvalueid = Convert.ToString(request.SelectToken("customFieldValueId"));
                    string obsApprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string checkListsetupid = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string selObserverid = Convert.ToString(request.SelectToken("selObserverId"));
                    string observationDate = Convert.ToString(request.SelectToken("observationDate"));
                    string shiftId = Convert.ToString(request.SelectToken("shiftId"));
                    string areaId = Convert.ToString(request.SelectToken("areaId"));
                    string subAreaid = Convert.ToString(request.SelectToken("subAreaId"));
                    string length = Convert.ToString(request.SelectToken("length"));
                    string peopleContacted = Convert.ToString(request.SelectToken("peopleContacted"));
                    string peopleObserved = Convert.ToString(request.SelectToken("peopleObserved"));
                    string safeComments = Convert.ToString(request.SelectToken("safeComments"));
                    string subAreaunSafecommentsid = Convert.ToString(request.SelectToken("subAreaUnSafeCommentsId"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    string areaManagerid = Convert.ToString(request.SelectToken("areaManagerId"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string unSafecomments = Convert.ToString(request.SelectToken("unSafeComments"));
                    if (String.IsNullOrEmpty(shiftId) || String.IsNullOrEmpty(siteId)
                        || String.IsNullOrEmpty(checkListsetupid) || String.IsNullOrEmpty(selObserverid) || String.IsNullOrEmpty(observationDate)
                         || String.IsNullOrEmpty(areaId) || String.IsNullOrEmpty(length) || String.IsNullOrEmpty(peopleContacted)
                         || String.IsNullOrEmpty(status))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATESTOPCARDCHECKLIST");
                        string[] paraminsert = { "CUSTOMFIELDVALUEID", "OBSAPPROVALPC", "CHECKLISTSETUPID", "SITEID", "SELOBSERVERID", "OBSERVATIONDATE", "SHIFTID",
                            "AREAID", "SUBAREAID","LENGTH","PEOPLECONTACTED","PEOPLEOBSERVED","SAFECOMMENTS","UNSAFECOMMENTS","STATUS","UPDATEDBY","AREAMANAGERID"
                            ,"CARDID" };
                        object[] pValuesinsert = {customFieldvalueid, obsApprovalpc, checkListsetupid, siteId, selObserverid, observationDate, shiftId,
                            areaId, subAreaid,length,peopleContacted,peopleObserved,safeComments,unSafecomments,status,userId,areaManagerid
                            ,cardId};
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        StatusMsgIU obj = new StatusMsgIU();
                        if (ds.Tables.Count != 0)
                        {
                            obj.status = true;
                            obj.message = "ALTCHECKLISTGENERALUPDATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj.status = false;
                            obj.message = "Some data problem";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When update observatoin data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the category comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getCategoryComment")]
        [APIAuthAttribute]
        public HttpResponseMessage getCategoryComment(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string obsAppprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string status = Convert.ToString(request.SelectToken("status"));
                    if (status=="4")
                    {
                        obsAppprovalpc = "0";
                    }

                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(mainCatid))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETCATEGORYCOMMENTS");
                        DataSet dsAllFilter = new DataSet();
                        string[] paraminsert2 = { "OBSAPPROVALPC", "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "LANGUAGEID" };
                        object[] pValuesinsert2 = { obsAppprovalpc, cardId, mainCatid, subCatid, languageId };
                        dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                        if (dsAllFilter.Tables[0].Rows.Count > 0)
                        {
                            ParameterCatCmnt obj = new ParameterCatCmnt();
                            obj.status = true;
                            obj.message = "success";
                            obj.catComment = ScriptEngine.GetTableRows(dsAllFilter.Tables[0]);
                            obj.imgPath = ScriptEngine.GetTableRows(dsAllFilter.Tables[1]);
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When Get Category Comment data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the corrective action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/getCorrectiveAction")]
        [APIAuthAttribute]
        public HttpResponseMessage getCorrectiveAction(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsFilter = new DataSet();
                DataSet dsTab = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string siteId = Convert.ToString(request.SelectToken("siteId"));
                    string corractionId = Convert.ToString(request.SelectToken("corractionId"));
                    string mode = Convert.ToString(request.SelectToken("mode"));
                    string obsAppprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
               
                    if (string.IsNullOrEmpty(corractionId))
                        corractionId = "0";

                    if(corractionId=="0")
                    {
                        mode = "0";
                    }
                    else
                    {
                        mode = "1";
                    }
                    if (String.IsNullOrEmpty(siteId) || String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(mainCatid) || String.IsNullOrEmpty(subCatid))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    strquery = ScriptEngine.GetValue("GETACTIONOWNER");
                    string[] paraminsert = { "MODE", "SITEID", "CORRACTIONID" };
                    object[] pValuesinsert = { mode, siteId, corractionId };
                    dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                    strquery = ScriptEngine.GetValue("GETCATEGORYCOMMENTS");
                    DataSet dsAllFilter = new DataSet();
                    string[] paraminsert2 = { "OBSAPPROVALPC", "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "LANGUAGEID" };
                    object[] pValuesinsert2 = { obsAppprovalpc, cardId, mainCatid, subCatid, languageId };
                    dsAllFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert2, pValuesinsert2);
                    
                    strquery = ScriptEngine.GetValue("GETREPONSIBLEPERSONFORCORRACTION");
                    DataSet dsAllFilter1 = new DataSet();
                    string[] paraminsert1 = { "MODE", "SITEID", "CORRACTIONID", "LANGUAGECODE", "USERID" };
                    object[] pValuesinsert1 = { mode, siteId, corractionId, languageId, userId };
                    dsAllFilter1 = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                    DataTable dtResAvail = new DataTable();
                    if (mode == "1")
                    {
                        dtResAvail = dsAllFilter1.Tables[0].Copy();
                        dtResAvail.Merge(dsAllFilter1.Tables[1]);
                    }
                    else
                    {
                        dtResAvail = dsAllFilter1.Tables[0];
                    }
                    
                    string[] paraminsert3 = { "MODE", "SITEID", "CORRACTIONID", "LANGUAGECODE", "USERID" };
                    object[] pValuesinsert3 = { mode, siteId, corractionId, languageId, userId };
                    strquery = ScriptEngine.GetValue("GETCORRACTIVEACTIONTABINFO");
                    dsTab = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert3, pValuesinsert3);
                    if (dsFilter.Tables[0].Rows.Count != 0)
                    {
                        //try
                        //{
                        //    string[] resperId = Regex.Split(dsTab.Tables[0].Rows[0]["SELPERSON"].ToString(), @"\D+");
                        //    resperId = resperId.Where(x => !string.IsNullOrEmpty(x)).ToArray();
                        //    string resId = String.Join(",", resperId.ToArray());
                        //    dsTab.Tables[0].Rows[0]["SELPERSON"] = resId.ToString();
                        //}
                        //catch (Exception ex)
                        //{ }
                        ParameterCAObsList obj = new ParameterCAObsList();
                        obj.status = true;
                        obj.message = "Success";
                        obj.user = ScriptEngine.GetTableRows(dsFilter.Tables[0]);
                        obj.resPerson = ScriptEngine.GetTableRows(dtResAvail);
                        obj.imgPath = ScriptEngine.GetTableRows(dsAllFilter.Tables[1]);
                        try
                        {
                            obj.correctivaData = ScriptEngine.GetTableRows(dsTab.Tables[0])[0];
                        }
                        catch (Exception ex)
                        {
                            obj.correctivaData = "";
                        }
                        try
                        {
                            obj.catComment = ScriptEngine.GetTableRows(dsAllFilter.Tables[0])[0];
                        }
                        catch (Exception ex)
                        {
                            obj.catComment = "";
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When get Corrective Action data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the corrective action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/updateCorrectiveAction")]
        [APIAuthAttribute]
        public HttpResponseMessage updateCorrectiveAction(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string obsApprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string updateCorractiveactiondata = Convert.ToString(request.SelectToken("updateCorractiveActionData"));
                    string otherRespperson = Convert.ToString(request.SelectToken("otherRespPerson"));
                    string respPerson = Convert.ToString(request.SelectToken("respPerson"));
                    string corractionId = Convert.ToString(request.SelectToken("corractionId"));
                    string responseDate = Convert.ToString(request.SelectToken("responseDate"));
                    string responseRequireDate = Convert.ToString(request.SelectToken("responseRequireDate"));
                    string priority = Convert.ToString(request.SelectToken("priority"));
                    string otherEmailId = Convert.ToString(request.SelectToken("otherEmailId"));
                    string actionPlanned = Convert.ToString(request.SelectToken("actionPlanned"));
                    string actionPrerformed = Convert.ToString(request.SelectToken("actionPerformed"));
                    string cardStatus = Convert.ToString(request.SelectToken("cardStatus"));
                    string owner = Convert.ToString(request.SelectToken("owner"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string checkListsetupid = Convert.ToString(request.SelectToken("checkListSetupId"));
                    DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(updateCorractiveactiondata);
                    string xml = "<modified>"
                        + "<respperson>" + dtTokenVals.Rows[0]["RESPPERSON"].ToString() + "</respperson>"
                        + "<respdate>" + dtTokenVals.Rows[0]["RESPDATE"].ToString() + "</respdate>"
                        + "<respreqdate>" + dtTokenVals.Rows[0]["RESPREQDATE"].ToString() + "</respreqdate>"
                        + "<priority>" + dtTokenVals.Rows[0]["PRIORITY"].ToString() + "</priority>"
                        + "<othermailid>" + dtTokenVals.Rows[0]["OTHEREMAILID"].ToString() + "</othermailid>"
                        + "<planned>" + dtTokenVals.Rows[0]["PLANNED"].ToString() + "</planned>"
                        + "<performed>" + dtTokenVals.Rows[0]["PERFORMED"].ToString() + "</performed>"
                        + "<castatus>" + dtTokenVals.Rows[0]["CASTATUS"].ToString() + "</castatus>"
                        + "<actowner>" + dtTokenVals.Rows[0]["ACTOWNER"].ToString() + "</actowner>"
                        + "<categoryassigned>" + dtTokenVals.Rows[0]["CATEGORYASSIGNED"].ToString() + "</categoryassigned>"
                        + "</modified>";
                    if (String.IsNullOrEmpty(actionPlanned) || String.IsNullOrEmpty(mainCatid) || String.IsNullOrEmpty(subCatid)
                        || String.IsNullOrEmpty(owner) || String.IsNullOrEmpty(responseRequireDate)
                        || String.IsNullOrEmpty(cardStatus) || String.IsNullOrEmpty(priority) || String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(corractionId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATECORRACTIVEACTIONCATEGORYASSIGN");
                        string[] paraminsert = { "UPDATECORRECTIVEACTIONDATA", "OTHERRESPONSIBLEPERSON", "RESPONSEDATE", "RESPONSEREQUIREDDATE", "PRIORITY", "OTHEREMAILID"
                        , "ACTIONPLANNED", "ACTIONPERFORMED", "CARDSTATUS", "OWNER", "UPDATEDBY", "CORRACTIONID","RESPONSIBLEPERSON","ASSIGNEDCATEGORYLIST","COMPANYID"
                        ,"CARDID"};
                        object[] pValuesinsert = { xml, otherRespperson, responseDate,responseRequireDate,priority,otherEmailId,actionPlanned
                                ,actionPrerformed,cardStatus,owner,userId,corractionId,respPerson,mainCatid+"=>"+subCatid,companyId ,cardId };
                        dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (dsFilter.Tables[0].Rows.Count != 0)
                        {
                            StatusMsgIU obj = new StatusMsgIU();
                            obj.status = true;
                            obj.message = "ALTCORRACTIVESELCARDUPDATED";

                            string[] paramcorrectiveaction = { "CORRACTIONID", "MODID" };
                            object[] pValuecorrectiveaction = { corractionId, dsFilter.Tables[0].Rows[0][1].ToString() };
                            strquery = ScriptEngine.GetValue("MAILCORRECTIVEACTIONUPDATE");
                            DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcorrectiveaction, pValuecorrectiveaction);
                            string str = "";
                            try
                            {
                                if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                {
                                    //DataPro configuration setting 
                                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                                    string strAPPID = dctpartdata["APPID"].ToString();
                                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                    string ES_WSURL = dctpartdata["WSURL"].ToString();

                                    Log.InfoFormat("==CorrectiveAction Email Process Start==");
                                    string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl;
                                    int ischeduleid;
                                    DateTime senddate;
                                    object objEmail;
                                    for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                    {
                                        ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                        strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                        strreplyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                        strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                        strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                        strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                        strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                        strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                        senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                        strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                        str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + companyId + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                        Log.InfoFormat("JSON String:" + str);
                                        //mail.SendNotificationDetails(str);
                                        object[] objSendNotificationDetails = new object[1];
                                        objSendNotificationDetails[0] = str;
                                        CallWebServices CWS = new CallWebServices();
                                        objEmail = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                        Log.InfoFormat("WebService Return Value:" + objEmail.ToString());
                                    }
                                }
                                else
                                {
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for Corrective Action:" + ex.StackTrace);
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { companyId, userId, "Corrective Actions Updated", 1, "MAILCORRECTIVEACTIONUPDATE", str, ex.Message };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When update Corrective Action data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the corrective action.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/insertCorrectiveAction")]
        [APIAuthAttribute]
        public HttpResponseMessage insertCorrectiveAction(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet ds = new DataSet();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string otherRespperson = Convert.ToString(request.SelectToken("otherRespPerson"));
                    string respPerson = Convert.ToString(request.SelectToken("respPerson"));
                    string corractionId = Convert.ToString(request.SelectToken("corractionId"));
                    string responseDate = Convert.ToString(request.SelectToken("responseDate"));
                    string responseRequireDate = Convert.ToString(request.SelectToken("responseRequireDate"));
                    string priority = Convert.ToString(request.SelectToken("priority"));
                    string otherEmailId = Convert.ToString(request.SelectToken("otherEmailId"));
                    string actionPlanned = Convert.ToString(request.SelectToken("actionPlanned"));
                    string actionPrerformed = Convert.ToString(request.SelectToken("actionPerformed"));
                    string cardStatus = Convert.ToString(request.SelectToken("cardStatus"));
                    string owner = Convert.ToString(request.SelectToken("owner"));
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string obsApprovalpc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string checkListsetupid = Convert.ToString(request.SelectToken("checkListSetupId"));
                    if (String.IsNullOrEmpty(actionPlanned) || String.IsNullOrEmpty(mainCatid) || String.IsNullOrEmpty(subCatid)
                        || String.IsNullOrEmpty(owner) || String.IsNullOrEmpty(responseRequireDate)
                        || String.IsNullOrEmpty(cardStatus) || String.IsNullOrEmpty(priority) || String.IsNullOrEmpty(cardId))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTCORRACTIVEACTIONCATEGORYASSIGN");
                        string[] paraminsert = { "OTHERRESPONSIBLEPERSON", "RESPONSEDATE", "RESPONSEREQUIREDDATE", "PRIORITY", "OTHEREMAILID"
                        , "ACTIONPLANNED", "ACTIONPERFORMED", "CARDSTATUS", "OWNER", "CREATEDBY","RESPONSIBLEPERSON","ASSIGNEDCATEGORYLIST","COMPANYID"
                        ,"CARDID","CAPAFROMTAB"};
                        object[] pValuesinsert = { otherRespperson, responseDate,responseRequireDate,priority,otherEmailId,actionPlanned
                                ,actionPrerformed,cardStatus,owner,userId,respPerson,mainCatid+"=>"+subCatid,companyId ,cardId,"1" };
                        dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (dsFilter.Tables[0].Rows.Count != 0)
                        {
                            ParameterObsUpd obj = new ParameterObsUpd();
                            obj.status = true;
                            obj.message = "ALTCORRACTIVESELCARDCREATED";
                            obj.corractionId = dsFilter.Tables[0].Rows[0][1].ToString();

                            string[] paramcorrectiveaction = { "CORRACTIONID" };
                            object[] pValuecorrectiveaction = { obj.corractionId };
                            strquery = ScriptEngine.GetValue("MAILCORRECTIVEACTION");
                            DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcorrectiveaction, pValuecorrectiveaction);

                            string str = "";
                            try
                            {
                                if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                {
                                    //DataPro configuration setting 
                                    Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                                    string strAPPID = dctpartdata["APPID"].ToString();
                                    string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                                    string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                                    string ES_WSURL = dctpartdata["WSURL"].ToString();

                                    Log.InfoFormat("==CorrectiveAction Email Process Start==");
                                    string strmailfrom, strreplyto, strto, strcc, strbcc, strsubject, strbody, strcompanyurl;
                                    int ischeduleid;
                                    DateTime senddate;
                                    object objEmail;
                                    for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                    {
                                        ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                        strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                        strreplyto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                        strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                        strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                        strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                        strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                        strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                        senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                        strcompanyurl = dsmaildetails.Tables[0].Rows[0]["URL"].ToString();
                                        str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + companyId + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + strcompanyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strreplyto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                        Log.InfoFormat("JSON String:" + str);
                                        //mail.SendNotificationDetails(str);
                                        object[] objSendNotificationDetails = new object[1];
                                        objSendNotificationDetails[0] = str;
                                        CallWebServices CWS = new CallWebServices();
                                        objEmail = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                        Log.InfoFormat("WebService Return Value:" + objEmail.ToString());
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Log.InfoFormat("Exception Occured When Send Mail for Corrective Action:" + ex.StackTrace);
                                strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                object[] pValuesrptinfo = { companyId, userId, "Corrective Actions Updated", 1, "MAILCORRECTIVEACTIONUPDATE", str, ex.Message };
                                int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                            }
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When Insert Corrective Action data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the update comment.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/ObservationChecklist/insertUpdateComment")]
        [APIAuthAttribute]
        public HttpResponseMessage insertUpdateComment(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = AppInfo.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsFilter = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string languageId = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string defaultRoleId = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string companyId = dtTokenVal.Rows[0]["COMPANYID"].ToString();
                    string cardId = Convert.ToString(request.SelectToken("cardId"));
                    string mainCatid = Convert.ToString(request.SelectToken("mainCatId"));
                    string subCatid = Convert.ToString(request.SelectToken("subCatId"));
                    string obsApprovalPc = dtTokenVal.Rows[0]["OBSAPPROVALPC"].ToString();
                    string safeComments = Convert.ToString(request.SelectToken("safeComments"));
                    string unsafeComments = Convert.ToString(request.SelectToken("unsafeComments"));
                    string imgName = Convert.ToString(request.SelectToken("imgName"));
                    string observations = Convert.ToString(request.SelectToken("observations"));
                    string isFromComments = Convert.ToString(request.SelectToken("isFromComments"));
                    string checkListSetupId = Convert.ToString(request.SelectToken("checkListSetupId"));
                    string UPLODEDIMAGE = Convert.ToString(request.SelectToken("uploadImage"));
                    DataTable dtTokenVals = JsonConvert.DeserializeObject<DataTable>(observations);
                    StringBuilder sb = new StringBuilder();
                    sb.Append("<ROOT>");
                    for (int i = 0; i < dtTokenVals.Rows.Count; i++)
                    {
                        sb.Append("<ROW MAINCATEGORYID='").Append(dtTokenVals.Rows[i]["MAINCATEGORYID"]).Append("' SUBCATEGORYID='").Append(dtTokenVals.Rows[i]["SUBCATEGORYID"]).Append("' SAFE='").Append(dtTokenVals.Rows[i]["SAFE"]).Append("' UNSAFE='").Append(dtTokenVals.Rows[i]["UNSAFE"]).Append("'/>");
                    }
                    sb.Append("</ROOT>");
                    if (String.IsNullOrEmpty(cardId) || String.IsNullOrEmpty(observations))
                    {
                        obj1.status = false;
                        obj1.message = "Please fill Mandatory Parameters";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        ParameterObsUpd obj = new ParameterObsUpd();
                        if (isFromComments == "1")
                        {
                            Dictionary<string, object> dict = new Dictionary<string, object>();
                            var httpRequest = HttpContext.Current.Request;
                            strquery = ScriptEngine.GetValue("INSERTUPDATECATEGORYCOMMENTS");
                            string[] paraminsert = { "OBSAPPROVALPC", "CARDID", "MAINCATEGORYID", "SUBCATEGORYID", "SAFECOMMENTS", "UNSAFECOMMENTS"
                        , "UPDATEDBY", "IMAGENAME"};
                            object[] pValuesinsert = { obsApprovalPc, cardId, mainCatid, subCatid, safeComments, unsafeComments, userId, UPLODEDIMAGE };
                            dsFilter = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        }
                        DataSet dsNew = new DataSet();
                        strquery = ScriptEngine.GetValue("INSERTSTOPCARDOBSERVATIONS");
                        int getcardid = BaseAdapter.ExecuteInlineQueries_XMLPARAM(strquery, cardId, userId, sb.ToString(), obsApprovalPc);
                        string obsunsafecount = "";
                        obj1.status = true;
                        obj1.message = "ALTCHECKLISTOBSERVATIONSUPDATED";

                        string[] paraminsert1 = { "CARDID", "COMPANYID" };
                        object[] pValuesinsert1 = { getcardid, companyId };

                        Log.InfoFormat("==1. Unsafe Observation Mail Process Start==");

                        strquery = ScriptEngine.GetValue("GETVALIDAREAMANAGERFOREMAIL");
                        DataSet dsareamgrforemail = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);

                        strquery = ScriptEngine.GetValue("GETGLOBALDATA");
                        DataSet dsGlobal = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        dsGlobal.Tables[0].Rows[0]["ENABLEAREAMANAGER"].ToString();

                        if (dsareamgrforemail.Tables[0].Rows.Count != 0)
                        {
                            if (dsareamgrforemail.Tables[0].Rows[0]["UNSAFECOMMENTSEXISTS"].ToString() == "1") // If only Unsafe comments are there //without Unsafe count then send mail.
                            {
                                obsunsafecount = "1";
                            }
                            if (dsareamgrforemail.Tables[0].Rows[0]["AREAMANAGEREXISTS"].ToString() == "0")  // If no Area manager are assigned dont //send mail.
                            {
                                obsunsafecount = "0";
                            }
                        }

                        Log.InfoFormat("2. ENABLEAREAMANAGER" + dsGlobal.Tables[0].Rows[0]["ENABLEAREAMANAGER"].ToString());
                        Log.InfoFormat("3. obsunsafecount" + obsunsafecount.ToString());
                        // Area Manager section for sending mail for unsafe observation//
                        if (dsGlobal.Tables[0].Rows[0]["ENABLEAREAMANAGER"].ToString() == "1")
                        {
                            //DataPro configuration setting 
                            Dictionary<string, object> dctpartdata = ScriptEngine.scriptengine_data();
                            string strAPPID = dctpartdata["APPID"].ToString();
                            string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                            string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                            string ES_WSMETHOD1 = dctpartdata["WSMETHOD1"].ToString();
                            string ES_WSURL = dctpartdata["WSURL"].ToString();
                            if (obsunsafecount.ToString() == "1")
                            {
                                try
                                {
                                    Log.InfoFormat("==4. Unsafe Observation Mail Processing==");
                                    string strmailfrom, strto, strcc, strbcc, strsubject, strbody, companyurl;
                                    int ischeduleid;
                                    DateTime senddate;
                                    string[] paramcardid = { "CARDID", "CREATEDBY" };
                                    object[] pValuescardid = { getcardid, userId };
                                    strquery = ScriptEngine.GetValue("MAILAREAMANAGER");
                                    DataSet dsmaildetails = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paramcardid, pValuescardid);
                                    if ((dsmaildetails.Tables.Count > 0) && (dsmaildetails.Tables[0].Rows.Count > 0))
                                    {
                                        Log.InfoFormat("==5. Mail Details Value found==");
                                        for (int i = 0; i <= dsmaildetails.Tables[0].Rows.Count - 1; i++)
                                        {
                                            ischeduleid = Convert.ToInt32(dsmaildetails.Tables[0].Rows[0]["SCHEDULEID"].ToString());
                                            strmailfrom = dsmaildetails.Tables[0].Rows[i]["FROM"].ToString();
                                            strto = dsmaildetails.Tables[0].Rows[i]["REPLYTO"].ToString();
                                            strto = dsmaildetails.Tables[0].Rows[i]["TO"].ToString();
                                            strcc = dsmaildetails.Tables[0].Rows[i]["CC"].ToString();
                                            strbcc = dsmaildetails.Tables[0].Rows[i]["BCC"].ToString();
                                            strsubject = dsmaildetails.Tables[0].Rows[i]["SUBJECT"].ToString();
                                            strbody = dsmaildetails.Tables[0].Rows[i]["BODY"].ToString();
                                            senddate = Convert.ToDateTime(dsmaildetails.Tables[0].Rows[i]["SENTDATE"].ToString());
                                            companyurl = dsmaildetails.Tables[0].Rows[i]["URL"].ToString();
                                            String str = "[{\"APPID\":\"" + strAPPID + "\",\"COMPANYID\":\"" + companyId + "\",\"TOKENID\":\"" + ischeduleid + "\",\"URL\":\"" + companyurl + "\",\"MAILFROM\":\"" + strmailfrom + "\",\"MAILREPLYTO\":\"" + strto + "\",\"MAILTO\":\"" + strto + "\",\"MAILCC\":\"" + strcc + "\",\"MAILBCC\":\"" + strbcc + "\",\"MAILSUBJECT\":\"" + strsubject + "\",\"MAILBODY\":\"" + strbody + "\",\"MAILTOBESENTDATE\":\"" + senddate.ToString("MM-dd-yyyy") + "\"}]";
                                            Log.InfoFormat("6. JSON String:" + str);
                                            object[] objSendNotificationDetails = new object[1];
                                            objSendNotificationDetails[0] = str;
                                            CallWebServices CWS = new CallWebServices();
                                            object objCWS = CWS.CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                            Log.InfoFormat("7. WebService Return Value:" + objCWS.ToString());
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strquery = ScriptEngine.GetValue("INSERTERRORLOG");
                                    string[] paramrptinfo = { "COMPANYID", "USERID", "FUNCTIONTYPE", "ACTION", "QUERY", "PARAMETER", "ERRORMSG" };
                                    object[] pValuesrptinfo = { companyId, userId, "INSERTSTOPCARDOBSERVATION", 1, "MAILAREAMANAGEROBSERVATION", "INSERTSTOPCARDOBSERVATION", ex.ToString() };
                                    int EL = BaseAdapter.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);
                                    Log.InfoFormat("Exception Occured When Send Mail for Update Request:" + ex.StackTrace);
                                }
                            }
                        }
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception occured When update Commment data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the value table.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="userid">The userid.</param>
        /// <param name="languageId">The language identifier.</param>
        /// <param name="SITEID">The siteid.</param>
        /// <param name="roleId">The role identifier.</param>
        /// <returns></returns>
        public DataSet getValueTable(string query, string userid, string languageId, string SITEID, string roleId)
        {
            DataSet ds = new DataSet();
            string strquery;
            strquery = ScriptEngine.GetValue(query);
            string[] paraminsert = { "USERID", "LANGUAGEID", "SITEID", "ROLEID" };
            object[] pValuesinsert = { userid, languageId, SITEID, roleId };
            ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
            return ds;
        }

        #endregion

    }
}

