﻿using DataPro;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StopDataPro.Business.Adapters;
using StopDataPro.Classes;
using StopDataPro.Services.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebAPI.Filters;

namespace StopDataPro.Controllers
{
    public class UserDefinedController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserDefinedController"/> class.
        /// </summary>
        public UserDefinedController()
        {
            this.ScriptEngine = new ScriptEngine();
            this.Infoconfig = new AppInfo();
            this.BaseAdapter = new BaseAdapter();
            this.Log = log4net.LogManager.GetLogger(typeof(UserDefinedController));
        }

        #region Public Properties

        /// <summary>
        /// Gets or sets the script engine.
        /// </summary>
        /// <value>
        /// The script engine.
        /// </value>
        public ScriptEngine ScriptEngine { get; set; }

        /// <summary>
        /// Gets or sets the Infoconfig.
        /// </summary>
        /// <value>
        /// The Infoconfig.
        /// </value>
        public AppInfo Infoconfig { get; set; }

        /// <summary>
        /// Gets or sets the bal.
        /// </summary>
        /// <value>
        /// The bal.
        /// </value>
        public BaseAdapter BaseAdapter { get; set; }

        /// <summary>
        /// Gets or sets the Log.
        /// </summary>
        /// <value>
        /// The Log.
        /// </value>
        public ILog Log { get; set; }
        #endregion

        #region Action Method

        /// <summary>
        /// Gets the user defined data.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/getUserDefinedData")]
        [APIAuthAttribute]
        public HttpResponseMessage getUserDefinedData(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsuserdefine = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));
                    string language = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    DataSet ds = new DataSet();
                    if (!String.IsNullOrEmpty(fieldId))
                    {
                        strquery = ScriptEngine.GetValue("GETUSERDEFINEDGENERALINFO");
                        string[] paraminsert = { "LANGUAGEID", "CUSTOMFIELDID" };
                        object[] pValuesinsert = { language, fieldId };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                        strquery = ScriptEngine.GetValue("GETUSERDEFINEDNAME");
                        dsuserdefine = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            ParameterDetail obj = new ParameterDetail();
                            List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(ds.Tables[0]);
                            List<Dictionary<string, object>> lstField = ScriptEngine.GetTableRows(dsuserdefine.Tables[0]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.Data = lstData;
                            obj.field = lstField[0]["CUSTOMFIELDNAME"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("GETUSERDEFINEDLIST");
                        string[] paraminsert = { "LANGUAGEID" };
                        object[] pValuesinsert = { language };
                        ds = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            List<Dictionary<string, object>> lstPersons = ScriptEngine.GetTableRows(ds.Tables[0]);
                            obj1.status = true;
                            obj1.message = "Success";
                            obj1.Data = lstPersons;
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get User defined data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Inserts the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/insert")]
        [APIAuthAttribute]
        public HttpResponseMessage insert(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgId obj1 = new StatusMsgId();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));
                    string fieldValue = Convert.ToString(request.SelectToken("fieldValue"));
                    string allSites = Convert.ToString(request.SelectToken("allSites"));
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(fieldValue) || String.IsNullOrEmpty(allSites))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("INSERTCUSTOMFIELDVALUE");
                        string[] paraminsert = { "CREATEDBY", "CUSTOMFIELDID", "CUSTOMFIELDVALUE", "ALLSITES" };
                        object[] pValuesinsert = { CREATEDBY, fieldId, fieldValue, allSites };
                        int iresult = BaseAdapter.ExecuteInlineQueries_ReturnVal(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult != 1)
                        {
                            obj1.status = true;
                            obj1.id = iresult;
                            obj1.message = "ALTCUSTOMVALUECREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else if (iresult == 1)
                        {
                            obj1.status = false;
                            obj1.message = "ALTCUSTOMVALUEEXISTS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTCUSTOMVALUENOTCREATED";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When insert User defined data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }


        /// <summary>
        /// Deletes the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/delete")]
        [APIAuthAttribute]
        public HttpResponseMessage delete(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleid = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();

                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));
                    string customfieldValue = Convert.ToString(request.SelectToken("customFieldValueId"));
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(customfieldValue))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("DELETEUSERDEFINEDFEILDS");
                        string[] paraminsert = { "SELCUSTOMFIELDVALUEID", "USERID", "CUSTOMFIELDID" };
                        object[] pValuesinsert = { customfieldValue, CREATEDBY, fieldId };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTDELSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update User defined data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Gets the user defined assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/getUserDefinedAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage getUserDefinedAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {  
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dsuserdefine = new DataSet();
                DataSet dsAssign = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string language = dtTokenVal.Rows[0]["LANGUAGEID"].ToString();
                    string userId = dtTokenVal.Rows[0]["USERID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));
                    string customfieldValue = Convert.ToString(request.SelectToken("customFieldValueId"));
                    DataSet dsAllSite = new DataSet();
                    if (!String.IsNullOrEmpty(fieldId))
                    {
                        string[] paraminsert = { "LANGUAGEID", "CUSTOMFIELDID" };
                        object[] pValuesinsert = { language, fieldId };
                        strquery = ScriptEngine.GetValue("GETUSERDEFINEDFIELDVALUE");
                        dsuserdefine = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);
                        if (dsuserdefine.Tables[0].Rows.Count != 0)
                        {
                            if (String.IsNullOrEmpty(customfieldValue))
                            {
                                customfieldValue = dsuserdefine.Tables[0].Rows[0]["CUSTOMFIELDVALUEID"].ToString();
                            }
                            string[] paraminsert1 = { "USERID", "CUSTOMFIELDID", "CUSTOMFIELDVALUEID" };
                            object[] pValuesinsert1 = { userId, fieldId, customfieldValue };
                            strquery = ScriptEngine.GetValue("GETUSERDEFINEDFIELDVALUEALLSITESSTATUS");
                            dsAllSite = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                            strquery = ScriptEngine.GetValue("GETUSERDEFINEDSITESASSGN");
                            dsAssign = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);


                            DataTable dAvailSort = dsAssign.Tables[1].Copy();
                            dAvailSort.Merge(dsAssign.Tables[0]);
                            if (dsAssign.Tables[2].Rows.Count == 0)
                            {
                                DataRow dr = dsAssign.Tables[2].NewRow();
                                dr[0] = "";
                                dsAssign.Tables[2].Rows.Add(dr);
                            }
                            ParameterUserDefineAssign obj = new ParameterUserDefineAssign();
                            List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(dsuserdefine.Tables[0]);
                            List<Dictionary<string, object>> lstAllSite = ScriptEngine.GetTableRows(dsAllSite.Tables[0]);
                            List<Dictionary<string, object>> lstAvailable = ScriptEngine.GetTableRows(dAvailSort);
                            List<Dictionary<string, object>> lstAssign = ScriptEngine.GetTableRows(dsAssign.Tables[2]);
                            obj.status = true;
                            obj.message = "Success";
                            obj.fieldValue = lstData;
                            obj.valueForAllSite = lstAllSite[0]["ALLSITES"];
                            obj.available = lstAvailable;
                            obj.assign = lstAssign[0]["siteIds"];
                            response = Request.CreateResponse(HttpStatusCode.OK, obj);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "FMKNORECS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get User defined assing data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }


        /// <summary>
        /// Updates the assign.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/updateAssign")]
        [APIAuthAttribute]
        public HttpResponseMessage updateAssign(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleid = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));
                    string customfieldValue = Convert.ToString(request.SelectToken("customFieldValueId"));
                    string assignIds = Convert.ToString(request.SelectToken("assignIds"));
                    string allSites = Convert.ToString(request.SelectToken("allSites"));
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(customfieldValue))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        strquery = ScriptEngine.GetValue("UPDATECUSTOMFIELDSSITEASSGN");
                        string[] paraminsert = { "UPDATEDBY", "CUSTOMFIELDID", "CUSTOMFIELDVALUEID", "ALLSITES", "SITEID" };
                        object[] pValuesinsert = { CREATEDBY, fieldId, customfieldValue, allSites, assignIds };
                        int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                        if (iresult != 0 && iresult > 0)
                        {
                            obj1.status = true;
                            obj1.message = "ALTCUSTOMFIELDSITEASSGNSUCCESS";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "LBLTECHERROR";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update User defined assign data:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }


        /// <summary>
        /// Gets the user defined translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/getUserDefinedTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage getUserDefinedTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsg obj1 = new StatusMsg();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                DataSet dslang = new DataSet();
                DataSet dsfield = new DataSet();
                DataSet dstrans = new DataSet();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    string customField = Convert.ToString(request.SelectToken("customField"));

                    strquery = ScriptEngine.GetValue("GETLANGUAGES");
                    dslang = BaseAdapter.ExecuteInlineQueries_Dataset(strquery);
                    if (String.IsNullOrEmpty(language))
                    {
                        language = dslang.Tables[0].Rows[0]["LANGUAGEID"].ToString();
                    }

                    strquery = ScriptEngine.GetValue("GETCUSTOMFIELDNAMEFORTRANSLATION");
                    string[] paraminsert = { "LANGUAGEID" };
                    object[] pValuesinsert = { language };
                    dsfield = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert, pValuesinsert);

                    if (dsfield.Tables[0].Rows.Count != 0)
                    {
                        if (String.IsNullOrEmpty(customField))
                        {
                            customField = dsfield.Tables[0].Rows[0]["ENTITYID"].ToString();
                        }
                        string[] paraminsert1 = { "LANGUAGEID", "CUSTOMFIELDID" };
                        object[] pValuesinsert1 = { language, customField };
                        strquery = ScriptEngine.GetValue("GETTRANSLATIONSFORUSERDEFINED");
                        dstrans = BaseAdapter.ExecuteInlineQueries_Dataset(strquery, paraminsert1, pValuesinsert1);
                        ParameterTranslation obj = new ParameterTranslation();
                        List<Dictionary<string, object>> lstLang = ScriptEngine.GetTableRows(dslang.Tables[0]);
                        List<Dictionary<string, object>> lstField = ScriptEngine.GetTableRows(dsfield.Tables[0]);
                        List<Dictionary<string, object>> lstData = ScriptEngine.GetTableRows(dstrans.Tables[0]);
                        obj.status = true;
                        obj.message = "Success";
                        obj.language = lstLang;
                        obj.Data = lstData;
                        obj.field = lstField;
                        response = Request.CreateResponse(HttpStatusCode.OK, obj);
                    }
                    else
                    {
                        obj1.status = false;
                        obj1.message = "FMKNORECS";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When get User defined translation:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        /// <summary>
        /// Updates the translation.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [HttpPost]
        [Route("api/UserDefined/updateTranslation")]
        [APIAuthAttribute]
        public HttpResponseMessage updateTranslation(JObject request)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            StatusMsgIU obj1 = new StatusMsgIU();
            string strquery;
            try
            {
                //Decrypt  request
                request = JObject.Parse(ScriptEngine.DecryptStringAES(Convert.ToString(request.SelectToken("hdndata"))));

                //Get connectionstring from appinfo config 
                string httpUrl = HttpContext.Current.Request.Headers["Origin"];
                string con = Infoconfig.createConn(httpUrl);
                BaseAdapter.constringBal = con;
                // Get User value from token
                DataTable dtTokenVal = ScriptEngine.TokenCheck();
                if (dtTokenVal.Rows.Count == 0)
                {
                    obj1.status = false;
                    obj1.message = "Token Invalid";
                    response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                }
                else
                {
                    string CREATEDBY = dtTokenVal.Rows[0]["USERID"].ToString();
                    string roleid = dtTokenVal.Rows[0]["DEFAULTROLEID"].ToString();
                    string fieldId = Convert.ToString(request.SelectToken("fieldId"));  
                    string parentId = Convert.ToString(request.SelectToken("parentId"));
                    string language = Convert.ToString(request.SelectToken("languageId"));
                    bool pipeLineValid = true;
                    if (String.IsNullOrEmpty(fieldId) || String.IsNullOrEmpty(parentId) || String.IsNullOrEmpty(language))
                    {
                        obj1.status = false;
                        obj1.message = "Please Add Mandatory Parameter";
                        response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                    }
                    else
                    {
                        DataTable dtSerialize = JsonConvert.DeserializeObject<DataTable>(fieldId);
                        string entity = "";
                        StringBuilder sb = new StringBuilder();
                        for (int i = 0; i < dtSerialize.Rows.Count; i++)
                        {
                            string id = dtSerialize.Rows[i]["eid"].ToString();
                            string value = dtSerialize.Rows[i]["eval"].ToString();
                            if (value.Contains("|"))
                            {
                                pipeLineValid = false;
                                break;
                            }
                            string type = dtSerialize.Rows[i]["etype"].ToString();
                            entity = id + "," + type + "," + value + "|";
                            sb.Append(entity);
                        }
                        if (pipeLineValid == true)
                        {
                            strquery = ScriptEngine.GetValue("UPDATEUSERDEFINEDTRANSLATION");
                            string[] paraminsert = { "UPDATEDBY", "ENTITYID", "LANGUAGEID", "PARENTID" };
                            object[] pValuesinsert = { CREATEDBY, sb.ToString(), language, parentId };
                            int iresult = BaseAdapter.ExecuteInlineQueries(strquery, paraminsert, pValuesinsert);
                            if (iresult != 0 && iresult > 0)
                            {
                                obj1.status = true;
                                obj1.message = "ALTSELECTEDITEMTRANSLATEDSUCESS";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                            else
                            {
                                obj1.status = false;
                                obj1.message = "ALTSELECTEDITEMTRANSLATEDFAIL";
                                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                            }
                        }
                        else
                        {
                            obj1.status = false;
                            obj1.message = "ALTVALIDATETRANSTEXT";
                            response = Request.CreateResponse(HttpStatusCode.OK, obj1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.InfoFormat("Exception Occured When update User defined translation:" + ex.StackTrace);
                obj1.status = false;
                obj1.message = ex.Message;
                response = Request.CreateResponse(HttpStatusCode.OK, obj1);
            }
            return response;
        }

        #endregion
    }
}
