var TITEMS = [ 
 ["Introduction to STOP DataPro®", "source/datapro/outline_0.htm", "11"],
 ["STOP DataPro® Workflow", "source/datapro/outline_11.htm", "11"],
 ["Logging into STOP DataPro®", "source/datapro/outline_5.htm", "11"],
 ["Retrieve Login Information", "source/datapro/outline_7.htm", "11"],
 ["Renew Password", "source/datapro/outline_8.htm", "11"],
 ["Access STOP DataPro® Application", "source/datapro/outline_12.htm", "1",
  ["User Management", "source/datapro/user_management.htm", "1",
   ["Users", "source/datapro/outline_30.htm", "1",
    ["User Listing Page", "source/datapro/outline_31.htm", "11"],
    ["Examine General Information of User", "source/datapro/outline_32.htm", "11"],
    ["Add, Edit and Delete User Accounts", "source/datapro/outline_34.htm", "11"],
    ["Assign Users to Groups", "source/datapro/users_assign_groups.htm", "11"],
    ["Specify Targets for Users", "source/datapro/outline_35.htm", "11"]
   ],
   ["Groups", "source/datapro/outline_27.htm", "1",
    ["Group Listing Page", "source/datapro/outline_28.htm", "11"],
    ["Add, Edit and Delete Groups", "source/datapro/outline_29.htm", "11"],
    ["Assign Users to a Group", "source/datapro/group_general_info.htm", "11"],
    ["Translate Group Names", "source/datapro/translate_grp_name.htm", "11"]
   ],
   ["Roles", "source/datapro/manage roles.htm", "1",
    ["Role Listing Page", "source/datapro/view role list.htm", "11"],
    ["Add, Edit and Delete Roles", "source/datapro/add edit delete roles.htm", "11"],
    ["Set Permissions for a Role", "source/datapro/assign permissions to role.htm", "11"],
    ["Assign Users to a Role", "source/datapro/assign users to role.htm", "11"],
    ["Assign Reports to a Role", "source/datapro/assign reports to role.htm", "11"]
   ]
  ],
  ["Site Management", "source/datapro/outline_13.htm", "1",
   ["Sites", "source/datapro/outline_14.htm", "1",
    ["Site Listing Page", "source/datapro/assign shifts to site.htm", "11"],
    ["Add, Edit and Delete Sites", "source/datapro/outline_15.htm", "11"],
    ["Inactivate Sites", "source/datapro/outline_18.htm", "11"],
    ["Email Users Assigned to a Site", "source/datapro/sites_email_users.htm", "11"],
    ["Assign Areas, Users, Shifts and Groups to Site", "source/datapro/outline_19.htm", "11"],
    ["Specify Targets for a Site", "source/datapro/outline_20.htm", "11"]
   ],
   ["Areas", "source/datapro/outline_21.htm", "1",
    ["Area Listing Page", "source/datapro/access area list page.htm", "11"],
    ["Add, Edit and Delete Areas", "source/datapro/outline_22.htm", "11"],
    ["Assign Sites to Area", "source/datapro/outline_23.htm", "11"]
   ],
   ["Shifts", "source/datapro/outline_24.htm", "1",
    ["Shift Listing Page", "source/datapro/shift_list.htm", "11"],
    ["Add, Edit and Delete Shifts", "source/datapro/shift_add.htm", "11"],
    ["Assign Sites to a Shift", "source/datapro/assign_shift.htm", "11"]
   ]
  ],
  ["Checklist Configuration", "source/datapro/outline_42.htm", "1",
   ["Categories", "source/datapro/outline_43.htm", "1",
    ["Category Listing Page", "source/datapro/outline_44.htm", "11"],
    ["Add, Edit and Delete Categories", "source/datapro/outline_45.htm", "11"],
    ["Add Sub Categories to a Main Category", "source/datapro/outline_46.htm", "1",
     ["Delete Sub Category", "source/datapro/delete_sub_category.htm", "11"]
    ],
    ["Translate Category Names", "source/datapro/outline_48.htm", "11"]
   ],
   ["Checklist Setup", "source/datapro/checklist_setup.htm", "1",
    ["Checklist Listing Page", "source/datapro/view_ line_items.htm", "11"],
    ["Design Checklists", "source/datapro/outline_49.htm", "11"],
    ["Assign Categories to Checklist", "source/datapro/outline_50.htm", "11"],
    ["Assign Sites to Checklist", "source/datapro/assign_sites_checklist.htm", "11"],
    ["Checklist Preview", "source/datapro/outline_52.htm", "11"]
   ]
  ],
  ["Data Entry", "source/datapro/outline_53.htm", "1",
   ["Observation Checklist", "source/datapro/outline_54.htm", "1",
    ["Add, Edit and Delete a Checklist", "source/datapro/add_checklist.htm", "11"],
    ["Examine General Information of a Checklist", "source/datapro/general_info_checklist.htm", "11"],
    ["Enter Observation Data in Checklist", "source/datapro/enter_observation_data.htm", "11"],
    ["Recommend Corrective Actions", "source/datapro/outline_57.htm", "11"]
   ],
   ["Corrective Actions", "source/datapro/outline_59.htm", "2",
    ["Corrective Actions Card Listing Page", "source/datapro/corrective actions_list.htm", "11"],
    ["Examine General Information of a Corrective Action Card", "source/datapro/direct_corrective_actions.htm", "11"],
    ["Edit Observation Data of a Checklist", "source/datapro/capa_observation.htm", "11"],
    ["Recommend Corrective Actions", "source/datapro/outline_63.htm", "11"]
   ],
   ["Injury Statistics", "source/datapro/injury_statistics.htm", "1",
    ["Injury Statistics Listing Page", "source/datapro/injury_statistics_add.htm", "11"]
   ],
   ["Red Flags", "source/datapro/outline_60.htm", "1",
    ["Red Flags Listing Page", "source/datapro/outline_61.htm", "11"],
    ["Add Red Flags", "source/datapro/outline_62.htm", "11"]
   ],
   ["Edit Groups on Checklists", "source/datapro/edit_observation_user_group.htm", "1"]
  ],
  ["Settings and Configuration", "source/datapro/outline_38.htm", "1",
   ["Global Options", "source/datapro/outline_39.htm", "1",
    ["General Settings", "source/datapro/go_gen.htm", "11"],
    ["Application Configuration", "source/datapro/app_config.htm", "11"]
   ],
   ["Translations", "source/datapro/outline_40.htm", "1",
    ["Translate Application Texts", "source/datapro/outline_41.htm", "11"]
   ]
  ],
  ["Manage Reports", "source/datapro/outline_64.htm", "1",
   ["Volume Analysis", "source/datapro/outline_65.htm", "1",
    ["Checklist Count", "source/datapro/outline_72.htm", "11"],
    ["Checklist Count Summary", "source/datapro/checklist count summary.htm", "11"],
    ["Corrective Action Report", "source/datapro/outline_71.htm", "11"],
    ["Observations per Hour", "source/datapro/outline_66.htm", "11"],
    ["Observations Report By Category", "source/datapro/outline_67.htm", "11"],
    ["Sites Report", "source/datapro/outline_69.htm", "11"],
    ["Target Vs Activity Report - Monthly", "source/datapro/outline_70.htm", "11"],
    ["Target Vs Activity Report - Weekly", "source/datapro/targetvsactivityweekly.htm", "11"],
    ["Users / Observers Report", "source/datapro/users_report.htm", "11"]
   ],
   ["Trend Analysis", "source/datapro/outline_73.htm", "1",
    ["Category Comments", "source/datapro/outline_76.htm", "11"],
    ["Checklist Report (By Category)", "source/datapro/outline_74.htm", "11"],
    ["Checklist Report (By Metrics)", "source/datapro/outline_75.htm", "11"],
    ["Index Report", "source/datapro/index_rpt.htm", "11"],
    ["Red Flags", "source/datapro/outline_77.htm", "11"],
    ["Pareto Chart", "source/datapro/outline_78.htm", "11"],
    ["Trend Line Report", "source/datapro/trend_line.htm", "11"]
   ],
   ["Run Reports", "source/datapro/outline_79.htm", "11"],
   ["Edit Reports", "source/datapro/outline_80.htm", "11"],
   ["Schedule Reports", "source/datapro/report scheduling.htm", "1",
    ["Schedule Report Listing Page", "source/datapro/rpt_sche_list_page.htm", "11"],
    ["Email a Report", "source/datapro/schedule_a_report.htm", "11"]
   ]
  ],
  ["Email Notifications", "source/datapro/email.htm", "1",
   ["Setting up Notifications", "source/datapro/email_setup.htm", "11"],
   ["Corrective Action Overdue", "source/datapro/corrective_act_ou.htm", "11"],
   ["Corrective Action Reminder", "source/datapro/corrective_actions_remainders.htm", "11"],
   ["Corrective Actions Assigned", "source/datapro/correct_act_assigned.htm", "11"],
   ["Corrective Actions Updated", "source/datapro/corrective_actions_updated.htm", "11"],
   ["Observation Reminder Monthly", "source/datapro/observation remainder monthly.htm", "11"],
   ["Observation Reminder Weekly", "source/datapro/observation_remainder_weekly.htm", "11"],
   ["New User Registration", "source/datapro/new_user_registration.htm", "11"],
   ["Forgot Password", "source/datapro/forgot_password.htm", "11"]
  ]
 ],
 ["Edit Your Profile", "source/datapro/outline_82.htm", "11"],
 ["Alert Subscription for Unsafe Observations", "source/datapro/alertsubscriptionsforunsafeobservation.htm", "11"],
 ["Inbox", "source/datapro/inbox.htm", "11"],
 ["Approval", "source/datapro/approval.htm", "11"],
 ["Log Out", "source/datapro/outline_89.htm", "11"]
];


var FITEMS = arr_flatten(TITEMS);

function arr_flatten (x) {
   var y = []; if (x == null) return y;
   for (var i=0; i<x.length; i++) {
      if (typeof(x[i]) == "object") {
         var flat = arr_flatten(x[i]);
         for (var j=0; j<flat.length; j++)
             y[y.length]=flat[j];
      } else {
         if ((i%3==0))
          y[y.length]=x[i+1];
      }
   }
   return y;
}

