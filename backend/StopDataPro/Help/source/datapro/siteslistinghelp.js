﻿var page_JSON = {
"pageid": "APPMODJS/ADMINJS/siteslistinghelp",
"pagehelp": "SITELISTING",
    "epreinclude": "APPMOD/SCRIPTS/sites-pre",
    "pagedesign": {
        "divbreadcrumps": {
            "child": [
                {
                    "etype": "elements",
                    "eclass": "divelement",
                    "child": [
                        {
                            "etype": "links",
                            "eclass": "links_custom",
                            "child": [
                                {
                                    "etype": "span",
                                    "eclass": "link_custom",
                                    "etext": "LBLSITEMGMT"
                                },
                                {
                                    "etype": "span",
                                    "eclass": "link_custom",
                                    "etext": "LBLBSITES"
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        "divcontent": {
            "exslt": "",
            "child": [
                {
                    "etype": "header_container",
                    "eclass": "header_container_custom3",
                    "child": [
                         {
                             "esection": "header",
                             "child": [
                                    {
                                        "etype": "span",
                                        "etext": "LBLSITELISTING"
                                    }
                                 ]
                         },
                        {
                            "esection": "content",
                            "child": [
                                {
                                    "eid": "popupid1",
                                    "etype": "popup",
                                    "etitle": "LBLEMAILSITE"
                                }
                            ]
                        },
                        {
                            "esection": "content",
                            "child": [
                                {
                                    "etype": "listview",
                                    "eid": "sitelist",
                                    "eclass": "tables",
                                    "esearch": "yes",
                                    "editable": "yes",
                                    "epageno": "1",
                                    "epagesize": "10",
                                    "eprocessmode": "paging",
                                    "erowcallback": "editsite",
                                    "emapdatafunction": "loadpagedata",
                                    
                                    "eheadermap": [
                                        {
                                            "elabel": "LBLSITENAME",
                                            "emapfield": "SITENAME",
                                            "ecolspan": "1"
                                        },
                                        {
                                            "elabel": "LBLCOUNTRY",
                                            "emapfield": "COUNTRY",
                                            "ecolspan": "1"
                                        },
                                        {
                                            "elabel": "LBLCITY",
                                            "emapfield": "CITY",
                                            "ecolspan": "1"
                                        },
                                        {
                                            "elabel": "LBLSTATUS",
                                            "emapfield": "STATUS",
                                            "esearch": "no"
                                        }
                                    ],
                                    "erowmap": [
                                        {
                                            "ecolumnmap": [
                                                {
                                                    "ecolspan": "1",
                                                    "child": [
                                                        {
                                                            "etype": "span",
                                                            "ekeyid": "key2",
                                                            "etextformat": [
                                                                {
                                                                    "fieldindex": "field1"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "ecolspan": "1",
                                                    "child": [
                                                        {
                                                            "etype": "span",
                                                            "ekeyid": "key3",
                                                            "etextformat": [
                                                                {
                                                                    "fieldindex": "field3"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "ecolspan": "1",
                                                    "child": [
                                                        {
                                                            "etype": "span",
                                                            "ekeyid": "key3",
                                                            "etextformat": [
                                                                {
                                                                    "fieldindex": "field4"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    "ecolspan": "1",
                                                    "child": [
                                                        {
                                                            "etype": "span",
                                                            "ekeyid": "key3",
                                                            "etextformat": [
                                                                {
                                                                    "fieldindex": "field5"
                                                                }
                                                            ]
                                                        }
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                },
                                {
                                    "etype": "divbutton",
                                    "events": {
                                        "event1": {
                                            "eaction": "onclick",
                                            "ecallback": "managesite",
                                            "ecallbackparam": "0"
                                        }
                                    },
                                    "elabel": "LBLADD"
                                },
                                {
                                    "etype": "divbutton",
                                    "eid": "btndelete",
                                    "events": {
                                        "event1": {
                                            "eaction": "onclick",
                                            "ecallback": "managesite",
                                            "ecallbackparam": "2"
                                        }
                                    },
                                    "elabel": "LBLDELETE"
                                },
                                {
                                    "etype": "divbutton",
                                    "eid": "btnstatus",
                                    "events": {
                                        "event1": {
                                            "eaction": "onclick",
                                            "ecallback": "managesite",
                                            "ecallbackparam": "3"
                                        }
                                    },
                                    "elabel": "LBLCHANGESTATUS"
                                },
                                {
                                    "etype": "divbutton",
                                    "eid": "btnsendmail",
                                    "events": {
                                        "event1": {
                                            "eaction": "onclick",
                                            "ecallback": "managesite",
                                            "ecallbackparam": "4"
                                        }
                                    },
                                    "elabel": "LBLMAILSEND"
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    },
    "page_onload": {
        "etype": "script",
        "emode": "javascript",
        "execute": [
            {
                "ecallback": "loadpagedata",
                "ecallbackparam": ""
            }
        ]
    },
    "pagelabels": "ALTSITELICENSECOUNT,ALTDELSITESWARNING,ALTSITEDEL,ALTSUCESSCHANGESTATUS,ALTDELCONFIRM,ALTDELSUCCESS,ALTSITEONESELECTEDIT,ALTCHANGESTATUSCONFIRM,ALTCHANGESTATUSSUCCESS,ALTCOMMONERROR,ALTSELECTEDIT,ALTSELECTDELETE,ALTSELECTCHANGESTATUS,ALTSELSITESENDMAIL,ALTSENDMAIL,LBLCONFIRM,LBLEMAILSITE,LBLOK,ALTAUTOLOGOUT"

    
}
/*CSFMKPART CALLBACK FUNCTION*/

try {
    CSFMK.loadedfile("pageinfo", page_JSON.pageid);
} catch (e) { }

