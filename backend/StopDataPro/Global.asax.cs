﻿using StopDataPro.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace StopDataPro
{
    public class WebApiApplication : System.Web.HttpApplication
    {        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            log4net.Config.XmlConfigurator.Configure();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            //clsLogWrite cs = new clsLogWrite();
            //string path = ((System.Web.HttpApplication)sender).Request.AppRelativeCurrentExecutionFilePath.ToString();
            //cs.writeToLog("Request Start:" + path);
            //if (HttpContext.Current.Request.HttpMethod == "OPTIONS")
            //{
            //    HttpContext.Current.Response.Flush();
            //}
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            //clsLogWrite cs = new clsLogWrite();
            //string path = ((System.Web.HttpApplication)sender).Request.AppRelativeCurrentExecutionFilePath.ToString();
            //cs.writeToLog("Request End:" + path);
        }
    }
}
