﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterDetail
    /// </summary>
    public class ParameterDetail
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object field { get; set; }
    }
}