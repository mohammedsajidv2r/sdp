﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterDetail
    /// </summary>
    public class ParameterUserDefineAssign
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object fieldValue { get; set; }
        public object valueForAllSite { get; set; }
        public object available { get; set; }
        public object assign { get; set; }
    }
}