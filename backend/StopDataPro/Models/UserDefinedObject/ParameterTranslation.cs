﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterTranslation
    /// </summary>
    public class ParameterTranslation
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object language { get; set; }
        public object field { get; set; }
        public object Data { get; set; }
    }
}