﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterListSubCatg
    /// </summary>
    public class ParameterListSubCatg
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object subCategory { get; set; }
        public object subCategoryId { get; set; }
    }
}