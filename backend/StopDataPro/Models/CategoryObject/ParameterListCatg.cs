﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterListCatg
    /// </summary>
    public class ParameterListCatg
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object GeneralData { get; set; }
        public object subCategory { get; set; }
        public object subCategoryId { get; set; }
    }
}