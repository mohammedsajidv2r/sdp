﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesLangTarget
    /// </summary>
    public class ParameterValuesLangTarget
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Language { get; set; }
        public object Category { get; set; }
        public object Translation { get; set; }
    }
}