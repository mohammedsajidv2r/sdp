﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///   Model ParameterAreaAssign
    /// </summary>
    public class ParameterAreaAssign
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object siteAvailable { get; set; }
        public object siteAssign { get; set; }
        public object subAreaAvailable { get; set; }
        public object subAreaAssign { get; set; }
    }
}
