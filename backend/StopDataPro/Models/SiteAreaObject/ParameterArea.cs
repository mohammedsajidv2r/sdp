﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///   Model ParameterArea
    /// </summary>
    public class ParameterArea
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object site { get; set; }
        public object subArea { get; set; }
        public string defaultSite { get; set; }
    }
}
