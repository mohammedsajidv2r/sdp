﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameteGroupOnCheckListAdd
    /// </summary>
    public class ParameteGroupOnCheckListAdd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object group { get; set; }
        public object groupData { get; set; }
        public object observerName { get; set; }
    }
}