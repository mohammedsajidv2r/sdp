﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameteGroupOnCheckList
    /// </summary>
    public class ParameteGroupOnCheckList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object GeneralData { get; set; }
        public object filterSites { get; set; }
        public object filterUserLevel { get; set; }
        public object filterRole { get; set; }
        public object filterStatus { get; set; }
    }
}