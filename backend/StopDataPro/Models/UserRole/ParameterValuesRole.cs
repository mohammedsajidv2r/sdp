﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesRole
    /// </summary>
    public class ParameterValuesRole
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object avail { get; set; }
        public object assign { get; set; }
    }
}