﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesRoleList
    /// </summary>
    public class ParameterValuesRoleList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public object defaultRole { get; set; }
    }
}