﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesForRedEdit
    /// </summary>
    public class ParameterValuesForRedEdit
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object generalData { get; set; }
        public object observer { get; set; }
        public object area { get; set; }
        public object subarea { get; set; }
        public object checklist { get; set; }
        public object maincat { get; set; }
        public object subcat { get; set; }
    }
}