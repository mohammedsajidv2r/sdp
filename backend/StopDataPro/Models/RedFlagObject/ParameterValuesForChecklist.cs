﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesForChecklist
    /// </summary>
    public class ParameterValuesForChecklist
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object mainCat { get; set; }
        public object subCat { get; set; }
    }
}