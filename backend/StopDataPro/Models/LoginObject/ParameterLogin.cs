﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterLogin
    /// </summary>
    public class ParameterLogin
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object userDashboardPermission { get; set; }
        public object rolePermission { get; set; }
        public object dashboard { get; set; }
        public object dupontNews { get; set; }
    }
}