﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValueCheckList
    /// </summary>
    public class ParameterValueCheckList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object mainCat { get; set; }
    }
}