﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesSite
    /// </summary>
    public class ParameterValuesSite
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object siteChart { get; set; }
        public object siteDetails { get; set; }
        public object designData { get; set; }
    }
}