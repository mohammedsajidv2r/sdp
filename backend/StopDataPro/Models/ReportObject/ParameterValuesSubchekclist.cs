﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesGraphically
    /// </summary>
    public class ParameterValuesGraphically
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Graphically { get; set; }
        public object sitelist { get; set; }
        public object designData { get; set; }
    }
}