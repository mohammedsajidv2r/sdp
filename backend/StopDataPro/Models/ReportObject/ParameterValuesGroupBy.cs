﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesGroupBy
    /// </summary>
    public class ParameterValuesGroupBy
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object group1 { get; set; }
        public object group2 { get; set; }
        public object group3 { get; set; }
    }
}