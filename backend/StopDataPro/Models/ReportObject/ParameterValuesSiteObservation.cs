﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesSiteObservation
    /// </summary>
    public class ParameterValuesSiteObservation
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object Total { get; set; }
        public object sites { get; set; }
        public object year { get; set; }
        public object designData { get; set; }
    }
}