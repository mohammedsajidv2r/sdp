﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesArea
    /// </summary>
    public class ParameterValuesArea
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object area { get; set; }
        public object shift { get; set; }
        public object observer { get; set; }
        public object checkList { get; set; }
        public object customField1 { get; set; }
        public object customField2 { get; set; }
        public object customField3 { get; set; }
    }
}