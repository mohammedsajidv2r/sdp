﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesDetail
    /// </summary>
    public class ParameterValuesDetail
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Details { get; set; }
        public object sitelist { get; set; }
        public object designData { get; set; }
    }

    public class ParameterValueFilter
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public string reportName { get; set; }
    }


}