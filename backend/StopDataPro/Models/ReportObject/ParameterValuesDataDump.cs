﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesDataDump
    /// </summary>
    public class ParameterValuesDataDump
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object DataStatus { get; set; }
        public object designData { get; set; }
    }
}