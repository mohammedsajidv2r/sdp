﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValueFilterRep
    /// </summary>
    public class ParameterValueFilterRep
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object savedFilter { get; set; }
        public object filterList { get; set; }
        public object filterOptional { get; set; }
        public object filterDate { get; set; }
        public object filterGroupBy { get; set; }
        public object filterCategories { get; set; }
        public object filterChartType { get; set; }
        public object filterLabel { get; set; }
        public object filterSetting { get; set; }
        public object reportDetail { get; set; }
        public int reportDesignId { get; set; }
        public object filterGroup { get; set; }
    }
}