﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesCA
    /// </summary>
    public class ParameterValuesCA
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object sitelist { get; set; }
        public object designData { get; set; }
    }
}