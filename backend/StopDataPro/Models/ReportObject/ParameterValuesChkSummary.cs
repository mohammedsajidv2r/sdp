﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesChkSummary
    /// </summary>
    public class ParameterValuesChkSummary
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
        public object headingValue { get; set; }
    }
}