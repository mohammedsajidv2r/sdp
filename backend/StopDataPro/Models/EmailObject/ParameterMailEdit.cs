﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterMailEdit
    /// </summary>
    public class ParameterMailEdit
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object emailBody { get; set; }
        public object language { get; set; }
        public object scheduleDetail { get; set; }
    }
}