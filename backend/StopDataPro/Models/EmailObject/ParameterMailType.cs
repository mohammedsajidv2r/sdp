﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterMailType
    /// </summary>
    public class ParameterMailType
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object emailType { get; set; }
        public object emailDetail { get; set; }
    }
}