﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterMailEdit
    /// </summary>
    public class ParameterMailAdd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object emailType { get; set; }
        public object schedule { get; set; }
        public object emailDetail { get; set; }
        public object filterSite { get; set; }
        public object filterGroup { get; set; }
        public object filterRole { get; set; }
        public object filterLabel { get; set; }
    }
}