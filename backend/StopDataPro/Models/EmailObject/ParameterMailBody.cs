﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterMailList
    /// </summary>
    public class ParameterMailBody
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object emailBody { get; set; }
    }
}