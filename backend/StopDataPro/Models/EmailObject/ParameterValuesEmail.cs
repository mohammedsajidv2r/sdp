﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesEmail
    /// </summary>
    public class ParameterValuesEmail
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object mailDetail { get; set; }
        public object subject { get; set; }
        public object body { get; set; }
    }
}