﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterMailEditList
    /// </summary>
    public class ParameterMailEditList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object editData { get; set; }
        public object filterSite { get; set; }
        public object filterGroup { get; set; }
        public object filterRole { get; set; }
        public object filterLabel { get; set; }
        public object filterSetting { get; set; }
    }
}