﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    /// Model StatusMsg
    /// </summary>
    public class StatusMsg
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
    }
    
    /// <summary>
    /// Model StatusMsgId
    /// </summary>
    public class StatusMsgId
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int id { get; set; }
    }

    /// <summary>
    /// Model StatusMsgIU
    /// </summary>
    public class StatusMsgIU
    {
        public bool status { get; set; }
        public string message { get; set; }
    }

    /// <summary>
    /// Model StatusMsgA
    /// </summary>
    public class StatusMsgData
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

    /// <summary>
    /// Model ParameterValuesTarget
    /// </summary>
    public class ParameterValuesTarget
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object GeneralData { get; set; }
    }
}