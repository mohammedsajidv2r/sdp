﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    public class AuthUser
    {
        [Key]
        public string pass { get; set; }
        public string ClientID { get; set; }
        public string ClientIP { get; set; }
        public DateTime CreateOn { get; set; }
        public string OS_Version { get; set; }
        public string ClientHash { get; set; }
        public string ClientSecret { get; set; }
    }

    public class NewToken
    {
        [Key]
        public string ClientID { get; set; }
        public string pass { get; set; }
        public string ClientIP { get; set; }
        public DateTime CreateOn { get; set; }
        public string ClientHash { get; set; }
        public string OS_Version { get; set; }
        public string ClientSecret { get; set; }
    }
    public class TokenKey
    {
        [Key]
        public string key { get; set; }
    }
}