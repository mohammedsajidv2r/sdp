﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterSubAreaAssign
    /// </summary>
    public class ParameterSubAreaAssign
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object available { get; set; }
        public object assign { get; set; }
    }
}