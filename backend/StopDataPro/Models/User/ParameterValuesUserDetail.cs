﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesUserDetail
    /// </summary>
    public class ParameterValuesUserDetail
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object userInfo { get; set; }
        public object userSiteAvail { get; set; }
        public object language { get; set; }
        public object timeZone { get; set; }
        public object role { get; set; }
        public object site { get; set; }
        public object validDetails { get; set; }
        public object allSiteTimezone { get; set; }
        public object groupMandatory { get; set; }
        public object userGroup { get; set; }
    }
}