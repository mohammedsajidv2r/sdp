﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesLoad
    /// </summary>
    public class ParameterValuesLoad
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object filterData { get; set; }
        public object optionalFilter { get; set; }
        public object siteFilter { get; set; }
    }
}