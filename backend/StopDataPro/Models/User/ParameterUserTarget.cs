﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterUserTarget
    /// </summary>
    public class ParameterUserTarget
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object site { get; set; }
        public object target { get; set; }
    }
}