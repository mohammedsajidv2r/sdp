﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesUserTarget
    /// </summary>
    public class ParameterValuesUserTarget
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string defaultSiteId { get; set; }
        public object site { get; set; }
        public object users { get; set; }
        public object filterOptional { get; set; }
        public object filterData { get; set; }
    }
}