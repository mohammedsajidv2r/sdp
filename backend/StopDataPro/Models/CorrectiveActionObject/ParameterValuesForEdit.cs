﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesForEdit
    /// </summary>
    public class ParameterValuesForEdit
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object actionOwner { get; set; }
        public object resPerson { get; set; }
        public object caData { get; set; }
        public object caAvail { get; set; }
        public object caAssing { get; set; }
        public object caCategory { get; set; }
        public object general { get; set; }
        public object area { get; set; }
        public object subArea { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
        public object observerId { get; set; }
        public object fieldValue { get; set; }
        public object fieldName { get; set; }
        public object observerMainCat { get; set; }
        public object observerSubCat { get; set; }
        public object checkListConfig { get; set; }
        public string areaName { get; set; }
        public string setupName { get; set; }
        public string shiftName { get; set; }
        public string observerName { get; set; }
    }
}