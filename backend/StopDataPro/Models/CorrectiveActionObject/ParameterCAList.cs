﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterGroupCheckList
    /// </summary>
    public class ParameterCAList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object filters { get; set; }
        public object siteFilters { get; set; }
        public object areaFilters { get; set; }
        public object subAreafilters { get; set; }
        public object shiftfilters { get; set; }
        public object optionalFilters { get; set; }
        public object optionCategory { get; set; }
        public string defaultSiteId { get; set; }
        public string defaultSiteName { get; set; }
        public object corractiveAction { get; set; }
    }
}