﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterApprovalList
    /// </summary>
    public class ParameterUpdateCA
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int corractionId { get; set; }
    }

    /// <summary>
    /// Model ParameterValuesTarget13
    /// </summary>
    public class ParameterInsertCA
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object corrActionId { get; set; }
    }
}