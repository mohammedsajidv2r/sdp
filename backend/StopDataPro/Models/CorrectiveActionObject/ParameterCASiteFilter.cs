﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterSiteFilter
    /// </summary>
    public class ParameterCASiteFilter
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object area { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
    }
}