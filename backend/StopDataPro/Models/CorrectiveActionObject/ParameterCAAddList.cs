﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCAAddList
    /// </summary>
    public class ParameterCAAddList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int defaultSiteId { get; set; }
        public object sites { get; set; }
        public object area { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
        public object fieldValue { get; set; }
        public object fieldName { get; set; }
    }
}