﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterApprovalList
    /// </summary>
    public class ParameterApprovalList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int defaultSiteId { get; set; }
        public object generalData { get; set; }
        public object filterSites { get; set; }
        public object filterArea { get; set; }
        public object filterSubArea { get; set; }
        public object filterShift { get; set; }
        public object filterObserver { get; set; }
        public object filterStatus { get; set; }
    }
}