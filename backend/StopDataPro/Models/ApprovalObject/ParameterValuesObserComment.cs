﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    /// Model ParameterValuesObserComment
    /// </summary>
    public class ParameterValuesObserComment
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object details { get; set; }
    }
}