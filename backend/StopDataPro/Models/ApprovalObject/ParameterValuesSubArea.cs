﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    public class ParameterValuesSubArea
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object subArea { get; set; }
    }
}