﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterGroupCheckList
    /// </summary>
    public class ParameterGroupCheckList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object approveData { get; set; }
        public object observerList { get; set; }
        public object observerrAssignId { get; set; }
        public object area { get; set; }
        public object shift { get; set; }
        public object subArea { get; set; }
        public object customName { get; set; }
        public object customValue { get; set; }
        public object checkListConfig { get; set; }
    }
}