﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameteGroupOnCheckList
    /// </summary>
    public class ParameterInboxList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object unRead { get; set; }
        public object emails { get; set; }
        public object scheduleReport { get; set; }
        public object allEmails { get; set; }
    }
}