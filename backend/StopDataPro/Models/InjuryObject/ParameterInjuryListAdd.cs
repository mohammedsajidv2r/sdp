﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterInjuryList
    /// </summary>
    public class ParameterInjuryListAdd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object sites { get; set; }
        public object year { get; set; }
    }
}