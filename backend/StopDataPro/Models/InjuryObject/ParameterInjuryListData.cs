﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterInjuryListData
    /// </summary>
    public class ParameterInjuryListData
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int defaultSiteId { get; set; }
        public object generalData { get; set; }
        public object filterSites { get; set; }
    }
}