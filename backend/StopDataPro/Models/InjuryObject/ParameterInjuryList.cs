﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterInjuryList
    /// </summary>
    public class ParameterInjuryList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object generalData { get; set; }
        public object avail { get; set; }
        public object assign { get; set; }
        public object year { get; set; }
    }
}