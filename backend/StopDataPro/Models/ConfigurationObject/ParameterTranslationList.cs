﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterTranslationList
    /// </summary>
    public class ParameterTranslationList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object language { get; set; }
        public object data { get; set; }
        public object label { get; set; }
    }
}