﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCheckConfig
    /// </summary>
    public class ParameterCheckConfig
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object GeneralData { get; set; }
        public object role { get; set; }
        public object assignRole { get; set; }
        public object entity { get; set; }
        public object assignEntity { get; set; }
        public object mandUserFeilds { get; set; }
    }
}