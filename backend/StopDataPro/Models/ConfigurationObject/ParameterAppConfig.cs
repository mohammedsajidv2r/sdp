﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterAppConfig
    /// </summary>
    public class ParameterAppConfig
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object GeneralData { get; set; }
        public object language { get; set; }
        public object assignLanguage { get; set; }
        public object entity { get; set; }
        public object assignEntity { get; set; }
        public object scheduletype { get; set; }
        public object assignScheduletype { get; set; }
        public object timezone { get; set; }
        public object assignTimezone { get; set; }
        public object mandatoryGroupType { get; set; }
    }
}