﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesScheduleEdit
    /// </summary>
    public class ParameterValuesScheduleEdit
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object report { get; set; }
        public object timeZone { get; set; }
        public object generalData { get; set; }
        public object reportVal { get; set; }
        public object reportDesign { get; set; }
        public object reportSaveFilter { get; set; }
        public object filter { get; set; }
        public object optionalFilter { get; set; }
        public object filterGroupBy { get; set; }
        public object filterChartType { get; set; }
        public object filterGroup { get; set; }
        public object filterCategories { get; set; }
        public object filterLabel { get; set; }
        public object filterSetting { get; set; }
        public object toUser { get; set; }
        public object bodyKeyword { get; set; }
    }
}