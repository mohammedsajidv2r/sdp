﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesScheduleAdd
    /// </summary>
    public class ParameterValuesScheduleAdd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object report { get; set; }
        public object timeZone { get; set; }
        public object toUser { get; set; }
        public object bodyKeyword { get; set; }
    }
}