﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesSchedule
    /// </summary>
    public class ParameterValuesSchedule
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object scheduleData { get; set; }
        public object reportData { get; set; }
    }
}