﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesForAdd
    /// </summary>
    public class ParameterValuesForAdd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object countries { get; set; }
        public object timeZone { get; set; }
        public object globaltimeZone { get; set; }
    }
}