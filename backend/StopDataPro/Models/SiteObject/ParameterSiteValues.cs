﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterSiteValues
    /// </summary>
    public class ParameterSiteValues
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
        public object country { get; set; }
        public object timeZone { get; set; }
        public object available { get; set; }
        public object assign { get; set; }
        public object targetMonthly { get; set; }
        public object targetWeekly { get; set; }
        public object areas { get; set; }
        public object areaAvailable { get; set; }
    }

    public class ParameterSiteAreaMangerValues
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object userAssign { get; set; }
        public object userAvailable { get; set; }
    }

}