﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterGroupCheckList
    /// </summary>
    public class ParameterCheckListData
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object copyCheckList { get; set; }
        public object checkListData { get; set; }
        //public object category { get; set; }
        //public object categoryId { get; set; }
        public object categoryAvailable { get; set; }
        public object categoryAssigned { get; set; }
        //public object subCategory { get; set; }
        //public object subCategoryId { get; set; }
        public object subCategoryAvailable { get; set; }
        public object subCategoryAssigned { get; set; }
        public object sites { get; set; }
        public object sitesId { get; set; }
        public object mainCategoryPreview { get; set; }
        public object subCategoryPreview { get; set; }
        public int selectedCategoryId { get; set; }
        public object mainCategoryList { get; set; }
    }
}