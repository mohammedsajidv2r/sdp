﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCheckList
    /// </summary>
    public class ParameterCheckList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object sites { get; set; }
        public object checkStatus { get; set; }
        public object checkList { get; set; }
    }

    /// <summary>
    /// Model ParameterSubCatg
    /// </summary>
    public class ParameterSubCatg
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object subCategoryAvailable { get; set; }
        public object subCategoryAssigned { get; set; }
    }
}