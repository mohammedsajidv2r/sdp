﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterSiteFilter
    /// </summary>
    public class ParameterValuesDelete
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int delStatus { get; set; }
    }
}