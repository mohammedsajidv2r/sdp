﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesGlobal
    /// </summary>
    public class ParameterValuesGlobal
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object globalData { get; set; }
        public string uploadUrl { get; set; }
        //public string CustomColor { get; set; }
        //public string CustomFont { get; set; }
    }
}