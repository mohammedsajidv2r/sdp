﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesDashboard
    /// </summary>
    public class ParameterValuesDashboard
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object rolePermission { get; set; }
        public object quickLink { get; set; }
        public object dupontNews { get; set; }
        public object dashboard { get; set; }
        public string activeUser { get; set; }
        public string activeSite { get; set; }
    }
}