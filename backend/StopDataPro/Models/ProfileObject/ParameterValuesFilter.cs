﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesFilter
    /// </summary>
    public class ParameterValuesFilter
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int unsafeAlert { get; set; }
        public object filter { get; set; }
        public object filterLabel { get; set; }
        public object filterSetting { get; set; }
    }
}