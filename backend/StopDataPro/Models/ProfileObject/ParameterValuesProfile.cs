﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterValuesProfile
    /// </summary>
    public class ParameterValuesProfile
    {
        public bool status { get; set; }
        public string message { get; set; }
        public int userId { get; set; }
        public object language { get; set; }
        public object site { get; set; }
        public object timeZone { get; set; }
        public object userInfo { get; set; }
        public object globalDetail { get; set; }
        public object allTimeZone { get; set; }
    }
}