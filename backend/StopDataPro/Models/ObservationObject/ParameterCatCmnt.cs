﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCatCmnt
    /// </summary>
    public class ParameterCatCmnt
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object catComment { get; set; }
        public object imgPath { get; set; }
    }
}