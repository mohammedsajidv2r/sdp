﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsUpd
    /// </summary>
    public class ParameterObsUpd
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object corractionId { get; set; }
    }
}