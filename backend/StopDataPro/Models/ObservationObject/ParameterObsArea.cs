﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsArea
    /// </summary>
    public class ParameterObsArea
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object area { get; set; }
    }
}