﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCAObsList
    /// </summary>
    public class ParameterCheckObsInsert
    {
        public bool status { get; set; }
        public string message { get; set; }
        public string cardReqId { get; set; }
        public object cardId { get; set; }
    }
}