﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsSubAreaList
    /// </summary>
    public class ParameterObsSubAreaList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object subArea { get; set; }
    }
}