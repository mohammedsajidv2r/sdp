﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterCAObsList
    /// </summary>
    public class ParameterCAObsList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object catComment { get; set; }
        public object user { get; set; }
        public object imgPath { get; set; }
        public object resPerson { get; set; }
        public object correctivaData { get; set; }
    }
}