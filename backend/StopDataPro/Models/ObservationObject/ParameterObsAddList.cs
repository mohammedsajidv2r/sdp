﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsAddList
    /// </summary>
    public class ParameterObsAddList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object groupType { get; set; }
        public object groupData { get; set; }
    }
}