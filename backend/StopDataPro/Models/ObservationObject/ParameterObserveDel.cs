﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObserveDel
    /// </summary>
    public class ParameterObserveDel
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object list { get; set; }
        public object checkList { get; set; }
    }
}