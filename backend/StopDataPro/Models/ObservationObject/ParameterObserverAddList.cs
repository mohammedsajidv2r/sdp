﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObserverAddList
    /// </summary>
    public class ParameterObserverAddList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object checkList { get; set; }
        public object sites { get; set; }
        public object area { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
        public object observerId { get; set; }
        public object fieldValue { get; set; }
        public object fieldName { get; set; }
        public object checkListConfig { get; set; }
        public object subArea { get; set; }
    }
}