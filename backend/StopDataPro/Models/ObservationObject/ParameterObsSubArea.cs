﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsSubArea
    /// </summary>
    public class ParameterObsSubArea
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object subArea { get; set; }
        public object user { get; set; }
    }
}