﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObsList
    /// </summary>
    public class ParameterObsList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object checkList { get; set; }
        public object sites { get; set; }
        public object area { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
        public object defaultFilter { get; set; }
        public object list { get; set; }
        public object subArea { get; set; }
        public object restAll { get; set; }
        public object statusData { get; set; }
        public object checkListConfig { get; set; }
        public object fromDate { get; set; }
        public object toDate { get; set; }
    }
}