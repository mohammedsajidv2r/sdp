﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterObservationEdit
    /// </summary>
    public class ParameterObservationEdit
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object cardData { get; set; }
        public object users { get; set; }
        public object subArea { get; set; }
        public object area { get; set; }
        public object shifts { get; set; }
        public object observer { get; set; }
        public object observerId { get; set; }
        public object fieldName { get; set; }
        public object fieldValue { get; set; }
        public object mainCat { get; set; }
        public object subCat { get; set; }
        public object checkListConfig { get; set; }
        public string areaName { get; set; }
        public string subAreaName { get; set; }
        public string shiftName { get; set; }
        public string observerName { get; set; }
    }
}