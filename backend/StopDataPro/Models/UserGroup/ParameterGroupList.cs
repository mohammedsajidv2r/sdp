﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterGroupList
    /// </summary>
    public class ParameterGroupList
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object language { get; set; }
        public object groupType { get; set; }
        public object data { get; set; }
    }
}