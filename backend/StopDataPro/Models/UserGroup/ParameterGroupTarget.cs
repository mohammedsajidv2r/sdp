﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///  Model ParameterGroupTarget
    /// </summary>
    public class ParameterGroupTarget
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object groupInfo { get; set; }
        public object userAvail { get; set; }
    }
}