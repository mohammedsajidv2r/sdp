﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StopDataPro.Services.Models
{
    /// <summary>
    ///   Model ParameterData
    /// </summary>
    public class ParameterData
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object Data { get; set; }
    }
}
