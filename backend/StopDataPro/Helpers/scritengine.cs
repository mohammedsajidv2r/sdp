﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using System.Data;
using System.Text;
using StopDataPro.Classes;
using System.Security.Cryptography;

using StopDataPro.Business.Adapters;
using log4net;

namespace DataPro
{
    public class ScriptEngine
    {
        public static Dictionary<string, object> scriptengine_Rdata;
        public string finalquery;

        //Log = log4net.LogManager.GetLogger(typeof(ScriptEngine));
        //public ScriptEngine()
        //{
        //    this.Log = log4net.LogManager.GetLogger(typeof(ScriptEngine));
        //}
        //public ILog Log { get; set; }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <param name="tmpname">The tmpname.</param>
        /// <returns></returns>
        public string GetValue(string tmpname)
        {
            scriptengine_Rdata = null;
            if (scriptengine_Rdata == null)
            {
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/MASTERS1.1.config"));
                string strSettings = sr.ReadToEnd();
                scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                tmpdata = scriptengine_Rdata;

                string str = tmpdata.FirstOrDefault(x => x.Key == tmpname).Value.ToString();
                string[] queries = str.Split('-');
                string[] finalqueries = new string[queries.Length];
                for (var k = 0; k <= queries.Length - 1; k++)
                {
                    string aa = "~/APPMOD/CONFIG/" + queries[k] + ".config";
                    StreamReader sr1 = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/SQL/" + queries[k] + ".config"));

                    finalqueries[k] = sr1.ReadToEnd();
                    sr.Close();
                }
                finalquery = string.Join(";", finalqueries).ToString();
            }
            return finalquery;
            scriptengine_Rdata = null;
        }

        /// <summary>
        /// Gets the value for application.
        /// </summary>
        /// <param name="tmpname">The tmpname.</param>
        /// <param name="appver">The appver.</param>
        /// <returns></returns>
        public string GetValueForApp(string tmpname, string appver)
        {
            scriptengine_Rdata = null;
            if (scriptengine_Rdata == null)
            {
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/MASTERS1.1.config"));
                string strSettings = sr.ReadToEnd();
                scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                tmpdata = scriptengine_Rdata;

                string str = tmpdata.FirstOrDefault(x => x.Key == tmpname).Value.ToString();
                string[] queries = str.Split('-');
                string[] finalqueries = new string[queries.Length];
                for (var k = 0; k <= queries.Length - 1; k++)
                {
                    string aa = "~/APPMOD/CONFIG/" + queries[k] + ".config";
                    StreamReader sr1 = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOBILE/" + appver + "/SQL/" + queries[k] + ".config"));

                    finalqueries[k] = sr1.ReadToEnd();
                    sr.Close();
                }
                finalquery = string.Join(";", finalqueries).ToString();
            }
            return finalquery;
            scriptengine_Rdata = null;
        }

        /// <summary>
        /// Gets the table rows.
        /// </summary>
        /// <param name="dtData">The dt data.</param>
        /// <returns></returns>
        public List<Dictionary<string, object>> GetTableRows(DataTable dtData)
        {
            List<Dictionary<string, object>>
            lstRows = new List<Dictionary<string, object>>();
            Dictionary<string, object> dictRow = null;
            foreach (DataRow dr in dtData.Rows)
            {
                dictRow = new Dictionary<string, object>();
                foreach (DataColumn col in dtData.Columns)
                {
                    dictRow.Add(col.ColumnName, dr[col]);
                }
                lstRows.Add(dictRow);
            }
            return lstRows;
        }

        /// <summary>
        /// Tokens the check.
        /// </summary>
        /// <returns></returns>
        public DataTable TokenCheck()
        {
            BaseAdapter bal = new BaseAdapter();
            string token = HttpContext.Current.Request.Headers["Authorization"];
            string url = HttpContext.Current.Request.Headers["Origin"];
            AppInfo infoconfig = new AppInfo();
            DataTable dt = new DataTable();
            string encodedString = "";
            char[] whitespace = new char[] { ' ', '\t' };
            string[] tokenstr = token.Split(whitespace);
            encodedString = tokenstr[1].ToString();
            string con = infoconfig.createConn(url);
            bal.constringBal = con;
            dt = bal.CheckLoginToken(encodedString);
            return dt;
        }

        /// <summary>
        /// Scriptengines the data.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> scriptengine_data()
        {
            Dictionary<string, object> scriptengine_dataReturn;
            StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
            string strsettings = srSettings.ReadToEnd();
            scriptengine_dataReturn = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
            srSettings.Close();
            Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
            Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
            dctfulldata = scriptengine_dataReturn;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
            return dctpartdata;
        }

        /// <summary>
        /// Scriptengines the data schedular.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> scriptengine_dataSchedular()
        {
            Dictionary<string, object> scriptengine_dataReturn;
            StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
            string strsettings = srSettings.ReadToEnd();
            scriptengine_dataReturn = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
            srSettings.Close();
            Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
            Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
            dctfulldata = scriptengine_dataReturn;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
            return dctpartdata;
        }

        /// <summary>
        /// Scriptengines the data schedular report service.
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> scriptengine_dataSchedularReportService()
        {
            Dictionary<string, object> scriptengine_dataReturn;
            StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
            string strsettings = srSettings.ReadToEnd();
            scriptengine_dataReturn = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
            srSettings.Close();
            Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
            Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
            dctfulldata = scriptengine_dataReturn;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "dataproreportservice").Value;
            return dctpartdata;
        }

        /// <summary>
        /// Decrypts the string aes.
        /// </summary>
        /// <returns></returns>
        public static string DecryptStringAES(string txt)
        {
            var keybytes = Encoding.UTF8.GetBytes("Se5ZfFTzJ36PH7T6");
            var iv = Encoding.UTF8.GetBytes("Se5ZfFTzJ36PH7T6");
            var encrypted = Convert.FromBase64String(txt);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);
            return decriptedFromJavascript;
        }

        /// <summary>
        /// Decrypts the string from bytes.
        /// </summary>
        /// <param name="cipherText">The cipher text.</param>
        /// <param name="key">The key.</param>
        /// <param name="iv">The iv.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">
        /// cipherText
        /// or
        /// key
        /// or
        /// key
        /// </exception>
        private static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;
                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream(cipherText))
                {
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (var srDecrypt = new StreamReader(csDecrypt))
                        {
                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
            return plaintext;
        }

        /// <summary>Encrypts the string to bytes.</summary>
        /// <param name="plainText">The plain text.</param>
        /// <param name="key">The key.</param>
        /// <param name="iv">The iv.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException">plainText
        /// or
        /// key
        /// or
        /// key</exception>
        private static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            // Check arguments.  
            if (plainText == null || plainText.Length <= 0)
            {
                throw new ArgumentNullException("plainText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            byte[] encrypted;
            // Create a RijndaelManaged object  
            // with the specified key and IV.  
            using (var rijAlg = new RijndaelManaged())
            {
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.  
                var encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption.  
                using (var msEncrypt = new MemoryStream())
                {
                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (var swEncrypt = new StreamWriter(csEncrypt))
                        {
                            //Write all data to the stream.  
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }

            // Return the encrypted bytes from the memory stream.  
            return encrypted;
        }

    }
}
