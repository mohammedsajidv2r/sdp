﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace StopDataPro.Classes
{
    /// <summary>
    /// AppInfo
    /// </summary>
    public class AppInfo
    {
        public static Dictionary<string, object> scriptengine_Rdata;
        public string URL, CmpId;

        /// <summary>
        /// Creates the connection.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public string createConn(string url)
        {
            string[] aa = url.Split('/');
            string httpUrl = aa[2];
            StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
            string strSettings = sr.ReadToEnd();
            scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
            sr.Close();
            URL = httpUrl; 
            Dictionary<string, object> tmpdata = new Dictionary<string, object>();
            Dictionary<string, object> cmpdata = new Dictionary<string, object>();
            Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();  
            tmpdata = scriptengine_Rdata;
            cmpdata = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key.ToLower() == URL.ToLower()).Value;
            CmpId = cmpdata["COMPANYID"].ToString();
            cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == CmpId).Value;
            string FA = cmpdata1["FA"].ToString();
            return FA;
        }

        /// <summary>
        /// Gets the URL host.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <returns></returns>
        public string getUrlHost(string url)
        {
            string[] aa = url.Split('/');
            string httpUrl = aa[2];
            StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
            string strSettings = sr.ReadToEnd();
            scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
            sr.Close();
            URL = httpUrl; 
            Dictionary<string, object> tmpdata = new Dictionary<string, object>();
            Dictionary<string, object> cmpdata = new Dictionary<string, object>();
            Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();  
            tmpdata = scriptengine_Rdata;
            cmpdata = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key.ToLower() == URL.ToLower()).Value;
            CmpId = cmpdata["COMPANYID"].ToString();
            cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == CmpId).Value;
            return cmpdata1["URL"].ToString();
        }
    }
}