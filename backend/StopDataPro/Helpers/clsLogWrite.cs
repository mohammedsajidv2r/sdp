﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;

namespace StopDataPro.Classes
{
    public class ClsLogWrite
    {
        public static string filePath = string.Empty;

        public void createFolder()
        {
            DateTime dateto = DateTime.Now;
            filePath = @"C:/DataPro/AllLogs/";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }

        public static void createFolderStructure()
        {
            DateTime dateto = DateTime.Now;
            string day = dateto.Day.ToString();
            string motth = dateto.Month.ToString();
            string year = dateto.Year.ToString();
            filePath = @"C:/DataPro/AllLogs/" + year + @"/" + motth + @"/" + day + @"/";
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
        }
        
        public void writeToLog(string towrite)
        {
            createFolderStructure();
            Thread.Sleep(10);
            using (var fs = File.Open(filePath + "Exception.txt", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
            {
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(DateTime.Now.ToString() + ": " + towrite + "  \n");
                m_streamWriter.Dispose();
            }
        }

        //for maintain exception in log
        public  void writeToLogPage(Exception ex,  string pagename)
        {
            string towrite = "Message:" + ex.Message + ",Trace:" + ex.StackTrace;
            createFolderStructure();
            Thread.Sleep(500);
            using (var fs = File.Open(filePath + "ExceptionLevelTrace.txt", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
            {
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine("\n" + DateTime.Now.ToString() + ": " + towrite + ", Page Name:" + pagename + "  \n");
                m_streamWriter.Dispose();
            }
        }

        public void writeToLog1(string towrite)
        {
            createFolderStructure();
            Thread.Sleep(500);
            using (var fs = File.Open(filePath + "Exception.txt", FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
            {
                StreamWriter m_streamWriter = new StreamWriter(fs);
                m_streamWriter.BaseStream.Seek(0, SeekOrigin.End);
                m_streamWriter.WriteLine(DateTime.Now.ToString() + ": " + towrite + "  \n");
                m_streamWriter.Dispose();
            }
        }

        public static string getip()
        {
            string ipMix = "";
            string SystemName = "";
            string ip = "";
            string ip1 = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList.GetValue(0).ToString();//ipv6
            string strHostName = System.Net.Dns.GetHostName();  //ARJDU-D-0234
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName); //IPV6-fe80::400a:da9:20e9:28ea%3 IPV4- 172.16.39.146
            IPAddress[] addr = ipEntry.AddressList; //172.16.39.146
            ip = addr[1].ToString();
            try
            {
                SystemName = ipEntry.HostName; //for get full computer name
            }
            catch (Exception ex)
            {
                SystemName = "";
            }
            ipMix = ip+","+ addr[0].ToString()+","+ SystemName+","+ ip1;
            return ipMix;
        }
        
    }
}