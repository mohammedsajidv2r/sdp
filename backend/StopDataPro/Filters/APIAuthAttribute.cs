﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using WebAPI.Helpers;
using WebAPI.Library;

namespace WebAPI.Filters
{
    public class APIAuthAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (Authorize(filterContext))
            {
                return;
            }
            HandleUnauthorizedRequest(filterContext);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
        }

        private bool Authorize(HttpActionContext actionContext)
        {
            try
            {
                bool validFlag = false;
                var encodedString = "";
                DateTime createdOn = DateTime.Now;
                var authorization = actionContext.Request.Headers.Authorization;

                if (authorization == null || authorization.Scheme != "Bearer")
                    return false;
                else if (string.IsNullOrEmpty(authorization.Parameter))
                    return false;
                else
                {
                    encodedString = authorization.Parameter.ToString();
                    validFlag = true;
                }

                return validFlag;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}