﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using CacheHandler;
using System.Collections.Generic;
using System.IO;
using System.Web.Script.Serialization;
using System.Net;
using System.Web.Services.Description;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Reflection;
using CommonData;

using log4net;
using ColorMine.ColorSpaces;

using StopDataPro.DataAccess.Repositories;
using DESFramework;
using System.Drawing;

namespace DataPro
{
    /// <summary>
    /// Summary description for dataproreportservice
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class dataproreportservice : System.Web.Services.WebService
    {
        BaseRepository dbcon = new BaseRepository();
        //ILog log = log4net.LogManager.GetLogger(typeof(dataproreportservice));
        [WebMethod]
        public string ReportSchedulingUrl(int ischeduleid, string icompanyid)
        {
            string strquery = string.Empty;
            string result = "Success";
            string URLERRORMSG = "";
            string strAPPID = "", ES_WSNAME = "", ES_WSMETHOD = "", ES_WSURL = "";
            Controller objfrk = new Controller();
            string strUrl = "";
            Dictionary<string, string> dctTimer = new Dictionary<string, string>();
            string strReportname = "", strRPTname = "", strRPTnameOpt = "";

           //log.InfoFormat("==Service Start==");
            try
            {
                dctTimer.Add("BeginCommanCode", DateTime.UtcNow.ToString());
                //DataPro configuration setting 
                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;

                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
                strAPPID = dctpartdata["APPID"].ToString();
                ES_WSNAME = dctpartdata["WSNAME"].ToString();
                ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                ES_WSURL = dctpartdata["WSURL"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "reportservice").Value;
                string RS_WSNAME = dctpartdata["WSNAME"].ToString();
                string RS_WSURL = dctpartdata["WSURL"].ToString();
                string ConfReportCaching = dctpartdata["SCHEDULEREPORTCACHING"].ToString().ToLower();//YES - caching server, NO - datatable cache
                string RS_WSMETHOD = "";

                if (ConfReportCaching == "yes")
                {
                    RS_WSMETHOD = dctpartdata["WSMETHOD1"].ToString();
                }
                else
                {
                    RS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                }
                string ReportCacheDuration = dctpartdata["CACHEDURATIONINMINUTES"].ToString().ToLower();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "commonreportservice").Value;
                string strPRODUCTID = dctpartdata["CRSPRODUCTID"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "dataproreportservice").Value;
                string DRS_SERVER = dctpartdata["SERVER"].ToString();
                string DRS_WSNAME = dctpartdata["WSNAME"].ToString();
                string DRS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                //string DRS_WSURL = "http://" + vdataproUrl + "" + dctpartdata["WSURL"].ToString();
                string DRS_WSURL = dctpartdata["WSURL"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string RSCH_WSURL = dctpartdata["WSURL"].ToString();
                //DataPro configuration setting

                //log.InfoFormat("RS_WSNAME:" + RS_WSNAME);
                //log.InfoFormat("RS_WSURL:" + RS_WSURL);
                DataSet dsScheduleInfo, dsReportResult;
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                string strSettings = sr.ReadToEnd();
                Dictionary<string, object> scriptengine_data1;
                scriptengine_data1 = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                tmpdata = scriptengine_data1;
                cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == icompanyid).Value;
                string FA = cmpdata1["FA"].ToString();
                dbcon.constring = FA;

                //  dbcon.constring = icompanyid.ToString() + ".FA";
                string strDataCachekey = "", strReportJSONkey = "", strLangkey = "";
                string StrReportVar;
                //CacheProcess cp = new CacheProcess();
                dctTimer.Add("EndCommanCode", "SERVER: " + DRS_SERVER + " |" + DateTime.UtcNow.ToString());
                dctTimer.Add("BeginGetScheduleInfo", DateTime.UtcNow.ToString());


                strquery = GetValue("GETSCHEDULEINFO");
                string[] paramrptinfo = { "SCHEDULEID" };
                object[] pValuesrptinfo = { ischeduleid };
                dsScheduleInfo = dbcon.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);

                string strParamCache = "";
                string strProcessCache = "";

                dctTimer.Add("EndGetScheduleInfo", DateTime.UtcNow.ToString());

                if (dsScheduleInfo.Tables.Count > 0)
                {
                   // log.InfoFormat("Schedule Table Count Check");
                    if ((dsScheduleInfo.Tables[0].Rows.Count > 0) && (dsScheduleInfo.Tables[1].Rows.Count > 0))
                    {
                        dctTimer.Add("BeginUpdateTockentobesentdate", DateTime.UtcNow.ToString());
                        string rptqueryvalue = dsScheduleInfo.Tables[0].Rows[0]["QUERYVALUE"].ToString();
                        long iuserid = Convert.ToInt64(dsScheduleInfo.Tables[0].Rows[0]["USERID"].ToString());
                        string vlanguagecode = dsScheduleInfo.Tables[0].Rows[0]["LANGUAGECODE"].ToString();
                        int ilanguageID = Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["LANGUAGEID"].ToString());
                        string vDateformat = dsScheduleInfo.Tables[0].Rows[0]["DATEFORMAT"].ToString();
                        string ireportid = dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString();
                        string iRptDesign = dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString();
                        string headerbgcolor = dsScheduleInfo.Tables[0].Rows[0]["CUSTOMCOLOR"].ToString();
                        //this code is use to convert hsl fornat into hax rgb for header color 
                        string vVal = "";
                        if (headerbgcolor.Contains("hsl"))
                        {
                            string[] bgcsv = headerbgcolor.Split(',');
                            string[] H = bgcsv[0].Split('(');
                            bgcsv[1].Remove(bgcsv[1].Length - 1, 1);
                            if (bgcsv.Length == 4)
                            {
                                vVal = bgcsv[2].Remove(bgcsv[2].Length - 1, 1);
                            }
                            else
                            {
                                vVal = bgcsv[2].Remove(bgcsv[2].Length - 2, 2);
                            }

                            var Hsl = new Hsl { H = float.Parse(H[1]), S = float.Parse(bgcsv[1].Remove(bgcsv[1].Length - 1, 1)), L = float.Parse(vVal) };
                            var rgb = Hsl.To<Rgb>();
                            //red, green, blue
                            Color myColor = Color.FromArgb(Convert.ToInt32(rgb.R), Convert.ToInt32(rgb.G), Convert.ToInt32(rgb.B));
                            string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");

                            //var bgcol = Hsl.GetHashCode();
                            headerbgcolor = "#"+hex;
                        }

                        //this code is use to convert hsl fornat into hax rgb for font color 
                        string headerfontcolor = dsScheduleInfo.Tables[0].Rows[0]["CUSTOMFONT"].ToString();
                        try
                        {
                            if (headerfontcolor.Contains("hsl"))
                            {
                                string[] bgcsv = headerfontcolor.Split(',');
                                string[] H = bgcsv[0].Split('(');
                                bgcsv[1].Remove(bgcsv[1].Length - 1, 1);
                                if (bgcsv.Length == 4)
                                {
                                    vVal = bgcsv[2].Remove(bgcsv[2].Length - 1, 1);
                                }
                                else
                                {
                                    vVal = bgcsv[2].Remove(bgcsv[2].Length - 2, 2);
                                }

                                //var hsv = new Hsv { H = float.Parse(H[1]), S = float.Parse(bgcsv[1].Remove(bgcsv[1].Length - 1, 1)), V = float.Parse(vVal) };
                                var Hsl = new Hsl { H = float.Parse(H[1]), S = float.Parse(bgcsv[1].Remove(bgcsv[1].Length - 1, 1)), L = float.Parse(vVal) };
                                var rgb = Hsl.To<Rgb>();
                                //red, green, blue
                                Color myColor = Color.FromArgb(Convert.ToInt32(rgb.R), Convert.ToInt32(rgb.G), Convert.ToInt32(rgb.B));
                                string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");
                                headerfontcolor = "#"+hex;
                                //var bgcol = hsv.GetHashCode();
                                //headerfontcolor = bgcol.ToString();
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        string strenablesubarea = dsScheduleInfo.Tables[0].Rows[0]["ENABLESUBAREAS"].ToString();
                        strUrl = dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString();
                        strRPTnameOpt = dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString();
                        //as discussed with report team we need to increment the TOCKENTOBESENTDATE as +1 day after processing the schedule tocken
                        DateTime startdate, enddate, tockendate;
                        startdate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["STARTDATE"].ToString());
                        enddate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["ENDDATE"].ToString());
                        tockendate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["TOCKENTOBESENTDATE"].ToString());

                        string strForm = "{0}?schid={1}&compid={2}";
                        DRS_WSURL = string.Format(strForm, DRS_WSURL, ischeduleid, icompanyid);
                        //use for call scheduler.asmx pass json parameter with Schedulara mehtod name
                        String strJSON1 = "{\"MODE\":" + 2
                            + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                            + ",\"COMPANYID\":\"" + icompanyid
                            + "\",\"TOKENID\":" + ischeduleid
                            + ",\"STARTDATE\":\"" + startdate.ToString("MM/dd/yyyy")
                            + "\",\"ENDDATE\":\"" + enddate.ToString("MM/dd/yyyy")
                            + "\",\"DWM\":" + dsScheduleInfo.Tables[0].Rows[0]["DWM"].ToString()
                            + ",\"EVERYDWM\":" + dsScheduleInfo.Tables[0].Rows[0]["EVERYDWM"].ToString()
                            + ",\"DAY\":" + dsScheduleInfo.Tables[0].Rows[0]["DAY"].ToString()
                            + ",\"MONTH\":" + dsScheduleInfo.Tables[0].Rows[0]["MONTH"].ToString()
                            + ",\"ACTIVE\":" + dsScheduleInfo.Tables[0].Rows[0]["STATUS"].ToString()
                            + ",\"TOKENTOBESENTDATE\":\"" + tockendate.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                            + "\",\"STARTTIME\":" + dsScheduleInfo.Tables[0].Rows[0]["STARTTIME"].ToString()
                            + ",\"TIMEZONE\":" + dsScheduleInfo.Tables[0].Rows[0]["TIMEZONE"].ToString()
                            + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                            + "\",\"CALLBACKWS\":\"" + DRS_WSURL
                            + "\",\"CALLBACKWSMETHOD\":\"" + DRS_WSMETHOD + "\"}]}";
                        //string mailstatus = ESch.SendScheduleDetails(strJSON);
                        object[] objSendScheduleDetails = new object[1];
                        objSendScheduleDetails[0] = strJSON1;

                     //   log.InfoFormat("Json String:" + strJSON1);

                        object obj1 = CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                        
                       // log.InfoFormat("WebService Return Value obj1:" + obj1.ToString());
                        if (obj1.ToString() != "1")
                        {
                            result = "Problem with updating the schedule tokentobesentdate";
                            strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                            string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                            object[] pValuesrptinfomail = { ischeduleid, result, strJSON1.ToString(), "", "", strDataCachekey, strReportJSONkey, strLangkey, obj1.ToString() };
                            int ELMAIL = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                        }
                        dctTimer.Add("EndUpdateTockentobesentdate", DateTime.UtcNow.ToString());
                        dctTimer.Add("BeforeParamConst", DateTime.UtcNow.ToString());

                        if (dsScheduleInfo.Tables[0].Rows[0]["ISQUERY"].ToString() == "1")
                        {
                            strquery = GetValue(rptqueryvalue);
                        }
                        else
                        {
                            strquery = rptqueryvalue;
                        }

                        string vfilterparam = dsScheduleInfo.Tables[0].Rows[0]["REPORTPARAM"].ToString();
                        string[] items = vfilterparam.TrimEnd(';').Split(';');

                        //REPORT PARAMETERS
                        List<string> paramrpt = new List<string>();
                        List<string> paramrptval = new List<string>();
                        foreach (string item in items)
                        {
                            if (item != "")
                            {
                                string[] keyValue = item.Split('=');
                                paramrpt.Add(keyValue[0]);
                                paramrptval.Add(keyValue[1]);
                            }
                        }
                        paramrpt.Add("COMPANYID");
                        paramrptval.Add(icompanyid.ToString());
                        paramrpt.Add("USERID");
                        paramrptval.Add(iuserid.ToString());
                        paramrpt.Add("LANGUAGEID");
                        paramrptval.Add(ilanguageID.ToString());
                        List<string> lstDATE = new List<string>();
                        List<string> lstDATERANGE = new List<string>();
                        if (ireportid != "3")//except sites report
                        {
                            if (dsScheduleInfo.Tables[0].Rows[0]["RELATIVEFOR"].ToString() == "1")
                            {
                                paramrpt.Add("OBSDATE");
                                lstDATE.Add("OBSDATE");
                            }
                            else if (dsScheduleInfo.Tables[0].Rows[0]["RELATIVEFOR"].ToString() == "2")
                            {
                                paramrpt.Add("CREATEDDATE");
                                lstDATE.Add("CREATEDDATE");
                            }
                            else if (dsScheduleInfo.Tables[0].Rows[0]["RELATIVEFOR"].ToString() == "3")
                            {
                                paramrpt.Add("RESDATEREQ");
                                lstDATE.Add("RESDATEREQ");
                            }
                            else if (dsScheduleInfo.Tables[0].Rows[0]["RELATIVEFOR"].ToString() == "4")
                            {
                                paramrpt.Add("RESPONDEDDATE");
                                lstDATE.Add("RESPONDEDDATE");
                            }
                            paramrptval.Add(dsScheduleInfo.Tables[0].Rows[0]["DATE"].ToString());
                            lstDATERANGE.Add(dsScheduleInfo.Tables[0].Rows[0]["DATE"].ToString());
                        }

                        if (ireportid == "9")//additional param for Checklist Report (By Metrics)
                        {
                            paramrpt.Add("SAFECHECK");
                            if (iRptDesign == "13")
                            {
                                paramrptval.Add("0");
                            }
                            else if (iRptDesign == "14")
                            {
                                paramrptval.Add("1");
                            }
                            else if (iRptDesign == "15")
                            {
                                paramrptval.Add("2");
                            }
                            else
                            {
                                paramrptval.Add("0");
                            }
                        }
                        if (ireportid == "10")//additional param for Checklist Report (By Category)
                        {
                            paramrpt.Add("SAFECHECK");
                            if (iRptDesign == "16")
                            {
                                paramrptval.Add("0");
                            }
                            else if (iRptDesign == "17")
                            {
                                paramrptval.Add("1");
                            }
                            else if (iRptDesign == "18")
                            {
                                paramrptval.Add("2");
                            }
                            else
                            {
                                paramrptval.Add("0");
                            }
                        }
                        if (ireportid == "7")
                        {
                            paramrpt.Add("METRICSVALUE");
                            if (iRptDesign == "11")
                            {
                                paramrptval.Add("0");
                            }
                            else if (iRptDesign == "29")
                            {
                                paramrptval.Add("1");
                            }
                            else if (iRptDesign == "30")
                            {
                                paramrptval.Add("2");
                            }
                            else
                            {
                                paramrptval.Add("0");
                            }
                        }
                        if (ireportid == "4")
                        {
                            paramrpt.Add("METRICSVALUE");
                            if (iRptDesign == "6")
                            {
                                paramrptval.Add("0");
                            }
                            else if (iRptDesign == "31")
                            {
                                paramrptval.Add("1");
                            }
                            else if (iRptDesign == "32")
                            {
                                paramrptval.Add("2");
                            }
                            else
                            {
                                paramrptval.Add("0");
                            }
                        }
                        if (ireportid == "8" || ireportid == "13")
                        {
                            paramrpt.Add("URL");
                            paramrptval.Add(DRS_WSURL.Replace("dataproreportservice.asmx", "").Replace("/", "a_b"));
                            paramrpt.Add("APPURL");
                            paramrptval.Add(DRS_WSURL.Replace("dataproreportservice.asmx", "").Replace("/", "a_b"));
                        }
                        //this is hard-coded designid for checklistcountsummary report display page
                        //if (ireportid == "16")
                        //{
                        //    paramrpt.Add("REPORTFILTERCHKCOUNT");
                        //    paramrptval.Add("0*0");
                        //}
                        string[] paramrpt1 = paramrpt.ToArray();
                        object[] paramrptval1 = paramrptval.ToArray();
                        string[] strparamrptval1 = paramrptval.ToArray();

                        strParamCache = "||" + Convert.ToString(icompanyid) + "||" + Convert.ToString(iuserid) + "||" + rptqueryvalue + "||" + string.Join("!", paramrpt1) + "||" + string.Join("!", strparamrptval1).Replace(":", "c_l");
                        strProcessCache = "||" + ischeduleid.ToString() + "||" + vlanguagecode + "||" + vDateformat + "||" + headerfontcolor + "||" + headerbgcolor + "||" + strenablesubarea + "||";
                        dctTimer.Add("AfterParamConst", DateTime.UtcNow.ToString());
                        dctTimer.Add("BeginCacheServerCheck", DateTime.UtcNow.ToString());
                        CacheProcess cp = new CacheProcess();
                        if (ConfReportCaching == "yes")
                        {
                            try
                            {
                                cp.KillCache("DATAPRO", "CACHEKEY");
                            }
                            catch (Exception Ex2)
                            {
                                dctTimer.Add("BeginCacheServerCheckIssue", Ex2.ToString());
                                ConfReportCaching = "no";
                            }
                        }
                        dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "reportservice").Value;
                        if (ConfReportCaching == "yes")
                        {
                            RS_WSMETHOD = dctpartdata["WSMETHOD1"].ToString();
                        }
                        else
                        {
                            RS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                        }
                        dctTimer.Add("EndCacheServerCheck", DateTime.UtcNow.ToString());
                        dctTimer.Add("BeforeDBHit", DateTime.UtcNow.ToString());

                        //data cachekey
                        if (dsScheduleInfo.Tables[0].Rows[0]["ISQUERY"].ToString() == "1")
                        {
                            dsReportResult = dbcon.ExecuteInlineQueries_Dataset(strquery, paramrpt1, paramrptval1);
                        }
                        else
                        {
                            dsReportResult = dbcon.ExecuteStoreProc_Dataset(strquery, paramrpt1, paramrptval1);
                            if (strquery == "REPORTTARGETVSACTIVITY")
                            {
                                int ck = 0;
                                for (int i = 0; i < dsReportResult.Tables[0].Columns.Count; i++)
                                {
                                    if (i>9)
                                    {
                                        string columnname = dsReportResult.Tables[0].Columns[i].ColumnName.ToString();
                                        if (columnname.Contains("PER"))
                                        {
                                            if(i!=ck)
                                            {
                                                ck = 2 + i;
                                                dsReportResult.Tables[0].Columns[columnname].SetOrdinal(ck);
                                            }
                                        }
                                    }
                                }
                            }
                            if (strquery == "REPORTTARGETVSACTIVITYWEEKLY")
                            {
                                int ck = 0;
                                for (int i = 0; i < dsReportResult.Tables[0].Columns.Count; i++)
                                {
                                    if (i > 9)
                                    {
                                        string columnname = dsReportResult.Tables[0].Columns[i].ColumnName.ToString();
                                        if (columnname.Contains("PER"))
                                        {
                                            if (i != ck)
                                            {
                                                ck = 2 + i;
                                                dsReportResult.Tables[0].Columns[columnname].SetOrdinal(ck);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        dctTimer.Add("AfterDBHit", DateTime.UtcNow.ToString());

                        if (ConfReportCaching == "yes")
                        {
                            dctTimer.Add("BeforeDataCache", DateTime.UtcNow.ToString());
                            strDataCachekey = Convert.ToString(icompanyid) + "SCH" + Convert.ToString(iuserid) + strParamCache + strProcessCache;
                            cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                            strDataCachekey = cp.CacheData(strPRODUCTID, dsReportResult, Convert.ToString(icompanyid) + "SCH", Convert.ToString(iuserid), strParamCache, strProcessCache);// change the Parameters for DataTable Cache 
                            dctTimer.Add("AfterDataCache", DateTime.UtcNow.ToString());
                        }
                        else
                        {
                            dctTimer.Add("BeforeSerialize", DateTime.UtcNow.ToString());
                            strDataCachekey = DataUtil.SerializeDataSet(dsReportResult);
                            dctTimer.Add("AfterSerialize", DateTime.UtcNow.ToString());
                        }

                        //report cachekey
                        strReportJSONkey = null;
                        if (ConfReportCaching == "yes")
                        {
                            dctTimer.Add("BeforeReportDesignKillCache", DateTime.UtcNow.ToString());
                            cp.KillCache(strPRODUCTID, Convert.ToString(icompanyid) + "SCH" + dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString() + rptqueryvalue + dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString());
                            dctTimer.Add("AfterReportDesignKillCache", DateTime.UtcNow.ToString());
                        }
                        if (strReportJSONkey == null)
                        {
                            dctTimer.Add("BeforeReportDesignDynamicChange", DateTime.UtcNow.ToString());
                            if (ireportid == "6")//if target vs activity monthly report, design will be generated dynamically
                            {
                                StrReportVar = TargetVsActivityReport(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "23")//if target vs activity period report, design will be generated dynamically
                            {
                                StrReportVar = TargetVsActivityReportPeriod(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "19")//if target vs activity weekly report, design will be generated dynamically
                            {
                                StrReportVar = TargetVsActivityReportWeekly(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "16")
                            {
                                StrReportVar = ChecklistCountSummaryReport(dsReportResult, dsScheduleInfo);
                            }
                            else if (ireportid == "17")
                            {
                                StrReportVar = TrendlineYeartoYear(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "18")
                            {
                                StrReportVar = UserObserverList(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "20")
                            {
                                StrReportVar = StatsByCategoryTabularReport(dsReportResult, dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "7")
                            {
                                StrReportVar = CustomFields(dsReportResult.Tables[1], dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "9")
                            {
                                StrReportVar = CustomFields(dsReportResult.Tables[1], dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else if (ireportid == "14" && iRptDesign == "28")
                            {
                                StrReportVar = CustomFields(dsReportResult.Tables[0], dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString());
                            }
                            else
                            {
                                StrReportVar = dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString();
                            }
                            StrReportVar = StrReportVar.Replace("USRDATEFORMAT", dsScheduleInfo.Tables[0].Rows[0]["DATEFORMAT"].ToString());
                            StrReportVar = StrReportVar.Replace("GROUPLABELFONTCOLOR", headerfontcolor);
                            StrReportVar = StrReportVar.Replace("GROUPLABELBGCOLOR", headerbgcolor);
                            dctTimer.Add("AfterReportDesignDynamicChange", DateTime.UtcNow.ToString());
                            //print date range on exports
                            if (lstDATE.Count > 0)
                            {
                                dctTimer.Add("BeforePrintDateRange", DateTime.UtcNow.ToString());
                                ArrayList listArrHeader = new ArrayList();
                                for (int i = 0; i <= lstDATE.Count - 1; i++)
                                {
                                    Dictionary<string, string> dctStyle1 = new Dictionary<string, string>();
                                    dctStyle1.Add("FontName", "Arial Unicode MS");
                                    dctStyle1.Add("FontColor", "#000000");
                                    dctStyle1.Add("FontSize", "6px");
                                    dctStyle1.Add("BgColor", "#FFFFFF");
                                    dctStyle1.Add("FontWeight", "normal");
                                    dctStyle1.Add("Alignment", "left");
                                    dctStyle1.Add("Indent", "25");

                                    Dictionary<string, object> dctStyleVal1 = new Dictionary<string, object>();
                                    dctStyleVal1.Add("Value", " ");
                                    dctStyleVal1.Add("Style", dctStyle1);
                                    listArrHeader.Add(dctStyleVal1);

                                    Dictionary<string, string> dctStyle2 = new Dictionary<string, string>();
                                    dctStyle2.Add("FontName", "Arial Unicode MS");
                                    dctStyle2.Add("FontColor", "#000000");
                                    dctStyle2.Add("FontSize", "14px");
                                    dctStyle2.Add("BgColor", "#e3f7ff");
                                    dctStyle2.Add("FontWeight", "bold");
                                    dctStyle2.Add("Alignment", "left");
                                    dctStyle2.Add("Indent", "15");

                                    Dictionary<string, object> dctStyleVal2 = new Dictionary<string, object>();
                                    dctStyleVal2.Add("Value", "LBL" + lstDATE[i]);
                                    dctStyleVal2.Add("FieldType", "Header");
                                    dctStyleVal2.Add("Style", dctStyle2);
                                    listArrHeader.Add(dctStyleVal2);

                                    Dictionary<string, string> dctStyle3 = new Dictionary<string, string>();
                                    dctStyle3.Add("FontName", "Arial Unicode MS");
                                    dctStyle3.Add("FontColor", "#000000");
                                    dctStyle3.Add("FontSize", "12px");
                                    dctStyle3.Add("BgColor", "#FFFFFF");
                                    dctStyle3.Add("FontWeight", "normal");
                                    dctStyle3.Add("Alignment", "left");
                                    dctStyle3.Add("Indent", "25");

                                    Dictionary<string, object> dctStyleVal3 = new Dictionary<string, object>();
                                    dctStyleVal3.Add("Value", lstDATERANGE[i] + " (MM/DD/YYYY)");
                                    dctStyleVal3.Add("Style", dctStyle3);
                                    listArrHeader.Add(dctStyleVal3);

                                    Dictionary<string, string> dctStyle4 = new Dictionary<string, string>();
                                    dctStyle4.Add("FontName", "Arial Unicode MS");
                                    dctStyle4.Add("FontColor", "#000000");
                                    dctStyle4.Add("FontSize", "6px");
                                    dctStyle4.Add("BgColor", "#FFFFFF");
                                    dctStyle4.Add("FontWeight", "normal");
                                    dctStyle4.Add("Alignment", "left");
                                    dctStyle4.Add("Indent", "25");

                                    Dictionary<string, object> dctStyleVal4 = new Dictionary<string, object>();
                                    dctStyleVal4.Add("Value", "##EOH##");
                                    dctStyleVal4.Add("Style", dctStyle4);
                                    listArrHeader.Add(dctStyleVal4);
                                }
                                Dictionary<string, ArrayList> dctHeader = new Dictionary<string, ArrayList>();
                                dctHeader.Add("Headers", listArrHeader);

                                var json_serializer1 = new JavaScriptSerializer();
                                string strdata = json_serializer1.Serialize(dctHeader);

                                StrReportVar = StrReportVar.Replace("DATERANGE", strdata);
                                dctTimer.Add("EndPrintDateRange", DateTime.UtcNow.ToString());
                            }
                            //print date range on exports

                            //include SUBAREA
                            if (strenablesubarea == "1")
                            {
                                Dictionary<string, string> dctSubAreaField = new Dictionary<string, string>();
                                ArrayList listSubAreaField = new ArrayList();

                                dctSubAreaField = new Dictionary<string, string>();
                                dctSubAreaField.Add("DetailFieldName", "SUBAREANAME");
                                dctSubAreaField.Add("DetailFieldLabel", "LBLSUBAREAFULLNAME");
                                dctSubAreaField.Add("DetailFieldWidth", "180");
                                dctSubAreaField.Add("DetailFieldRowPosition", "1");
                                dctSubAreaField.Add("DetailFieldAlignment", "left");
                                dctSubAreaField.Add("DatailDataType", "1");
                                dctSubAreaField.Add("DetailShowSort", "false");
                                dctSubAreaField.Add("DetailSortType", "1");
                                dctSubAreaField.Add("DetailShowInOutput", "true");
                                dctSubAreaField.Add("DetailTotalType", "0");
                                dctSubAreaField.Add("DetailShowInSection", "0");
                                dctSubAreaField.Add("DetailDisplayIn", "0");
                                dctSubAreaField.Add("DetailIsClickable", "false");
                                listSubAreaField.Add(dctSubAreaField);

                                var json_subAreaserializer = new JavaScriptSerializer();
                                string strSubAreadata = json_subAreaserializer.Serialize(listSubAreaField);
                                StrReportVar = StrReportVar.Replace("SUBAREAFIELD", strSubAreadata.Replace("[", "").Replace("]", ""));
                            }
                            else
                            {
                                StrReportVar = StrReportVar.Replace("SUBAREAFIELD,", "");
                            }
                            //include SUBAREA

                            strReportJSONkey = StrReportVar;
                            if (ConfReportCaching == "yes")
                            {
                                dctTimer.Add("BeforeReportDesignCache", DateTime.UtcNow.ToString());
                                strReportJSONkey = cp.CacheData(strPRODUCTID, strReportJSONkey, Convert.ToString(icompanyid) + "SCH", dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString(), rptqueryvalue, dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString());
                                dctTimer.Add("AfterReportDesignCache", DateTime.UtcNow.ToString());
                            }
                        }

                        //lang cachekey
                        strLangkey = null;
                        if (ConfReportCaching == "yes")
                        {
                            dctTimer.Add("BeforeLangCacheExistCheck", DateTime.UtcNow.ToString());
                            Boolean Hascache = cp.isCacheExist(strPRODUCTID, Convert.ToString(icompanyid) + "ALLLANG" + vlanguagecode + "ALLLANG");
                            if (Hascache == true)
                            {
                                strLangkey = Convert.ToString(icompanyid) + "ALLLANG" + vlanguagecode + "ALLLANG";
                            }
                            dctTimer.Add("AfterLangCacheExistCheck", DateTime.UtcNow.ToString());
                        }
                        dctTimer.Add("BeforeGetLangLabels", DateTime.UtcNow.ToString());
                        Dictionary<string, object> dctDefaultLang = new Dictionary<string, object>();
                        try
                        {
                            dctDefaultLang = objfrk.GetlanguageLabels(vlanguagecode);
                        }
                        catch (Exception ex)
                        {
                            Dictionary<string, object> scriptengine_dataLang;
                            Dictionary<string, object> value = new Dictionary<string, object>();
                            StreamReader streamReader = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/LANGUAGES/" + vlanguagecode + ".js"));
                            string text = streamReader.ReadToEnd();
                            scriptengine_dataLang = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(text);
                            srSettings.Close();
                            Dictionary<string, object> dctfulldata1 = new Dictionary<string, object>();
                            dctDefaultLang = scriptengine_dataLang;
                        }


                        //To get the translated text of report name and rpt name
                        object objReportName;
                        //string strReportname = "", strRPTname = "";
                        bool bReportName = dctDefaultLang.TryGetValue(dsScheduleInfo.Tables[0].Rows[0]["REPORTNAME"].ToString(), out objReportName);
                        if (bReportName)
                        {
                            strReportname = objReportName.ToString();
                        }
                        object objRPTName;
                        bool bRPTName = dctDefaultLang.TryGetValue(dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString(), out objRPTName);
                        if (bRPTName)
                        {
                            strRPTname = objRPTName.ToString();
                        }
                        //To get the translated text of report name and rpt name
                        dctTimer.Add("AfterGetLangLabels", DateTime.UtcNow.ToString());

                        if (strLangkey == null)
                        {
                            dctTimer.Add("BeforeGetLangLabelsAll", DateTime.UtcNow.ToString());
                            try
                            {
                                DataSet dscustomlabels;
                                string[] paramdeflangid = { "LANGUAGEID" };
                                object[] pValuesdeflangid = { vlanguagecode };
                                string strquerylang = GetValue("GETCUSTOMLABELBYID");
                                dscustomlabels = dbcon.ExecuteInlineQueries_Dataset(strquerylang, paramdeflangid, pValuesdeflangid);
                                var dctCustomLang = new Dictionary<string, object>();

                                if (dscustomlabels.Tables[0].Rows.Count > 0)
                                {
                                    foreach (DataRow row in dscustomlabels.Tables[0].Rows)
                                    {
                                        dctCustomLang.Add(row[0].ToString(), row[1].ToString());
                                    }

                                    foreach (var kv in dctCustomLang)
                                    {
                                        object secondValue;
                                        if (dctDefaultLang.TryGetValue(kv.Key, out secondValue))
                                        {
                                            if (!object.Equals(kv.Value, secondValue))
                                                dctDefaultLang[kv.Key] = kv.Value;
                                        }
                                    }
                                }
                            }
                            catch (Exception ex1) { }

                            var json_StrLangJSONserializer = new JavaScriptSerializer();
                            strLangkey = json_StrLangJSONserializer.Serialize(dctDefaultLang);
                            dctTimer.Add("AfterGetLangLabelsAll", DateTime.UtcNow.ToString());

                            if (ConfReportCaching == "yes")
                            {
                                dctTimer.Add("BeforeLangCache", DateTime.UtcNow.ToString());
                                strLangkey = cp.CacheData(strPRODUCTID, strLangkey, Convert.ToString(icompanyid), "ALLLANG", vlanguagecode, "ALLLANG");
                                dctTimer.Add("AfterLangCache", DateTime.UtcNow.ToString());
                            }
                        }

                        //string ExportFilename = dsScheduleInfo.Tables[0].Rows[0]["REPORTNAME"].ToString() + "_" + dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString();
                        string ExportFilename = "RPT" + ireportid + "DES" + iRptDesign + DateTime.Now.ToString();
                        //string ExportFilename = "";
                        //if (strRPTname == "")
                        //{
                        //    ExportFilename = strReportname;
                        //}
                        //else
                        //{
                        //    ExportFilename = strReportname + "_" + strRPTname.Replace("-","_");
                        //}
                        //DataPro.ReportServices.Service1 RS = new DataPro.ReportServices.Service1();
                        //string CacheUrl = RS.ExportWithCache(strPRODUCTID, Convert.ToString(ischeduleid), Convert.ToString(icompanyid), strReportJSONkey, strDataCachekey, strLangkey, dsScheduleInfo.Tables[0].Rows[0]["EXPORTTYPE"].ToString(), Reportname, Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["EXPORTFORMAT"].ToString()));

                        //Report service to return download URL
                        object[] objExportWithCache = new object[9];
                        objExportWithCache[0] = strPRODUCTID;//productid
                        objExportWithCache[1] = ischeduleid.ToString();//tockenid
                        objExportWithCache[2] = icompanyid.ToString();//companyid
                        objExportWithCache[3] = strReportJSONkey;//report cachekey
                        objExportWithCache[4] = strDataCachekey;//data cachekey
                        objExportWithCache[5] = strLangkey;//lang cachekey
                        objExportWithCache[6] = dsScheduleInfo.Tables[0].Rows[0]["EXPORTTYPE"].ToString();//export format type
                        objExportWithCache[7] = ExportFilename;//export file name
                        objExportWithCache[8] = Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["EXPORTFORMAT"].ToString());//export format
                        object CacheUrl = new object();
                        CacheUrl = "TestUrl";
                        //int URLFlag = 0;

                        //log.InfoFormat("RS_WSURL:" + RS_WSURL);
                        //log.InfoFormat("RS_WSNAME:" + RS_WSNAME);
                        //log.InfoFormat("RS_WSMETHOD:" + RS_WSMETHOD);
                        //
                        //log.InfoFormat("strPRODUCTID:" + strPRODUCTID);
                        //log.InfoFormat("ischeduleid:" + ischeduleid.ToString());
                        //log.InfoFormat("icompanyid:" + icompanyid.ToString());
                        //log.InfoFormat("strReportJSONkey:" + strReportJSONkey);
                        //log.InfoFormat("strDataCachekey:" + strDataCachekey);
                        //log.InfoFormat("strLangkey:" + strLangkey);
                        //log.InfoFormat("EXPORTTYPE:" + dsScheduleInfo.Tables[0].Rows[0]["EXPORTTYPE"].ToString());
                        //log.InfoFormat("ExportFilename:" + ExportFilename);
                        //log.InfoFormat("EXPORTFORMAT:" + dsScheduleInfo.Tables[0].Rows[0]["EXPORTFORMAT"].ToString());
                        //try
                        //{
                        if (ConfReportCaching == "yes")
                        {
                            dctTimer.Add("BeforeCacheReportURL", DateTime.UtcNow.ToString());
                            dctTimer.Add("strDataCachekey: " + strDataCachekey, DateTime.UtcNow.ToString());

                            //CallWebServices CWS = new CallWebServices();
                            DataSet a = null;
                            a = cp.GetData(strPRODUCTID, strDataCachekey) as DataSet;
                            if (a.Tables[0].Rows.Count == 0)
                            {
                                dctTimer.Add("NODATAFORCACHE", DateTime.UtcNow.ToString());
                                CacheUrl = "NODATAFORCACHE";
                                cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                            }
                            else
                            {
                                dctTimer.Add("BeforeCacheUrlHit", DateTime.UtcNow.ToString());
                                CacheUrl = CallWebService(RS_WSURL, RS_WSNAME, RS_WSMETHOD, objExportWithCache);
                                dctTimer.Add("AfterCacheUrlHit", DateTime.UtcNow.ToString());
                            }
                            dctTimer.Add("AfterCacheReportURL", DateTime.UtcNow.ToString());
                        }
                        else
                        {
                            dctTimer.Add("BeforeSerailReportURL", DateTime.UtcNow.ToString());
                            CacheUrl = CallWebService(RS_WSURL, RS_WSNAME, RS_WSMETHOD, objExportWithCache);
                            dctTimer.Add("AfterSerailReportURL", DateTime.UtcNow.ToString());
                        }

                       // log.InfoFormat("CacheUrl:" + CacheUrl.ToString());
                        //URLFlag = 1;
                        //}
                        //catch (Exception Ex2)
                        //{
                        //    cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                        //    URLERRORMSG = (Ex2.InnerException != null)
                        //      ? Ex2.InnerException.Message + Ex2.StackTrace
                        //      : Ex2.Message + Ex2.StackTrace;
                        //    result = "Problem with gettting the report URL; check audit table";
                        //    strquery = se.GetValue("INSERTSCHEDULEAUDITLOG");
                        //    string[] paramrptinfo1 = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                        //    object[] pValuesrptinfo1 = { ischeduleid, result, rptqueryvalue, strParamCache, strProcessCache, strDataCachekey, strReportJSONkey, strLangkey, URLERRORMSG };
                        //    int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);

                        //    //send mail regarding failure in getting URL
                        //    string BodyContent = "Problem getting the report URL</br></br>Report name : " + strReportname;
                        //    if (dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString() != "Option name")
                        //    {
                        //        BodyContent = BodyContent + "</br>Report type : " + strRPTname;
                        //    }
                        //    BodyContent = BodyContent + "</br>Company ID: " + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString();
                        //    BodyContent = BodyContent + "</br>Schedule ID: " + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString();
                        //    BodyContent = BodyContent + "</br>URL: " + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString();
                        //    BodyContent = BodyContent + "</br>Error Message: " + URLERRORMSG;

                        //    String strJSON = "[{\"APPID\":" + strAPPID
                        //            + ",\"COMPANYID\":\"" + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString()
                        //            + "\",\"TOKENID\":" + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString()
                        //            + ",\"URL\":\"" + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString()
                        //            + "\",\"MAILFROM\":\"" + dsScheduleInfo.Tables[0].Rows[0]["MAILFROM"].ToString()
                        //            + "\",\"MAILREPLYTO\":\"" + dsScheduleInfo.Tables[0].Rows[0]["REPLYTO"].ToString()
                        //            + "\",\"MAILTO\":\"" + "BharathiRaja.Pachiyappan@dupont.com"
                        //            + "\",\"MAILCC\":\"" + "Ganapathi.SubramanianNagarajan@dupont.com;Elumalai.Venkatesan@dupont.com;Anbarasi.Subramaniyam@dupont.com"
                        //            + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString()// + " - " + ExportFilename
                        //            + "\",\"MAILBODY\":\"" + BodyContent
                        //            + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString() + "\"}]";
                        //    object[] objSendNotificationDetails = new object[1];
                        //    objSendNotificationDetails[0] = strJSON;
                        //    object obj2 = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                        //}

                        //if (URLFlag == 1)
                        //{
                        dctTimer.Add("BeforeReportURLSuccessÈmail", DateTime.UtcNow.ToString());
                        string BodyContent = "</br></br>Report name : " + strReportname;
                        if (dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString() != "Option name")
                        {
                            BodyContent = BodyContent + "</br>Report type : " + strRPTname;
                        }
                        if (CacheUrl.ToString() == "NODATAFORCACHE")
                        {
                            BodyContent = BodyContent + "</br>No records found for this schedule.";
                        }
                        else
                        {
                            BodyContent = BodyContent + "</br>Download URL : <a style='font:bold; color:red' href=" + CacheUrl.ToString() + ">Click here to download</a><BR><BR>";
                        }
                        //Email service to send email for indual
                        int mailstatus = 0;
                        int iemailcount = dsScheduleInfo.Tables[1].Rows.Count;
                       // log.InfoFormat("Mail count Check:" + iemailcount);

                        for (int s = 0; s <= iemailcount - 1; s++)
                        {
                            String strJSON = "[{\"APPID\":" + strAPPID
                                + ",\"COMPANYID\":\"" + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString()
                                + "\",\"TOKENID\":" + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString()
                                + ",\"URL\":\"" + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString()
                                + "\",\"MAILFROM\":\"" + dsScheduleInfo.Tables[0].Rows[0]["MAILFROM"].ToString()
                                + "\",\"MAILREPLYTO\":\"" + dsScheduleInfo.Tables[0].Rows[0]["REPLYTO"].ToString()
                                + "\",\"MAILTO\":\"" + dsScheduleInfo.Tables[1].Rows[s]["EMAIL"].ToString()
                                + "\",\"MAILCC\":\"" + dsScheduleInfo.Tables[0].Rows[0]["CC"].ToString()
                                + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString()// + " - " + ExportFilename
                                + "\",\"MAILBODY\":\"" + dsScheduleInfo.Tables[1].Rows[s]["BODY"].ToString() + "" + BodyContent
                                + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}]";
                            object[] objSendNotificationDetails = new object[1];
                            objSendNotificationDetails[0] = strJSON;

                            //log.InfoFormat("Mail Json String:" + strJSON);

                            object obj3 = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);

                           // log.InfoFormat("WebService Return Value obj3:" + obj3.ToString());
                            if (obj3.ToString() != "1")
                            {
                                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                object[] pValuesrptinfomail = { ischeduleid, "Problem with email notifications", strJSON.ToString(), "", "", strDataCachekey, strReportJSONkey, strLangkey, obj3.ToString() };
                                int ELMAIL = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                            }
                            //entry on userinbox
                            strquery = GetValue("SCHEDULEUSERINBOXENTRY");
                            string[] paramrptinfo1 = { "COMPANYID", "USERID", "TO", "SUBJECT", "BODY", "TOKENID" };
                            object[] pValuesrptinfo1 = { icompanyid, dsScheduleInfo.Tables[1].Rows[s]["USERID"].ToString(), dsScheduleInfo.Tables[1].Rows[s]["EMAIL"].ToString(), dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString(), dsScheduleInfo.Tables[1].Rows[s]["BODY"].ToString() + "" + BodyContent, ischeduleid };
                            int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);
                            mailstatus++;
                        }
                        dctTimer.Add("AfterReportURLSuccessÈmail", DateTime.UtcNow.ToString());
                    }
                    else
                    {
                        result = "No schedule found";
                    }
                }
                else
                {
                    result = "No schedule found";
                }
                var json_serializer = new JavaScriptSerializer();
                string strtimer = json_serializer.Serialize(dctTimer);
                //log on AUDIT table
                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                string[] paramrptinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                object[] pValuesrptinfores = { ischeduleid, "SCHEDULE_REPORTS_FINAL", result, "", "", strDataCachekey, strReportJSONkey, strLangkey, strtimer.ToString() };
                int ELres = dbcon.ExecuteInlineQueries(strquery, paramrptinfores, pValuesrptinfores);
                return result;
            }
            catch (Exception Ex)
            {
                string strtimer = "";
                try
                {
                    //log.InfoFormat("Exception:" + Ex.ToString());
                    var json_serializer = new JavaScriptSerializer();
                    strtimer = json_serializer.Serialize(dctTimer);
                    result = "Problem with scheduling the DP50 report; check audit table";
                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                    string[] paramrptinfo = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                    object[] pValuesrptinfo = { ischeduleid, "SCHEDULE_REPORTS_EXCEPTION", "", "", "", "", "", "", Ex.Message + "Trace:" + strtimer.ToString() };
                    int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);

                    //send mail regarding failure in scheduling the report 
                    string BodyContent = "Problem getting the report URL</br></br>Report name : " + strReportname;
                    if (strRPTnameOpt != "Option name")
                    {
                        BodyContent = BodyContent + "</br>Report type : " + strRPTname;
                    }
                    //string BodyContent = "Problem : " + result;
                    BodyContent = BodyContent + "</br>Company ID: " + icompanyid.ToString();
                    BodyContent = BodyContent + "</br>Schedule ID: " + ischeduleid.ToString();
                    BodyContent = BodyContent + "</br>URL: " + strUrl.ToString();
                    BodyContent = BodyContent + "</br>Exception : " + Ex.Message;

                    String strJSON = "[{\"APPID\":" + strAPPID.ToString()
                            + ",\"COMPANYID\":\"" + icompanyid.ToString()
                            + "\",\"TOKENID\":" + ischeduleid.ToString()
                            + ",\"URL\":\"" + ""
                            + "\",\"MAILFROM\":\"" + "noreply-sdp@training.consultdss.com"
                            + "\",\"MAILREPLYTO\":\"" + "noreply-sdp@training.consultdss.com"
                            + "\",\"MAILTO\":\"" + "Elumalai.Venkatesan@Consultdss.Com"
                            + "\",\"MAILCC\":\"" + "Ganapathi.Nagarajan@consultdss.com;Anbarasi.Subramaniyam@Consultdss.Com;BharathiRaja.Pachiyappan@Consultdss.Com"
                            + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + result
                            + "\",\"MAILBODY\":\"" + BodyContent
                            + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}]";
                    object[] objSendNotificationDetails = new object[1];
                    objSendNotificationDetails[0] = strJSON;

                   // log.InfoFormat("Exception Json String:" + strJSON);

                    object obj4 = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);

                   // log.InfoFormat("WebService Return Value obj4:" + obj4.ToString());

                    return result;
                }
                catch (Exception ExCatch)
                {
                    string strRes = String.Concat("Company ID invalid", ExCatch.StackTrace, strtimer.ToString());
                   // log.InfoFormat(strRes);
                    return result = strRes;
                }
            }
        }

        [WebMethod]
        public string EmailSchedule(int ischeduleid, string icompanyid)
        {
            //log.InfoFormat("==Email Service Start==");
            DataSet dsScheduleInfo = new DataSet();
            string strquery = string.Empty;
            string result = "Success";
            int catchresult;
            object[] objSendNotificationDetails = new object[1];
            try
            {
                //log on AUDIT table
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                string strSettings = sr.ReadToEnd();
                Dictionary<string, object> scriptengine_data1;
                scriptengine_data1 = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                tmpdata = scriptengine_data1;
                cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == icompanyid).Value;
                string FA = cmpdata1["FA"].ToString();
                dbcon.constring = FA;
                // dbcon.constring = icompanyid.ToString() + ".FA";

                //DataPro configuration setting 
                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;

                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
                string strAPPID = dctpartdata["APPID"].ToString();
                string ES_WSNAME = dctpartdata["WSNAME"].ToString();
                string ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string ES_WSURL = dctpartdata["WSURL"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string RSCH_WSURL = dctpartdata["WSURL"].ToString();
                //End DataPro configuration setting 
                //get scheduletypeid
                try
                {
                    strquery = GetValue("GETSCHEDULETYPEID");
                    string[] paramscheduletype = { "SCHEDULEID" };
                    object[] pValuesscheduletype = { ischeduleid };

                    string scheduletypeid = dbcon.ExecuteInlineQueries_ReturnVal(strquery, paramscheduletype, pValuesscheduletype).ToString();
                    if (scheduletypeid != "")
                    {
                        strquery = "";
                        if ((scheduletypeid == "1") || (scheduletypeid == "2"))
                        {
                            strquery = GetValue("GETCORRECTIVEACTIONREMINDER");
                        }
                        else if (scheduletypeid == "7")
                        {
                            strquery = GetValue("GETOBSRVATIONREMINDERWEEK");
                        }
                        else if (scheduletypeid == "8")
                        {
                            strquery = GetValue("GETOBSRVATIONREMINDERMONTH");
                        }
                        else if (scheduletypeid == "12")
                        {
                            strquery = GetValue("GETUNSAFEOBSERVATIONDATAFOREMAIL");
                        }
                        //end get scheduletypeid

                        //get data for scheduletypeid and construct json for make entry in notification
                        try
                        {
                            string[] parammailinfo = { "SCHEDULEID" };
                            object[] pValuesmailinfo = { ischeduleid };
                            dsScheduleInfo = dbcon.ExecuteInlineQueries_Dataset(strquery, parammailinfo, pValuesmailinfo);

                           // log.InfoFormat("==Check Schedule Data==");

                            if ((dsScheduleInfo.Tables.Count > 0) && (dsScheduleInfo.Tables[0].Rows.Count > 0))
                            {
                                //use for call email scheduler.asmx and pass json parameter with Schedulara mehtod name
                                String strJSON = null;
                                string strconstructjson = null;
                                try
                                {
                                    for (int i = 0; i <= dsScheduleInfo.Tables[0].Rows.Count - 1; i++)
                                    {
                                        if (i == 0)
                                        {
                                            strJSON = strJSON + "[{\"APPID\":" + strAPPID;
                                        }
                                        else
                                        {
                                            strJSON = "{\"APPID\":" + strAPPID;
                                        }

                                        strJSON = strJSON + ",\"COMPANYID\":\"" + icompanyid
                                                + "\",\"TOKENID\":\"" + ischeduleid
                                                + "\",\"URL\":\"" + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString()
                                                + "\",\"MAILFROM\":\"" + dsScheduleInfo.Tables[0].Rows[i]["FROM"].ToString()
                                                + "\",\"MAILREPLYTO\":\"" + dsScheduleInfo.Tables[0].Rows[i]["REPLYTO"].ToString()
                                                + "\",\"MAILTO\":\"" + dsScheduleInfo.Tables[0].Rows[i]["TO"].ToString()
                                                + "\",\"MAILCC\":\"" + dsScheduleInfo.Tables[0].Rows[i]["CC"].ToString()
                                                + "\",\"MAILBCC\":\"" + dsScheduleInfo.Tables[0].Rows[i]["BCC"].ToString()
                                                + "\",\"MAILSUBJECT\":\"" + dsScheduleInfo.Tables[0].Rows[i]["SUBJECT"].ToString()
                                                + "\",\"MAILBODY\":\"" + dsScheduleInfo.Tables[0].Rows[i]["BODY"].ToString()
                                                + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}";


                                        int cnt = dsScheduleInfo.Tables[0].Rows.Count - 1;
                                        if (i != cnt)
                                        {
                                            strconstructjson += strJSON + ",";
                                        }
                                        else if (i == cnt)
                                        {
                                            strconstructjson += strJSON + "]";
                                        }
                                    }
                                    objSendNotificationDetails[0] = strconstructjson;
                                }
                                catch (Exception ex)
                                {
                                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                    string[] parammailinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                    object[] pValuesmailinfores = { ischeduleid, "Problem with construct json for make entry in notification", "", "", "", "", "", "", ex.ToString() };
                                    catchresult = dbcon.ExecuteInlineQueries(strquery, parammailinfores, pValuesmailinfores);
                                }
                                object obj = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                if (obj.ToString() != "1")
                                {
                                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                    string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                    object[] pValuesrptinfomail = { ischeduleid, "Problem with entry in email notifications", "", strconstructjson.ToString(), "", "", "", "", obj.ToString() };
                                    catchresult = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                                }
                            }
                            else
                            {
                                result = "No data for schedule";
                              //  log.InfoFormat(result);
                            }
                        }
                        catch (Exception ex)
                        {
                           // log.InfoFormat(ex.ToString());
                            strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                            string[] parammailinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                            object[] pValuesmailinfores = { ischeduleid, "Problem with getting schedule information", "", "", "", "", "", "", ex.ToString() };
                            catchresult = dbcon.ExecuteInlineQueries(strquery, parammailinfores, pValuesmailinfores);
                        }
                        //end get data for scheduletypeid and construct json for make entry in notification

                        //construct json for make entry in scheduler
                        if ((scheduletypeid == "1") || (scheduletypeid == "2") || (scheduletypeid == "7") || (scheduletypeid == "8") || (scheduletypeid == "12"))
                        {
                            if ((dsScheduleInfo.Tables.Count > 0) && (dsScheduleInfo.Tables[1].Rows.Count > 0))
                            {
                                try
                                {

                                    DateTime tockendate;
                                    int itimezone = Convert.ToInt32(dsScheduleInfo.Tables[1].Rows[0]["TIMEZONEID"].ToString());
                                    tockendate = Convert.ToDateTime(dsScheduleInfo.Tables[1].Rows[0]["TOKENTOBESENTDATE"].ToString());
                                    string strMode = "2";

                                    String strJSON = "{\"MODE\":" + strMode
                                        + ",\"SCHEDULER\" : [{\"APPID\":" + strAPPID
                                        + ",\"COMPANYID\":\"" + icompanyid
                                        + "\",\"TOKENID\":" + ischeduleid
                                        + ",\"TOKENTOBESENTDATE\":\"" + tockendate.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                                        + "\",\"TIMEZONE\":" + itimezone + "}]}";


                                    object[] objSendScheduleDetails = new object[1];
                                    objSendScheduleDetails[0] = strJSON;
                                    object obj = CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                                    if (obj.ToString() != "1")
                                    {
                                        result = "Problem with update schedule details";
                                        strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                        string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                        object[] pValuesrptinfomail = { ischeduleid, "Problem with update schedule details", strJSON.ToString(), "", "", "", "", "", "" };
                                        catchresult = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                    string[] paramremailerrstage3 = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                    object[] pValuesemailerrstage3 = { ischeduleid, "Problem with construct json for update in scheduler", "", "", "", "", "", "", ex.ToString() };
                                    catchresult = dbcon.ExecuteInlineQueries(strquery, paramremailerrstage3, pValuesemailerrstage3);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                    string[] paramremailerrstage3 = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                    object[] pValuesemailerrstage3 = { ischeduleid, "Problem with getting schedule type", "", "", "", "", "", "", ex.ToString() };
                    catchresult = dbcon.ExecuteInlineQueries(strquery, paramremailerrstage3, pValuesemailerrstage3);
                }
            }
            catch (Exception ex)
            {
                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                string[] paramrptinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                object[] pValuesrptinfores = { ischeduleid, "problem with getting the email service and schedule settings", "", "", "", "", "", "", ex.ToString() };
                catchresult = dbcon.ExecuteInlineQueries(strquery, paramrptinfores, pValuesrptinfores);
            }
            //finally {             
            //    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
            //    string[] paramrptinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
            //    object[] pValuesrptinfores = { ischeduleid, "EMAIL SCHEDULE START", "", "", "", "", "", "", "" };
            //    int ELres = dbcon.ExecuteInlineQueries(strquery, paramrptinfores, pValuesrptinfores);
            //}

            return result;
        }

        private string TargetVsActivityReport(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 9;
            if (clmscount > 9)
            {
                ArrayList listArrHeatChart = new ArrayList();
                ArrayList listArrDetailField = new ArrayList();
                Dictionary<string, string> dctDetailFieldObserver = new Dictionary<string, string>();
                dctDetailFieldObserver.Add("DetailFieldName", "OBSERVERNAME");
                dctDetailFieldObserver.Add("DetailFieldLabel", "Observer Name");
                dctDetailFieldObserver.Add("DetailFieldWidth", "256");
                dctDetailFieldObserver.Add("DetailFieldRowPosition", "1");
                dctDetailFieldObserver.Add("DetailTotalType", "0");
                dctDetailFieldObserver.Add("DetailFieldAlignment", "left");
                dctDetailFieldObserver.Add("DetailShowSort", "false");
                dctDetailFieldObserver.Add("DetailShowInOutput", "false");
                listArrDetailField.Add(dctDetailFieldObserver);

                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    //Construct DYNMICHEATMAPVALUE value
                    Dictionary<string, string> dctHeatChart = new Dictionary<string, string>();
                    dctHeatChart.Add("ValueField", arrColumns[month + 3]);
                    dctHeatChart.Add("LabelFieldCaption", arrColumns[month + 1].Replace("TGT", ""));
                    dctHeatChart.Add("TopLeftLabel", arrColumns[month + 1]);
                    dctHeatChart.Add("TopRightLabel", arrColumns[month + 2]);
                    dctHeatChart.Add("BottomLeftLabel", "");
                    dctHeatChart.Add("BottomRightLabel", "");
                    listArrHeatChart.Add(dctHeatChart);

                    //Construct DYNAMICDETAILFIELDS value
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 3;
                }
                while (month < clmscount);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrHeatChart);
                varRptDesign = varRptDesign.Replace("DYNMICHEATMAPVALUE", strdata);

                strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNAMICDETAILFIELDS", strdata);
            }
            return varRptDesign;
        }
        private string TargetVsActivityReportPeriod(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 9;
            if (clmscount > 9)
            {
                ArrayList listArrHeatChart = new ArrayList();
                ArrayList listArrDetailField = new ArrayList();
                Dictionary<string, string> dctDetailFieldObserver = new Dictionary<string, string>();
                dctDetailFieldObserver.Add("DetailFieldName", "OBSERVERNAME");
                dctDetailFieldObserver.Add("DetailFieldLabel", "Observer Name");
                dctDetailFieldObserver.Add("DetailFieldWidth", "256");
                dctDetailFieldObserver.Add("DetailFieldRowPosition", "1");
                dctDetailFieldObserver.Add("DetailTotalType", "0");
                dctDetailFieldObserver.Add("DetailFieldAlignment", "left");
                dctDetailFieldObserver.Add("DetailShowSort", "false");
                dctDetailFieldObserver.Add("DetailShowInOutput", "false");
                listArrDetailField.Add(dctDetailFieldObserver);

                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    //Construct DYNMICHEATMAPVALUE value
                    Dictionary<string, string> dctHeatChart = new Dictionary<string, string>();
                    dctHeatChart.Add("ValueField", arrColumns[month + 3]);
                    dctHeatChart.Add("LabelFieldCaption", arrColumns[month + 1].Replace("TGT", ""));
                    dctHeatChart.Add("TopLeftLabel", arrColumns[month + 1]);
                    dctHeatChart.Add("TopRightLabel", arrColumns[month + 2]);
                    dctHeatChart.Add("BottomLeftLabel", "");
                    dctHeatChart.Add("BottomRightLabel", "");
                    listArrHeatChart.Add(dctHeatChart);

                    //Construct DYNAMICDETAILFIELDS value
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 3;
                }
                while (month < clmscount);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrHeatChart);
                varRptDesign = varRptDesign.Replace("DYNMICHEATMAPVALUE", strdata);

                strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNAMICDETAILFIELDS", strdata);
            }
            return varRptDesign;
        }
        private string TargetVsActivityReportWeekly(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 9;
            if (clmscount > 9)
            {
                ArrayList listArrHeatChart = new ArrayList();
                ArrayList listArrDetailField = new ArrayList();
                Dictionary<string, string> dctDetailFieldObserver = new Dictionary<string, string>();
                dctDetailFieldObserver.Add("DetailFieldName", "OBSERVERNAME");
                dctDetailFieldObserver.Add("DetailFieldLabel", "LBLOBSERVERNAME");
                dctDetailFieldObserver.Add("DetailFieldWidth", "256");
                dctDetailFieldObserver.Add("DetailFieldRowPosition", "1");
                dctDetailFieldObserver.Add("DetailTotalType", "0");
                dctDetailFieldObserver.Add("DetailFieldAlignment", "left");
                dctDetailFieldObserver.Add("DetailShowSort", "false");
                dctDetailFieldObserver.Add("DetailShowInOutput", "false");
                listArrDetailField.Add(dctDetailFieldObserver);

                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    //Construct DYNMICHEATMAPVALUE value
                    Dictionary<string, string> dctHeatChart = new Dictionary<string, string>();
                    dctHeatChart.Add("ValueField", arrColumns[month + 3]);
                    dctHeatChart.Add("LabelFieldCaption", arrColumns[month + 2]);
                    dctHeatChart.Add("TopLeftLabel", arrColumns[month + 1]);
                    dctHeatChart.Add("TopRightLabel", arrColumns[month + 2]);
                    dctHeatChart.Add("BottomLeftLabel", "");
                    dctHeatChart.Add("BottomRightLabel", "");
                    listArrHeatChart.Add(dctHeatChart);

                    //Construct DYNAMICDETAILFIELDS value
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 3]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 2]);
                    dctDetailField.Add("DetailFieldWidth", "256");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DetailShowInOutput", "false");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 3;
                }
                while (month < clmscount);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrHeatChart);
                varRptDesign = varRptDesign.Replace("DYNMICHEATMAPVALUE", strdata);

                strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNAMICDETAILFIELDS", strdata);
            }
            return varRptDesign;
        }
        private string ChecklistCountSummaryReport(DataSet DS, DataSet DSDesign)
        {
            string varRptDesign = "";
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count;
            if (clmscount == 4)
            {
                varRptDesign = DSDesign.Tables[0].Rows[0]["RPTDESIGN"].ToString();

                varRptDesign = varRptDesign.Replace("GROUPNAME1", arrColumns[1]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL1", arrColumns[1]);
            }
            else if (clmscount == 6)
            {
                varRptDesign = DSDesign.Tables[0].Rows[0]["RPTDESIGN"].ToString();

                varRptDesign = varRptDesign.Replace("GROUPNAME1", arrColumns[1]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL1", arrColumns[1]);

                varRptDesign = varRptDesign.Replace("GROUPNAME2", arrColumns[3]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL2", arrColumns[3]);
            }
            else if (clmscount == 8)
            {
                varRptDesign = DSDesign.Tables[0].Rows[0]["RPTDESIGN"].ToString();

                varRptDesign = varRptDesign.Replace("GROUPNAME1", arrColumns[1]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL1", arrColumns[1]);

                varRptDesign = varRptDesign.Replace("GROUPNAME2", arrColumns[3]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL2", arrColumns[3]);

                varRptDesign = varRptDesign.Replace("GROUPNAME3", arrColumns[5]);
                varRptDesign = varRptDesign.Replace("GROUPLABEL3", arrColumns[5]);
            }
            return varRptDesign;
        }
        private string TrendlineYeartoYear(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 1;
            if (clmscount >= 2)
            {
                ArrayList listArrValueField = new ArrayList();
                ArrayList listArrDetailField = new ArrayList();
                Dictionary<string, string> dctDetailFieldObserver = new Dictionary<string, string>();
                dctDetailFieldObserver.Add("DetailFieldName", "MONTH");
                dctDetailFieldObserver.Add("DetailFieldLabel", "LBLMONTH");
                dctDetailFieldObserver.Add("DetailFieldWidth", "100");
                dctDetailFieldObserver.Add("DetailFieldRowPosition", "1");
                dctDetailFieldObserver.Add("DetailFieldAlignment", "left");
                dctDetailFieldObserver.Add("DatailDataType", "1");
                dctDetailFieldObserver.Add("DetailShowSort", "false");
                dctDetailFieldObserver.Add("DetailSortType", "1");
                dctDetailFieldObserver.Add("DetailShowInOutput", "true");
                dctDetailFieldObserver.Add("DetailTotalType", "0");
                dctDetailFieldObserver.Add("DetailShowInSection", "0");
                dctDetailFieldObserver.Add("DetailDisplayIn", "0");
                dctDetailFieldObserver.Add("DetailIsClickable", "false");
                listArrDetailField.Add(dctDetailFieldObserver);

                int width = 800 / (clmscount - 1);//split the year width
                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    //Construct DYNMICHEATMAPVALUE value
                    Dictionary<string, string> dctValueField = new Dictionary<string, string>();
                    dctValueField.Add("ValueField", arrColumns[month + 1]);
                    dctValueField.Add("ValueFieldCaption", arrColumns[month + 1]);
                    dctValueField.Add("ChartType", "2");
                    listArrValueField.Add(dctValueField);

                    //Construct DYNAMICDETAILFIELDS value
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month + 1] + " %");
                    dctDetailField.Add("DetailFieldWidth", width.ToString());
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailFieldAlignment", "center");
                    dctDetailField.Add("DatailDataType", "2");
                    dctDetailField.Add("DetailShowSort", "false");
                    dctDetailField.Add("DetailSortType", "1");
                    dctDetailField.Add("DetailShowInOutput", "true");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailShowInSection", "0");
                    dctDetailField.Add("DetailDisplayIn", "0");
                    dctDetailField.Add("DetailIsClickable", "false");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 1;
                }
                while (month < clmscount);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrValueField);
                varRptDesign = varRptDesign.Replace("DYNMICVALUEFIELDS", strdata);

                strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNAMICDETAILFIELDS", strdata);
            }
            return varRptDesign;
        }
        private string UserObserverList(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 12;
            if (clmscount >= 12)
            {
                ArrayList listArrValueField = new ArrayList();
                ArrayList listArrDetailField = new ArrayList();

                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    //Construct DYNAMICDETAILFIELDS value
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month]);
                    dctDetailField.Add("DetailFieldLabel", arrColumns[month]);
                    dctDetailField.Add("DetailFieldWidth", "200");
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailFieldAlignment", "left");
                    dctDetailField.Add("DatailDataType", "2");
                    dctDetailField.Add("DetailShowSort", "false");
                    dctDetailField.Add("DetailSortType", "1");
                    dctDetailField.Add("DetailShowInOutput", "true");
                    dctDetailField.Add("DetailTotalType", "0");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 1;
                }
                while (month < clmscount + 1);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNMICVALUEFIELDS", strdata.Replace("[", "").Replace("]", ""));
            }
            else
            {
                varRptDesign = varRptDesign.Replace(",DYNMICVALUEFIELDS", "");
            }
            return varRptDesign;
        }
        private string StatsByCategoryTabularReport(DataSet DS, string strDesign)
        {
            string varRptDesign = strDesign;
            string[] arrColumns = DS.Tables[0].Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

            int clmscount = DS.Tables[0].Columns.Count - 1;
            int month = 5;
            if (clmscount > 5)
            {
                ArrayList listArrDetailField = new ArrayList();
                Dictionary<string, string> dctDetailFieldMetrics = new Dictionary<string, string>();
                dctDetailFieldMetrics.Add("DetailFieldName", "METRICS");
                dctDetailFieldMetrics.Add("DetailFieldLabel", "LBLMETRICS");
                dctDetailFieldMetrics.Add("DetailFieldWidth", "200");
                dctDetailFieldMetrics.Add("DetailFieldRowPosition", "1");
                dctDetailFieldMetrics.Add("DetailTotalType", "0");
                dctDetailFieldMetrics.Add("DetailFieldAlignment", "left");
                dctDetailFieldMetrics.Add("DetailShowSort", "false");
                dctDetailFieldMetrics.Add("DetailShowInOutput", "true");
                listArrDetailField.Add(dctDetailFieldMetrics);

                int width = 660 / (clmscount - 5);//split the year width
                Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                do
                {
                    dctDetailField = new Dictionary<string, string>();
                    dctDetailField.Add("DetailFieldName", arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldLabel", "LBL" + arrColumns[month + 1]);
                    dctDetailField.Add("DetailFieldWidth", width.ToString());
                    dctDetailField.Add("DetailFieldRowPosition", "1");
                    dctDetailField.Add("DetailTotalType", "0");
                    dctDetailField.Add("DetailFieldAlignment", "center");
                    dctDetailField.Add("DetailShowInOutput", "true");
                    listArrDetailField.Add(dctDetailField);

                    month = month + 1;
                }
                while (month < clmscount);

                var json_serializer = new JavaScriptSerializer();
                string strdata = json_serializer.Serialize(listArrDetailField);
                varRptDesign = varRptDesign.Replace("DYNAMICDETAILFIELDS", strdata);
            }
            return varRptDesign;
        }
        private string CustomFields(DataTable DS, string strDesign)
        {
            string varRptDesign = strDesign;
            if (DS.Rows.Count > 0)
            {
                string[] arrColumns = DS.Columns.Cast<DataColumn>().Select(x => x.ColumnName).ToArray();

                if (arrColumns.Contains("CUSPOSITION") == true)
                {
                    string strfields = DS.Rows[0]["CUSPOSITION"].ToString();
                    string[] arrcusfields = strfields.Trim().Split(',');
                    if (arrcusfields.Length > 1 || arrcusfields[0] != "")
                    {
                        ArrayList listArrValueField = new ArrayList();
                        ArrayList listArrDetailField = new ArrayList();

                        Dictionary<string, string> dctDetailField = new Dictionary<string, string>();
                        foreach (string i in arrcusfields)
                        {
                            //Construct DYNAMICDETAILFIELDS value
                            dctDetailField = new Dictionary<string, string>();
                            dctDetailField.Add("DetailFieldName", arrColumns[Convert.ToInt32(i)]);
                            dctDetailField.Add("DetailFieldLabel", arrColumns[Convert.ToInt32(i)]);
                            dctDetailField.Add("DetailFieldWidth", "200");
                            dctDetailField.Add("DetailFieldRowPosition", "1");
                            dctDetailField.Add("DetailFieldAlignment", "left");
                            dctDetailField.Add("DatailDataType", "2");
                            dctDetailField.Add("DetailShowSort", "false");
                            dctDetailField.Add("DetailSortType", "1");
                            dctDetailField.Add("DetailShowInOutput", "true");
                            dctDetailField.Add("DetailTotalType", "0");
                            listArrDetailField.Add(dctDetailField);
                        }
                        var json_serializer = new JavaScriptSerializer();
                        string strdata = json_serializer.Serialize(listArrDetailField);
                        varRptDesign = varRptDesign.Replace("DYNMICCUSTOMFIELDS", strdata.Replace("[", "").Replace("]", ""));
                    }
                    else
                    {
                        varRptDesign = varRptDesign.Replace(",DYNMICCUSTOMFIELDS", "");
                    }
                }
                else
                {
                    varRptDesign = varRptDesign.Replace(",DYNMICCUSTOMFIELDS", "");
                }
            }
            else
            {
                varRptDesign = varRptDesign.Replace(",DYNMICCUSTOMFIELDS", "");
            }
            return varRptDesign;
        }

        internal static object CallWebService(string webServiceAsmxUrl, string serviceName, string methodName, object[] args)
        {
            System.Net.WebClient client = new System.Net.WebClient();
            try
            {

                string proxyConfig = ConfigurationSettings.AppSettings.Get("NotifyProxy");
                WebProxy proxy = null;
                if (proxyConfig != "")
                {
                    proxy = new WebProxy(proxyConfig);
                }
                client.Proxy = proxy;

                // Connect To the web service
                System.IO.Stream stream = client.OpenRead(webServiceAsmxUrl + "?wsdl");
                // Now read the WSDL file describing a service.
                ServiceDescription description = ServiceDescription.Read(stream);
                ///// LOAD THE DOM /////////
                // Initialize a service description importer.
                ServiceDescriptionImporter importer = new ServiceDescriptionImporter();
                importer.ProtocolName = "Soap12"; // Use SOAP 1.2.
                importer.AddServiceDescription(description, null, null);
                // Generate a proxy client.
                importer.Style = ServiceDescriptionImportStyle.Client;
                // Generate properties to represent primitive values.
                importer.CodeGenerationOptions = System.Xml.Serialization.CodeGenerationOptions.GenerateProperties;
                // Initialize a Code-DOM tree into which we will import the service.
                CodeNamespace nmspace = new CodeNamespace();
                CodeCompileUnit unit1 = new CodeCompileUnit();
                unit1.Namespaces.Add(nmspace);
                // Import the service into the Code-DOM tree. This creates proxy code that uses the service.
                ServiceDescriptionImportWarnings warning = importer.Import(nmspace, unit1);
                if (warning == 0) // If zero then we are good to go
                {
                    // Generate the proxy code
                    CodeDomProvider provider1 = CodeDomProvider.CreateProvider("CSharp");
                    // Compile the assembly proxy with the appropriate references
                    string[] assemblyReferences = new string[5] { "System.dll", "System.Web.Services.dll", "System.Web.dll", "System.Xml.dll", "System.Data.dll" };
                    CompilerParameters parms = new CompilerParameters(assemblyReferences);
                    parms.GenerateInMemory = true;
                    CompilerResults results = provider1.CompileAssemblyFromDom(parms, unit1);
                    // Check For Errors
                    if (results.Errors.Count > 0)
                    {
                        foreach (CompilerError oops in results.Errors)
                        {
                            System.Diagnostics.Debug.WriteLine("========Compiler error============");
                            System.Diagnostics.Debug.WriteLine(oops.ErrorText);
                        }
                        throw new System.Exception("Compile Error Occured calling webservice. Check Debug ouput window.");
                    }
                    // Finally, Invoke the web service method
                    object wsvcClass = results.CompiledAssembly.CreateInstance(serviceName);
                    int timeout = 500000000;
                    System.Reflection.PropertyInfo propertyInfo = wsvcClass.GetType().GetProperty("Timeout");

                    //set value
                    propertyInfo.SetValue(wsvcClass, timeout, null);

                    //get value (test)
                    object check = propertyInfo.GetValue(wsvcClass, null);

                    // create a list of data types for each argument
                    List<Type> listTypes = new List<Type>();
                    for (int i = 0; i < args.Length; i++)
                    {
                        listTypes.Add(args[i].GetType());
                    }
                    Type[] paramTypes = listTypes.ToArray<Type>();
                    MethodInfo mi = wsvcClass.GetType().GetMethod(methodName, paramTypes);
                    return mi.Invoke(wsvcClass, args);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //log.InfoFormat("Exception Call Web Service:" + ex.ToString());
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }

        [WebMethod]
        public string CommonSchedulingUrl(int ischeduleid, string icompanyid)
        {
            string strquery = string.Empty;
            string result = "Success";
            string URLERRORMSG = "";
            string strAPPID = "", ES_WSNAME = "", ES_WSMETHOD = "", ES_WSURL = "";
            Controller objfrk = new Controller();
            string strUrl = "";
            try
            {
                //DataPro configuration setting 
                Dictionary<string, object> scriptengine_data;
                StreamReader srSettings = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/dataprosetting.config"));
                string strsettings = srSettings.ReadToEnd();
                scriptengine_data = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strsettings);
                srSettings.Close();
                Dictionary<string, object> dctfulldata = new Dictionary<string, object>();
                Dictionary<string, object> dctpartdata = new Dictionary<string, object>();
                dctfulldata = scriptengine_data;

                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailservice").Value;
                strAPPID = dctpartdata["APPID"].ToString();
                ES_WSNAME = dctpartdata["WSNAME"].ToString();
                ES_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                ES_WSURL = dctpartdata["WSURL"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "reportservice").Value;
                string RS_WSNAME = dctpartdata["WSNAME"].ToString();
                string RS_WSURL = dctpartdata["WSURL"].ToString();
                string ConfReportCaching = dctpartdata["SCHEDULEREPORTCACHING"].ToString().ToLower();//YES - caching server, NO - datatable cache
                string RS_WSMETHOD = "";
                if (ConfReportCaching == "yes")
                {
                    RS_WSMETHOD = dctpartdata["WSMETHOD1"].ToString();
                }
                else
                {
                    RS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                }
                string ReportCacheDuration = dctpartdata["CACHEDURATIONINMINUTES"].ToString().ToLower();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "commonreportservice").Value;
                string strPRODUCTID = dctpartdata["CRSPRODUCTID"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "commonschedule").Value;
                string DRS_WSNAME = dctpartdata["WSNAME"].ToString();
                string DRS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string DRS_WSURL = dctpartdata["WSURL"].ToString();
                dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "emailscheduler").Value;
                string RSCH_WSNAME = dctpartdata["WSNAME"].ToString();
                string RSCH_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                string RSCH_WSURL = dctpartdata["WSURL"].ToString();
                //DataPro configuration setting

                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                Dictionary<string, object> cmpdata1 = new Dictionary<string, object>();
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/cf65aee436df4ba5b17f1bbbfca35354appinfo.config"));
                string strSettings = sr.ReadToEnd();
                Dictionary<string, object> scriptengine_data1;
                scriptengine_data1 = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                tmpdata = scriptengine_data1;
                cmpdata1 = (Dictionary<string, object>)tmpdata.FirstOrDefault(x => x.Key == icompanyid).Value;
                string FA = cmpdata1["FA"].ToString();
                dbcon.constring = FA;
                //dbcon.constring = icompanyid.ToString() + ".FA";

                string strDataCachekey = "", strReportJSONkey = "", strLangkey = "";
                string StrReportVar;
                //CacheProcess cp = new CacheProcess();
                DataSet dsScheduleInfo = new DataSet();
                DataSet dsReportResult = new DataSet();

                if (ischeduleid == 99999)
                {
                    strquery = GetValue("GETCOMMONSCHEDULEINFO");
                }
                else if (ischeduleid > 99999)
                {
                    strquery = GetValue("GETCOMMONCHECKLISTINFO");
                }

                string[] paramrptinfo = { "SCHEDULEID", "COMPANYID" };
                object[] pValuesrptinfo = { ischeduleid, icompanyid };
                dsScheduleInfo = dbcon.ExecuteInlineQueries_Dataset(strquery, paramrptinfo, pValuesrptinfo);


                string strParamCache = "";
                string strProcessCache = "";

                if (dsScheduleInfo.Tables.Count > 0)
                {
                    if ((dsScheduleInfo.Tables[0].Rows.Count > 0) && (dsScheduleInfo.Tables[1].Rows.Count > 0))
                    {
                        if (dsScheduleInfo.Tables[0].Rows[0]["STATUS"].ToString() == "1")
                        {
                            string rptqueryvalue = dsScheduleInfo.Tables[0].Rows[0]["QUERYVALUE"].ToString();
                            long iuserid = Convert.ToInt64(dsScheduleInfo.Tables[0].Rows[0]["USERID"].ToString());
                            string vlanguagecode = dsScheduleInfo.Tables[0].Rows[0]["LANGUAGECODE"].ToString();
                            int ilanguageID = Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["LANGUAGEID"].ToString());
                            string vDateformat = dsScheduleInfo.Tables[0].Rows[0]["DATEFORMAT"].ToString();
                            string ireportid = dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString();
                            string iRptDesign = dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString();
                            string headerbgcolor = dsScheduleInfo.Tables[0].Rows[0]["CUSTOMCOLOR"].ToString();
                            string headerfontcolor = dsScheduleInfo.Tables[0].Rows[0]["CUSTOMFONT"].ToString();
                            strUrl = dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString();

                            //as discussed with report team we need to increment the TOCKENTOBESENTDATE as +1 day after processing the schedule tocken
                            DateTime startdate, enddate, tockendate;
                            startdate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["STARTDATE"].ToString());
                            enddate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["ENDDATE"].ToString());
                            tockendate = Convert.ToDateTime(dsScheduleInfo.Tables[0].Rows[0]["TOCKENTOBESENTDATE"].ToString());
                            DRS_WSURL = DRS_WSURL + "?schid=" + ischeduleid + "&compid=" + icompanyid;
                            String strJSON1 = "{\"MODE\":" + 2
                                + ", \"SCHEDULER\" : [{\"APPID\":" + strAPPID
                                + ",\"COMPANYID\":\"" + icompanyid
                                + "\",\"TOKENID\":" + ischeduleid
                                + ",\"STARTDATE\":\"" + startdate.ToString("MM/dd/yyyy")
                                + "\",\"ENDDATE\":\"" + enddate.ToString("MM/dd/yyyy")
                                + "\",\"DWM\":" + dsScheduleInfo.Tables[0].Rows[0]["DWM"].ToString()
                                + ",\"EVERYDWM\":" + dsScheduleInfo.Tables[0].Rows[0]["EVERYDWM"].ToString()
                                + ",\"DAY\":" + dsScheduleInfo.Tables[0].Rows[0]["DAY"].ToString()
                                + ",\"MONTH\":" + dsScheduleInfo.Tables[0].Rows[0]["MONTH"].ToString()
                                + ",\"ACTIVE\":" + dsScheduleInfo.Tables[0].Rows[0]["STATUS"].ToString()
                                + ",\"TOKENTOBESENTDATE\":\"" + tockendate.ToString("yyyy-MM-dd HH:mm:ss.FFF")
                                + "\",\"STARTTIME\":" + dsScheduleInfo.Tables[0].Rows[0]["STARTTIME"].ToString()
                                + ",\"TIMEZONE\":" + dsScheduleInfo.Tables[0].Rows[0]["TIMEZONE"].ToString()
                                + ",\"CALLBACKWSNAME\":\"" + DRS_WSNAME
                                + "\",\"CALLBACKWS\":\"" + DRS_WSURL
                                + "\",\"CALLBACKWSMETHOD\":\"" + DRS_WSMETHOD + "\"}]}";
                            //string mailstatus = ESch.SendScheduleDetails(strJSON);
                            object[] objSendScheduleDetails = new object[1];
                            objSendScheduleDetails[0] = strJSON1;
                            object obj1 = CallWebService(RSCH_WSURL, RSCH_WSNAME, RSCH_WSMETHOD, objSendScheduleDetails);
                            if (obj1.ToString() != "1")
                            {
                                result = "Problem with updating the schedule tokentobesentdate for common report URL";
                                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                object[] pValuesrptinfomail = { ischeduleid, result, strJSON1.ToString(), "", "", strDataCachekey, strReportJSONkey, strLangkey, "" };
                                int ELMAIL = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                            }

                            if (dsScheduleInfo.Tables[0].Rows[0]["ISQUERY"].ToString() == "1")
                            {
                                strquery = GetValue(rptqueryvalue);
                            }
                            else
                            {
                                strquery = rptqueryvalue;
                            }


                            //REPORT PARAMETERS
                            List<string> paramrpt = new List<string>();
                            List<string> paramrptval = new List<string>();

                            if (ischeduleid == 99999)
                            {
                                paramrpt.Add("SCHEDULEID");
                                paramrptval.Add(ischeduleid.ToString());
                                paramrpt.Add("COMPANYID");
                                paramrptval.Add(icompanyid.ToString());
                                paramrpt.Add("USERID");
                                paramrptval.Add(iuserid.ToString());
                                paramrpt.Add("LANGUAGEID");
                                paramrptval.Add(ilanguageID.ToString());
                            }
                            else if (ischeduleid > 99999)
                            {
                                paramrpt.Add("COMPANYID");
                                paramrptval.Add(icompanyid.ToString());
                            }



                            string[] paramrpt1 = paramrpt.ToArray();
                            object[] paramrptval1 = paramrptval.ToArray();
                            string[] strparamrptval1 = paramrptval.ToArray();

                            strParamCache = "||" + Convert.ToString(icompanyid) + "||" + Convert.ToString(iuserid) + "||" + rptqueryvalue + "||" + string.Join("!", paramrpt1) + "||" + string.Join("!", strparamrptval1).Replace(":", "c_l");
                            strProcessCache = "||" + ischeduleid.ToString() + "||" + vlanguagecode + "||" + vDateformat + "||" + headerfontcolor + "||" + headerbgcolor + "||";

                            CacheProcess cp = new CacheProcess();
                            if (ConfReportCaching == "yes")
                            {
                                try
                                {
                                    cp.KillCache("DATAPRO", "CACHEKEY");
                                }
                                catch (Exception Ex2)
                                {
                                    ConfReportCaching = "no";
                                }
                            }
                            dctpartdata = (Dictionary<string, object>)dctfulldata.FirstOrDefault(x => x.Key.ToLower() == "reportservice").Value;
                            if (ConfReportCaching == "yes")
                            {
                                RS_WSMETHOD = dctpartdata["WSMETHOD1"].ToString();
                            }
                            else
                            {
                                RS_WSMETHOD = dctpartdata["WSMETHOD"].ToString();
                            }

                            //data cachekey
                            if (dsScheduleInfo.Tables[0].Rows[0]["ISQUERY"].ToString() == "1")
                            {
                                dsReportResult = dbcon.ExecuteInlineQueries_Dataset(strquery, paramrpt1, paramrptval1);
                            }
                            else
                            {
                                dsReportResult = dbcon.ExecuteStoreProc_Dataset(strquery, paramrpt1, paramrptval1);
                            }

                            if (ConfReportCaching == "yes")
                            {
                                strDataCachekey = Convert.ToString(icompanyid) + Convert.ToString(iuserid) + strParamCache + strProcessCache;
                                cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                                strDataCachekey = cp.CacheData(strPRODUCTID, dsReportResult, Convert.ToString(icompanyid), Convert.ToString(iuserid), strParamCache, strProcessCache);// change the Parameters for DataTable Cache 
                            }
                            else
                            {
                                strDataCachekey = DataUtil.SerializeDataSet(dsReportResult);
                            }

                            //report cachekey
                            strReportJSONkey = null;
                            if (ConfReportCaching == "yes")
                            {
                                cp.KillCache(strPRODUCTID, Convert.ToString(icompanyid) + dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString() + rptqueryvalue + dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString());
                            }
                            if (strReportJSONkey == null)
                            {
                                StrReportVar = dsScheduleInfo.Tables[0].Rows[0]["RPTDESIGN"].ToString();

                                StrReportVar = StrReportVar.Replace("USRDATEFORMAT", dsScheduleInfo.Tables[0].Rows[0]["DATEFORMAT"].ToString());
                                StrReportVar = StrReportVar.Replace("GROUPLABELFONTCOLOR", headerfontcolor);
                                StrReportVar = StrReportVar.Replace("GROUPLABELBGCOLOR", headerbgcolor);

                                strReportJSONkey = StrReportVar;
                                if (ConfReportCaching == "yes")
                                {
                                    strReportJSONkey = cp.CacheData(strPRODUCTID, strReportJSONkey, Convert.ToString(icompanyid), dsScheduleInfo.Tables[0].Rows[0]["REPORTID"].ToString(), rptqueryvalue, dsScheduleInfo.Tables[0].Rows[0]["DESIGNID"].ToString());
                                }
                            }

                            //lang cachekey
                            strLangkey = null;
                            if (ConfReportCaching == "yes")
                            {
                                Boolean Hascache = cp.isCacheExist(strPRODUCTID, Convert.ToString(icompanyid) + "ALLLANG" + vlanguagecode + "ALLLANG");
                                if (Hascache == true)
                                {
                                    strLangkey = Convert.ToString(icompanyid) + "ALLLANG" + vlanguagecode + "ALLLANG";
                                }
                            }
                            Dictionary<string, object> dctDefaultLang = new Dictionary<string, object>();
                            dctDefaultLang = objfrk.GetlanguageLabels(vlanguagecode);

                            //To get the translated text of report name and rpt name
                            object objReportName;
                            string strReportname = "", strRPTname = "";
                            bool bReportName = dctDefaultLang.TryGetValue(dsScheduleInfo.Tables[0].Rows[0]["REPORTNAME"].ToString(), out objReportName);
                            if (bReportName)
                            {
                                strReportname = objReportName.ToString();
                            }
                            object objRPTName;
                            bool bRPTName = dctDefaultLang.TryGetValue(dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString(), out objRPTName);
                            if (bRPTName)
                            {
                                strRPTname = objRPTName.ToString();
                            }
                            //To get the translated text of report name and rpt name

                            if (strLangkey == null)
                            {
                                try
                                {
                                    DataSet dscustomlabels;
                                    string[] paramdeflangid = { "LANGUAGEID" };
                                    object[] pValuesdeflangid = { vlanguagecode };
                                    string strquerylang = GetValue("GETCUSTOMLABELBYID");
                                    dscustomlabels = dbcon.ExecuteInlineQueries_Dataset(strquerylang, paramdeflangid, pValuesdeflangid);
                                    var dctCustomLang = new Dictionary<string, object>();

                                    if (dscustomlabels.Tables[0].Rows.Count > 0)
                                    {
                                        foreach (DataRow row in dscustomlabels.Tables[0].Rows)
                                        {
                                            dctCustomLang.Add(row[0].ToString(), row[1].ToString());
                                        }

                                        foreach (var kv in dctCustomLang)
                                        {
                                            object secondValue;
                                            if (dctDefaultLang.TryGetValue(kv.Key, out secondValue))
                                            {
                                                if (!object.Equals(kv.Value, secondValue))
                                                    dctDefaultLang[kv.Key] = kv.Value;
                                            }
                                        }
                                    }
                                }
                                catch (Exception ex1) { }

                                var json_StrLangJSONserializer = new JavaScriptSerializer();
                                strLangkey = json_StrLangJSONserializer.Serialize(dctDefaultLang);

                                if (ConfReportCaching == "yes")
                                {
                                    strLangkey = cp.CacheData(strPRODUCTID, strLangkey, Convert.ToString(icompanyid), "ALLLANG", vlanguagecode, "ALLLANG");
                                }
                            }

                            string ExportFilename = "COMMON" + ireportid + "DES" + iRptDesign + DateTime.Now.ToString();

                            //Report service to return download URL
                            object[] objExportWithCache = new object[9];
                            objExportWithCache[0] = strPRODUCTID;//productid
                            objExportWithCache[1] = ischeduleid.ToString();//tockenid
                            objExportWithCache[2] = icompanyid.ToString();//companyid
                            objExportWithCache[3] = strReportJSONkey;//report cachekey
                            objExportWithCache[4] = strDataCachekey;//data cachekey
                            objExportWithCache[5] = strLangkey;//lang cachekey
                            objExportWithCache[6] = dsScheduleInfo.Tables[0].Rows[0]["EXPORTTYPE"].ToString();//export format type
                            objExportWithCache[7] = ExportFilename;//export file name
                            objExportWithCache[8] = Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["EXPORTFORMAT"].ToString());//export format
                            object CacheUrl = "TestUrl";
                            int URLFlag = 0;

                            try
                            {
                                if (ConfReportCaching == "yes")
                                {
                                    //CallWebServices CWS = new CallWebServices();
                                    DataSet a = null;
                                    a = cp.GetData(strPRODUCTID, strDataCachekey) as DataSet;
                                    if (a.Tables[0].Rows.Count == 0)
                                    {
                                        CacheUrl = "NODATAFORCACHE";
                                        cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                                    }
                                    else
                                    {
                                        CacheUrl = CallWebService(RS_WSURL, RS_WSNAME, RS_WSMETHOD, objExportWithCache);
                                    }
                                }
                                else
                                {
                                    CacheUrl = CallWebService(RS_WSURL, RS_WSNAME, RS_WSMETHOD, objExportWithCache);
                                }
                                URLFlag = 1;
                            }
                            catch (Exception Ex2)
                            {
                                cp.KillCache(strPRODUCTID, strDataCachekey);//kill the cache
                                URLERRORMSG = (Ex2.InnerException != null)
                                  ? Ex2.InnerException.Message + Ex2.StackTrace
                                  : Ex2.Message + Ex2.StackTrace;
                                result = "Problem with gettting the common report URL; check audit table";
                                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                string[] paramrptinfo1 = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                object[] pValuesrptinfo1 = { ischeduleid, result, rptqueryvalue, strParamCache, strProcessCache, strDataCachekey, strReportJSONkey, strLangkey, URLERRORMSG };
                                int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);

                                //send mail regarding failure in getting URL
                                string BodyContent = "Problem getting common the report URL</br></br>Report name : " + strReportname;
                                if (dsScheduleInfo.Tables[0].Rows[0]["RPTNAME"].ToString() != "Option name")
                                {
                                    BodyContent = BodyContent + "</br>Report type : " + strRPTname;
                                }
                                BodyContent = BodyContent + "</br>Company ID: " + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString();
                                BodyContent = BodyContent + "</br>Schedule ID: " + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString();
                                BodyContent = BodyContent + "</br>URL: " + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString();
                                BodyContent = BodyContent + "</br>Error Message: " + URLERRORMSG;

                                String strJSON = "[{\"APPID\":" + strAPPID
                                        + ",\"COMPANYID\":\"" + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString()
                                        + "\",\"TOKENID\":" + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString()
                                        + ",\"URL\":\"" + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString()
                                        + "\",\"MAILFROM\":\"" + dsScheduleInfo.Tables[0].Rows[0]["MAILFROM"].ToString()
                                        + "\",\"MAILREPLYTO\":\"" + dsScheduleInfo.Tables[0].Rows[0]["REPLYTO"].ToString()
                                        + "\",\"MAILTO\":\"" + "elumalai.venkatesan@consultdss.com"
                                        + "\",\"MAILCC\":\"" + "anbarasi.subramaniyam@consultdss.com"
                                        + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString()// + " - " + ExportFilename
                                        + "\",\"MAILBODY\":\"" + BodyContent
                                        + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}]";
                                object[] objSendNotificationDetails = new object[1];
                                objSendNotificationDetails[0] = strJSON;
                                object obj = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                            }

                            int ischeduletypeid = Convert.ToInt32(dsScheduleInfo.Tables[0].Rows[0]["SCHEDULETYPEID"].ToString());

                            if (URLFlag == 1)
                            {
                                string BodyContent = string.Empty;
                                if (ischeduletypeid == 1)
                                {
                                    BodyContent = "<p>Hello Admin&nbsp;;</p><p><span style='color: #000000;'>Please find the <strong>Monthly Usage Report</strong>.</span></p>";
                                }
                                if (ischeduletypeid == 2)
                                {
                                    BodyContent = "<p>Hello Admin&nbsp;;</p><p><span style='color: #000000;'>Please find the <strong>Observation Checklist Details Report</strong>.</span></p>";
                                }

                                if (ischeduletypeid >= 3)
                                {
                                    BodyContent = "<p>Hello Admin&nbsp;;</p><p><span style='color: #000000;'>Please find the <strong>" + dsScheduleInfo.Tables[0].Rows[0]["REPORTNAME"].ToString() + "</strong>.</span></p>";
                                }


                                BodyContent = BodyContent + "<table style='height: 100px;' width='628'><tbody>";
                                BodyContent = BodyContent + "<tr><td width='40%'><p>Scheduled On</p></td><td width='60%'><p>" + dsScheduleInfo.Tables[0].Rows[0]["IMPORTDATE"].ToString() + "</p></td></tr>";
                                BodyContent = BodyContent + "<tr><td width='40%'><p>Date Range</p></td><td width='60%'><p>" + dsScheduleInfo.Tables[0].Rows[0]["DATE"].ToString() + "</p></td></tr>";

                                if (CacheUrl.ToString() == "NODATAFORCACHE")
                                {
                                    BodyContent = BodyContent + "</br>No records found for this schedule.";
                                }
                                else
                                {
                                    if (ischeduletypeid == 1)
                                    {
                                        BodyContent = BodyContent + "<tr><td width='40%'><p>URL for viewing the Monthly Usage Report:</p></td><td width='60%'><p><a style='font:bold; color:red' href=" + CacheUrl.ToString() + ">Click here to download</a></p></td></tr>";
                                    }
                                    if (ischeduletypeid == 2)
                                    {
                                        BodyContent = BodyContent + "<tr><td width='40%'><p>URL for viewing the Observation Checklist Details Report:</p></td><td width='60%'><p><a style='font:bold; color:red' href=" + CacheUrl.ToString() + ">Click here to download</a></p></td></tr>";
                                    }

                                    if (ischeduletypeid >= 3)
                                    {
                                        BodyContent = BodyContent + "<tr><td width='40%'><p>Download URL:</p></td><td width='60%'><p><a style='font:bold; color:red' href=" + CacheUrl.ToString() + ">Click here to download</a></p></td></tr>";
                                    }
                                }
                                BodyContent = BodyContent + "</tbody></table><p>&nbsp;Thanks</p><p>System Administrator-STOP DataPro</p>";


                                //Email service to send email for indual
                                int mailstatus = 0;
                                int iemailcount = dsScheduleInfo.Tables[1].Rows.Count;
                                for (int s = 0; s <= iemailcount - 1; s++)
                                {
                                    String strJSON = "[{\"APPID\":" + strAPPID
                                        + ",\"COMPANYID\":\"" + dsScheduleInfo.Tables[0].Rows[0]["COMPANYID"].ToString()
                                        + "\",\"TOKENID\":" + dsScheduleInfo.Tables[0].Rows[0]["SCHEDULEID"].ToString()
                                        + ",\"URL\":\"" + dsScheduleInfo.Tables[0].Rows[0]["URL"].ToString()
                                        + "\",\"MAILFROM\":\"" + dsScheduleInfo.Tables[0].Rows[0]["MAILFROM"].ToString()
                                        + "\",\"MAILREPLYTO\":\"" + dsScheduleInfo.Tables[0].Rows[0]["REPLYTO"].ToString()
                                        + "\",\"MAILTO\":\"" + dsScheduleInfo.Tables[1].Rows[s]["EMAIL"].ToString()
                                        + "\",\"MAILCC\":\"" + dsScheduleInfo.Tables[0].Rows[0]["CC"].ToString()
                                        + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString()// + " - " + ExportFilename
                                        + "\",\"MAILBODY\":\"" + BodyContent
                                        + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}]";
                                    object[] objSendNotificationDetails = new object[1];
                                    objSendNotificationDetails[0] = strJSON;
                                    object obj = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);
                                    if (obj.ToString() != "1")
                                    {
                                        strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                                        string[] paramrptinfomail = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                                        object[] pValuesrptinfomail = { ischeduleid, "Problem with email notifications for common report URL", strJSON.ToString(), "", "", strDataCachekey, strReportJSONkey, strLangkey, "" };
                                        int ELMAIL = dbcon.ExecuteInlineQueries(strquery, paramrptinfomail, pValuesrptinfomail);
                                    }

                                    //entry on userinbox
                                    strquery = GetValue("SCHEDULEUSERINBOXENTRY");
                                    string[] paramrptinfo1 = { "COMPANYID", "USERID", "TO", "SUBJECT", "BODY", "TOKENID" };
                                    object[] pValuesrptinfo1 = { icompanyid, dsScheduleInfo.Tables[1].Rows[s]["USERID"].ToString(), dsScheduleInfo.Tables[1].Rows[s]["EMAIL"].ToString(), dsScheduleInfo.Tables[0].Rows[0]["SUBJECT"].ToString(), BodyContent, ischeduleid };
                                    int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo1, pValuesrptinfo1);
                                    mailstatus++;
                                }
                            }
                        }
                        else
                        {
                            result = "schedule found is Inactive for common report URL";
                        }
                    }
                    else
                    {
                        result = "No schedule found for common report URL";
                    }
                }
                else
                {
                    result = "No schedule found for common report URL";
                }
                //log on AUDIT table
                strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                string[] paramrptinfores = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                object[] pValuesrptinfores = { ischeduleid, "SCHEDULE_REPORTS_FINAL", result, "", "", strDataCachekey, strReportJSONkey, strLangkey, "" };
                int ELres = dbcon.ExecuteInlineQueries(strquery, paramrptinfores, pValuesrptinfores);
                return result;
            }
            catch (Exception Ex)
            {
                try
                {
                    result = "Problem with scheduling the report for common report URL; check audit table";
                    strquery = GetValue("INSERTSCHEDULEAUDITLOG");
                    string[] paramrptinfo = { "SCHEDULEID", "FUNCTIONTYPE", "QUERY", "PARAM", "VALUE", "DATACACHE", "REPORTCACHE", "LANGUAGECACHE", "ERRORLOG" };
                    object[] pValuesrptinfo = { ischeduleid, "SCHEDULE_REPORTS_EXCEPTION", "", "", "", "", "", "", Ex.Message };
                    int EL = dbcon.ExecuteInlineQueries(strquery, paramrptinfo, pValuesrptinfo);

                    //send mail regarding failure in scheduling the report 
                    string BodyContent = "Problem : " + result;
                    BodyContent = BodyContent + "</br>Company ID: " + icompanyid.ToString();
                    BodyContent = BodyContent + "</br>Schedule ID: " + ischeduleid.ToString();
                    BodyContent = BodyContent + "</br>URL: " + strUrl.ToString();
                    BodyContent = BodyContent + "</br>Exception : " + Ex.Message;

                    String strJSON = "[{\"APPID\":" + strAPPID.ToString()
                            + ",\"COMPANYID\":\"" + icompanyid.ToString()
                            + "\",\"TOKENID\":" + ischeduleid.ToString()
                            + ",\"URL\":\"" + ""
                            + "\",\"MAILFROM\":\"" + "noreply-sdp@training.consultdss.com"
                            + "\",\"MAILREPLYTO\":\"" + "noreply-sdp@training.consultdss.com"
                            + "\",\"MAILTO\":\"" + "elumalai.venkatesan@consultdss.com"
                            + "\",\"MAILCC\":\"" + "anbarasi.subramaniyam@consultdss.com"
                            + "\",\"MAILBCC\":\"\",\"MAILSUBJECT\":\"" + result
                            + "\",\"MAILBODY\":\"" + BodyContent
                            + "\",\"MAILTOBESENTDATE\":\"" + DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\"}]";
                    object[] objSendNotificationDetails = new object[1];
                    objSendNotificationDetails[0] = strJSON;
                    object obj = CallWebService(ES_WSURL, ES_WSNAME, ES_WSMETHOD, objSendNotificationDetails);

                    return result;
                }
                catch (Exception ExCatch)
                {
                    return result = "Company ID invalid" + ExCatch.StackTrace;

                }
            }
        }

        public static Dictionary<string, object> scriptengine_Rdata;
        public string finalquery;
        public string GetValue(string tmpname)
        {
            scriptengine_Rdata = null;
            if (scriptengine_Rdata == null)
            {
                StreamReader sr = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/CONFIG/MASTERS1.1.config"));
                string strSettings = sr.ReadToEnd();
                scriptengine_Rdata = (new JavaScriptSerializer()).Deserialize<Dictionary<string, object>>(strSettings);
                sr.Close();
                Dictionary<string, object> tmpdata = new Dictionary<string, object>();
                tmpdata = scriptengine_Rdata;

                string str = tmpdata.FirstOrDefault(x => x.Key == tmpname).Value.ToString();
                string[] queries = str.Split('-');
                string[] finalqueries = new string[queries.Length];
                for (var k = 0; k <= queries.Length - 1; k++)
                {
                    string aa = "~/APPMOD/CONFIG/" + queries[k] + ".config";
                    StreamReader sr1 = File.OpenText(HttpContext.Current.Server.MapPath("~/APPMOD/SQL/" + queries[k] + ".config"));

                    finalqueries[k] = sr1.ReadToEnd();
                    sr.Close();
                }
                finalquery = string.Join(";", finalqueries).ToString();
            }
            return finalquery;
            //  scriptengine_Rdata = null;
        }

    }
}
