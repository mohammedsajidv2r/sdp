﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using StopDataPro.DataAccess.Repositories;

namespace StopDataPro.Business.Adapters
{
    /// <summary>
    /// BaseAdapter
    /// </summary>
    public class BaseAdapter
    {
        BaseRepository baseRepository = new BaseRepository();

        /// <summary>
        /// Gets or sets the constring bal.
        /// </summary>
        /// <value>
        /// The constring bal.
        /// </value>
        public string constringBal
        {
            get { return baseRepository.constring; }
            set { baseRepository.constring = value; }
        }

        /// <summary>
        /// Tokens the check.
        /// </summary>
        /// <returns></returns>
        public DataTable TokenCheck()
        {
            return baseRepository.TokenCheck();
        }

        /// <summary>
        /// Executes the inline queries dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public DataSet ExecuteInlineQueries_Dataset(string query, string[] pNames, object[] pValues)
        {            
            return baseRepository.ExecuteInlineQueries_Dataset(query, pNames, pValues);            
        }

        /// <summary>
        /// Executes the inline queries dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public DataSet ExecuteInlineQueries_Dataset(string query)
        {            
            return baseRepository.ExecuteInlineQueries_Dataset(query);            
        }

        /// <summary>
        /// Executes the inline queries return value.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries_ReturnVal(string query, string[] pNames, object[] pValues)
        {            
            return baseRepository.ExecuteInlineQueries_ReturnVal(query, pNames, pValues);            
        }

        /// <summary>
        /// Executes the inline queries.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries(string query, string[] pNames, object[] pValues)
        {            
            return baseRepository.ExecuteInlineQueries(query, pNames, pValues);            
        }

        /// <summary>
        /// Executes the store proc dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public DataSet ExecuteStoreProc_Dataset(string query, string[] pNames, object[] pValues)
        {            
            return baseRepository.ExecuteStoreProc_Dataset(query, pNames, pValues);            
        }

        /// <summary>
        /// Checks the login token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public DataTable CheckLoginToken(string token)
        {            
            return baseRepository.CheckLoginToken(token);     
        }

        /// <summary>
        /// Gets the alert unsafe.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public DataTable getAlertUnsafe(string userId)
        {            
            return baseRepository.getAlertUnsafe(userId);            
        }

        /// <summary>
        /// Executes the inline queries xmlparam.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="CardId">The card identifier.</param>
        /// <param name="Updatedby">The updatedby.</param>
        /// <param name="ObsList">The obs list.</param>
        /// <param name="obsapprovalpc">The obsapprovalpc.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries_XMLPARAM(string query, string CardId, string Updatedby, string ObsList, string obsapprovalpc)
        {            
            return baseRepository.ExecuteInlineQueries_XMLPARAM(query, CardId, Updatedby, ObsList, obsapprovalpc);            
        }
    }
}

