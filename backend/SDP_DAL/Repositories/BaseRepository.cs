﻿using log4net;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace StopDataPro.DataAccess.Repositories
{
    public class BaseRepository
    {
        #region Private Variables

        private string varfuntype, varparameters, varqueryname;
        private int iaction = 0;
        private long iloggeduserid, icompanyid;
        private string ErrMsg = "";
        private int OutVal = 0;
        private string _constring;
        SqlConnection sqlcon;
        SqlCommand sqlcmd;
        SqlCommand sqlcmdAudit;
        DESFramework.MODEL.encdec ecdc;

        #endregion

        #region Public Properties

        public string constring
        {
            get { return _constring; }
            set { _constring = value; }
        }
        public string FunctionType
        {
            get { return varfuntype; }
            set { varfuntype = value; }
        }
        public string QueryName
        {
            get { return varqueryname; }
            set { varqueryname = value; }
        }
        public long LoggedUserid
        {
            get { return iloggeduserid; }
            set { iloggeduserid = value; }
        }
        public int Action
        {
            get { return iaction; }
            set { iaction = value; }
        }
        public long CompanyId
        {
            get { return icompanyid; }
            set { icompanyid = value; }
        }

        /// <summary>
        /// Gets or sets the log.
        /// </summary>
        /// <value>
        /// The log.
        /// </value>
        public ILog log { get; set; }

        #endregion

        #region Constructor

        public BaseRepository()
        {
            ecdc = new DESFramework.MODEL.encdec();
            this.log = log4net.LogManager.GetLogger(typeof(BaseRepository));
        }

        #endregion

        #region Methods

        /// <summary>
        /// Tokens the check.
        /// </summary>
        /// <returns></returns>
        public DataTable TokenCheck()
        {
            var dt = new DataTable();
            var encodedString = "";
            // BaseAdapter bal = new BaseAdapter();
            var token = HttpContext.Current.Request.Headers["Authorization"];
            var url = HttpContext.Current.Request.Headers["Origin"];          
            var whitespace = new char[] { ' ', '\t' };
            var tokenstr = token.Split(whitespace);
            encodedString = tokenstr[1].ToString();
            // string con = infoconfig.createConn(url);
            // bal.constringBal = con;
            dt = this.CheckLoginToken(encodedString);
            return dt;
        }

        /// <summary>
        /// Executes the inline queries.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries(string query, string[] pNames, object[] pValues)
        {
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon)
                {
                    CommandType = CommandType.Text,
                    CommandTimeout = 1200
                };
                sqlcon.Open();
                for (int i = 0; i < pNames.Length; i++)
                {
                    sqlcmd.Parameters.AddWithValue(pNames[i], pValues[i]);
                }
                OutVal = Convert.ToInt32(sqlcmd.ExecuteNonQuery());
            }
            catch (Exception Ex)
            {
                OutVal = -2;
                ErrMsg = Ex.Message.ToString();
                log.InfoFormat("ExecuteInlineQueries:" + ErrMsg);
                sqlcon.Close();
            }
            finally
            {
                if (iaction != 0)
                {
                    string[] arr = ((IEnumerable)pValues).Cast<object>().Select(x => x.ToString()).ToArray();
                    varparameters = String.Join(",", pNames);
                    varparameters = varparameters + " [VALUES] " + String.Join("|", arr);
                    if (OutVal != -2)
                    {
                        sqlcmdAudit = new SqlCommand("INSERT INTO AUDITQUERY(USERID,FUNCTIONTYPE,[ACTION],QUERY,PARAMETER) VALUES (@USERID,@FUNCTIONTYPE,@ACTION,@QUERY,@PARAMETER)", sqlcon)
                        {
                            CommandType = CommandType.Text
                        };
                        sqlcmdAudit.Parameters.Add("@USERID", SqlDbType.BigInt).Value = iloggeduserid;
                        sqlcmdAudit.Parameters.Add("@FUNCTIONTYPE", SqlDbType.VarChar).Value = FunctionType;
                        sqlcmdAudit.Parameters.Add("@ACTION", SqlDbType.SmallInt).Value = iaction;
                        sqlcmdAudit.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = varqueryname;
                        sqlcmdAudit.Parameters.Add("@PARAMETER", SqlDbType.NVarChar).Value = varparameters;
                        sqlcmdAudit.ExecuteNonQuery();
                        sqlcmdAudit.Dispose();
                    }
                    else
                    {
                        sqlcmdAudit = new SqlCommand("INSERT INTO AUDITERRORLOG(COMPANYID,USERID,FUNCTIONTYPE,ACTION,QUERY,PARAMETER,ERRORMSG)VALUES(@COMPANYID,@USERID,@FUNCTIONTYPE,@ACTION,@QUERY,@PARAMETER,@ERRORMSG)", sqlcon)
                        {
                            CommandType = CommandType.Text
                        };
                        sqlcmdAudit.Parameters.Add("@COMPANYID", SqlDbType.BigInt).Value = icompanyid;
                        sqlcmdAudit.Parameters.Add("@USERID", SqlDbType.BigInt).Value = iloggeduserid;
                        sqlcmdAudit.Parameters.Add("@FUNCTIONTYPE", SqlDbType.VarChar).Value = FunctionType;
                        sqlcmdAudit.Parameters.Add("@ACTION", SqlDbType.SmallInt).Value = iaction;
                        sqlcmdAudit.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = varqueryname;
                        sqlcmdAudit.Parameters.Add("@PARAMETER", SqlDbType.NVarChar).Value = varparameters;
                        sqlcmdAudit.Parameters.Add("@ERRORMSG", SqlDbType.NVarChar).Value = ErrMsg;
                        sqlcmdAudit.ExecuteNonQuery();
                        sqlcmdAudit.Dispose();
                    }
                }
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return OutVal;
        }

        /// <summary>
        /// Executes the inline queries return value.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries_ReturnVal(string query, string[] pNames, object[] pValues)
        {
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon)
                {
                    CommandType = CommandType.Text,
                    CommandTimeout = 1200
                };
                sqlcon.Open();
                for (int i = 0; i < pNames.Length; i++)
                {
                    sqlcmd.Parameters.AddWithValue(pNames[i], pValues[i]);
                }
                OutVal = Convert.ToInt32(sqlcmd.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                OutVal = -2;
                log.InfoFormat("ExecuteInlineQueries_ReturnVal:" + Ex.ToString());
                ErrMsg = Ex.Message.ToString();
            }
            finally
            {
                if (iaction != 0)
                {
                    string[] arr = ((IEnumerable)pValues).Cast<object>().Select(x => x.ToString()).ToArray();
                    varparameters = String.Join(",", pNames);
                    varparameters = varparameters + " [VALUES] " + String.Join("|", arr);
                    if (OutVal != -2)
                    {
                        sqlcmdAudit = new SqlCommand("INSERT INTO AUDITQUERY(USERID,FUNCTIONTYPE,[ACTION],QUERY,PARAMETER) VALUES (@USERID,@FUNCTIONTYPE,@ACTION,@QUERY,@PARAMETER)", sqlcon)
                        {
                            CommandType = CommandType.Text
                        };
                        sqlcmdAudit.Parameters.Add("@USERID", SqlDbType.BigInt).Value = iloggeduserid;
                        sqlcmdAudit.Parameters.Add("@FUNCTIONTYPE", SqlDbType.VarChar).Value = FunctionType;
                        sqlcmdAudit.Parameters.Add("@ACTION", SqlDbType.SmallInt).Value = iaction;
                        sqlcmdAudit.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = varqueryname;
                        sqlcmdAudit.Parameters.Add("@PARAMETER", SqlDbType.NVarChar).Value = varparameters;
                        sqlcmdAudit.ExecuteNonQuery();
                        sqlcmdAudit.Dispose();
                    }
                    else
                    {
                        sqlcmdAudit = new SqlCommand("INSERT INTO AUDITERRORLOG(COMPANYID,USERID,FUNCTIONTYPE,ACTION,QUERY,PARAMETER,ERRORMSG)VALUES(@COMPANYID,@USERID,@FUNCTIONTYPE,@ACTION,@QUERY,@PARAMETER,@ERRORMSG)", sqlcon);
                        sqlcmdAudit.CommandType = CommandType.Text;
                        sqlcmdAudit.Parameters.Add("@COMPANYID", SqlDbType.BigInt).Value = icompanyid;
                        sqlcmdAudit.Parameters.Add("@USERID", SqlDbType.BigInt).Value = iloggeduserid;
                        sqlcmdAudit.Parameters.Add("@FUNCTIONTYPE", SqlDbType.VarChar).Value = FunctionType;
                        sqlcmdAudit.Parameters.Add("@ACTION", SqlDbType.SmallInt).Value = iaction;
                        sqlcmdAudit.Parameters.Add("@QUERY", SqlDbType.VarChar).Value = varqueryname;
                        sqlcmdAudit.Parameters.Add("@PARAMETER", SqlDbType.NVarChar).Value = varparameters;
                        sqlcmdAudit.Parameters.Add("@ERRORMSG", SqlDbType.NVarChar).Value = ErrMsg;
                        sqlcmdAudit.ExecuteNonQuery();
                        sqlcmdAudit.Dispose();
                    }
                }
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return OutVal;
        }

        /// <summary>
        /// Executes the inline queries dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public DataSet ExecuteInlineQueries_Dataset(string query, string[] pNames, object[] pValues)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                for (int i = 0; i < pNames.Length; i++)
                {
                    sqlcmd.Parameters.AddWithValue(pNames[i], pValues[i]);
                }
                sda.Fill(ds, "DT1");
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message.ToString();
                log.InfoFormat("ExecuteInlineQueries_Dataset with Parameter:" + ErrMsg);
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return ds;
        }

        /// <summary>
        /// Executes the inline queries xmlparam.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="CardId">The card identifier.</param>
        /// <param name="Updatedby">The updatedby.</param>
        /// <param name="ObsList">The obs list.</param>
        /// <param name="obsapprovalpc">The obsapprovalpc.</param>
        /// <returns></returns>
        public int ExecuteInlineQueries_XMLPARAM(string query, string CardId, string Updatedby, string ObsList, string obsapprovalpc)
        {
            int OutVal = 0;
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon);
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                sqlcmd.Parameters.Add("@CARDID", SqlDbType.BigInt).Value = CardId;
                sqlcmd.Parameters.Add("@UPDATEDBY", SqlDbType.BigInt).Value = Updatedby;
                sqlcmd.Parameters.Add("@XMLOBSVATION", SqlDbType.Xml).Value = ObsList;
                sqlcmd.Parameters.Add("@OBSAPPROVALPC", SqlDbType.BigInt).Value = obsapprovalpc;
                OutVal = Convert.ToInt32(sqlcmd.ExecuteScalar());
            }
            catch (Exception Ex)
            {
                OutVal = -2;
                log.InfoFormat("ExecuteInlineQueries_XMLPARAM:" + Ex.ToString());
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return OutVal;
        }

        /// <summary>
        /// Executes the inline queries application dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public DataSet ExecuteInlineQueriesApp_Dataset(string query, string[] pNames, object[] pValues)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                for (int i = 0; i < pNames.Length; i++)
                {
                    sqlcmd.Parameters.AddWithValue(pNames[i], pValues[i]);
                }
                sda.Fill(ds);
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message.ToString();
                log.InfoFormat("ExecuteInlineQueriesApp_Dataset:" + ErrMsg);
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return ds;
        }

        /// <summary>
        /// Executes the inline queries dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <returns></returns>
        public DataSet ExecuteInlineQueries_Dataset(string query)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
                sqlcmd.CommandType = CommandType.Text;
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                sda.Fill(ds);
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message.ToString();
                log.InfoFormat("ExecuteInlineQueries_Dataset with Query:" + ErrMsg);
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return ds;
        }

        /// <summary>
        /// Executes the store proc dataset.
        /// </summary>
        /// <param name="query">The query.</param>
        /// <param name="pNames">The p names.</param>
        /// <param name="pValues">The p values.</param>
        /// <returns></returns>
        public DataSet ExecuteStoreProc_Dataset(string query, string[] pNames, object[] pValues)
        {
            DataSet ds = new DataSet();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(query, sqlcon);
                SqlDataAdapter sda = new SqlDataAdapter(sqlcmd);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.CommandTimeout = 200000;
                sqlcon.Open();
                for (int i = 0; i < pNames.Length; i++)
                {
                    sqlcmd.Parameters.AddWithValue(pNames[i], pValues[i]);
                }
                sda.Fill(ds, "DT1");
            }
            catch (Exception Ex)
            {
                ErrMsg = Ex.Message.ToString();
                log.InfoFormat("ExecuteStoreProc_Dataset:" + ErrMsg);
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return ds;
        }
        
        /// <summary>
        /// Checks the login token.
        /// </summary>
        /// <param name="token">The token.</param>
        /// <returns></returns>
        public DataTable CheckLoginToken(string token)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(@"select u.USERID, U.[COMPANYID],[USERNAME],[LASTNAME],[EMAIL], u.[STATUS], u.[CREATEDBY], u.[CREATEDDATE], u.UPDATEDBY, u.UPDATEDDATE, u.OLDUSERID,t.Token[Token], isnull(OBSAPPROVEPC, 0) OBSAPPROVALPC, (select OPTIONVALUE from COMPANYOPTIONS  WHERE OPTIONNAME='ENABLESUBAREAS') ENABLESUBAREAS,
(select OPTIONVALUE from COMPANYOPTIONS  WHERE OPTIONNAME='ENABLEAREAMANAGER') ENABLEAREAMANAGER,uf.* 
                from USERS u inner join USERINFO uf on u.USERID = uf.USERID 
                inner join USERTOKEN t on u.USERID = t.USERID
                inner join ROLES R on R.ROLEID = DEFAULTROLEID and R.COMPANYID = u.COMPANYID 
                 where t.Token = @token", sqlcon);
                SqlDataAdapter sd = new SqlDataAdapter(sqlcmd);
                sqlcmd.Parameters.AddWithValue("@token", token);
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                sd.Fill(dt);
            }
            catch (Exception ex)
            {
                sqlcon.Close();
                log.InfoFormat("CheckLoginToken:" + ex.ToString());
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return dt;
        }

        /// <summary>
        /// Gets the alert unsafe.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        public DataTable getAlertUnsafe(string userId)
        {
            DataTable dt = new DataTable();
            try
            {
                sqlcon = new SqlConnection(ecdc.decrypt(_constring));
                sqlcmd = new SqlCommand(@"SELECT CASE WHEN EXISTS(SELECT 1 FROM SCHEDULEASSIGNMENTS WHERE ALERTUSERID=@USERID AND SCHEDULEID = (SELECT SCHEDULEID FROM SCHEDULE WHERE SCHEDULETYPEID = 12)) THEN 1 ELSE 0 END  UNSAFEALERTENABLE", sqlcon);

                SqlDataAdapter sd = new SqlDataAdapter(sqlcmd);
                sqlcmd.Parameters.AddWithValue("@USERID", userId);
                sqlcmd.CommandTimeout = 1200;
                sqlcon.Open();
                sd.Fill(dt);
            }
            catch (Exception ex)
            {
                log.InfoFormat("getAlertUnsafe:" + ex.ToString());
                sqlcon.Close();
            }
            finally
            {
                sqlcon.Dispose();
                sqlcmd.Dispose();
            }
            return dt;
        }
        
        #endregion
    }
}
